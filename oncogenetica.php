<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Oncogenética</h1>
		</div>

		<div class="long-text">
			<p>
				A Oncogenética é uma especialidade médica que atua em identificar mutações genéticas de pacientes saudáveis ou diagnosticados anteriormente no objetivo que encontrar sinais que possam significar um câncer futuro.<br>
				O Santé Cancer Center em parceria com o <a href="https://www.fleurygenomica.com.br/">Laboratório Fleury</a> oferece o serviço de sequenciamento de DNA e testes moleculares. É um procedimento ágil e simples que consiste na coleta de saliva do paciente. O material coletado é enviado diretamente para o laboratório responsável por analisar e identificar alterações genéticas, o que nos proporciona um grande avanço no diagnóstico precoce do câncer.<br> 
			</p>
		</div>

	</div>
</section>