<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Biópsias guiadas por ultrassom</h1>
		</div>

		<div class="long-text">
			<p>
				A biópsia guiada por ultrassom é realizada quando a mamografia digital detecta algum tipo de alteração mamária, sendo necessário que a paciente se submeta a uma ultrassom, onde o médico irá manipular uma agulha com auxílio do ultrassom para realizar uma biópsia da alteração, podendo identificar se é um nódulo benigno ou maligno.
			</p>
		</div>

	</div>
</section>

<!-- <section id="galery">
	<div class="container p-0">
		
		<div id="images">
			
			<a href='/img/infraestrutura.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/infraestrutura.jpg);"></div>
			</a>

			<a href='/img/sobre-nos.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/sobre-nos.jpg);"></div>
			</a>

			<a href='/img/infraestrutura.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/infraestrutura.jpg);"></div>
			</a>

			<br>

		</div>

	</div>
</section> -->