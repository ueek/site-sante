<?php
	
    //Arquivos externos
    include_once 'config/database.php';

    include_once 'models/about.php';
    include_once 'models/about-photo.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // ==========================================================
    // Load About data
    // ==========================================================
    $urlImages_About = "/content-adm/uploads/about/"; 
    
    $myAbout = new About($db);
    $stmtAbout = $myAbout->read("", "id", "LIMIT 1");
    if ($stmtAbout->rowCount() > 0) {

	    $row = $stmtAbout->fetch(PDO::FETCH_ASSOC);
		extract($row);
		$myAbout->setId($id);
        $myAbout->setMission($mission);
        $myAbout->setVision($vision);
        $myAbout->setCompanyValues($company_values);
        $myAbout->setInfrastructure($infrastructure);
        $myAbout->setAttendance($attendance);
        $myAbout->setTeam($team);
        $myAbout->setPhone($phone);
        $myAbout->setWhatsapp($whatsapp);
        $myAbout->setEmail($email);
        $myAbout->setFacebook($facebook);
        $myAbout->setInstagram($instagram);
        $myAbout->setTwitter($twitter);
        $myAbout->setLinkedin($linkedin);
        $myAbout->setYoutube($youtube);
        $myAbout->setImageStructure($urlImages_About.$image_structure);
        $myAbout->setImageAttendance($urlImages_About.$image_attendance);
        $myAbout->setImageTeam($urlImages_About.$image_team);
    }

  	$myAboutPhoto = new AboutPhoto($db);
    $stmtAboutPhoto = $myAboutPhoto->read("", "RAND()");
    // ==========================================================

?>

<!-- <section id="header-page" class="container-fluid" style="background-image: linear-gradient(185.99deg, rgba(255, 255, 255, 0.156) 13.84%, rgba(0, 0, 0, 0.3) 58.26%), url(/img/banner.jpg);"></section> -->

<section id="destaques-cancer-center" class="container-fluid">
	<div  class="container">

		<div class='col-lg-3 col-md-6 col-sm-12 item sequenced-left'>
			<div class='img' style='background-image: url(/img/icones/centro-de-infusao.png);'></div>
			<h2>Centro de infusão</h2>
		</div>

		<div class='col-lg-3 col-md-6 col-sm-12 item sequenced-left'>
			<div class='img' style='background-image: url(/img/icones/radioterapia.png);'></div>
			<h2>Radioterapia</h2>
		</div>

		<div class='col-lg-3 col-md-6 col-sm-12 item sequenced-left'>
			<div class='img' style='background-image: url(/img/icones/centro-de-terapia-assistida.png);'></div>
			<h2>Centro de Terapia Assistida</h2>
		</div>

		<div class='col-lg-3 col-md-6 col-sm-12 item sequenced-left'>
			<div class='img' style='background-image: url(/img/icones/mama.png);'></div>
			<h2>Centro de Diagnóstico de Mama</h2>
		</div>

		<br>

	</div>
</section>

<section class="container-fluid" id="history">
	<div class="container">

		<div class="line">

			<div class="col-lg-12 float-left text slide-left p-0 mb-5">
				<div class="text-about-us">

					<h1>O modelo de Cancer Center</h1>

					<p>
						O modelo de cancer center veio para oferecer um formato de maior sucesso para diagnosticar e tratar o câncer de forma mais precisa, oferecendo a atenção necessária para o paciente e familiares.  Dentro do cancer center o paciente encontrará uma equipe multidisciplinar totalmente especializada, atenção dedicada e desenvolvimento científico. Além disso, o paciente terá acesso ao corpo clínico para diagnosticar e tratar tumores comuns. Nos casos mais raros, o corpo clínico e equipe multidisciplinar terão acesso à reuniões de discussão de casos com médicos de outros grandes centros como por exemplo o Hospital Israelita Albert Einstein de São Paulo.
						<br>
						<br>
						Com foco no desenvolvimento constante e a vontade de proporcionar o melhor tratamento aos pacientes, o Santé Cancer Center vem com a proposta de combater o câncer coordenando ensino, pesquisa, diagnóstico, prevenção, tratamento e manutenção da saúde; tudo isso alinhado com a melhor tecnologia do mundo quando falamos em Radioterapia e tratamento oncológico.
					</p>

				</div>
			</div>

			<br>

		</div>

		<div class="line line-1">

			<div class="desktop">
				
				<div class="col-lg-8 col-md-12 col-sm-12 float-left img slide-left">
					<div class="img-about-us" style='background-image: url(<?php echo $myAbout->getImageStructure() ?>)'></div><br>
				</div>

				<div class="col-lg-4 col-md-12 col-sm-12 float-left text slide-right">
					<div class="text-about-us">

						<h1>Infraestrutura</h1>

						<p>
							<?php echo nl2br($myAbout->getInfrastructure()) ?>
						</p>

					</div>
				</div>

				<br>

			</div>

			<div class="responsive">
				
				<div class="col-md-12 col-sm-12 float-left text p-0">
					<div class="text-about-us">
						<h1>Infraestrutura</h1>
					</div>
				</div>

				<div class="col-md-12 col-sm-12 float-left img">
					<div class="img-about-us" style='background-image: url(<?php echo $myAbout->getImageStructure() ?>)'></div>
				</div>

				<div class="col-md-12 col-sm-12 float-left text p-0 slide-up">
					<div class="text-about-us">
						<p><?php echo nl2br($myAbout->getInfrastructure()) ?></p>
					</div>
				</div>

				<br>

			</div>

		</div>

		<div class="line line-2">

			<div class="desktop">
				
				<div class="col-lg-4 col-md-12 col-sm-12 float-left text slide-left">
					<div class="text-about-us">

						<h1>Atendimento</h1>

						<p>
							<?php echo nl2br($myAbout->getAttendance()) ?>
						</p>

					</div>
				</div>

				<div class="col-lg-8 col-md-12 col-sm-12 float-left img slide-right">
					<div class="img-about-us" style='background-image: url(<?php echo $myAbout->getImageAttendance() ?>)'></div><br>
				</div>

				<br>

			</div>

			<div class="responsive">
				
				<div class="col-md-12 col-sm-12 float-left text p-0">
					<div class="text-about-us">
						<h1>Atendimento</h1>
					</div>
				</div>

				<div class="col-md-12 col-sm-12 float-left img">
					<div class="img-about-us" style='background-image: url(<?php echo $myAbout->getImageAttendance() ?>)'></div>
				</div>

				<div class="col-md-12 col-sm-12 float-left text p-0 slide-up">
					<div class="text-about-us">
						<p><?php echo nl2br($myAbout->getAttendance()) ?></p>
					</div>
				</div>

				<br>

			</div>

		</div>

		<div class="line line-3">

			<div class="desktop">
				
				<div class="col-lg-8 col-md-12 col-sm-12 float-left img slide-left">
					<div class="img-about-us" style='background-image: url(<?php echo $myAbout->getImageTeam() ?>)'></div><br>
				</div>

				<div class="col-lg-4 col-md-12 col-sm-12 float-left text slide-right">
					<div class="text-about-us">

						<h1>Equipe</h1>

						<p>
							<?php echo nl2br($myAbout->getTeam()) ?>
						</p>

					</div>
				</div>

				<br>

			</div>

			<div class="responsive">
				
				<div class="col-md-12 col-sm-12 float-left text p-0">
					<div class="text-about-us">
						<h1>Equipe</h1>
					</div>
				</div>

				<div class="col-md-12 col-sm-12 float-left img">
					<div class="img-about-us" style='background-image: url(<?php echo $myAbout->getImageTeam() ?>)'></div>
				</div>

				<div class="col-md-12 col-sm-12 float-left text p-0 slide-up">
					<div class="text-about-us">
						<p><?php echo nl2br($myAbout->getTeam()) ?></p>
					</div>
				</div>

				<br>

			</div>

		</div>

	</div>
</section>

<section id="timeline">
	<div class="container">
		
		<div class="header-title">
			<h1>Linha do tempo</h1>
		</div>

		<div class="time-img"></div>

	</div>
</section>

<section id="galery">
	<div class="container p-0">
		
		<div class="header-title">
			<h1>Conheça o Cancer Center</h1>
		</div>

		<div id="images">

		    <?php 

			    while ($row = $stmtAboutPhoto->fetch(PDO::FETCH_ASSOC)){
			        extract($row);

			        echo 
			        "
						<a href='{$urlImages_About}{$image}' data-fancybox='galery-desktop'>
							<div class='img' style='background-image: url({$urlImages_About}{$image});'></div>
						</a>
			        ";

			    }

		    ?>
			<br>

		</div>

	</div>
</section>