<?php
	
    //Arquivos externos
    include_once 'config/database.php';

    include_once 'models/blog.php';
    include("funcoes/paginacao.php");

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // ==========================================================
    // Load Blogs data
    // ==========================================================
    $myBlog = new Blog($db);
    $countDados = $myBlog->getCount();
    // ==========================================================
?>

<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/blog.jpg);"></section> -->

<section class="container-fluid" id="blog">
	<div class="container p-0">
		
		<div class="header">
			<a href="/blog">
				<img src="/img/sante-blog.png">
			</a>
		</div>

		<div class="posts">

			<?php 

				if($countDados <> 0){
					$itensPagina = 6;
				    $inicio = calcula_inicio_limite_query($itensPagina);

					$stmtBlog = $myBlog->read("AND published_at <= NOW()", "published_at DESC", "LIMIT {$inicio}, {$itensPagina}");
					
					while ($rowBlog = $stmtBlog->fetch(PDO::FETCH_ASSOC)){ 
						extract($rowBlog);

						$image = "/content-adm/uploads/blogs/{$image}";
						$published_at = date("d/m/Y", strtotime($published_at));

						if (strlen($title) > 80) {
							$title = substr($title, 0, 80)."...";
						}								

						echo 
						"
							<a class='soft-hover' href='/post/{$url}'>
								
								<div class='card'>

									<div class='card-img soft-hover' style='background-image: url({$image});'></div>
									
									<div class='card-body'>

										<h5 class='card-title'>
											{$title}
										</h5>

									</div>

									<div class='card-footer'>

										<span class='author'>{$author}</span>
										<span class='date'>{$published_at}</span>
										
									</div>

								</div>

							</a>
						";
					}

					if(isset($_GET['linha']) and !empty($_GET['linha'])){
						paginacao($countDados, $itensPagina, 5, "blog");
					}else{
						paginacao($countDados, $itensPagina, 5, "blog");
					}
					
				}else{
					echo "Nenhum post encontrado";
				}

			?>	

		</div>

	</div>
</section>