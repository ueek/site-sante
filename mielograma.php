<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Mielograma – Biópsia de Medula Óssea</h1>
		</div>

		<h6 class="content-title">O que é?</h6>

		<div class="long-text">
			<p>
				Tanto a biópsia de medula quanto o mielograma são exames que avaliam a medula óssea. São utilizados para diagnosticar cânceres que atingem principalmente as células sanguíneas, como a leucemia, linfoma e mieloma. Podem avaliar também o acometimento secundário por algum outro tipo de câncer, como câncer de mama ou próstata.
				<br>
				<br>
				A medula óssea é um tecido que preenche a cavidade dos ossos do corpo humano, também conhecido como tutano e sua principal função é produzir os leucócitos (glóbulos brancos), hemácias (glóbulos vermelhos) e plaquetas. Cada uma dessas células tem papel fundamental no correto funcionamento do organismo.
				<br>
				<br>
				No mielograma é coletado uma amostra de medula óssea, já na biópsia é retirado um pequeno pedaço do osso. Na maioria das vezes os dois exames são realizados em conjunto durante o mesmo procedimento. Nestes exames poderá ser avaliado se há alguma presença ou sinal de células malignas na medula óssea.  Através do mielograma podem  ser realizadas outras avaliações, no campo da genética, como análise molecular, imunofenotipagem e citogenética.
			</p>
		</div>

		<h6 class="content-title">Como funciona?</h6>

		<div class="long-text">
			<p>
				Ambos os exames consistem em retirar uma pequena quantidade de material para amostra, seja um fragmento do osso ou da medula óssea. Normalmente as áreas selecionadas para coleta são o osso ilíaco (bacia) ou osso esterno (peito).  Todo o exame é realizado sob anestesia local, uma vez que é necessário uma agulha especial que consiga perfurar o osso. Após a coleta, o material é analisado em laboratório.
			</p>
		</div>

		<h6 class="content-title">Efeitos colaterais</h6>

		<div class="long-text">
			<p>
				Na semana após o exame é comum que o paciente sinta dores no local. As complicações são raras mas pode existir caso de sangramento em excesso, alergia ou infecção local. 
				Caso o paciente sinta dores, febre ou sangramentos, é necessário buscar auxílio da sua equipe médica imediatamente.
			</p>
		</div>

		<h6 class="content-title">Duração do tratamento</h6>

		<div class="long-text">
			<p>
				A duração do exame costuma ser rápida, porém demanda um tempo de preparação para anestesia e algumas vezes é necessário sedação. 
			</p>
		</div>

	</div>
</section>