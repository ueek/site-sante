<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Política de Cookies</h1>
		</div>

		<div class="long-text">
			<p>Utilizamos cookies para melhorar o desempenho e a sua experiência como utilizador no nosso site.</p>
			<h5><b>O que são cookies?</b></h5>
			<p>Os cookies são pequenos arquivos de texto que um site, quando visitado, coloca no computador do usuário ou no seu dispositivo móvel, através do navegador de internet (browser). A colocação de cookies ajudará o site a reconhecer o seu dispositivo numa próxima visita. Usamos o termo cookies nesta política para referir todos os arquivos que recolhem informações desta forma.</p>
			<p>Os cookies utilizados não recolhem informação que identifica o usuário, no entanto, se já for nosso cliente poderemos monitorar as suas visitas ao site desde que, pelo menos, por uma vez, tenha iniciado a sua navegação a partir de alguma comunicação enviada por nós, por exemplo, SMS e e-mail.</p>
			<p>Os cookies recolhem também informações genéricas, designadamente a forma como os usuários chegam e utilizam os sites ou a zona do país/países através do qual acedem ao site, etc.</p>
			<p>Os cookies retêm apenas informação relacionada com as suas preferências.</p>
			<p>A qualquer momento o usuário pode, através do seu navegador de internet (browser) decidir ser notificado sobre a recepção de cookies, bem como bloquear a respectiva entrada no seu sistema.</p>
			<p>A recusa de uso de cookies no site, pode resultar na impossibilidade de ter acesso a algumas das suas áreas ou de receber informação personalizada.</p>
			<h6><b>Para que servem os cookies?</b></h6>
			<p>Os cookies são usados para ajudar a determinar a utilidade, interesse e o número de utilizações dos sites, permitindo uma navegação mais rápida e eficiente e eliminando a necessidade de introduzir repetidamente as mesmas informações.</p>
			<h6><b>Que tipo de cookies utilizamos?</b></h6>
			<p>Utilizamos estes cookies para analisar a forma como os usuários usam o site e monitorar a performance deste. Isto permite-nos fornecer uma experiência de alta qualidade ao personalizar a nossa oferta e rapidamente identificar e corrigir quaisquer problemas que surjam. Por exemplo, usamos cookies de desempenho para saber quais as páginas mais populares, qual o método de ligação entre páginas que é mais eficaz, ou para determinar a razão de algumas páginas estarem a receber mensagens de erro. Baseado na utilização do site, podemos também utilizar estes cookies para destacar artigos ou serviços do site que pensamos ser do interesse dos usuários. Estes cookies são utilizados apenas para efeitos de criação e análise estatística, sem nunca recolher informação de carácter pessoal.</p>
			<h6><b>Os cookies utilizados podem ser:</b></h6>
			<p><b>Cookies permanentes</b> - Ficam armazenados ao nível do navegador de internet (browser) nos seus dispositivos de acesso (pc, mobile e tablet) e são utilizados sempre que o usuário faz uma nova visita ao site. Geralmente são utilizados para direcionar a navegação de acordo com os interesses do usuário, permitindo-nos prestar um serviço mais personalizado.</p>
			<p><b>Cookies de sessão</b> - São temporários, permanecem nos cookies do seu navegador de internet (browser) até sair do site. A informação obtida permite identificar problemas e fornecer uma melhor experiência de navegação.</p>
			<p>Depois de autorizar o uso de cookies, o usuário pode sempre desativar parte ou a totalidade dos nossos cookies.</p>
			<p>Todos os browsers permitem ao utilizador aceitar, recusar ou apagar cookies, nomeadamente através da seleção das definições apropriadas no respetivo navegador. Pode configurar os cookies no menu "opções" ou "preferências" do seu browser.</p>
			<p>Note-se que, ao desativar cookies, pode impedir que alguns serviços da web funcionem corretamente, afetando, parcial ou totalmente, a navegação no website.</p>
		</div>

	</div>
</section>