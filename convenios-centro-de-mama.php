<?php
	
    //Arquivos externos
    include_once 'config/database.php';

    include_once 'models/health-insurance-breast-center.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // ==========================================================
    // Load HealthInsurance data
    // ==========================================================
    $urlImages_HealthInsurance = "/content-adm/uploads/health-insurance-breast-center/"; 
    
    $myHealthInsurance = new HealthInsuranceBreastCenter($db);
    $stmtHealthInsurance = $myHealthInsurance->read("", "name");
    // ==========================================================

?>
<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content">
	<div class="container p-0">
		
		<div class="header-title">

			<h1>Convênios do Centro de Mama</h1>

			<p>
				Oferecemos uma gama de possibilidades e convênios para melhor te atender.<br>Confira abaixo as opções no Santé Cancer Center.
			</p>
		
		</div>

		<div class="health-insurances">

		    <?php 

			    while ($row = $stmtHealthInsurance->fetch(PDO::FETCH_ASSOC)){
			        extract($row);

			        echo 
			        "
						<div class='col-lg-2 col-md-4 col-6 insurance' style='background-image: url({$urlImages_HealthInsurance}{$image});'></div>
			        ";

			    }

		    ?>
			<br>
				
		</div>


	</div>
</section>