<?php

	// require_once(__DIR__ . '/../lib/iloveimg-php/init.php');

	// use Iloveimg\CompressImageTask;
	// use Iloveimg\ResizeImageTask;

	// /**
	// * Função para fazer upload básico de imagem
	// * @daboit [Ueek Ag] 20.06.18
	// *
	// * @param string $nameInput: Name do Input File a ser manipulado
	// * @param string $diretorio: Local onde a imagem será armazenada, precisa ter "/" no final
	// * @param int $tamanhoMax: Tamanho máximo da imagem, default 5MB
	// *
	// */

	// function resizeImage($fileToResize, $diretorio, $nomeFinal) {

	// 	$resize = new ResizeImageTask('project_public_2b536249f34de58cc11c6a5b85d6b033_iiux29c16b41db387697d52e7a819e047e1fa','secret_key_19ea3c9d06288ef956ac45fb2cb58b1e_1J4fD9bb745d37e80583161d0283ff3ca3616');

	// 	$file = $resize->addFile($fileToResize);

	// 	//set resize mode to pixels
	// 	$resize->setResizeMode('pixels');

	// 	$resize->setPixelsWidth('1920');

	// 	//do not make it bigger
	// 	$resize->setNoEnlargeIfSmaller(true);

	// 	//queep relation
	// 	$resize->setMaintainRatio(true);

	// 	// and set name for output file.
	// 	// the task will set the correct file extension for you.
	// 	$resize->setOutputFilename($nomeFinal);

	// 	// process files
	// 	$resize->execute();

	// 	unlink ($fileToResize);

	// 	// and finally download file. If no path is set, it will be downloaded on current folder
	// 	$resize->download($diretorio);

	// }

	// function compressImage($fileToResize, $diretorio) {

	// 	$compress = new CompressImageTask('project_public_2b536249f34de58cc11c6a5b85d6b033_iiux29c16b41db387697d52e7a819e047e1fa','secret_key_19ea3c9d06288ef956ac45fb2cb58b1e_1J4fD9bb745d37e80583161d0283ff3ca3616');

	// 	$file = $compress->addFile($fileToResize);

	// 	// set compression level
	// 	$compress->setCompressionLevel('extreme');

	// 	// process files
	// 	$compress->execute();

	// 	unlink ($fileToResize);

	// 	// and finally download file. If no path is set, it will be downloaded on current folder
	// 	$compress->download($diretorio);

	// }
 
	function uploadImagem ($nameInput, $diretorio, $tamanhoMax = 10485760){
		if($_FILES[$nameInput]["name"] != ""){
			//Verifica tamanho do arquivo
			if($_FILES[$nameInput]["size"] > $tamanhoMax){ return 1; } //1 = problema no tamanho

			//Verificar os tipos de arquivos permitidos
			$extensao = strtolower(pathinfo($_FILES[$nameInput]["name"], PATHINFO_EXTENSION));
			if($extensao != "png" && $extensao != "jpg" && $extensao != "jpeg" && $extensao != "gif" && $extensao != "mp4"){ return 2; } //2 = problema de extensão

			//Gera MD5 único
			$nomeFinal = md5(date('Y-m-d-H:i:s').rand(1,1000)).".".$extensao;

			if(!is_dir($diretorio)) { mkdir(__DIR__.$diretorio, 0755, true); }

			if(move_uploaded_file($_FILES[$nameInput]["tmp_name"], $diretorio.$nomeFinal)) { 

				// list($width, $height, $type, $attr) = getimagesize($diretorio.$nomeFinal); 

				// if ($width > 1920) {
				// 	resizeImage($diretorio.$nomeFinal, $diretorio, $nomeFinal);
				// }

				// compressImage($diretorio.$nomeFinal, $diretorio);
				
				return $nomeFinal; 
			} 
			else{ 
				return 3; 
			} //3 = problema no upload
		}else{
			return 4; //não setado
		}
	}


?>