<?php

	date_default_timezone_set('America/Sao_Paulo');

	function verificaRestauranteAberto($idRestaurante){		
		$id = (int) $idRestaurante;
		if($id > 0){
			$sqlRestaurante   = "SELECT * FROM restaurantes WHERE id = {$id} LIMIT 1";
			$queryRestaurante = mysql_query($sqlRestaurante);

			if(mysql_num_rows($queryRestaurante) == 1){
				$dadosR = mysql_fetch_array($queryRestaurante);

				$diaAgora = date("w");

				if($dadosR['horario'] == 0){

					switch ($dadosR['delivery_de']) {
						case "domingo":
							$de = 0;
							break;
						case "segunda":
							$de = 1;
							break;
						case "terca":
							$de = 2;
							break;
						case "quarta":
							$de = 3;
							break;
						case "quinta":
							$de = 4;
							break;
						case "sexta":
							$de = 5;
							break;
						case "sabado":
							$de = 6;
							break;
					}
					switch ($dadosR['delivery_ate']) {
						case "domingo":
							$ate = 0;
							break;
						case "segunda":
							$ate = 1;
							break;
						case "terca":
							$ate = 2;
							break;
						case "quarta":
							$ate = 3;
							break;
						case "quinta":
							$ate = 4;
							break;
						case "sexta":
							$ate = 5;
							break;
						case "sabado":
							$ate = 6;
							break;
					}

					if($diaAgora >= $de and $diaAgora <= $ate){

						if(verificaHoraAbertoFechado($dadosR['hora_delivery_de'], $dadosR['hora_delivery_ate'])){
							//echo "Aberto";
							return true;
						}else{
							//echo "Fechado";
							return false;
						}

					}else{

						if($de > $ate){


							$diaHojeTeste = date("Y-m-d");

							//TO DO pegar o DE dessa SEMANA!!!!!

							if($diaAgora > $ate){
								$soma = ($diaAgora-$ate);
								$soma = ($soma > 1) ? "{$soma} days" : "{$soma} day";
								$novaDataAte = date("Y-m-d", strtotime("+1 week -{$soma}", strtotime($diaHojeTeste)));
							}else{
								$subtracao = ($ate-$diaAgora);
								$subtracao = ($subtracao > 1) ? "{$subtracao} days" : "{$subtracao} day";
								$novaDataAte = date("Y-m-d", strtotime("+1 week +{$subtracao}", strtotime($diaHojeTeste)));
							}

							if($de > $diaAgora){
								$soma = ($de-$diaAgora);
								$soma = ($soma > 1) ? "{$soma} days" : "{$soma} day";
								$novaDataDe = date("Y-m-d", strtotime("+{$soma}", strtotime($diaHojeTeste)));
							}elseif($de == $diaAgora){
								$novaDataDe = $diaHojeTeste;
							}else{
								$subtracao = ($diaAgora-$de);
								$subtracao = ($subtracao > 1) ? "{$subtracao} days" : "{$subtracao} day";
								$novaDataDe = date("Y-m-d", strtotime("-{$subtracao}", strtotime($diaHojeTeste)));
							}

							if(strtotime(date("{$diaHojeTeste}")) >= strtotime($novaDataDe) and strtotime(date("{$diaHojeTeste}")) <= strtotime($novaDataAte)){
								if(verificaHoraAbertoFechado($dadosR['hora_delivery_de'], $dadosR['hora_delivery_ate'])){
									//echo "Aberto";
									return true;
								}else{
									//echo "Fechado";
									return false;
								}
							}else{
								//echo "Fechado";
								return false;
							}

						}else{
							if(verificaHoraAbertoFechado($dadosR['hora_delivery_de'], $dadosR['hora_delivery_ate'])){
								//echo "Aberto";
								return true;
							}else{
								//echo "Fechado";
								return false;
							}
						}

					}					

				}else{

					if($diaAgora == 0 and $dadosR['hora_dom_de'] <> "00:00:00" and $dadosR['hora_dom_ate'] <> "00:00:00"){
						if(verificaHoraAbertoFechado($dadosR['hora_dom_de'], $dadosR['hora_dom_ate'])){
							//echo "Aberto";
							return true;
						}else{
							//echo "Fechado";
							return false;
						}
					}elseif($diaAgora == 1 and $dadosR['hora_seg_de'] <> "00:00:00" and $dadosR['hora_seg_ate'] <> "00:00:00"){
						if(verificaHoraAbertoFechado($dadosR['hora_seg_de'], $dadosR['hora_seg_ate'])){
							//echo "Aberto";
							return true;
						}else{
							//echo "Fechado";
							return false;
						}
					}elseif($diaAgora == 2 and $dadosR['hora_ter_de'] <> "00:00:00" and $dadosR['hora_ter_ate'] <> "00:00:00"){
						if(verificaHoraAbertoFechado($dadosR['hora_ter_de'], $dadosR['hora_ter_ate'])){
							//echo "Aberto";
							return true;
						}else{
							//echo "Fechado";
							return false;
						}
					}elseif($diaAgora == 3 and $dadosR['hora_qua_de'] <> "00:00:00" and $dadosR['hora_qua_ate'] <> "00:00:00"){
						if(verificaHoraAbertoFechado($dadosR['hora_qua_de'], $dadosR['hora_qua_ate'])){
							//echo "Aberto";
							return true;
						}else{
							//echo "Fechado";
							return false;
						}
					}elseif($diaAgora == 4 and $dadosR['hora_qui_de'] <> "00:00:00" and $dadosR['hora_qui_ate'] <> "00:00:00"){
						if(verificaHoraAbertoFechado($dadosR['hora_qui_de'], $dadosR['hora_qui_ate'])){
							//echo "Aberto";
							return true;
						}else{
							//echo "Fechado";
							return false;
						}
					}elseif($diaAgora == 5 and $dadosR['hora_sex_de'] <> "00:00:00" and $dadosR['hora_sex_ate'] <> "00:00:00"){
						if(verificaHoraAbertoFechado($dadosR['hora_sex_de'], $dadosR['hora_sex_ate'])){
							//echo "Aberto";
							return true;
						}else{
							//echo "Fechado";
							return false;
						}
					}elseif($diaAgora == 6 and $dadosR['hora_sab_de'] <> "00:00:00" and $dadosR['hora_sab_ate'] <> "00:00:00"){
						if(verificaHoraAbertoFechado($dadosR['hora_sab_de'], $dadosR['hora_sab_ate'])){
							//echo "Aberto";
							return true;
						}else{
							//echo "Fechado";
							return false;
						}
					}else{
						//echo "Fechado";
						return false;
					}

				}				

			}
		}
	}

	function verificaTempoEntrega($idRestaurante){
		$id = (int) $idRestaurante;
		if($id > 0){
			$sqlTempoEntrega   = "SELECT * FROM tempo_entrega WHERE id_restaurante = {$id}";
			$queryTempoEntrega = mysql_query($sqlTempoEntrega);

			if(mysql_num_rows($queryTempoEntrega) > 0){
				while($dadosR = mysql_fetch_array($queryTempoEntrega)){
					if(verificaHoraAbertoFechado($dadosR['hora_inicio'], $dadosR['hora_fim'])){
						return $dadosR['tempo'];
						break;
					}
				}
			}else{
				return false;
			}
		}
	}


	function verificaHoraAbertoFechado($horaInicio, $horaFim){
		$horaDiaHoje = date("Y-m-d H:i:s");

		if($horaInicio >= $horaFim){
			$horaDiaDe  = date("Y-m-d")." {$horaInicio}";
			$horaDiaAte = date("Y-m-d", strtotime("+1 days"))." {$horaFim}";

			if(date("H:i:s") < $horaInicio and date("H:i:s") < $horaFim){
				$horaDiaHoje = date("Y-m-d H:i:s", strtotime("+1 days"));
			}

			if(strtotime($horaDiaHoje) >= strtotime($horaDiaDe) and strtotime($horaDiaHoje) <= strtotime($horaDiaAte)){
				//echo "Aberto";
				return true;
			}else{
				//echo "Fechado";
				return false;
			}
		}else{
			$horaDiaDe  = date("Y-m-d")." {$horaInicio}";
			$horaDiaAte = date("Y-m-d")." {$horaFim}";

			if(strtotime($horaDiaHoje) >= strtotime($horaDiaDe) and strtotime($horaDiaHoje) <= strtotime($horaDiaAte)){
				//echo "Aberto";
				return true;
			}else{
				//echo "Fechado";
				return false;
			}
		}
	}

?>