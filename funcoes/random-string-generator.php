<?php
	/**
	* Função para gerar string aleatória
	* @klvrtn [Ueek Ag] 16.05.19
	*
	* @param string $length: Tamanho da string
	*
	*/
 
	function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return  strtolower($randomString);
	}


?>

