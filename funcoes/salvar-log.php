<?php

	function saveLog($action) {
    
    	session_start();

		try {

		    $database = new Database();
		    $db = $database->getConnection();

		    $myLog = new Log($db);

			$myLog->setAdminId($_SESSION['idGestor']);
	    	$myLog->setAction($action);

	    	return $myLog->create();
	    	
	    } catch (Exception $e) {

	    	echo "Ocorreu um erro: ",  $e->getMessage(), "\n";
	    	
	    }

	}

?>