<?php



/**

 * Funções usadas para gerar paginação.

 * 

 */



	/**

	 * Função que retorna o valor para ser usado no "LIMIT $valor, qtd_itens" da query

	 * junto com a quantidade de resultados a serem montrados.

	 * 

	 * @param int $itens_pagina = Quantidade de ítens a serem mostrados por página

	 * @return number

	 */

	function calcula_inicio_limite_query($itens_pagina){

		// Verifica se o GET foi setado ou não

		$pagina_atual = (isset($_GET['pag'])) ? $_GET['pag'] : 1;

		return ($pagina_atual * $itens_pagina) - $itens_pagina;

	}

	

	/**

	 * Função que retorna a quantidade de páginas referente à quantidade de ítens

	 * e à quantidade de ítens por página.

	 * 

	 * @param int $total_itens = Quantidade total de ítens selecionados

	 * @param int $itens_pagina = Quantidade de ítens a serem mostrados por página

	 * @return number

	 */

	function calcula_total_paginas($total_itens, $itens_pagina){

		return ceil($total_itens / $itens_pagina);

	}

	

	/**

	 * Função que gera uma string com os parametros a serem usados em um link.

	 * 

	 * @param array $lista = Array com os dados da query_string

	 * @param int $pagina = Inteiro referente à página atual

	 * @return String

	 */

	function gera_parametros_url($lista, $pagina){

		if (array_key_exists("pag", $lista)) {

			$lista["pag"] = $pagina;

		}

		

		$string = null;

		$i = 1;

		foreach($lista as $key => $val){

			//if sem a url amigável
			/*if($i == 1){

				$string .= "index.php?pg=".$val;

			}else{

				$string .= "&pag=".$val;

			}*/

			//if com a url amigável já configurada
			if($i == 1){

				$string .= "/".$val;

			}else{

				$string .= "/".$val;

			}

			$i++;

		}

		

		return $string;

	}



	/**

	 * Função que retorna os links que constituem a paginação.

	 * 

	 * @param int $total_itens = Quantidade total de ítens selecionados

	 * @param int $itens_pagina = Quantidade de ítens a serem mostrados por página

	 * @param int $qtd_link_paginas = Quantidade de links de paginação, este número precisa ser ímpar

	 */

	function paginacao($total_itens, $itens_pagina, $qtd_max_links = 3, $route){

		

		// Verifica se há necessidade de mostrar a paginação

		if($total_itens > $itens_pagina){

			

			// Verifica se o GET foi setado ou não

			$pagina_atual  = (isset($_GET['pag'])) ? $_GET['pag'] : 1;	

	

			// Calcula o total de páginas com relação aos itens e itens por página

			$total_paginas = calcula_total_paginas($total_itens, $itens_pagina);

			

			// Verifica se foi passado o parametro de quantidade de links

			$qtd_max_links = ($qtd_max_links < 3) ? 3 : $qtd_max_links;

			

			// Verifica se o parametro de quantidade de links passado é ímpar

			if(fmod($qtd_max_links, 2) == 0){

				die("É necessário usar um número inteiro ímpar para a quantidade de links de paginação.");

			}

			

			// Pega o host

			$host = '';

			

			// Pega os dados de GET

			if($_SERVER['QUERY_STRING'] <> ""){		

				$querystring = explode("&", $_SERVER['QUERY_STRING']);

			}

			

			// Cria um array que é carregado com cada parametro e valor vindos do GET

			$lista = array();

			if(isset($querystring)){

				foreach ($querystring as $get){

					list($param, $val) = explode("=", $get);

					$lista[$param] = $val;

				}

			}

			if (!array_key_exists("pag", $lista)) {

				$lista["pag"] = 1;

			}

			

			

			// Inicio da paginação, aqui fica legal!

			echo "

			<br>

			<div id='nav-control'>

			<nav id='pagination'>

			<ul>

			";

			

			// Se o total de páginas for mior que a qtd de links, usa a paginação complexa

			if($total_paginas > $qtd_max_links){

				

				if($pagina_atual == 1){

					echo "<li class='pag primeira soft-hover disable'>&laquo;</li>";

				}else{

					$querystring = gera_parametros_url($lista, 1);

					echo "<a href='/{$route}'><li class='pag primeira soft-hover' title='Primeira P&aacute;gina'>&laquo;</li></a>";

				}

				

				$divisao_links = ($qtd_max_links-1)/2;

				

				if($pagina_atual > $divisao_links){

					$dif_1 = $total_paginas - $pagina_atual;

					if($dif_1 < $divisao_links){

						$inicio_a = $pagina_atual-$divisao_links-$divisao_links+$dif_1;

					}else{

						$inicio_a = $pagina_atual-$divisao_links;

					}

					$fim_a = $pagina_atual;

				}else{

					$inicio_a = 1;

					$fim_a = $pagina_atual;

				}

				

				$contador = 1;

				for($i = $inicio_a; $i < $fim_a; $i++){

					$querystring = gera_parametros_url($lista, $i);

					echo "<a href='/{$route}/{$i}'><li class='pag soft-hover'> {$i} </li></a>";

					$contador++;

				}

				

				echo "<li class='pag soft-hover active'> {$pagina_atual} </li>";

				

				

				$inicio_p = $pagina_atual+1;

				$dif = $qtd_max_links-$contador;

				if($pagina_atual <= $divisao_links){

					$fim_p = $inicio_p+$dif-1;

				}else{

					$fim_p = $pagina_atual+$dif;

					if($fim_p <= $total_paginas){

						$fim_p = $fim_p;

					}else{

						$fim_p = $total_paginas;

					}

				}

				for($i = $inicio_p; $i <= $fim_p; $i++){

					$querystring = gera_parametros_url($lista, $i);

					echo "<a href='/{$route}/{$i}'><li class='pag soft-hover'> {$i} </li></a>";

				}

				

				if($pagina_atual >= $total_paginas){

					echo "<li class='pag ultima disable soft-hover'>&raquo;</li>";

				}else{

					$querystring = gera_parametros_url($lista, $total_paginas);

					echo "<a href='/{$route}/{$i}'><li class='pag ultima soft-hover' title='&Uacute;ltima P&aacute;gina'>&raquo;</li></a>";

				}

				

			}else{

				

				if($pagina_atual == 1){

					echo "<li class='pag primeira soft-hover disable'>&laquo;</li>";

				}else{

					$querystring = gera_parametros_url($lista, 1);

					echo "<a href='/{$route}'><li class='pag primeira soft-hover' title='Primeira P&aacute;gina'>&laquo;</li></a>";

				}
				

				

				for($i = 1; $i <= $total_paginas; $i++){

					if($i == $pagina_atual){

						echo "<li class='pag soft-hover active'> {$i} </li>";

					}else{

						$querystring = gera_parametros_url($lista, $i);

						echo "<a href='/{$route}/{$i}'><li class='pag soft-hover'> {$i} </li></a>";

					}

				}

				

				if($pagina_atual >= $total_paginas){

					echo "<li class='pag ultima disable soft-hover'>&raquo;</li>";

				}else{

					$querystring = gera_parametros_url($lista, $total_paginas);

					echo "<a href='/{$route}/{$i}'><li class='pag ultima soft-hover' title='&Uacute;ltima P&aacute;gina'>&raquo;</li></a>";

				}

				

			}

			echo "

			<br>

			</ul>

			</nav>

			</div>

			";


		}

	}



?>