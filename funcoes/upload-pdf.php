<?php
	/**
	* Função para fazer upload básico de imagem
	* @daboit [Ueek Ag] 20.06.18
	*
	* @param string $nameInput: Name do Input File a ser manipulado
	* @param string $diretorio: Local onde a imagem será armazenada, precisa ter "/" no final
	* @param int $tamanhoMax: Tamanho máximo da imagem, default 5MB
	*
	*/
 
	function uploadArquivo ($nameInput, $diretorio, $tamanhoMax = 1000000, $nome_arquivo = ""){
		if($_FILES[$nameInput]["name"] != ""){
			//Verifica tamanho do arquivo
			if($_FILES[$nameInput]["size"] > $tamanhoMax){ return 1; } //1 = problema no tamanho

			//Verificar os tipos de arquivos permitidos
			$extensao = strtolower(pathinfo($_FILES[$nameInput]["name"], PATHINFO_EXTENSION));
			if($extensao != "pdf"){ return 2; } //2 = problema de extensão

			if ($nome_arquivo != "") {
				$nomeFinal = $nome_arquivo.".".$extensao;
			}else{
				//Gera MD5 único
				$nomeFinal = md5(date('Y-m-d-H:i:s').rand(1,1000)).".".$extensao;				
			}


			if(!is_dir($diretorio)) { mkdir(__DIR__.$diretorio, 0755, true); }

			if(move_uploaded_file($_FILES[$nameInput]["tmp_name"], $diretorio.$nomeFinal)) { return $nomeFinal; } 
			else{ return 3; } //3 = problema no upload
		}else{
			return 4; //não setado
		}
	}


?>