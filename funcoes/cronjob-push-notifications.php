<?php
    include_once '../models/push-notification.php';
    include_once '../models/coupon.php';
    include_once '../models/mistery-box.php';
    include_once '../config/database.php';

    // API access key from Google API's Console
    define( 'API_ACCESS_KEY', 'AIzaSyAINso3TWUcS3PjhyNk5lo14c3rxu7QOxY' );


    // Start database
    $database = new Database();
    $db = $database->getConnection();

    // Instantiate the object and look for pending notifications
    $myPushNotification = new PushNotification($db);
    $pushNotificationsToBeSent = $myPushNotification->readNotificationsToSentNow();

    // send the notifications to Firebase
    if ($pushNotificationsToBeSent->rowCount() > 0) {
        
        while ($notification = $pushNotificationsToBeSent->fetch(PDO::FETCH_ASSOC)){

            extract($notification);

            // before sending, check if the item on the notification stil exists
            if ($coupon_id != 0) {
                $myCoupon = new Coupon($db);
                $coupon = $myCoupon->read("id", "", "AND id = ".$coupon_id." AND status = 1");

                if ($coupon->rowCount() == 0) {
                    // update the register on the database
                    $myPushNotification->setAsSent($id);
                    echo "Coupon expired.";
                    return;
                }
            }else{
                $myMisteryBox = new MisteryBox($db);
                $mysterybox = $myMisteryBox->read("id", "", "AND id = ".$mysterybox_id." AND status = 1");

                if ($mysterybox->rowCount() == 0) {
                    // update the register on the database
                    $myPushNotification->setAsSent($id);
                    echo "Mystery Box expired.";
                    return;
                }
            }




            //$registrationIds = array( TOKENS );
            // prepare the notification bundle
            $msg = array
            (
                'body'  => $text,
                'title'     => "20Levels",
                'vibrate'   => 1,
                'sound'     => 1,
            );

            $fields = array
            (
                'to'  => '/topics/Test',
                'notification'          => $msg
            );

            $headers = array
            (
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            );

            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $result = curl_exec($ch );
            curl_close( $ch );

            // update the register on the database
            $myPushNotification->setAsSent($id);


            echo $result;



        }

    }else{
        echo "No notifications to be sent.";
    }
?>