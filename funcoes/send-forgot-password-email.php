<?php
    function sendForgotPasswordEmail($player_email, $forgot_password_token){
    	//Configurações
		$destinatario    = $player_email;

		//Dados do formulario
		$nomeRemetente      	= "Hoo from 20Levels!";
		$emailRemetente     	= "holla@my20levels.com";
		/*$mensagemRemetente  	= $_POST['mensagem'];*/


		$titulo = utf8_encode("Password reset.");   

	    //Montando o cabeçalho da mensagem
	    $headers = "MIME-Version: 1.1\r\n";
	    $headers .= "Content-type: text/html; charset=UTF-8\r\n";
	    $headers .= "From: $emailRemetente\r\n"; // remetente
	    //$headers .= "Cc: $copia\r\n"; // Cópia
	    $headers .= "Return-Path: $emailRemetente\r\n"; // return-path

		//Texto do e-mail
		$mensagem = 
		"
			<!DOCTYPE html>
			<html>
			<head>
				<title>20 Levels - Forgot your password?</title>

				<meta charset='utf-8'>
				<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700&display=swap' rel='stylesheet'>
				<style>
					body { padding: 20px 0; }
					br { clear: both; }

					#container {
						background-image: url(https://my20levels.com/content-adm/img/email-bg.png);
						background-position: center;
						background-repeat: none;
						box-sizing: border-box;
						color: #fff;
						font-family: 'Open Sans', sans-serif;
						font-size: 18px;
						height: 1012px;
						margin: 0 auto;
						text-align: center;
						padding: 170px 60px 40px;
						width: 607px;
					}

					#container h1 {
						color: #fb7a8d;
						font-size: 38px;
						font-weight: 700;
						margin-bottom: 50px;
					}

					#container p { 
						margin-bottom: 22px;
						line-height: 25px;
					}

					#container button {
						background-color: #fb7a8d;
						border: 0;
						border-radius: 40px;
						color: #fff;
						cursor: pointer;
						font-family: 'Open Sans', sans-serif;
						font-size: 18px;
						margin: 10px 0;
						height: 60px;
						width: 310px;
					}

					#container button:hover { background-color: #6550db; }

					#container footer {
						color: #061b88;
						font-size: 12px;
						margin-top: 261px;
						width: 476px;
					}

					#container footer #ig-site #ig {
						float: left;
						height: 23px;
						width: 23px;
					}

					#container footer #ig-site span#site {
						color: #061b88;
						display: block;
						float: right;
						line-height: 23px;
					}

					#container footer #message { 
						font-size: 11px;
						margin-top: 25px;
					}

					#container footer #message a { color: #061b88; }

					/*Responsive*/
					@media only screen and (max-width: 680px) {

						body { min-width: 300px; }

						#container {
							font-size: 14px;
							padding: 180px 20px 40px;
							width: 300px;
						}

						#container h1 {
							font-size: 23px;
							margin-bottom: 30px;
						}

						#container p br { display: none; }

						#container p { 
							margin-bottom: 22px;
							line-height: 25px;
						}

						#container button {
							font-size: 16px;
							width: 100%;
						}

						#container footer {
							margin-top: 245px;
							width: 100%;
						}
					}
				</style>
			</head>
			<body>
				<div id='container'>
					<h1>Forgot your Password?</h1>

					<p>
						Oh no! It happens to me all the time. Not to
						<br>worry, you can re-set your password by clicking
						<br>on the shiny button below:
					</p>

					<!-- Confirm email PHP link into href attr -->
					<a href='https://my20levels.com/content-adm/reset-password.php?token=$forgot_password_token' target='_blank'>
						<button>Choose a new password</button>
					</a>

					<p>
						If you didn’t mean to change your password then
						<br>please ignore me and this email. Your password
						<br>will stay the same.
					</p>

					<p>
						Love,
						<br>HOO :)
					</p>

					<footer>
						<div id='ig-site'>
							<!-- Insert instagram link into href attr -->
							<a href='#' target='_blank'>
								<div id='ig'></div>
							</a>

							<!-- Insert site link into href attr -->
							<a href='#' target='_blank'>
								<span id='site'>my20levels.com</span>
							</a>
						<br>
						</div>
					</footer>
				</div>
			</body>
			</html>
		"; // Corpo da mensagem em formato HTML


		// Enviando a mensagem
		if(mail($destinatario, $titulo, $mensagem, $headers)){
			return true;
		}else{
			return false;
		}
    }
?>