<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Psicologia</h1>
		</div>

		<div class="long-text">
			<p>
				A psicologia tem papel essencial no tratamento oncológico. O psicólogo é um dos profissionais responsáveis pelo acompanhamento do paciente ao longo do tratamento. Este profissional poderá auxiliar com conselhos e melhores formas de lidar com o câncer e suas dificuldades, optando sempre pela abordagem que deixará o paciente mais confortável e acolhido.
			</p>
		</div>

	</div>
</section>