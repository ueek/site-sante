<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Mamografia Digital</h1>
		</div>

		<div class="long-text">
			<p>
				A mamografia digital é uma radiografia das mamas, é utilizado um equipamento de alta complexidade, o que auxilia na detecção de pequenos nódulos e tumores em estágio inicial, proporcionando maiores chances de tratamento menos agressivos. <br>
				O mamógrafo armazena as radiografias em formato digital, onde ficam disponíveis para impressão. Um dos maiores benefícios do mamógrafo é um diagnóstico mais confiável e com imagens mais nítidas, facilitando a detecção de alterações nas mamas. 
			</p>
		</div>

	</div>
</section>

<section id="galery">
	<div class="container p-0">
		
		<div id="images">
			
			<a href='/img/institucionais/mamografiadigital1.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/institucionais/mamografiadigital1.jpg);"></div>
			</a>

			<a href='/img/institucionais/mamografiadigital2.png' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/institucionais/mamografiadigital2.png);"></div>
			</a>

			<a href='/img/institucionais/mamografiadigital3.JPG' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/institucionais/mamografiadigital3.JPG);"></div>
			</a>

			<br>

		</div>

	</div>
</section>