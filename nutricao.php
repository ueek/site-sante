<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Nutrição</h1>
		</div>

		<div class="long-text">
			<p>
				Um acompanhamento nutricional de qualidade é fundamental para ajudar na qualidade de vida do paciente em tratamento. Uma alimentação saudável pode auxiliar na elevação da qualidade de vida além de aliviar os sintomas de alguns tratamentos. Os hábitos alimentares saudáveis podem garantir ao paciente redução dos efeitos colaterais e melhora significativa na imunidade. 
			</p>
		</div>

	</div>
</section>