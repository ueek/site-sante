$(document).ready(function(){

    /* Funcionamento do menu */
    $("#menuToggle").click(function(){

        if ($("#checkbox").prop("checked")) {

            $("#menuToggle input:checked ~ span").css("opacity", "1");
            $("#menuToggle input:checked ~ span").css("transform", "rotate(45deg) translate(-2px, -1px)");

            $("#menuToggle input:checked ~ span:nth-last-child(3)").css("opacity", "0");
            $("#menuToggle input:checked ~ span:nth-last-child(3)").css("transform", "rotate(0deg) scale(0.2, 0.2)");

            $("#menuToggle input:checked ~ span:nth-last-child(2)").css("opacity", "1");
            $("#menuToggle input:checked ~ span:nth-last-child(2)").css("transform", "rotate(-45deg) translate(0, -1px)");

            $("#menu-cel").css("transform", "scale(1.0, 1.0)");
            $("#menu-cel").css("opacity", "1");
            $("#menu-cel").fadeIn(300);

            $("#menuToggle span").css({
                width: '30px',
                background: '#b6c72c',
            });

            $('html, body').css({
                overflow:'hidden',
                height:'100%'
            });
        
        } else { 

            $(".span-menu").css("opacity", "1");
            $(".span-menu").css("transform", "none");
            $(".span-menu").css("background", "#b6c72c");

            $("#menu-cel").fadeOut(300);

            $('html, body').css({
                overflow:'auto',
                overflowX:'hidden',
                height:'auto'
            });

        }

    });

    $("#menuToggle .regular-link").click(function(){
        
        $("#checkbox").prop("checked", false);

        $("#menu-cel").fadeToggle(200);
        
        $(".span-menu").css("opacity", "1");
        $(".span-menu").css("transform", "none");
        $(".span-menu").css("background", "#000CC7");

    });

});
