<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Quimioterapia</h1>
		</div>

		<h6 class="content-title">O que é?</h6>

		<div class="long-text">
			<p>
				A quimioterapia é um dos principais tratamentos utilizados no combate ao câncer. Consiste na introdução de substâncias químicas que atuam sobre as células cancerígenas e são transportadas por toda corrente sanguínea, percorrendo todo o corpo, destruindo as células doentes e evitando que elas se espalhem. 
				<br>
				<br>
				O tratamento pode ser indicado como <b>terapia exclusiva</b>, <b>adjuvante</b> ou <b>neoadjuvante</b>. A terapia exclusiva é quando o principal tratamento adotado para combater o câncer é o de quimioterapia.<br>
				<b>Tratamento Adjuvante:</b> é geralmente o tratamento complementar aplicado após o tratamento primário, como a cirurgia, por exemplo.<br>
				<b>Tratamento neoadjuvante:</b> é feito antes da cirurgia, utilizado para diminuir o tumor e a agressividade do procedimento.
				A quimioterapia pode ser utilizada de forma isolada ou ser aliada a outros tratamentos como radioterapia e/ou cirurgia. 
			</p>
		</div>

		<h6 class="content-title">O que são protocolos quimioterápicos?</h6>

		<div class="long-text">
			<p>
				Protocolo de quimioterapia é o termo empregado para definir as propostas de tratamento. Ele estabelece os medicamentos a serem utilizados, determina suas doses em função do peso ou da área de superfície corporal do paciente (calculada com base no peso e na altura) e propõe as datas para sua administração e por quanto tempo.
				<br>
				A recuperação do organismo do paciente é também estimada pelo protocolo, que prevê um período livre de tratamento antes do início de cada novo ciclo de quimioterapia.
			</p>
		</div>

		<h6 class="content-title">Como funciona?</h6>

		<div class="long-text">
			<p>
				O tipo de quimioterapia varia para cada paciente, considerando o tipo do tumor, subtipo, localização, estadiamento, idade, histórico e condições gerais de saúde. Há várias formas de administrar os medicamentos, podendo ser: via oral, intravenosa, intramuscular, subcutânea e intravesical, intra-arterial e intratecal.  
				<br>
				As formas mais comuns da administração da quimioterapia podem ser:<br> 
				<b>Via oral</b><br>
				Medicamentos em forma de comprimidos, líquidos ou cápsulas e podem ser tomados em casa.<br>
				<b>Intravenosa</b><br>
				O medicamento é aplicado diretamente na veia ou por meio de cateter totalmente implantado, dentro do soro ou em forma de injeções.<br>
				<b>Intramuscular</b><br>
				O medicamento é aplicado por meio de injeções no músculo.<br>
				<b>Subcutânea</b><br>
				O medicamento é aplicado com injeções no tecido gorduroso (subcutâneo), abaixo da pele.<br>
				<b>Intravesical</b><br>
				O medicamento é infundido pela uretra até a bexiga.
			</p>
		</div>

		<h6 class="content-title">Quais os efeitos colaterais?</h6>

		<div class="long-text">
			<p>
				As medicações atingem também as células saudáveis do corpo. Sendo assim, a quimioterapia pode provocar alguns efeitos colaterais como queda de cabelo, vômito, náuseas, dores, diarreia,  feridas na boca, entre outros. A intensidade e duração dos efeitos colaterais dependem da quantidade de quimioterápicos recebidos pelo paciente. Eles podem ser minimizados com o uso de medicamentos auxiliares.
			</p>
		</div>

		<h6 class="content-title">Qual a duração do tratamento?</h6>

		<div class="long-text">
			<p>
				O tempo de aplicação e números de sessões será determinado pela equipe médica responsável. A sessão de quimioterapia pode levar uma ou várias horas. Em alguns casos é necessário que o paciente permaneça internado para receber o tratamento por tempo prolongado. A duração normal do tratamento é definida de acordo com o tipo e avanço da doença, variando de paciente para paciente.<br>
				Durante a consulta com o seu médico, ele informará sobre o número de aplicações e ciclos que deverão ser realizados ao longo do procedimento.  
			</p>
		</div>

	</div>
</section>

<section id="galery">
	<div class="container p-0">
		
		<div id="images">
			
			<a href='/img/institucionais/tratamentosquimio1.JPG' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/institucionais/tratamentosquimio1.JPG);"></div>
			</a>

			<a href='/img/institucionais/tratamentosquimio2.JPG' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/institucionais/tratamentosquimio2.JPG);"></div>
			</a>

<!-- 			<a href='/img/institucionais/tratamentosquimio3.JPG' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/institucionais/tratamentosquimio3.JPG);"></div>
			</a> -->

			<br>

		</div>

	</div>
</section>