<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Ligas Acadêmicas</h1>
		</div>

		<div class="desktop">
			
			<div class="col-lg-8 float-left text slide-left">

				<h5 class="content-title">LAGOM</h5>

				<p>
					O Santé Cancer Center, focado no ensino e pesquisa, temos uma colaboração com a Universidade do Plantalto Catarinense (UNIPLAC) onde a Liga Acadêmica de Ginecologia, Obstetrícia e Mastologia (LAGOM) faz parte das nossas programações e eventos. Da mesma forma, o nosso corpo clínico participa de eventos de medicina, onde os nossos médicos proporcionam maior interação na vivência médica e relação com o paciente.
				</p>

			</div>

			<div class="col-lg-4 float-left img slide-right" style="background-image: url(/img/lagom.jpg);"></div>

			<br>
			<br>

		</div>

		<div class="responsive">
			
			<div class="col-md-12 col-sm-12 float-left img" style="background-image: url(/img/lagom.jpg);"></div>

			<div class="col-md-12 col-sm-12 float-left text">

				<h5 class="content-title">LAGOM</h5>

				<p>
					O Santé Cancer Center, focado no ensino e pesquisa, temos uma colaboração com a Universidade do Plantalto Catarinense (UNIPLAC) onde a Liga Acadêmica de Ginecologia, Obstetrícia e Mastologia (LAGOM) faz parte das nossas programações e eventos. Da mesma forma, o nosso corpo clínico participa de eventos de medicina, onde os nossos médicos proporcionam maior interação na vivência médica e relação com o paciente.
				</p>

			</div>

			<br>
			<br>

		</div>

	</div>
</section>