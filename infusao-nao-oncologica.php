<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Infusões não oncológicas</h1>
		</div>

		<h6 class="content-title">O que são?</h6>

		<div class="long-text">
			<p>
				Esta terapia também pode ser conhecida como tratamento imunobiológico e se baseia em ativar o próprio sistema imunológico do paciente, combatendo as células anormais ou processos inflamatórios que venham a ocorrer. É muito utilizada em tratamento para doenças reumáticas e também em casos de câncer e doenças autoimunes (lúpus, psoríase, esclerose múltipla e outras). 
				<br>
				<br>
				Algumas formas de utilização são: 
				<br>
				<b>Terapia imunoestimulante:</b> Utilização de substâncias que aumentam a atividade do sistema imunobiológico 
				<br>
				<b>Terapia de imunoglobulinas:</b> Transferência de anticorpos. 
				<br>
				<b>Terapia de anticorpos monoclonais:</b> Aplicação de algumas proteínas que auxiliam o sistema imunológico a combater alguns tipos de câncer.   
			</p>
		</div>

		<h6 class="content-title">Como funciona?</h6>

		<div class="long-text">
			<p>
				Estes medicamentos imunobiológicos se ligam às citocinas ou outras substâncias que realizam o processo inflamatório no organismo, inibindo-as. O principal papel das citocinas é realizar a comunicação das células que realizam a regulação do sistema imunológico. 
			</p>
		</div>

		<h6 class="content-title">Efeitos colaterais</h6>

		<div class="long-text">
			<p>
				Este tratamento é composto por drogas muito fortes, o que pode causar fraqueza do organismo contra uma variedade de infecções. Podem surgir complicações alérgicas e eventos autoimunes logo após a aplicação da terapia. Por este motivo, antes de iniciar o tratamento o paciente passará por um rigoroso período de avaliações médicas. 
				<br>
				<br>
				Cada organismo reage de uma forma a cada tratamento, alguns fatores essenciais são: idade, exposição prévia a medicamentos, tipo e grau da doença diagnosticada. Em alguns casos, o paciente pode ser assintomático e em outros pode sofrer de enjoos, vômitos, anemia, insônia, diarreia, retenção de líquidos, hipertensão, dor muscular, irritabilidade, fadiga, dor muscular, entre outros. 
			</p>
		</div>

		<h6 class="content-title">Duração do tratamento</h6>

		<div class="long-text">
			<p>
				A duração do tratamento será determinada pelo médico e equipe multidisciplinar que cuida de você. O tipo de doença e características do paciente também podem influenciar no tempo do tratamento. 
			</p>
		</div>

	</div>
</section>