<?php
    
    //Arquivos externos
    include_once 'models/blog.php';
    include_once 'models/blog-photo.php';    
    include_once 'config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Ler dados post
    // ======================================================
    $myBlog = new Blog($db);
    $stmtBlog = $myBlog->read("AND url = '{$_GET['url']}'", "id", "LIMIT 1");

    if ($stmtBlog->rowCount() > 0) {
        $row = $stmtBlog->fetch(PDO::FETCH_ASSOC);
        extract($row);
        $myBlog->setId($id);
        $myBlog->setTitle($title);
        $myBlog->setText($text);
        $myBlog->setImage($image);
        $myBlog->setBlogCategoryId($blog_category_id);
    }


    // Ler dados serviços
    // ======================================================
    $myBlogPhoto = new BlogPhoto($db);
    $listOfBlogPhotos = $myBlogPhoto->read("AND blog_id = {$myBlog->getId()}", "rand()", "LIMIT 3");

     // Recomendados
    $recommendedPosts = $myBlog->read("AND blog_category_id = {$myBlog->getBlogCategoryId()}", "RAND()", "LIMIT 3");

    $urlImages_Blogs = "/content-adm/uploads/blogs/";

?>

<section id="header-post" class="container-fluid" style="background-image: url(<?php echo $urlImages_Blogs.$myBlog->getImage() ?>);"></section>

<section id="content-post">
    <div class="container">
        
        <div class="header-title">
            <h1><?php echo $myBlog->getTitle() ?></h1>
        </div>

        <div class="long-text">
            <p>
                <?php echo nl2br($myBlog->getText()) ?>
            </p>
        </div>

    </div>
</section>

<section id="galery-post">
    <div class="container p-0">
        <div id="images">

            <?php 
    
                if ($listOfBlogPhotos->rowCount() > 0) { 

                    while ($row = $listOfBlogPhotos->fetch(PDO::FETCH_ASSOC)){
                        extract($row);

                        $image = $urlImages_Blogs.$image;

                        echo "
                            <a href='{$image}' data-fancybox='galery-desktop'>
                                <div class='img' style='background-image: url({$image});'></div>
                            </a>
                        ";

                    }

                    echo "<br>";
                    
                }

            ?>

        </div>
    </div>
</section>

<section id="recommended-posts">
    <div class="container p-0">
        
        <div class="col-12">
            <h5>Recomendados para você</h5>
        </div>        

        <div class="posts">

            <?php 
                
                while ($rowBlog = $recommendedPosts->fetch(PDO::FETCH_ASSOC)){ 
                    extract($rowBlog);

                    $image = "/content-adm/uploads/blogs/{$image}";
                    $published_at = date("d/m/Y", strtotime($published_at));

                    if (strlen($title) > 80) {
                        $title = substr($title, 0, 80)."...";
                    }                               

                    echo 
                    "
                        <a class='soft-hover' href='/post/{$url}'>
                            
                            <div class='card'>

                                <div class='card-img soft-hover' style='background-image: url({$image});'></div>
                                
                                <div class='card-body'>

                                    <h5 class='card-title'>
                                        {$title}
                                    </h5>

                                </div>

                                <div class='card-footer'>

                                    <span class='author'>{$author}</span>
                                    <span class='date'>{$published_at}</span>
                                    
                                </div>

                            </div>

                        </a>
                    ";
                }

            ?>
            
            <br>

        </div>

    </div>
</section> 