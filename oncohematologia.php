<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Oncohematologia</h1>
		</div>

		<div class="long-text">
			<p>
				A oncohematologia é responsável por identificar e tratar as doenças malignas do sangue, sendo elas: leucemias, linfomas, mielodisplasias, mieloproliferativas e mieloma múltiplo. 
			</p>
		</div>

	</div>
</section>

<section id="galery">
	<div class="container p-0">
		
		<div id="images">
			
			<a href='/img/infraestrutura.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/infraestrutura.jpg);"></div>
			</a>

			<a href='/img/sobre-nos.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/sobre-nos.jpg);"></div>
			</a>

			<a href='/img/infraestrutura.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/infraestrutura.jpg);"></div>
			</a>

			<br>

		</div>

	</div>
</section>