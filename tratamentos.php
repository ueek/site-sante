<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Tratamentos</h1>
		</div>

		<div class="long-text">
			<p>
				Comprometidos em oferecer tratamentos baseados em evidência científica, personalizado e acompanhado por uma equipe multidisciplinar especializada onde o centro do cuidado é o paciente, o Santé Cancer Center acompanha os cenários e avanços nacionais e internacionais do tratamento oncológico.
				<br>
				<br>
				Desde o diagnóstico, cada etapa do tratamento é realizado de forma clara. O paciente precisa saber se o objetivo é a cura, redução do avanço ou crescimento do tumor, aumento de sobrevida ou busca pelo alívio dos sintomas. Em quaisquer que sejam os objetivos nós estaremos dedicados para que o resultado seja alcançado com qualidade de vida.
				<br>
				<br>
				Pensando nisso, o Santé Cancer Center oferece o que há de mais avançado em tratamento oncológico. Confira:
			</p>
		</div>

		<div class="icons-list">
			<a class="item sequenced-left" href="/quimioterapia">
				<div class="icon soft-hover" style="background-image: url(/img/icones/quimioterapia.png);"></div>
				<h6 class="name soft-hover">Quimioterapia</h6>
			</a>
			<a class="item sequenced-left" href="/radioterapia">
				<div class="icon soft-hover" style="background-image: url(/img/icones/radioterapia.png);"></div>
				<h6 class="name soft-hover">Radioterapia e Radiocirurgia</h6>
			</a>
			<a class="item sequenced-left" href="/truebeam">
				<div class="icon soft-hover" style="background-image: url(/img/icones/truebeam.png);"></div>
				<h6 class="name soft-hover">TrueBeam</h6>
			</a>
			<a class="item sequenced-left" href="/imunoterapia">
				<div class="icon soft-hover" style="background-image: url(/img/icones/imunoterapia.png);"></div>
				<h6 class="name soft-hover">Imunoterapia</h6>
			</a>
			<a class="item sequenced-left" href="/terapia-alvo">
				<div class="icon soft-hover" style="background-image: url(/img/icones/terapia-alvo.png);"></div>
				<h6 class="name soft-hover">Terapia Alvo</h6>
			</a>
			<a class="item sequenced-left" href="/infusao-nao-oncologica">
				<div class="icon soft-hover" style="background-image: url(/img/icones/infusao-nao-oncologica.png);"></div>
				<h6 class="name soft-hover">Infusão não oncológica</h6>
			</a>
			<a class="item sequenced-left" href="/mielograma">
				<div class="icon soft-hover" style="background-image: url(/img/icones/mielograma.png);"></div>
				<h6 class="name soft-hover">Mielograma</h6>
			</a>
			<a class="item sequenced-left" href="/manutencao-de-cateter">
				<div class="icon soft-hover" style="background-image: url(/img/icones/catheter.png);"></div>
				<h6 class="name soft-hover">Manutenção do Cateter</h6>
			</a>
		</div>

		<h6 class="content-title">Acompanhamento Multidisciplinar</h6>

		<div class="icons-list">
			<a class="item sequenced-left" href="/farmacia">
				<div class="icon soft-hover" style="background-image: url(/img/icones/farmacia.png);"></div>
				<h6 class="name soft-hover">Farmácia</h6>
			</a>
			<a class="item sequenced-left" href="/farmacia-clinica">
				<div class="icon soft-hover" style="background-image: url(/img/icones/farmacia-clinica.png);"></div>
				<h6 class="name soft-hover">Farmácia Clínica</h6>
			</a>			
			<a class="item sequenced-left" href="/enfermagem">
				<div class="icon soft-hover" style="background-image: url(/img/icones/enfermagem.png);"></div>
				<h6 class="name soft-hover">Enfermagem</h6>
			</a>
			<a class="item sequenced-left" href="/psicologia">
				<div class="icon soft-hover" style="background-image: url(/img/icones/psicologia.png);"></div>
				<h6 class="name soft-hover">Psicologia</h6>
			</a>
			<a class="item sequenced-left" href="/terapias-integrativas">
				<div class="icon soft-hover" style="background-image: url(/img/icones/terapias-integrativas.png);"></div>
				<h6 class="name soft-hover">Terapias Integrativas</h6>
			</a>
			<a class="item sequenced-left" href="/nutricao">
				<div class="icon soft-hover" style="background-image: url(/img/icones/nutricao.png);"></div>
				<h6 class="name soft-hover">Nutrição</h6>
			</a>
		</div>

	</div>
</section>