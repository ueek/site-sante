<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Mastologia</h1>
		</div>

		<div class="long-text">
			<p>
				A mastologia é a especialidade médica que estuda as glândulas mamárias. Essa especialidade auxilia no diagnóstico, prevenção e tratamento do câncer de mama. <br>
				Além da mastologia, contamos com o Centro de Mama, um ambiente totalmente pensado para o cuidado feminino, onde é realizado mamografia digital e ultrassom de mamas. 
			</p>
		</div>

	</div>
</section>

<section id="galery">
	<div class="container p-0">
		
		<div id="images">

			<a href='/img/institucionais/espec.mastologia2.JPG' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/institucionais/espec.mastologia2.JPG);"></div>
			</a>

			<a href='/img/institucionais/espec.mastologia3.JPG' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/institucionais/espec.mastologia3.JPG);"></div>
			</a>

			<a href='/img/institucionais/espec.mastologia1.JPG' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/institucionais/espec.mastologia1.JPG);"></div>
			</a>

			<br>

		</div>

	</div>
</section>