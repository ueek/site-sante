<?php
	
    //Arquivos externos
    include_once 'config/database.php';

    include_once 'models/team.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // ==========================================================
    // Load Team data
    // ==========================================================
    $urlImages_Team = "/content-adm/uploads/team/"; 
    
    $myTeam = new Team($db);
    $stmtTeam = $myTeam->read("", "sequence, name");
    // ==========================================================

?>

<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/banner.jpg);"></section> -->

<section id="content">
	<div class="container p-0">
		
		<div class="team-items">

			<h1>Equipe Multidisciplinar</h1>

		    <?php 

			    while ($row = $stmtTeam->fetch(PDO::FETCH_ASSOC)){
			        extract($row);

			        $linkedin_area = "";

			        if ($linkedin != '') {
			        	$linkedin_area = "
				        	<a class='linkedin soft-hover' href='{$linkedin}'>
								<i class='fab fa-linkedin'></i>LinkedIn
							</a>";
			        }

			        echo 
			        "
						<div class='item'>
							
							<div class='img-profile' style='background-image: url({$urlImages_Team}{$image});'>
								<div class='watermark' style='background-image: url(/img/profile.png)'></div>
							</div>

							<div class='main-infos'>

								<h2>{$name}</h2>
								<h5>{$charge}</h5>

								{$linkedin_area}

							</div>

							<br>

						</div>
			        ";

			    }

		    ?>

			<br>

		</div>



	</div>
</section>