<section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section>

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Farmacêutico</h1>
		</div>

		<div class="long-text">
			<p>
				Os profissionais da área farmacêutica são de extrema importância para o serviço de quimioterapia uma vez que os mesmos realizam a manipulação dos medicamentos quimioterápicos. Estes profissionais seguem a prescrição médica, tornando o tratamento mais seguro e eficaz para o paciente. 
			</p>
		</div>

	</div>
</section>

<section id="galery">
	<div class="container p-0">
		
		<div id="images">
			
			<a href='/img/institucionais/farmacia1.JPG' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/institucionais/farmacia1.JPG);"></div>
			</a>

			<a href='/img/institucionais/farmacia2.JPG' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/institucionais/farmacia2.JPG);"></div>
			</a>

			<a href='/img/institucionais/farmacia3.JPG' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/institucionais/farmacia3.JPG);"></div>
			</a>

			<br>

		</div>

	</div>
</section>