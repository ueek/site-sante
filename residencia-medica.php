<?php
	
    //Arquivos externos
    include_once 'config/database.php';

    include_once 'models/residency.php';
    include_once 'models/residency-doctor.php';
    include_once 'models/residency-academic-team.php';
    include_once 'models/residency-partner.php';
    include_once 'models/residency-publish.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // ==========================================================
    // Load Residency data
    // ==========================================================
    $urlImages_Residency 			= "/content-adm/uploads/health-insurance/"; 
    $urlImages_Doctor 				= "/content-adm/uploads/doctors/"; 
    $urlImages_AcademicTeam 		= "/content-adm/uploads/academic-team/"; 
	$url_Publishes 					= "/content-adm/uploads/publishes/"; 

    $myResidency = new Residency($db);
    $stmtResidency = $myResidency->read("", "name");
    // ==========================================================

?>

<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Ensino & Pesquisa</h1>
		</div>

		<div class="long-text">

		    <?php 

			    while ($rowResidency = $stmtResidency->fetch(PDO::FETCH_ASSOC)){
			        extract($rowResidency);

			        $residency_id 		= $id;
			        $residency_name 	= $name;

			        // =========================================================================================

			        // Assemble residency doctors
		            $myResidencyDoctor 			= new ResidencyDoctor($db);
    				$stmtResidencyDoctor 		= $myResidencyDoctor->readDoctorsByResidencyId($residency_id);

    				$residency_doctors 			= "";
    				while ($rowResidencyDoctor = $stmtResidencyDoctor->fetch(PDO::FETCH_ASSOC)){
			        	extract($rowResidencyDoctor);

			        	$residency_doctors .=
			        	"
							<div class='item'>
													
								<div class='img-profile' style='background-image: url({$urlImages_Doctor}{$image});'>
									<div class='watermark' style='background-image: url(/img/profile.png)'></div>
								</div>

								<div class='main-infos'>

									<h2>{$name}</h2>
									<h5>{$charge}</h5>
									<span>CRM: {$crm} | RQE: {$rqe}</span>

									<a class='linkedin soft-hover' href='{$linkedin}'>
										<i class='fab fa-linkedin'></i>LinkedIn
									</a>

								</div>

								<br>

							</div>
			        	";
			        }

			        // =========================================================================================

			        // Assemble Residency Academic Team
		            $myResidencyTeam 		= new ResidencyAcademicTeam($db);
    				$stmtResidencyDoctor 	= $myResidencyTeam->read("AND residencies_id = {$residency_id}");

    				$coordination 						= "";
    				$preceptors 						= "";
    				$pedagogical_coordination 			= "";
    				while ($rowResidencyTeam = $stmtResidencyDoctor->fetch(PDO::FETCH_ASSOC)){
			        	extract($rowResidencyTeam);

			        	$item =
			        	"
							<div class='item'>
													
								<div class='img-profile' style='background-image: url({$urlImages_AcademicTeam}{$image});'>
									<div class='watermark' style='background-image: url(/img/profile.png)'></div>
								</div>

								<div class='main-infos'>

									<h2>{$name}</h2>
									<h5>{$charge}</h5>
									<span>CRM: {$crm} | RQE: {$rqe}</span>

									<a class='linkedin soft-hover' href='{$linkedin}'>
										<i class='fab fa-linkedin'></i>LinkedIn
									</a>

								</div>

								<br>

							</div>
			        	";

			        	switch ($sector) {
			        		case 1:
			        			$coordination 				.= $item;
			        			break;

			        		case 2:
			        			$preceptors 				.= $item;
			        			break;

			        		case 3:
			        			$pedagogical_coordination 	.= $item;
			        			break;
			        		
			        		default:
			        			# code...
			        			break;
			        	}
			        }

					// =========================================================================================

			        // Assemble Partners
		            $myPartners = new ResidencyPartner($db);
				    $stmtPartners = $myPartners->readById($residency_id);

				    $residency_partners = "";
				    if ($stmtPartners->rowCount() > 0) {
				        $row = $stmtPartners->fetch(PDO::FETCH_ASSOC);
				        extract($row);

				        $partners_arr		= explode(";", $institutions);

				        foreach ($partners_arr as $partner) {
				        	$residency_partners .=  $partner." <br>";
				        }
				    }

					// =========================================================================================

			        // Assemble Publishes

			        $myPublishes = new ResidencyPublish($db);
    				$listOfPublishes = $myPublishes->read("AND residency_id = {$residency_id}");

    				$residency_publishes 			= "";
    				while ($rowPublishes = $listOfPublishes->fetch(PDO::FETCH_ASSOC)){
			        	extract($rowPublishes);
			        	$residency_publishes 			.= 
			        	"
							<a class='link-accordion soft-hover' target='blank' href='{$url_Publishes}{$url}' download>
								{$title}
							</a>
			        	";
			        }


			        echo 
			        "
						<h5 class='content-title'>{$residency_name}</h5>

						<div class='accordion' id='residency-{$residency_id}-area-1'>
			                <div id='residency-{$residency_id}-heading-1'>

			                    <div class='col-12 toggle-accordion'>
			                        <button data-toggle='collapse' data-target='#residency-{$residency_id}-step-1'>
			                            <i class='fas fa-plus'></i>O Programa
			                        </button>
			                    </div>

			                    <div id='residency-{$residency_id}-step-1' class='collapse' aria-labelledby='residency-{$residency_id}-heading-1' data-parent='#residency-{$residency_id}-area-1'>
			                    	
			                    	<p class='text-accordion'>
			                    		{$description}
			                    	</p>

			                    	<br>

			                    </div>

			                </div>
			            </div>

			            <div class='accordion' id='residency-{$residency_id}-area-2'>
			                <div id='residency-{$residency_id}-heading-2'>

			                    <div class='col-12 toggle-accordion'>
			                        <button data-toggle='collapse' data-target='#residency-{$residency_id}-step-2'>
			                            <i class='fas fa-plus'></i>Informações
			                        </button>
			                    </div>

			                    <div id='residency-{$residency_id}-step-2' class='collapse' aria-labelledby='residency-{$residency_id}-heading-2' data-parent='#residency-{$residency_id}-area-2'>
			                    	
			                    	<p class='text-accordion'>
			                    		{$information}
			                    	</p>

			                    	<br>

			                    </div>

			                </div>
			            </div>

			            <div class='accordion' id='residency-{$residency_id}-area-3'>
			                <div id='residency-{$residency_id}-heading-3'>

			                    <div class='col-12 toggle-accordion'>
			                        <button data-toggle='collapse' data-target='#residency-{$residency_id}-step-3'>
			                            <i class='fas fa-plus'></i>Formas de Ingresso
			                        </button>
			                    </div>

			                    <div id='residency-{$residency_id}-step-3' class='collapse' aria-labelledby='residency-{$residency_id}-heading-3' data-parent='#residency-{$residency_id}-area-3'>
			                    	
			                    	<p class='text-accordion'>
			                    		{$admition}
			                    	</p>

			                    	<br>

			                    </div>

			                </div>
			            </div>

			            <div class='accordion' id='residency-{$residency_id}-area-4'>
			                <div id='residency-{$residency_id}-heading-4'>

			                    <div class='col-12 toggle-accordion'>
			                        <button data-toggle='collapse' data-target='#residency-{$residency_id}-step-4'>
			                            <i class='fas fa-plus'></i>Médicos Residentes
			                        </button>
			                    </div>

			                    <div id='residency-{$residency_id}-step-4' class='collapse' aria-labelledby='residency-{$residency_id}-heading-4' data-parent='#residency-{$residency_id}-area-4'>
			                    	
			                    	{$residency_doctors}

									<br>

			                    </div>

			                </div>
			            </div>

			            <div class='accordion' id='residency-{$residency_id}-area-5'>
			                <div id='residency-{$residency_id}-heading-5'>

			                    <div class='col-12 toggle-accordion'>
			                        <button data-toggle='collapse' data-target='#residency-{$residency_id}-step-5'>
			                            <i class='fas fa-plus'></i>Corpo Acadêmico
			                        </button>
			                    </div>

			                    <div id='residency-{$residency_id}-step-5' class='collapse' aria-labelledby='residency-{$residency_id}-heading-5' data-parent='#residency-{$residency_id}-area-5'>

			                    	<h5 class='subtitle-accordion'>Coordenação do programa</h5>
			                    	
			                    	{$coordination}

									<h5 class='subtitle-accordion'>Preceptores</h5>
			                    	
			                    	{$preceptors}

									<h5 class='subtitle-accordion'>Coordenação pedagógica</h5>
			                    	
			                    	{$pedagogical_coordination}

									<h5 class='subtitle-accordion'>Instituições parceiras</h5>

									<p class='text-accordion'>
			                    		{$residency_partners}
			                    	</p>

			                    	<br>

			                    </div>

			                </div>
			            </div>

			            <div class='accordion' id='residency-{$residency_id}-area-6'>
			                <div id='residency-{$residency_id}-heading-6'>

			                    <div class='col-12 toggle-accordion'>
			                        <button data-toggle='collapse' data-target='#residency-{$residency_id}-step-6'>
			                            <i class='fas fa-plus'></i>Editais
			                        </button>
			                    </div>

			                    <div id='residency-{$residency_id}-step-6' class='collapse' aria-labelledby='residency-{$residency_id}-heading-6' data-parent='#residency-{$residency_id}-area-6'>
			                    	{$residency_publishes}
			                    </div>

			                </div>
			            </div>
			        ";

			    }

		    ?>

			

		</div>
		
	</div>
</section>