<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Radioterapia</h1>
		</div>

		<div class="long-text">
			<p>
				A Radioterapia hoje é considerada um dos tratamentos mais comuns para combate ao câncer e pode ser utilizada alinhada com cirurgia ou quimioterapia. Cada paciente é um caso único, que será analisado e estudado pelo médico responsável, podendo decidir qual o tratamento mais eficaz.<br>
				A Radioterapia faz o uso de radiação ionizante que age combatendo ou evitando a multiplicação desenfreada das células causadoras do câncer.<br>
				O tratamento utilizando Radioterapia normalmente é realizado em hospitais, clínicas e em grandes centros de Oncologia uma vez que os equipamentos de Radioterapia são altamente tecnológicos e possuem um valor expressivo.<br>
				No Santé Cancer Center, contamos com um dos equipamentos mais modernos mundialmente em termo de tecnologia e precisão. O <a href="/truebeam">Varian TrueBeam</a> é referência em radioterapia e proporciona vários benefícios ao paciente oncológico. 
			</p>
		</div>

	</div>
</section>

<section id="galery">
	<div class="container p-0">
		
		<div id="images">
			
			<a href='/img/institucionais/radio1.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/institucionais/radio1.jpg);"></div>
			</a>

			<a href='/img/institucionais/radio2.png' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/institucionais/radio2.png);"></div>
			</a>

			<a href='/img/institucionais/radio3.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/institucionais/radio3.jpg);"></div>
			</a>

			<br>

		</div>

	</div>
</section>