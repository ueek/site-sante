<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Radio-oncologia</h1>
		</div>

		<div class="long-text">
			<p>
				A radio-oncologia é uma especialidade médica que utiliza a Radioterapia (radiação ionizante) no tratamento contra o câncer. A radioterapia é bastante utilizada no tratamento e pode ser utilizada sozinha ou combinada com outros tratamentos como a quimioterapia ou cirurgia.<br> 
				<a href="https://santecancercenter.com.br/radioterapia">Conheça mais sobre a Radioterapia</a>
			</p>
		</div>

	</div>
</section>

<section id="galery">
	<div class="container p-0">
		
		<div id="images">
			
			<a href='/img/institucionais/radio1.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/institucionais/radio1.jpg);"></div>
			</a>

			<a href='/img/institucionais/radio2.png' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/institucionais/radio2.png);"></div>
			</a>

			<a href='/img/institucionais/radio3.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/institucionais/radio3.jpg);"></div>
			</a>

			<br>

		</div>

	</div>
</section>