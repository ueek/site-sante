<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Terapias Integrativas</h1>
		</div>

		<div class="long-text">
			<p>
				As terapias integrativas são utilizadas como completo do tratamento oncológico. Essa abordagem consiste em reunir um vasto conjunto de técnicas que estimulam os mecanismos naturais de cura do organismo humano, promovendo bem-estar físico e emocional. A relação entre paciente e terapeuta  amplia as possibilidades de melhora da saúde e promove a qualidade de vida.
				<br>
				<br>
				Dos tratamentos inclusos na Terapia Integrativa, contamos com:<br> 
				- Auriculoterapia;<br>
				- Acupuntura;<br>
				- Cromoterapia;<br>
				- Dança circular;<br>
				- Yoga Restaurativa e muitos outros.<br>
				<br>
				<br>
				As terapias propostas têm o objetivo de diminuir sintomas físicos, mentais e emocionais do paciente, sejam esses resultantes ou não do tratamento. As terapias são totalmente naturais e podem ser utilizadas por qualquer pessoa. (Esse atendimento é optativo, cabendo ao paciente a escolha de recebê-lo ou não.)
			</p>
		</div>

	</div>
</section>