<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content">
	<div class="container p-0">
		
		<div class="header-title">
			<h1>Perfil Santé</h1>
		</div>

		<div class="profile">

			<div class="line slide-left">
				
				<div class="img" style="background-image: url(/img/institucionais/perfil-equipe-multidisciplinar.svg);"></div>
				
				<div class="text">
					<h3>Equipe Multidisplinar</h3>
					<p>Uma equipe multidisciplinar conta com diversos profissionais de várias áreas específicas, todos os profissionais são especializados em suas respectivas áreas, trazendo um cuidado exclusivo para cada paciente e fazendo com que tenham acompanhamento médico, terapêutico, psicológico, nutricional e mais.</p>
				</div>

				<br>
			</div>

			<hr>

			<div class="line slide-left">
				
				<div class="img" style="background-image: url(/img/institucionais/perfil-alta-tecnologia.svg);"></div>
				
				<div class="text">
					<h3>Alta Tecnologia</h3>
					<p>A tecnologia proporciona ao paciente oncológico maior segurança, menos efeitos colaterais e maior assertividade no tratamento e eliminação de tumores. Cuidamos para que o paciente tenha sempre acesso ao melhor atendimento alinhado com alta tecnologia.</p>
				</div>

				<br>
			</div>

			<hr>

			<div class="line slide-left">
				
				<div class="img" style="background-image: url(/img/institucionais/perfil-medicina-de-referencia.svg);"></div>
				
				<div class="text">
					<h3>Medicina de Referência</h3>
					<p>A medicina de referência oferece tratamento, medicamento e atendimento de altíssima qualidade, alinhado com tecnologia e equipe especializada para que o paciente tenha acesso ao melhor tratamento com uma assertividade mais alta.</p>
				</div>

				<br>
			</div>

			<hr>

			<div class="line slide-left">
				
				<div class="img" style="background-image: url(/img/institucionais/perfil-seguranca.svg);"></div>
				
				<div class="text">
					<h3>Segurança</h3>
					<p>A segurança do paciente é ponto chave no nosso atendimento. Estar seguro, contar com profissionais capacitados e realizar um tratamento com alta tecnologia faz toda diferença para saúde e segurança do paciente.</p>
				</div>

				<br>
			</div>

			<hr>

			<div class="line slide-left">
				
				<div class="img" style="background-image: url(/img/institucionais/perfil-tratamento-humanizado.svg);"></div>
				
				<div class="text">
					<h3>Tratamento Humanizado</h3>
					<p>Um tratamento humanizado faz toda diferença para o paciente e seus familiares. A humanização faz com que o tratamento e todo o acompanhamento médico seja feito de forma exclusiva e individual, onde cada paciente recebe atenção especial e dedicada.</p>
				</div>

				<br>
			</div>
			
		</div>

	</div>
</section>
