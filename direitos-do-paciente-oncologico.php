<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Direitos do Paciente Oncológico</h1>
		</div>

		<div class="long-text">

			<p>
				O Santé Cancer Center batalha diariamente para sempre haja progresso na luta contra o câncer. Durante os nossos atendimentos buscamos sempre oferecer a melhor tecnologia, aliada com ética, respeito e tratamento exclusivo para cada paciente e familiar. 
			</p>

			<p>				
				Para auxiliar o paciente, selecionamos este conteúdo de grande valia onde está disponível os direitos do paciente com câncer. 
			</p>

			<p>
				Este conteúdo trata sobre formas e benefícios legais do direito do paciente oncológico ou pessoas com doenças graves para que possa facilitar e instruir no processo de solicitação de benefícios estipulados por lei. 
			</p>

			<!-- Saque do FGTS -->
			<div class="accordion" id="area-1">
                <div id="heading-1">

                    <div class="col-12 toggle-accordion">
                        <button data-toggle="collapse" data-target="#step-1">
                            <i class="fas fa-plus"></i>Saque do FGTS
                        </button>
                    </div>

                    <div id="step-1" class="collapse" aria-labelledby="heading-1" data-parent="#area-1">
                    	
                    	<p class="text-accordion">
							Na fase sintomática da doença, o trabalhador cadastrado no FGTS que tiver neoplasia maligna (câncer) ou que tenha dependente portador de câncer poderá fazer o saque do FGTS.
						</p>
							 
						 <p class="text-accordion">
							Uma das documentações exigidas é o atestado médico com validade não superior a trinta dias, contados a partir de sua expedição, firmado com assinatura sobre carimbo e CRM do médico responsável pelo tratamento, contendo diagnóstico no qual relate as patologias ou enfermidades que molestam o paciente, o estágio clínico atual da moléstia e do enfermo.
						</p>

						<p class="text-accordion">
							O valor recebido será o saldo de todas as contas pertencentes ao trabalhador, inclusive a conta do atual contrato de trabalho. No caso de motivo de incapacidade relacionado ao câncer, persistindo os sintomas da doença, o saque na conta poderá ser efetuado enquanto houver saldo, sempre que forem apresentados os documentos necessários.
                    	</p>

                    	<p class="text-accordion">
                    		Mais informações: <a href="http://www.caixa.gov.br/Paginas/home-caixa.aspx" target="_blank">http://www.caixa.gov.br/Paginas/home-caixa.aspx</a>
                    	</p>

                    	<br>

                    </div>

                </div>
            </div>

            <!-- PIS/PASEP -->
            <div class="accordion" id="area-2">
                <div id="heading-2">

                    <div class="col-12 toggle-accordion">
                        <button data-toggle="collapse" data-target="#step-2">
                            <i class="fas fa-plus"></i>PIS/PASEP
                        </button>
                    </div>

                    <div id="step-2" class="collapse" aria-labelledby="heading-2" data-parent="#area-2">
                    	
                    	<p class="text-accordion">
                    		O PIS pode ser retirado na Caixa Econômica Federal e o PASEP no Banco do Brasil pelo trabalhador cadastrado no PIS/PASEP antes de 1988 que tiver neoplasia maligna (câncer), na fase sintomática da doença, ou que possuir dependente portador de câncer. O trabalhador receberá o saldo total de suas quotas e rendimentos.
                    	</p>

                    	<p class="text-accordion">
                    		Consultar o site aqui: <a href="http://www.cef.gov.br/" target="_blank">http://www.cef.gov.br/</a>
                    	</p>

                    	<br>

                    </div>

                </div>
            </div>

            <!-- Auxílio Doença -->
            <div class="accordion" id="area-3">
                <div id="heading-3">

                    <div class="col-12 toggle-accordion">
                        <button data-toggle="collapse" data-target="#step-3">
                            <i class="fas fa-plus"></i>Auxílio Doença
                        </button>
                    </div>

                    <div id="step-3" class="collapse" aria-labelledby="heading-3" data-parent="#area-3">
                    	
                    	<p class="text-accordion">
                    		Auxílio-doença é um benefício mensal a que tem direito o segurado quando este fica temporariamente incapaz para o trabalho em virtude de doença por mais de 15 dias consecutivos. 
                		</p>

                		<p class="text-accordion">
							O portador de câncer terá direito ao benefício, independente do pagamento de 12 contribuições, desde que esteja na qualidade de segurado. A incapacidade para o trabalho deve ser comprovada por meio de exame realizado pela perícia médica do INSS.
                    	</p>

                    	<h5 class="content-title">Como solicitar o auxílio doença? </h5>

                    	<p class="text-accordion">
                    		A pessoa deve comparecer à agência da Previdência Social mais próxima de sua residência ou ligar para 135 solicitando o agendamento da perícia médica. É indispensável Carteira de trabalho ou documentos que comprovem a sua contribuição ao INSS, além de declaração ou exame médico (com validade de 30 dias) que descreva o estado clínico do segurado.
                		</p>

						<p class="text-accordion">
							Já servidores públicos e militares são regidos por leis específicas ( lei 8.112/90 e outras Leis). Portanto, para fins de licença para tratamento de saúde e/ou outros benefícios, como licença para acompanhamento de familiar, procure seu órgão pagador (Fundações, Institutos, Autarquias , Comando Militar) ou o Serviço Social da unidade em que realiza o tratamento, para mais orientações.
                    	</p>

                    	<br>

                    </div>

                </div>
            </div>

            <!-- O paciente oncológico pode se aposentar por invalidez? -->
            <div class="accordion" id="area-4">
                <div id="heading-4">

                    <div class="col-12 toggle-accordion">
                        <button data-toggle="collapse" data-target="#step-4">
                            <i class="fas fa-plus"></i>O paciente oncológico pode se aposentar por invalidez?
                        </button>
                    </div>

                    <div id="step-4" class="collapse" aria-labelledby="heading-4" data-parent="#area-4">
                    	
                    	<p class="text-accordion">
                    		A aposentadoria por invalidez é concedida desde que a incapacidade para o trabalho seja considerada definitiva pela perícia médica do INSS. Tem direito ao benefício o segurado que não esteja em processo de reabilitação para o exercício de atividade que lhe garanta a subsistência (independente de estar recebendo ou não o auxílio-doença). O portador de câncer terá direito ao benefício, independente do pagamento de 12 contribuições, desde que esteja na qualidade de segurado. Caso necessite de assistência permanente de outra pessoas, o valor da aposentadoria por invalidez poderá ser aumentado em 25% nas situações previstas no anexo I, do Decreto 3.048/99.
                    	</p>

                    	<p class="text-accordion">
							Servidores públicos e militares são regidos por leis específicas ( lei 8.112/90 e outras Leis). Portanto, procure seu órgão pagador (Fundações, Institutos, Autarquias , Comando Militar) ou o Serviço Social da unidade em que realiza o tratamento, para mais orientações.

                		</p>

                    	<br>

                    </div>

                </div>
            </div>

            <!-- Amparo assistencial / Benefício de prestação continuada -->
            <div class="accordion" id="area-5">
                <div id="heading-5">

                    <div class="col-12 toggle-accordion">
                        <button data-toggle="collapse" data-target="#step-5">
                            <i class="fas fa-plus"></i>Amparo assistencial / Benefício de prestação continuada
                        </button>
                    </div>

                    <div id="step-5" class="collapse" aria-labelledby="heading-5" data-parent="#area-5">
                    	
                    	<p class="text-accordion">
                    		A Lei Orgânica de Assistência Social (LOAS) garante um benefício de um salário-mínimo mensal ao idoso com 65 anos ou mais, que não exerça atividade remunerada, e ao portador de deficiência incapacitado para o trabalho e para uma vida independente. Crianças de zero a 10 anos e adolescentes entre 12 e 18 anos têm os mesmos direitos. Para ter direito ao benefício, outro critério fundamental é de que a renda familiar seja inferior a ¼ (um quarto) do salário-mínimo. Esse cálculo considera o número de pessoas que vivem no mesmo domicílio: o cônjuge, o(a) companheiro(a), os pais, os filhos e irmãos não emancipados de qualquer condição, menores de idade e inválidos. O critério de renda caracteriza a impossibilidade do paciente e de sua família de garantir seu sustento Nos casos em que o paciente sofra de doença em estágio avançado, ou sofra consequências de sequelas irreversíveis do tratamento oncológico, pode-se também recorrer ao benefício, desde que haja uma implicação do seu estado de saúde na incapacidade para o trabalho e nos atos da vida independente.
                    	</p>

                    	<p class="text-accordion"> 
							O requerente também não pode estar vinculado a nenhum regime de previdência social ou receber quaisquer benefícios. Mesmo quando internados, tanto o idoso como o deficiente têm direito ao benefício. O amparo assistencial é intransferível, não gerando direito à pensão a herdeiros ou sucessores. O beneficiário não recebe 13º salário.
						</p>

                    	<p class="text-accordion"> 
							Para solicitar o benefício, a pessoa deve fazer exame médico pericial no INSS. Informações sobre as documentações necessárias você pode obter ligando para o 135 ou pelo site da previdência: <a href="http://www.previdencia.gov.br/" target="_blank">http://www.previdencia.gov.br/</a>
                    	</p>



                    	<br>

                    </div>

                </div>
            </div>

            <!-- Tratamento fora de domicílio (SUS) -->
            <div class="accordion" id="area-6">
                <div id="heading-6">

                    <div class="col-12 toggle-accordion">
                        <button data-toggle="collapse" data-target="#step-6">
                            <i class="fas fa-plus"></i>Tratamento fora de domicílio (SUS)
                        </button>
                    </div>

                    <div id="step-6" class="collapse" aria-labelledby="heading-6" data-parent="#area-6">
                    	
                    	<p class="text-accordion">
							A Portaria SAS nº 055, de 24 de fevereiro de 1999, dispõe sobre a rotina de Tratamento Fora de Domicílio. Esta normatização tem por objetivo garantir o acesso de pacientes de um município a serviços assistenciais em outro município, ou ainda, em caso especiais, de um Estado para outro Estado. O TFD pode envolver a garantia de transporte para tratamento e hospedagem, quando indicado. O TFD será concedido, exclusivamente, a pacientes atendidos na rede pública e referenciada. Nos casos em que houver indicação médica, será autorizado o pagamento de despesas para acompanhante.
                    	</p>


                    	<br>

                    </div>

                </div>
            </div>

            <!-- O que é e quem tem direito ao vale social? -->
            <div class="accordion" id="area-7">
                <div id="heading-7">

                    <div class="col-12 toggle-accordion">
                        <button data-toggle="collapse" data-target="#step-7">
                            <i class="fas fa-plus"></i>O que é e quem tem direito ao vale social?
                        </button>
                    </div>

                    <div id="step-7" class="collapse" aria-labelledby="heading-7" data-parent="#area-7">
                    	
                    	<p class="text-accordion">
							Trata-se de um documento que assegura a gratuidade em ônibus intermunicipais, trem, metrô e barca no Estado do Rio de Janeiro, para portadores de deficiência ou doença crônica. Todo portador de deficiência ou doente crônico que esteja em tratamento médico continuado cuja interrupção acarrete o risco de morte.
						</p>

                    	<p class="text-accordion">
							Acompanhantes de pacientes menores de idade e adultos incapazes, que sejam doentes crônicos ou mentais com indispensável indicação de acompanhante, mencionada em laudo médico, têm direito ao vale social.
						</p>

                    	<p class="text-accordion">
							O cadastro é realizado nos Centros Comunitários de Defesa da Cidadania (CCDCs), Fundação Leão XIII ou outros postos de cadastramento.Veja a lista dos locais no site (<a href="http://www.rj.gov.br/web/setrans" target="_blank">http://www.rj.gov.br/web/setrans</a>). 
						</p>
							
                    	<p class="text-accordion">
							Sobre a documentação consulte o link <a href="http://www.riocard.com/noticias/TEXTO_VALE_SOCIAL.htm" target="_blank">http://www.riocard.com/noticias/TEXTO_VALE_SOCIAL.htm</a>.                    		
                    	</p>


                    	<br>

                    </div>

                </div>
            </div>

            <!-- O paciente oncológico tem direito ao RIOcard? -->
<!--             <div class="accordion" id="area-8">
                <div id="heading-8">

                    <div class="col-12 toggle-accordion">
                        <button data-toggle="collapse" data-target="#step-8">
                            <i class="fas fa-plus"></i>O paciente oncológico tem direito ao RIOcard?
                        </button>
                    </div>

                    <div id="step-8" class="collapse" aria-labelledby="heading-8" data-parent="#area-8">
                    	
                    	<p class="text-accordion">
							Para os pacientes com doença crônica, incluindo o câncer, residentes no município do Rio de Janeiro, o cartão RIOcard está sendo concedido judicialmente desde 2008, mediante laudo médico contido no formulário próprio fornecido pelos postos de cadastramento. É um cartão eletrônico assegurado pelos municípios que oferece gratuidade no transporte rodoviário. O acompanhante também terá direito mediante indicação de acompanhante definida em laudo médico.
						</p>
							 
                    	<p class="text-accordion">							
							Nos Centros de Referência da Assistência Social (CRAS) localizados em seu município você obterá as informações necessárias para realizar o cadastramento.
                    	</p>


                    	<br>

                    </div>

                </div>
            </div> -->

            <!-- Isenção de imposto de renda durante a aposentadoria -->
            <div class="accordion" id="area-9">
                <div id="heading-9">

                    <div class="col-12 toggle-accordion">
                        <button data-toggle="collapse" data-target="#step-9">
                            <i class="fas fa-plus"></i>Isenção de imposto de renda durante a aposentadoria
                        </button>
                    </div>

                    <div id="step-9" class="collapse" aria-labelledby="heading-9" data-parent="#area-9">
                    	
                    	<p class="text-accordion">
							Os pacientes estão isentos do imposto de renda relativo aos rendimentos de aposentadoria, reforma e pensão, inclusive as complementações (RIR/1999, art. 39, XXXIII; IN SRF nº 15, de 2001,art. 5º, XII). Mesmo os rendimentos de aposentadoria ou pensão recebidos acumuladamente não sofrem tributação, ficando isento quem recebeu os referidos rendimentos (Lei nº 7.713, de 1988, art. 6º, inciso XIV).
						</p>

                    	<p class="text-accordion">							 
							Para solicitar a isenção a pessoa deve procurar o órgão pagador da sua aposentadoria (INSS, Prefeitura, Estado etc.) munido de requerimento fornecido pela Receita Federal. A doença será comprovada por meio de laudo médico, que é emitido por serviço médico oficial da União, dos Estados, do Distrito Federal e dos municípios, sendo fixado prazo de validade do laudo pericial, nos casos passíveis de controle (Lei nº 9.250, de 1995, art. 30; RIR/1999, art. 39, §§ 4º e 5º; IN SRF nº 15, de 2001, art. 5º, §§ 1º e 2º).
                    	</p>


                    	<br>

                    </div>

                </div>
            </div>

            <!-- Solicitação de quitação de financiamento da casa própria -->
            <div class="accordion" id="area-10">
                <div id="heading-10">

                    <div class="col-12 toggle-accordion">
                        <button data-toggle="collapse" data-target="#step-10">
                            <i class="fas fa-plus"></i>Solicitação de quitação de financiamento da casa própria
                        </button>
                    </div>

                    <div id="step-10" class="collapse" aria-labelledby="heading-10" data-parent="#area-10">
                    	
                    	<p class="text-accordion">
							A pessoa com invalidez total e permanente, causada por acidente ou doença, possui direito à quitação, caso exista esta cláusula no seu contrato. Para isso deve estar inapto para o trabalho e a doença determinante da incapacidade deve ter sido adquirida após a assinatura do contrato de compra do imóvel.
						</p>

						<p class="text-accordion">	 
							Está incluído nas parcelas do imóvel financiado pelo Sistema Financeiro de Habitação (SFH) um seguro que garante a quitação do imóvel em caso de invalidez ou morte.
						</p>

						<p class="text-accordion">	 
							Em caso de invalidez, este seguro cobre o valor correspondente à cota de participação do paciente no financiamento. A entidade financeira que efetuou o financiamento do imóvel deve encaminhar os documentos necessários à seguradora responsável.

                    	</p>


                    	<br>

                    </div>

                </div>
            </div>

            <!-- Solicitação de isenção do IPI na compra de veículos  -->
            <div class="accordion" id="area-11">
                <div id="heading-11">

                    <div class="col-12 toggle-accordion">
                        <button data-toggle="collapse" data-target="#step-11">
                            <i class="fas fa-plus"></i>Solicitação de isenção do IPI na compra de veículos 
                        </button>
                    </div>

                    <div id="step-11" class="collapse" aria-labelledby="heading-11" data-parent="#area-11">
                    	
                    	<p class="text-accordion">
							O IPI é o imposto federal sobre produtos industrializados. O paciente com câncer é isento deste imposto apenas quando apresenta deficiência física nos membros superiores ou inferiores que o impeça de dirigir veículos comuns. É necessário que o solicitante apresente exames e laudo médico que descrevam e comprovem a deficiência.
						</p>

                    	<p class="text-accordion">
							A Lei nº 10.182, de 12/02/2001, restaura a vigência da Lei nº 8.989, de 24/02/1995, que dispõe sobre a isenção do IPI na aquisição de automóveis destinados ao transporte autônomo de passageiros e ao uso de portadores de deficiência.
						</p>
							
                    	<p class="text-accordion">						 
							Dessa forma os interessados poderão se dirigir a esses locais ou acessá-los pela internet: <a href="www.receita.gov.br" target="_blank">www.receita.gov.br</a> ou pelo link: <a href="http://www.receita.fazenda.gov.br/GuiaContribuinte/IsenDGraves.htm" target="_blank">http://www.receita.fazenda.gov.br/GuiaContribuinte/IsenDGraves.htm</a>
                    	</p>

                    	<h5 class="content-title">Veículos que podem ser adquiridos com a isenção do IPI</h5>

                    	<p class="text-accordion">
							Automóveis de passageiros ou veículos de uso misto de fabricação nacional, movidos a combustível de origem renovável. O veículo precisa apresentar características especiais, originais ou resultantes de adaptação, que permitam a sua adequada utilização por portadores de deficiência física. Entre estas características, o câmbio automático ou hidramático (acionado por sistema hidráulico) e a direção hidráulica.
						</p>

                    	<p class="text-accordion">							 
							A adaptação do veículo poderá ser efetuada na própria montadora ou em oficina especializada. O IPI incidirá normalmente sobre quaisquer acessórios opcionais que não constituam equipamentos originais do veículo adquirido. O benefício somente poderá ser utilizado uma vez. Mas se o veículo tiver sido adquirido há mais de três anos, poderá ser utilizado uma segunda vez.
						</p>


                    	<br>

                    </div>

                </div>
            </div>

            <!-- Isenção de IPVA  -->
            <div class="accordion" id="area-12">
                <div id="heading-12">

                    <div class="col-12 toggle-accordion">
                        <button data-toggle="collapse" data-target="#step-12">
                            <i class="fas fa-plus"></i>Isenção de IPVA 
                        </button>
                    </div>

                    <div id="step-12" class="collapse" aria-labelledby="heading-12" data-parent="#area-12">
                    	
                    	<p class="text-accordion">
							O IPVA é o imposto estadual referente à propriedade de veículos automotores. Cada Estado tem a sua própria legislação sobre o imposto. Confira na lei do seu Estado se existe a regulamentação para isentar de impostos os veículos especialmente adaptados e adquiridos por deficientes físicos.
						</p>

                    	<p class="text-accordion">														 
							Os estados que possuem a regulamentação são Distrito Federal, Espírito Santo, Goiás, Minas Gerais, Paraíba, Paraná, Pernambuco, Piauí, Rio de Janeiro, Rio Grande do Norte, Rio Grande do Sul e São Paulo.
						</p>

                    	<p class="text-accordion">
							<strong>OBS: A isenção do IPVA é concedida simultaneamente à obtenção da isenção do ICMS.</strong> 
                    	</p>


                    	<br>

                    </div>

                </div>
            </div>

            <!-- Isenção de IPTU -->
            <div class="accordion" id="area-13">
                <div id="heading-13">

                    <div class="col-13 toggle-accordion">
                        <button data-toggle="collapse" data-target="#step-13">
                            <i class="fas fa-plus"></i>Isenção de IPTU
                        </button>
                    </div>

                    <div id="step-13" class="collapse" aria-labelledby="heading-13" data-parent="#area-13">
                    	
                    	<p class="text-accordion">
							Alguns municípios preveem, em sua Lei Orgânica, isenção do IPTU para pessoas portadoras de doença crônica, segundo critérios estabelecidos por cada Prefeitura. Confira se você tem direito a este benefício na Prefeitura do seu município.
                    	</p>


                    	<br>

                    </div>

                </div>
            </div>

            <!-- Bilhete de viagem do idoso para transporte interestadual -->
            <div class="accordion" id="area-14">
                <div id="heading-14">

                    <div class="col-14 toggle-accordion">
                        <button data-toggle="collapse" data-target="#step-14">
                            <i class="fas fa-plus"></i>Bilhete de viagem do idoso para transporte interestadual
                        </button>
                    </div>

                    <div id="step-14" class="collapse" aria-labelledby="heading-14" data-parent="#area-14">
                    	
                    	<p class="text-accordion">
							A carteira do idoso é um documento de direito ao acesso a transporte interestadual gratuito (duas vagas por veículo) ou desconto de 50% (cinquenta por cento), no mínimo, no valor das passagens para Idosos com 60 anos de idade ou mais e com renda individual de até dois salários mínimos.
						</p>

                    	<p class="text-accordion">						 
							Este direito está determinado no Estatuto do Idoso - Lei Nº 10741/2003, no art. 40 e o Decreto Nº 5934/2006 estabelece os mecanismos e critérios a serem adotados na aplicação do ICMS.
						</p>

                    	<p class="text-accordion">						 
							Trata de duas vagas gratuitas em cada veículo, comboio ferroviário ou embarcação do serviço de transporte interestadual de passageiros. O idoso terá direito ao “Bilhete de Viagem do Idoso”, que é intransferível. Caso as duas vagas reservadas para este fim tenham sido ocupadas, outros idosos que queiram fazer o mesmo percurso poderão obter descontos de, no mínimo, 50% (cinquenta por cento) no valor da passagem para os demais assentos do veículo. Não estão incluídas no benefício, as tarifas de pedágio e de utilização dos terminais-tarifa de embarque, que serão pagas pelo idoso, no momento da aquisição da passagem.
						</p>

                    	<p class="text-accordion">						 
							Os interessados devem solicitar um único “Bilhete de Viagem do Idoso” nos pontos de venda próprios da transportadora, com antecedência de, pelo menos, três horas em relação ao horário de partida do ponto inicial da linha.
						</p>

                    	<p class="text-accordion">
							<strong>Obs: O idoso que não tiver renda deverá procurar o Centro de Referência de Assistência Social (CRAS) de seu município ou a secretaria municipal de Assistência Social. O idoso será incluído no Cadastro Único para Programas Sociais do Governo Federal e vai receber o Número de Identificação Social (NIS).</strong> 
						</p>

                    	<p class="text-accordion">						 
							Idosos que podem comprovar renda: aposentados, pensionistas ou trabalhadores ativos devem procurar as Empresas de Transporte, levando os documentos exigidos que você encontra no link <a href="http://www.mds.gov.br/falemds/perguntas-frequentes/assistencia-social/assistencia-social/usuario/carteira-do-idoso-usuario/" target="_blank">http://www.mds.gov.br/falemds/perguntas-frequentes/assistencia-social/assistencia-social/usuario/carteira-do-idoso-usuario/</a>
                    	</p>

                    	<br>

                    </div>

                </div>
            </div>

            <!-- Laudo médico para afastamento do trabalho  -->
            <div class="accordion" id="area-15">
                <div id="heading-15">

                    <div class="col-15 toggle-accordion">
                        <button data-toggle="collapse" data-target="#step-15">
                            <i class="fas fa-plus"></i>Laudo médico para afastamento do trabalho 
                        </button>
                    </div>

                    <div id="step-15" class="collapse" aria-labelledby="heading-15" data-parent="#area-15">
                    	
                    	<p class="text-accordion">
							O laudo médico é uma documentação exigida para acesso aos diferentes direitos previdenciários, das iniciativas pública ou privada.
						</p>

                    	<p class="text-accordion"> 
							O médico assistente é o profissional que acompanha o paciente em sua doença e evolução e, quando necessário, emite o devido atestado ou relatório médico. De acordo com o artigo 3º da Resolução CFM 1851/2008, o médico assistente especificará o tempo concedido de dispensa às atividades de trabalho e estudantil, necessário para recuperação do paciente.
                    	</p>

                    	<br>

                    </div>

                </div>
            </div>

            <!-- Laudo médico para atestado de lucidez -->
            <div class="accordion" id="area-16">
                <div id="heading-16">

                    <div class="col-16 toggle-accordion">
                        <button data-toggle="collapse" data-target="#step-16">
                            <i class="fas fa-plus"></i>Laudo médico para atestado de lucidez
                        </button>
                    </div>

                    <div id="step-16" class="collapse" aria-labelledby="heading-16" data-parent="#area-16">
                    	
                    	<p class="text-accordion">
							Este atestado é usualmente utilizado para fins de procuração a terceiros.
 
                     	<p class="text-accordion">
							Conforme Resolução CFM 1658/2002, o atestado médico é parte integrante do ato médico, sendo seu fornecimento direito inalienável do paciente, inclusive para fornecimento de atestados de sanidade, em suas diversas finalidades (arts. 1º e 7°).
                    	</p>

                    	<br>

                    </div>

                </div>
            </div>


		</div>
		
	</div>
</section>