<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Farmácia Clínica</h1>
		</div>

		<div class="long-text">
			<p>
				O objetivo da Farmácia Clínica é garantir o uso correto e racional dos medicamentos, orientando os pacientes de acordo com seu perfil e doença, para assegurar maior qualidade e segurança ao longo de todo o tratamento.
			</p>

			<p>
				A atuação do farmacêutico é fundamental para acompanhar, avaliar e intervir no tratamento quando necessário, para diminuir riscos, prevenir doenças e promover o bem-estar. 
			</p>
			
			<p>
				Nossos farmacêuticos estão sempre investigando e estudando possíveis problemas relacionados aos medicamentos utilizados no tratamento oncológico. Além disso, buscam identificar potenciais interações com alimentos ou outros medicamentos que possam prejudicar o tratamento como um todo. Cabe a eles também fornecerem informações sobre a administração e armazenamento dos medicamentos, e orientarem o paciente e seus familiares sobre os efeitos adversos associados à quimioterapia e radioterapia para minimizá-los. 
			</p>
		</div>

	</div>
</section>