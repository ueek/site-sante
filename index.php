<?php 

	// Redirects
    $actual_link = $_SERVER['REQUEST_URI'];

    if (stripos($actual_link, 'umtoque') !== false) {
		Header( "HTTP/1.1 301 Moved Permanently" );
		Header( "Location: https://santecancercenter.com.br/convenios-centro-de-mama" );
		exit;
    }elseif (stripos($actual_link, 'novo-blog') !== false) {
		Header( "HTTP/1.1 301 Moved Permanently" );
		Header( "Location: https://santecancercenter.com.br/blog" );
		exit;
    }elseif (stripos($actual_link, 'centrodemama') !== false) {
		Header( "HTTP/1.1 301 Moved Permanently" );
		Header( "Location: https://santecancercenter.com.br/mastologia" );
		exit;
    }elseif (stripos($actual_link, 'farmaceuticos') !== false) {
		Header( "HTTP/1.1 301 Moved Permanently" );
		Header( "Location: https://santecancercenter.com.br/farmacia" );
		exit;
    }elseif (stripos($actual_link, 'le-sante') !== false) {
		Header( "HTTP/1.1 301 Moved Permanently" );
		Header( "Location: https://santecancercenter.com.br/cancer-center" );
		exit;
    }

	setlocale(LC_TIME, 'pt_BR');
	date_default_timezone_set('America/Sao_Paulo');

    //Arquivos externos
    include_once 'config/database.php';
    include_once 'models/about.php';
    include_once 'models/banner.php';
    include_once 'models/blog.php';
    include_once 'models/blog-photo.php';
    include_once 'models/service.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // // Ler dados institucionais
    // // ======================================================
    $myAbout = new About($db);
    $stmtAbout = $myAbout->read("", "id", "LIMIT 1");
    if ($stmtAbout->rowCount() > 0) {

        $row = $stmtAbout->fetch(PDO::FETCH_ASSOC);
        extract($row);
        $myAbout->setInfrastructure($infrastructure);
        $myAbout->setPhone($phone);
        $myAbout->setWhatsapp($whatsapp);
        $myAbout->setEmail($email);
        $myAbout->setLinkedin($linkedin);
        $myAbout->setInstagram($instagram);
        $myAbout->setFacebook($facebook);
        $myAbout->setYouTube($youtube);
    }

    // Ler dados banners
    // ======================================================
    $myBanner = new Banner($db);
    $listOfBannersDesktop = $myBanner->read("", "sequence ASC, id DESC");
    $listOfBannersMobile = $myBanner->read("", "sequence ASC, id DESC");

    // Ler dados serviços
    // ======================================================
    $myService = new Service($db);
    $listOfServices = $myService->read();

    // Ler dados posts
    // ======================================================
    $myBlog  = new Blog($db);
    $listOfPosts = $myBlog->read("AND published_at <= NOW()", "published_at DESC", "LIMIT 6");

    $urlImages_Banners  = "/content-adm/uploads/banners/";
    $urlImages_Services = "/content-adm/uploads/services/";
    $urlImages_Post = "/content-adm/uploads/blogs/";

    $home_page = '';
    $institucional_page = '';
    $convenios_page = '';
    $corpo_clinico_page = '';
    $especialidades_page = '';
    $tratamentos_page = '';
    $diagnostico_page = '';
    $ensino_pesquisa_page = '';
    $blog_page = '';

    if(!isset($_GET['pg']) or empty($_GET['pg'])){
    	$home_page 	  = "active";
    }else{
        if($_SERVER['REQUEST_METHOD'] == 'GET'){
        	switch ($_GET['pg']) {

        		case 'cancer-center':
			    	$institucional_page = "active";
        			break;

        		case 'corpo-multidisciplinar':
			    	$institucional_page = "active";
        			break;

        		case 'convenios':
			    	$convenios_page = "active";
        			break;

        		case 'convenios-centro-de-mama':
			    	$convenios_page = "active";
        			break;

        		case 'perfil':
			    	$institucional_page = "active";
        			break;

        		case 'direitos-do-paciente-oncologico':
			    	$institucional_page = "active";
        			break;

        		case 'corpo-clinico':
			    	$corpo_clinico_page = "active";
        			break;

        		case 'oncologia':
			    	$especialidades_page = "active";
        			break;

        		case 'oncohematologia':
			    	$especialidades_page = "active";
        			break;
        		
        		case 'hematologia':
			    	$especialidades_page = "active";
        			break;

        		case 'mastologia':
			    	$especialidades_page = "active";
        			break;

        		case 'radio-oncologia':
			    	$especialidades_page = "active";
        			break;

        		case 'oncogenetica':
			    	$especialidades_page = "active";
        			break;

        		case 'tratamentos':
			    	$tratamentos_page = "active";
        			break;

        		case 'radioterapia':
			    	$tratamentos_page = "active";
        			break;

        		case 'truebeam':
			    	$tratamentos_page = "active";
        			break;

        		case 'quimioterapia':
			    	$tratamentos_page = "active";
        			break;

        		case 'imunoterapia':
			    	$tratamentos_page = "active";
        			break;

    			case 'farmacia':
			    	$tratamentos_page = "active";
        			break;

    			case 'farmacia-clinica':
			    	$tratamentos_page = "active";
        			break;        			

        		case 'enfermagem':
			    	$tratamentos_page = "active";
        			break;

        		case 'nutricao':
			    	$tratamentos_page = "active";
        			break;

        		case 'diagnostico-por-imagem-e-pet-ct':
			    	$diagnostico_page = "active";
        			break;

				case 'mamografia-digital':
			    	$diagnostico_page = "active";
        			break;

        		case 'ultrassonografia':
			    	$diagnostico_page = "active";
        			break;

        		case 'biopsias-guiadas-por-ultrassom':
			    	$diagnostico_page = "active";
        			break;

        		case 'estereotaxia':
			    	$diagnostico_page = "active";
        			break;

        		case 'residencia-medica':
			    	$ensino_pesquisa_page = "active";
        			break;

        		case 'lagom':
			    	$ensino_pesquisa_page = "active";
        			break;

        		case 'lahonco':
			    	$ensino_pesquisa_page = "active";
        			break;

        		case 'estagios-e-visitas':
			    	$ensino_pesquisa_page = "active";
        			break;

        		case stripos($_GET['pg'], 'blog') !== false:
			    	$blog_page = "active";
        			break;

    			case stripos($_GET['pg'], 'post') !== false:
			    	$blog_page = "active";
        			break;

    			case stripos($_GET['pg'], 'preview-post') !== false:
			    	$blog_page = "active";
        			break;        			

        		default:
			    	$home_page = "active";
        			break;
        	}
        }else{
			$home_page = "active";
        }
    }



?>

<!DOCTYPE html>
<html lang="pt-br" class="no-js">

<head>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-57CM8HJ');</script>
	<!-- End Google Tag Manager -->

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-176638649-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-176638649-1');
	</script>

	<meta charset="UTF-8">
	<meta name="robots" content="index,follow">
	<meta name="author" content="Ueek Agência Digital">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=0">

	<?php

		if(!isset($_GET['pg']) or empty($_GET['pg'])){ 

			echo"
	    	<title>Santé Cancer Center</title>
			<meta name='description' content='O Santé é referência no tratamento de câncer. Oncologia, hematologia, oncohematologia, mastologia, quimioterapia, radioterapia e imunoterapia em Lages, Santa Catarina.'>";

            echo"
            <meta property='og:title' content='Santé Cancer Center'>
            <meta property='og:type' content='website'>
            <meta property='og:image' content='http://sante.com.br/'>
            <meta property='og:site_name' content='Santé Cancer Center'>
            <meta property='og:description' content='O Santé é referência no tratamento de câncer. Oncologia, hematologia, oncohematologia, mastologia, quimioterapia, radioterapia e imunoterapia em Lages, Santa Catarina.'>";
            
		}else{
			$pagina = $_GET['pg'];

			switch ($pagina) {
			    
			    case "convenios":
			        echo "<title>Conheça os convênios médicos disponíveis do Santé Cancer Center</title>";
			        echo "<meta name='description' content='O Santé oferece diversos convênios para facilitar o atendimento e proporcionar o melhor aos seus pacientes. Confira.'>";

			        echo "<meta property='og:title' content='Conheça os convênios médicos disponíveis do Santé Cancer Center'>";
			    	echo "<meta property='og:description' content='O Santé oferece diversos convênios para facilitar o atendimento e proporcionar o melhor aos seus pacientes. Confira.'>";
			        break;

			    case "convenios-centro-de-mama":
			        echo "<title>Conheça os convênios disponíveis no Centro de Mama do Santé Cancer Center</title>";
			        echo "<meta name='description' content='O Santé oferece diversos convênios para facilitar o atendimento e proporcionar o melhor aos seus pacientes. Confira.'>";

			        echo "<meta property='og:title' content='Conheça os convênios disponíveis no Centro de Mama do Santé Cancer Center'>";
			    	echo "<meta property='og:description' content='O Santé oferece diversos convênios para facilitar o atendimento e proporcionar o melhor aos seus pacientes. Confira.'>";
			        break;

			    case "corpo-clinico":
			        echo "<title>Corpo Clínico do Santé Cancer Center: Conheça nossos médicos</title>";
			        echo "<meta name='description' content='Conheça a equipe médica que trabalha para oferecer o melhor para seus pacientes.'>";

			        echo "<meta property='og:title' content='Corpo Clínico do Santé Cancer Center: Conheça nossos médicos'>";
			    	echo "<meta property='og:description' content='Conheça a equipe médica que trabalha para oferecer o melhor para seus pacientes.'>";
			        break;

			    case "corpo-multidisciplinar":
			        echo "<title>Equipe Multidisciplinar do Santé Cancer Center: Conheça nossos colaboradores</title>";
			        echo "<meta name='description' content='Conheça a equipe multidisciplinar que trabalha para oferecer o melhor para os pacientes do Santé Cancer Center.'>";

			        echo "<meta property='og:title' content='Equipe Multidisciplinar do Santé Cancer Center: Conheça nossos colaboradores'>";
			    	echo "<meta property='og:description' content='Conheça a equipe multidisciplinar que trabalha para oferecer o melhor para os pacientes do Santé Cancer Center.'>";
			        break;

			    case "perfil":
			        echo "<title>Perfil Santé: O que faz do Santé Cancer Center uma referência no tratamento oncológico</title>";
			        echo "<meta name='description' content='Confira as características do Santé Cancer Center e saiba mais sobre os nossos objetivos e valores.'>";

			        echo "<meta property='og:title' content='Perfil Santé: O que faz do Santé Cancer Center uma referência no tratamento oncológico'>";
			    	echo "<meta property='og:description' content='Confira as características do Santé Cancer Center e saiba mais sobre os nossos objetivos e valores.'>";
			        break;

			    case "enfermagem":
			        echo "<title>A importância da enfermagem no Santé Cancer Center</title>";
			        echo "<meta name='description' content='Os enfermeiros da área oncológica têm um papel fundamental no tratamento do câncer. Clique e saiba mais.'>";

			        echo "<meta property='og:title' content='A importância da enfermagem no Santé Cancer Center'>";
			    	echo "<meta property='og:description' content='Os enfermeiros da área oncológica têm um papel fundamental no tratamento do câncer. Clique e saiba mais.'>";
			        break;

			    case "residencia-medica":
			        echo "<title>Residência médica em Oncologia - Cancerologia Clínica</title>";
			        echo "<meta name='description' content='O Santé trouxe a Lages o curso de Residência Médica em Cancerologia Clínica. Confira e saiba mais.'>";

			        echo "<meta property='og:title' content='Residência médica em Oncologia - Cancerologia Clínica'>";
			    	echo "<meta property='og:description' content='O Santé trouxe a Lages o curso de Residência Médica em Cancerologia Clínica. Confira e saiba mais.'>";
			        break;

			    case "farmacia":
			        echo "<title>O trabalho dos farmacêuticos no Santé Cancer Center</title>";
			        echo "<meta name='description' content='Os farmacêuticos são peças fundamentais na manipulação e gerenciamento dos quimioterápicos. Conheça mais sobre o trabalho deles no Santé Cancer Center.'>";

			        echo "<meta property='og:title' content='O trabalho dos farmacêuticos no Santé Cancer Center'>";
			    	echo "<meta property='og:description' content='Os farmacêuticos são peças fundamentais na manipulação e gerenciamento dos quimioterápicos. Conheça mais sobre o trabalho deles no Santé Cancer Center.'>";
			        break;

			    case "farmacia-clinica":
			        echo "<title>Farmácia Clínica - Santé Cancer Center - Oncologia em Lages</title>";
			        echo "<meta name='description' content='A Farmácia Clínica tem como objetivo orientar e garantir o uso correto e racional dos medicamentos levando em consideração a doença e o perfil de cada paciente.'>";

			        echo "<meta property='og:title' content='Farmácia Clínica - Santé Cancer Center - Oncologia em Lages'>";
			    	echo "<meta property='og:description' content='A Farmácia Clínica tem como objetivo orientar e garantir o uso correto e racional dos medicamentos levando em consideração a doença e o perfil de cada paciente.'>";
			        break;

			    case "hematologia":
			        echo "<title>Hematologia - Especialidade Santé Cancer Center</title>";
			        echo "<meta name='description' content='A Hematologia é a especialidade médica que estuda os elementos figurados do sangue. Consulte nosso hematologista e saiba mais.'>";

			        echo "<meta property='og:title' content='Hematologia - Especialidade Santé Cancer Center'>";
			    	echo "<meta property='og:description' content='A Hematologia é a especialidade médica que estuda os elementos figurados do sangue. Consulte nosso hematologista e saiba mais.'>";
			        break;

			    case "radio-oncologia":
			        echo "<title>Radio-oncologia - Especialidade Santé Cancer Center</title>";
			        echo "<meta name='description' content='A radio-oncologia é uma especialidade médica que utiliza a Radioterapia (radiação ionizante) no tratamento contra o câncer. Saiba mais no site.'>";

			        echo "<meta property='og:title' content='Radio-oncologia - Especialidade Santé Cancer Center'>";
			    	echo "<meta property='og:description' content='A radio-oncologia é uma especialidade médica que utiliza a Radioterapia (radiação ionizante) no tratamento contra o câncer. Saiba mais no site.'>";
			        break;

			    case "oncogenetica":
			        echo "<title>Oncogenética - Especialidade Santé Cancer Center</title>";
			        echo "<meta name='description' content='A Oncogenética é uma especialidade médica que atua em identificar mutações genéticas com o objetivo de encontrar sinais que possam significar um câncer futuro. Saiba mais sobre o tema no nosso site.'>";

			        echo "<meta property='og:title' content='Radio-oncologia - Especialidade Santé Cancer Center'>";
			    	echo "<meta property='og:description' content='A Oncogenética é uma especialidade médica que atua em identificar mutações genéticas com o objetivo de encontrar sinais que possam significar um câncer futuro. Saiba mais sobre o tema no nosso site.'>";
			        break;

			    case "psicologia":
			        echo "<title>Saiba mais sobre a Psicologia no tratamento oncológico</title>";
			        echo "<meta name='description' content='A Psicologia faz parte do Acompanhamento Multidisciplinar no Santé Cancer Center.'>";

			        echo "<meta property='og:title' content='Saiba mais sobre a Psicologia no tratamento oncológico'>";
			    	echo "<meta property='og:description' content='A Psicologia faz parte do Acompanhamento Multidisciplinar no Santé Cancer Center.'>";
			        break;

			    case "terapias-integrativas":
			        echo "<title>Saiba mais sobre as Terapias Integrativas no tratamento oncológico</title>";
			        echo "<meta name='description' content='As Terapias Integrativas fazem parte do Acompanhamento Multidisciplinar no Santé Cancer Center.'>";

			        echo "<meta property='og:title' content='Saiba mais sobre as Terapias Integrativas no tratamento oncológico'>";
			    	echo "<meta property='og:description' content='As Terapias Integrativas fazem parte do Acompanhamento Multidisciplinar no Santé Cancer Center.'>";
			        break;

			    case "cancer-center":
			        echo "<title>Sobre o Santé Cancer Center</title>";
			        echo "<meta name='description' content='Aqui o paciente oncológico é tratado de maneira única e o foco de todo o ambiente e de todos os profissionais é o cuidado direcionado e a atenção dedicada.'>";

			        echo "<meta property='og:title' content='Sobre o Santé Cancer Center'>";
			    	echo "<meta property='og:description' content='Aqui o paciente oncológico é tratado de maneira única e o foco de todo o ambiente e de todos os profissionais é o cuidado direcionado e a atenção dedicada.'>";
			        break;

			    case "mastologia":
			        echo "<title>Mastologia - Especialidade Santé Cancer Center</title>";
			        echo "<meta name='description' content='A Mastologia inclui prevenção, diagnóstico, tratamento e reabilitação dos pacientes com alterações na mama. Consulte nossos mastologistas.'>";

			        echo "<meta property='og:title' content='Mastologia - Especialidade Santé Cancer Center'>";
			    	echo "<meta property='og:description' content='A Mastologia inclui prevenção, diagnóstico, tratamento e reabilitação dos pacientes com alterações na mama. Consulte nossos mastologistas.'>";
			        break;

			    case "nutricao":
			        echo "<title>A nutrição como prevenção e tratamento do câncer</title>";
			        echo "<meta name='description' content='Conheça mais sobre esse fator tão impactante na prevenção e no tratamento de pacientes oncológicos.'>";

			        echo "<meta property='og:title' content='A nutrição como prevenção e tratamento do câncer'>";
			    	echo "<meta property='og:description' content='Conheça mais sobre esse fator tão impactante na prevenção e no tratamento de pacientes oncológicos.'>";
			        break;

			    case "tratamentos":
			        echo "<title>Tratamentos disponíveis no Santé Cancer Center</title>";
			        echo "<meta name='description' content='O Santé Cancer Center conta com o que há de melhor para que seus pacientes tenham tratamentos modernos e de qualidade. Confira!'>";

			        echo "<meta property='og:title' content='Tratamentos disponíveis no Santé Cancer Center'>";
			    	echo "<meta property='og:description' content='O Santé Cancer Center conta com o que há de melhor para que seus pacientes tenham tratamentos modernos e de qualidade. Confira!'>";
			        break;

			    case "oncohematologia":
			        echo "<title>Oncohematologia  - Especialidade Santé Cancer Center</title>";
			        echo "<meta name='description' content='A Oncohematologia é a área que cuida das doenças malignas do sangue e gânglios ou ínguas. Para mais informações, clique e confira.'>";

			        echo "<meta property='og:title' content='Oncohematologia  - Especialidade Santé Cancer Center'>";
			    	echo "<meta property='og:description' content='A Oncohematologia é a área que cuida das doenças malignas do sangue e gânglios ou ínguas. Para mais informações, clique e confira.'>";
			        break;

			    case "oncologia":
			        echo "<title>O que é Oncologia?</title>";
			        echo "<meta name='description' content='A oncologia é a especialidade médica responsável pelo diagnóstico e indicação do tratamento específico para cada tipo de tumor. Consulte nossos médicos.'>";

			        echo "<meta property='og:title' content='O que é Oncologia?'>";
			    	echo "<meta property='og:description' content='A oncologia é a especialidade médica responsável pelo diagnóstico e indicação do tratamento específico para cada tipo de tumor. Consulte nossos médicos.'>";
			        break;

			    case "imunoterapia":
			        echo "<title>Como funciona e o que é a Imunoterapia?</title>";
			        echo "<meta name='description' content='A imunoterapia é um tratamento que fortalece o sistema imunológico, induzindo o combate das células cancerígenas pelo próprio organismo. Saiba mais sobre o tema no nosso site.'>";

			        echo "<meta property='og:title' content='O que é Oncologia?'>";
			    	echo "<meta property='og:description' content='A imunoterapia é um tratamento que fortalece o sistema imunológico, induzindo o combate das células cancerígenas pelo próprio organismo. Saiba mais sobre o tema no nosso site.'>";
			        break;

			    case "quimioterapia":
			        echo "<title>Como funciona e o que é a Quimioterapia?</title>";
			        echo "<meta name='description' content='A quimioterapia é um tipo de tratamento médico que introduz na circulação sanguínea compostos químicos. O Santé dispõe do tratamento. Clique e saiba mais.'>";

			        echo "<meta property='og:title' content='Como funciona e o que é a Quimioterapia?'>";
			    	echo "<meta property='og:description' content='A quimioterapia é um tipo de tratamento médico que introduz na circulação sanguínea compostos químicos. O Santé dispõe do tratamento. Clique e saiba mais.'>";

			        break;

			    case "radioterapia":
			        echo "<title>Como funciona e o que é a Radioterapia?</title>";
			        echo "<meta name='description' content='A radioterapia utiliza radiações ionizantes para destruir ou impedir que as células do tumor aumentem. O Santé dispõe do tratamento. Clique e saiba mais.'>";

			        echo "<meta property='og:title' content='Como funciona e o que é a Radioterapia?'>";
			    	echo "<meta property='og:description' content='A radioterapia utiliza radiações ionizantes para destruir ou impedir que as células do tumor aumentem. O Santé dispõe do tratamento. Clique e saiba mais.'>";

			        break;

		        case "truebeam":
			        echo "<title>Tratamento de radioterapia com o TrueBeam</title>";
			        echo "<meta name='description' content='Mais conforto, menos efeitos colaterais e maior assertividade com o equipamento mais avançado no tratamento de radioterapia em Lages/SC.'>";

			        echo "<meta property='og:title' content='Tratamento de radioterapia com o TrueBeam'>";
			    	echo "<meta property='og:description' content='Mais conforto, menos efeitos colaterais e maior assertividade com o equipamento mais avançado no tratamento de radioterapia em Lages/SC.'>";

			        break;

			    case "terapia-alvo":
			        echo "<title>Saiba o que é a Terapia Alvo e como funciona</title>";
			        echo "<meta name='description' content='A Terapia Alvo é um dos tratamentos ofertados no Santé Cancer Center, acesse o site e saiba mais.'>";

			        echo "<meta property='og:title' content='Saiba o que é a Terapia Alvo e como funciona'>";
			    	echo "<meta property='og:description' content='A Terapia Alvo é um dos tratamentos ofertados no Santé Cancer Center, acesse o site e saiba mais.'>";
			        break;

			    case "infusao-nao-oncologica":
			        echo "<title>Saiba o que é a Infusão não oncológica e como funciona</title>";
			        echo "<meta name='description' content='A Infusão não oncológica é um dos tratamentos ofertados no Santé Cancer Center, acesse o site e saiba mais.'>";

			        echo "<meta property='og:title' content='Saiba o que é a Infusão não oncológica e como funciona'>";
			    	echo "<meta property='og:description' content='A Infusão não oncológica é um dos tratamentos ofertados no Santé Cancer Center, acesse o site e saiba mais.'>";
			        break;

			    case "mielograma":
			        echo "<title>Saiba o que é Mielograma e como funciona</title>";
			        echo "<meta name='description' content='Mielograma é um dos tratamentos ofertados no Santé Cancer Center, acesse o site e saiba mais.'>";

			        echo "<meta property='og:title' content='Saiba o que é Mielograma e como funciona'>";
			    	echo "<meta property='og:description' content='Mielograma é um dos tratamentos ofertados no Santé Cancer Center, acesse o site e saiba mais.'>";
			        break;

			    case "manutencao-de-cateter":
			        echo "<title>Saiba o que é a Manutenção de Cateter e como funciona</title>";
			        echo "<meta name='description' content='A Manutenção de Cateter é um dos procedimentos ofertados no Santé Cancer Center, acesse o site e saiba mais.'>";

			        echo "<meta property='og:title' content='Saiba o que é a Manutenção de Cateter e como funciona'>";
			    	echo "<meta property='og:description' content='A Manutenção de Cateter é um dos procedimentos ofertados no Santé Cancer Center, acesse o site e saiba mais.'>";
			        break;

			    case "mamografia-digital":
			        echo "<title>Mamografia Digital - Diagnóstico Santé Cancer Center</title>";
			        echo "<meta name='description' content='A mamografia digital é uma radiografia das mamas que auxilia na detecção de pequenos nódulos e tumores em estágio inicial. Saiba mais sobre esse tipo de diagnóstico no nosso site.'>";

			        echo "<meta property='og:title' content='Mamografia Digital - Diagnóstico Santé Cancer Center'>";
			    	echo "<meta property='og:description' content='A mamografia digital é uma radiografia das mamas que auxilia na detecção de pequenos nódulos e tumores em estágio inicial. Saiba mais sobre esse tipo de diagnóstico no nosso site.'>";
			        break;

			    case "ultrassonografia":
			        echo "<title>Ultrassonografia - Diagnóstico Santé Cancer Center</title>";
			        echo "<meta name='description' content='O ultrassom de mamas é utilizado para examinar algumas alterações que foram identificados na mamografia. Saiba mais sobre esse tipo de diagnóstico no nosso site.'>";

			        echo "<meta property='og:title' content='Ultrassonografia - Diagnóstico Santé Cancer Center'>";
			    	echo "<meta property='og:description' content='O ultrassom de mamas é utilizado para examinar algumas alterações que foram identificados na mamografia. Saiba mais sobre esse tipo de diagnóstico no nosso site.'>";
			        break;

			    case "biopsias-guiadas-por-ultrassom":
			        echo "<title>Biópsias guiadas por ultrassom - Diagnóstico Santé Cancer Center</title>";
			        echo "<meta name='description' content='A biópsia guiada por ultrassom é realizada quando a mamografia digital detecta algum tipo de alteração mamária. Saiba mais sobre esse tipo de diagnóstico no nosso site.'>";

			        echo "<meta property='og:title' content='Biópsias guiadas por ultrassom - Diagnóstico Santé Cancer Center'>";
			    	echo "<meta property='og:description' content='A biópsia guiada por ultrassom é realizada quando a mamografia digital detecta algum tipo de alteração mamária. Saiba mais sobre esse tipo de diagnóstico no nosso site.'>";
			        break;

			    case "estereotaxia":
			        echo "<title>Estereotaxia - Diagnóstico Santé Cancer Center</title>";
			        echo "<meta name='description' content='A estereotaxia é utilizada para identificar nódulos mamários não palpáveis. Saiba mais sobre esse tipo de diagnóstico no nosso site.'>";

			        echo "<meta property='og:title' content='Estereotaxia - Diagnóstico Santé Cancer Center'>";
			    	echo "<meta property='og:description' content='A estereotaxia é utilizada para identificar nódulos mamários não palpáveis. Saiba mais sobre esse tipo de diagnóstico no nosso site.'>";
			        break;

			   	case "estagios-e-visitas":
			        echo "<title>Saiba mais sobre os estágios e visitas no Santé Cancer Center</title>";
			        echo "<meta name='description' content='Saiba mais e confira as oportunidades de estágios e visitas no Santé Cancer Center.'>";

			    	echo "<meta property='og:title' content='Saiba mais sobre os estágios e visitas no Santé Cancer Center'>";
			    	echo "<meta property='og:description' content='Saiba mais e confira as oportunidades de estágios e visitas no Santé Cancer Center.'>";
			        break;

			   	case "direitos-do-paciente-oncologico":
			        echo "<title>Saiba quais são os direitos dos pacientes oncológicos</title>";
			        echo "<meta name='description' content='Conheça direitos como Isenção de impostos, Saque de FGTS, PIS/PASEP, Auxílio Doença dentre outras garantias para o paciente em tratamento contra o câncer.'>";

			    	echo "<meta property='og:title' content='Saiba quais são os direitos dos pacientes oncológicos'>";
			    	echo "<meta property='og:description' content='Conheça direitos como Isenção de impostos, Saque de FGTS, PIS/PASEP, Auxílio Doença dentre outras garantias para o paciente em tratamento contra o câncer.'>";
			        break;

			    case "lagom":
			        echo "<title>LAGOM - Ligas Universitárias do Santé Cancer Center</title>";
			        echo "<meta name='description' content='Saiba mais sobre a parceria do Santé Cancer Center com Universidade do Planalto Catarinense.'>";

			    	echo "<meta property='og:title' content='LAGOM - Ligas Universitárias do Santé Cancer Center'>";
			    	echo "<meta property='og:description' content='Saiba mais sobre a parceria do Santé Cancer Center com Universidade do Planalto Catarinense.'>";
			        break;

			    case "lahonco":
			        echo "<title>LAHONCO - Ligas Universitárias do Santé Cancer Center</title>";
			        echo "<meta name='description' content='Saiba mais sobre a parceria do Santé Cancer Center com Universidade do Planalto Catarinense.'>";

			    	echo "<meta property='og:title' content='LAHONCO - Ligas Universitárias do Santé Cancer Center'>";
			    	echo "<meta property='og:description' content='Saiba mais sobre a parceria do Santé Cancer Center com Universidade do Planalto Catarinense.'>";
			        break;

			    case "cookies":
			        echo "<title>Conheça nossa política de privacidade</title>";
			        echo "<meta name='description' content='Saiba mais sobre a política de privacidade do Santé Cancer Center'>";

			    	echo "<meta property='og:title' content='Conheça nossa política de privacidade'>";
			    	echo "<meta property='og:description' content='Saiba mais sobre a política de privacidade do Santé Cancer Center'>";
			        break;

			    case stripos($_GET['pg'], 'blog') !== false:
			        echo "<title>Blog Santé Cancer Center</title>";
			        echo "<meta name='description' content='Artigos e novidades relativas ao tratamento oncológico, cuidados com o paciente e dicas de qualidade de vida.'>";

			    	echo "<meta property='og:title' content='Blog Santé Cancer Center'>";
			    	echo "<meta property='og:description' content='Artigos e novidades relativas ao tratamento oncológico, cuidados com o paciente e dicas de qualidade de vida.'>";
			        break;

			    case stripos($_GET['pg'], 'post') !== false:

			    	include_once 'models/blog.php';

					$myBlog = new Blog($db);
					$stmtBlog = $myBlog->read("AND url ='{$_GET['url']}'");

				    if ($stmtBlog->rowCount() > 0) {
					    $row = $stmtBlog->fetch(PDO::FETCH_ASSOC);
						extract($row);
						$myBlog->setId($id);
				        $myBlog->setTitle($title);

						$text = htmlspecialchars_decode($text);
						$text = strip_tags($text);
						$text = substr($text, 0, 160)."...";
				        $myBlog->setText($text);

				        echo "<title>Santé Cancer Center - {$myBlog->getTitle()}</title>";
				        echo "<meta name='description' content='{$myBlog->getText()}'>";

				    	echo "<meta property='og:title' content='Santé Cancer Center - {$myBlog->getTitle()}'>";
				    	echo "<meta property='og:description' content='{$myBlog->getText()}'>";
				        break;  


				    }	


			    case stripos($_GET['pg'], 'preview-post') !== false:
			        echo "<title>Blog Santé Cancer Center</title>";
			        echo "<meta name='description' content='Artigos e novidades relativas ao tratamento oncológico, cuidados com o paciente e dicas de qualidade de vida.'>";

			    	echo "<meta property='og:title' content='Blog Santé Cancer Center'>";
			    	echo "<meta property='og:description' content='Artigos e novidades relativas ao tratamento oncológico, cuidados com o paciente e dicas de qualidade de vida.'>";
			        break;  			        

			    default:
			        echo "<title>Página não encontrada</title>";
			    	echo "<meta property='og:title' content='Página não encontrada'>";
			        break;  

			}
		}
	?>

	<link rel="shortcut icon" href="/img/icon-sante.png">
	
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700|Roboto:300,400,700&display=swap" rel="stylesheet">
	<link href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" rel="stylesheet" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="/lib/OwlCarousel2-2.3.4/docs/assets/owlcarousel/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="/lib/OwlCarousel2-2.3.4/docs/assets/owlcarousel/assets/owl.theme.default.css">
	<link rel="stylesheet" href="/lib/bootstrap-4.1.3/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@9.5.4/dist/sweetalert2.min.css"/>

	<!-- Responsive Menu -->
	<link rel="stylesheet" href="/lib/menu-ueek/css/estilo.css?vs=13">
	<!-- Responsive Menu -->

	<link rel="stylesheet" href="/css/estilo.min.css?vs=17">
	<link rel="stylesheet" href="/css/paginas.min.css?vs=17">
	<link rel="stylesheet" href="/css/mqueries.min.css?vs=17">
</head>

<body>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-57CM8HJ"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<header id="top">

		<div class='container-fluid' id='bar-header'>
			<div class='container'>
				
				<ul>
					<a target="_blank" rel="noopener" href='<?php echo $myAbout->getFacebook() ?>'>
						<li><i class="soft-hover fab fa-facebook"></i></li>
					</a>
					<a target="_blank" rel="noopener" href='<?php echo $myAbout->getInstagram() ?>'>
						<li><i class="soft-hover fab fa-instagram"></i></li>
					</a>
					<a target="_blank" rel="noopener" href='<?php echo $myAbout->getLinkedin() ?>'>
						<li><i class="soft-hover fab fa-linkedin"></i></li>
					</a>
					<a target="_blank" rel="noopener" href='<?php echo $myAbout->getYoutube() ?>'>
						<li><i class="soft-hover fab fa-youtube"></i></li>
					</a>
				</ul>	

				<br>

			</div>
		</div>

		<div class="container-fluid" id="header-desktop">
			<div class="container">

				<div class="col-lg-4 col-md-4 col-sm-6 logo">
					<a href="/">
						<img alt="Santé" src="/img/logo.png?vs=17">
					</a>
				</div>

				<div class="col-lg-8 col-md-8 col-sm-6 infos">
					
					<ul>
						
						<li>
							<i class="fas fa-phone fa-rotate-90"></i>
							<span>Contate-nos</span>
							<a href="tel:<?php echo $myAbout->getPhone(); ?>">
								<p><?php echo $myAbout->getPhone() ?></p>
							</a>
							<br>
						</li>

						<li>
							<i class="far fa-clock"></i>
							<span>Horário Santé</span>
							<a>
								<p>Seg. a Sex.: 08h-12h | 13h30-19h</p>
							</a>
							<br>
						</li>

					</ul>

				</div>

				<br>

			</div>
		</div>

		<div class="container-fluid" id="menu-desktop">
			<div class="container">
				
				<nav id="menu">

					<ul>
						<li class="soft-hover"><a class="soft-hover <?php echo $home_page ?>" href="/">Home</a></li>

						<div class="drop-1">
							<li class="soft-hover">

								<a class="drop soft-hover <?php echo $institucional_page ?>">Institucional</a>
								<div class="drop-content-1">
									<ul>
										<li><a class='drop-link-1' href='/cancer-center'>O Cancer Center</a></li>
										<li><a class='drop-link-1' href='/corpo-multidisciplinar'>Equipe Multidisciplinar</a></li>
										<li><a class='drop-link-1' href='/perfil'>Perfil Santé</a></li>
										<li><a class='drop-link-1' href='/direitos-do-paciente-oncologico'>Direitos do Paciente</a></li>
									</ul>
								</div>

							</li>
						</div>

						<div class="drop-1">
							<li class="soft-hover">

								<a class="drop soft-hover <?php echo $convenios_page ?>">Convênios</a>
								<div class="drop-content-1">
									<ul>
										<li><a class='drop-link-1' href='/convenios'>Convênios</a></li>
										<li><a class='drop-link-1' href='/convenios-centro-de-mama'>Convênios<br>do Centro de Mama</a></li>
									</ul>
								</div>

							</li>
						</div>

						<li class="soft-hover"><a class="soft-hover <?php echo $corpo_clinico_page ?>" href="/corpo-clinico">Corpo Clínico</a></li>

						<div class="drop-1">
							<li class="soft-hover">

								<a class="drop soft-hover <?php echo $especialidades_page ?>">Especialidades</a>
								<div class="drop-content-1">
									<ul>
										<li><a class='drop-link-1' href='/oncologia'>Oncologia</a></li>
										<li><a class='drop-link-1' href='/oncohematologia'>Oncohematologia</a></li>
										<li><a class='drop-link-1' href='/hematologia'>Hematologia</a></li>
										<li><a class='drop-link-1' href='/mastologia'>Mastologia<span>Centro de Mama</span></a></li>
										<li><a class='drop-link-1' href='/radio-oncologia'>Radio-oncologia</a></li>
										<li><a class='drop-link-1' href='/oncogenetica'>Oncogenética</a></li>

									</ul>
								</div>

							</li>
						</div>

						<li class="soft-hover"><a class="soft-hover <?php echo $tratamentos_page ?>" href="/tratamentos">Tratamentos</a></li>

						<!-- <div class="drop-1">
							<li class="soft-hover">

								<a class="drop soft-hover <?php echo $tratamentos_page ?>" href="/tratamentos">Tratamentos</a>
								<div class="drop-content-1">
									<ul>

										<li><a class='drop-link-1' href='/quimioterapia'>Quimioterapia</a></li>
										<li><a class='drop-link-1' href='/radioterapia'>Radioterapia e Radiocirurgia</a></li>
										<li><a class='drop-link-1' href='/truebeam'>TrueBeam<br>Acelerador Linear</a></li>
										<li><a class='drop-link-1' href='/imunoterapia'>Imunoterapia</a></li>
										<li><a class='drop-link-1' href='/terapia-alvo'>Terapia Alvo</a></li>
										<li><a class='drop-link-1' href='/infusao-nao-oncologica'>Infusão não oncológica</a></li>
										<li><a class='drop-link-1' href='/mielograma'>Mielograma</a></li>
										<li><a class='drop-link-1' href='/manutencao-de-cateter'>Manutenção de Cateter</a></li>

										<div class="drop-2">
											<li class="soft-hover">
											
												<svg class="left-arrow" xmlns="http://www.w3.org/2000/svg" width="6" height="7" viewBox="0 0 6 7" fill="none">
													<path d="M0 3.5L5.25 0.468911L5.25 6.53109L0 3.5Z" fill="#1D2C0E"/>
												</svg>

												<svg class="right-arrow" xmlns="http://www.w3.org/2000/svg" width="6" height="7" viewBox="0 0 6 7" fill="none">
													<path d="M6 3.5L0.75 6.53109L0.75 0.468911L6 3.5Z" fill="#1D2C0E"/>
												</svg>

												<a class="drop-link-1 soft-hover">Acompanhamento<br>Multidisciplinar</a>
												<div class="drop-content-2">
													<ul>
														<li><a class='soft-hover drop-link-2' href='/farmacia'>Farmácia</a></li>
														<li><a class='soft-hover drop-link-2' href='/enfermagem'>Enfermagem</a></li>
														<li><a class='soft-hover drop-link-2' href='/psicologia'>Psicologia</a></li>
														<li><a class='soft-hover drop-link-2' href='/terapias-integrativas'>Terapias Integrativas</a></li>
														<li><a class='soft-hover drop-link-2' href='/nutricao'>Nutrição</a></li>
													</ul>
												</div>	
											</li>
										</div>

									</ul>
								</div>

							</li>
						</div> -->

						<div class="drop-1">
							<li class="soft-hover">

								<a class="drop soft-hover <?php echo $diagnostico_page ?>">Diagnóstico</a>
								<div class="drop-content-1">
									<ul>
										<!-- <li><a class='drop-link-1' href='/diagnostico-por-imagem-e-pet-ct'>Diagnóstico por<br>imagem e PET-CT</a></li> -->
										<li><a class='drop-link-1' href='/mamografia-digital'>Mamografia Digital</a></li>
										<li><a class='drop-link-1' href='/ultrassonografia'>Ultrassonografia</a></li>
										<li><a class='drop-link-1' href='/biopsias-guiadas-por-ultrassom'>Biópsias guiadas<br>por ultrassom</a></li>
										<li><a class='drop-link-1' href='/estereotaxia'>Estereotaxia</a></li>
									</ul>
								</div>

							</li>
						</div>

						<div class="drop-1">
							<li class="soft-hover">

								<a class="drop soft-hover <?php echo $ensino_pesquisa_page ?>">Ensino & Pesquisa</a>
								<div class="drop-content-1">
									<ul>

										<!-- <div class="drop-2">
											<li class="soft-hover">
											
												<svg class="left-arrow" xmlns="http://www.w3.org/2000/svg" width="6" height="7" viewBox="0 0 6 7" fill="none">
													<path d="M0 3.5L5.25 0.468911L5.25 6.53109L0 3.5Z" fill="#1D2C0E"/>
												</svg>

												<svg class="right-arrow" xmlns="http://www.w3.org/2000/svg" width="6" height="7" viewBox="0 0 6 7" fill="none">
													<path d="M6 3.5L0.75 6.53109L0.75 0.468911L6 3.5Z" fill="#1D2C0E"/>
												</svg>

												<a class='drop-link-1' href='/residencia-medica'>Residência médica<br>em Cancerologia Clínica</a>
												<div class="drop-content-2">
													<ul>
														<li><a class='soft-hover drop-link-2' href='/residencia-medica'>O Programa</a></li>
														<li><a class='soft-hover drop-link-2' href='/residencia-medica'>Informações</a></li>
														<li><a class='soft-hover drop-link-2' href='/residencia-medica'>Formas de Ingresso</a></li>
														<li><a class='soft-hover drop-link-2' href='/residencia-medica'>Médicos Residentes</a></li>
														<li><a class='soft-hover drop-link-2' href='/residencia-medica'>Corpo Acadêmico</a></li>
														<li><a class='soft-hover drop-link-2' href='/residencia-medica'>Editais</a></li>
													</ul>
												</div>	
											
											</li>
										</div> -->

										<li><a class='drop-link-1' href='/residencia-medica'>Residência médica<br>em Cancerologia Clínica</a></li>

										<div class="drop-2">
											<li class="soft-hover">
											
												<svg class="left-arrow" xmlns="http://www.w3.org/2000/svg" width="6" height="7" viewBox="0 0 6 7" fill="none">
													<path d="M0 3.5L5.25 0.468911L5.25 6.53109L0 3.5Z" fill="#1D2C0E"/>
												</svg>

												<svg class="right-arrow" xmlns="http://www.w3.org/2000/svg" width="6" height="7" viewBox="0 0 6 7" fill="none">
													<path d="M6 3.5L0.75 6.53109L0.75 0.468911L6 3.5Z" fill="#1D2C0E"/>
												</svg>

												<a class='drop-link-1'>Ligas Acadêmicas</a>
												<div class="drop-content-2">
													<ul>
														<li><a class='soft-hover drop-link-2' href='/lagom'>LAGOM<span>Liga Acadêmica de Ginecologia,<br>Obstetrícia e Mastologia</span></a></li>
														<li><a class='soft-hover drop-link-2' href='/lahonco'>LAHONCO<span>Liga Acadêmica de Hematologia<br>e Oncologia</span></a></li>
													</ul>
												</div>	
											
											</li>
										</div>

										<li><a class='drop-link-1' href='/estagios-e-visitas'>Estágios e Visitas</a></li>

									</ul>
								</div>

							</li>
						</div>

						<li class="soft-hover"><a class="soft-hover <?php echo $blog_page ?>" href="/blog">Blog</a></li>
						<li class="soft-hover"><a class="soft-hover" href="/?area-de-contato=fale-conosco">Contato</a></li>
					
					</ul>

				</nav>

			</div>
		</div>
		
		<nav role='navigation' id="menu-responsive">
			<div class="container">

				<div class="col-3 col-logo">
					<a href="/"><img src="/img/logo.png?vs=17" id="logo" alt="Santé"></a>
				</div>

				<div id="menuToggle">

					<input id="checkbox" type="checkbox" />

					<span class="span-menu"></span>
					<span class="span-menu"></span>
					<span class="span-menu"></span>

					<ul id="menu-cel">

					  	<a class="regular-link" href="/">
					  		<li class="<?php echo $home_page ?>">Home</li>
					  	</a>
						
						<div role="tab" id="submenu-area-1">
				            <a data-toggle="collapse" href="#submenu-1" aria-expanded="false" aria-controls="submenu-1">
				                <li class="<?php echo $institucional_page ?>"><i class="fas fa-caret-right"></i>Institucional</li>
				            </a>
				        </div>

				        <div id="submenu-1" class="collapse" role="tabpanel" aria-labelledby="submenu-area-1">
				            <div class="first-dropdown">
				                <ul>
				                    <a class="regular-link soft-hover" href="/cancer-center"><li>O Cancer Center</li></a>
				                    <a class="regular-link soft-hover" href="/corpo-multidisciplinar"><li>Equipe Multidisciplinar</li></a>
				                    <a class="regular-link soft-hover" href="/perfil"><li>Perfil Santé</li></a>
				                    <a class="regular-link soft-hover" href="/direitos-do-paciente-oncologico"><li>Direitos do Paciente</li></a>
				                </ul>
				            </div>
				        </div>

				        <div role="tab" id="submenu-area-2">
				            <a data-toggle="collapse" href="#submenu-2" aria-expanded="false" aria-controls="submenu-2">
				                <li class="<?php echo $convenios_page ?>"><i class="fas fa-caret-right"></i>Convênios</li>
				            </a>
				        </div>

				        <div id="submenu-2" class="collapse" role="tabpanel" aria-labelledby="submenu-area-2">
				            <div class="first-dropdown">
				                <ul>
				                    <a class="regular-link soft-hover" href="/convenios"><li>Convênios Médicos</li></a>
				                    <a class="regular-link soft-hover" href="/convenios-centro-de-mama"><li>Convênios<br>do Centro de Mama</li></a>
				                </ul>
				            </div>
				        </div>

						<a class="regular-link" href="/corpo-clinico">
							<li class="<?php echo $corpo_clinico_page ?>">Corpo Clínico</li>
						</a>

						<div role="tab" id="submenu-area-3">
				            <a data-toggle="collapse" href="#submenu-3" aria-expanded="false" aria-controls="submenu-3">
				                <li class="<?php echo $especialidades_page ?>"><i class="fas fa-caret-right"></i>Especialidades</li>
				            </a>
				        </div>

				        <div id="submenu-3" class="collapse" role="tabpanel" aria-labelledby="submenu-area-3">
				            <div class="first-dropdown">
				                <ul>
				                    <a class="regular-link soft-hover" href="/oncologia"><li>Oncologia</li></a>
				                    <a class="regular-link soft-hover" href="/oncohematologia"><li>Oncohematologia</li></a>
				                    <a class="regular-link soft-hover" href="/hematologia"><li>Hematologia</li></a>
				                    <a class="regular-link soft-hover" href="/mastologia"><li>Mastologia</li></a>
				                    <a class="regular-link soft-hover" href="/radio-oncologia"><li>Radio-oncologia</li></a>
				                    <a class="regular-link soft-hover" href="/oncogenetica"><li>Oncogenética</li></a>

				                </ul>
				            </div>
				        </div>

				        <a class="regular-link" href="/tratamentos">
							<li class="<?php echo $tratamentos_page ?>">Tratamentos</li>
						</a>

				        <!-- <div role="tab" id="submenu-area-3">
				            <a data-toggle="collapse" href="#submenu-3" aria-expanded="false" aria-controls="submenu-3">
				                <li class="<?php echo $tratamentos_page ?>"><i class="fas fa-caret-right"></i>Tratamentos</li>
				            </a>
				        </div>

				        <div id="submenu-3" class="collapse" role="tabpanel" aria-labelledby="submenu-area-3">
				            <div class="first-dropdown">
				                <ul>

				                    <a class='regular-link soft-hover' href='/quimioterapia'><li>Quimioterapia</li></a>
									<a class='regular-link soft-hover' href='/radioterapia'><li>Radioterapia e Radiocirurgia</li></a>
									<a class='regular-link soft-hover' href='/truebeam'><li>TrueBeam<br>Acelerador Linear</li></a>
									<a class='regular-link soft-hover' href='/imunoterapia'><li>Imunoterapia</li></a>
									<a class='regular-link soft-hover' href='/terapia-alvo'><li>Terapia Alvo</li></a>
									<a class='regular-link soft-hover' href='/infusao-nao-oncologica'><li>Infusão não oncológica</li></a>
									<a class='regular-link soft-hover' href='/mielograma'><li>Mielograma</li></a>
									<a class='regular-link soft-hover' href='/manutencao-de-cateter'><li>Manutenção de Cateter</li></a>
				                    	
				                    <div role="tab" id="submenu-area-4">
							            <a data-toggle="collapse" href="#submenu-4" aria-expanded="false" aria-controls="submenu-4">
							                <li><i class="fas fa-caret-right"></i>Acompanhamento Multidisciplinar</li>
							            </a>
							        </div>

							        <div id="submenu-4" class="collapse" role="tabpanel" aria-labelledby="submenu-area-4">
							            <div class="second-dropdown">
							                <ul>
							                    <a class="regular-link soft-hover" href="/farmacia"><li>Farmácia</li></a>
							                    <a class="regular-link soft-hover" href="/enfermagem"><li>Enfermagem</li></a>
							                    <a class="regular-link soft-hover" href="/psicologia"><li>Psicologia</li></a>
							                    <a class="regular-link soft-hover" href="/terapias-integrativas"><li>Terapias Integrativas</li></a>
							                    <a class="regular-link soft-hover" href="/nutricao"><li>Nutrição</li></a>
							                </ul>
							            </div>
							        </div>

				                </ul>
				            </div>
				        </div> -->

				        <div role="tab" id="submenu-area-5">
				            <a data-toggle="collapse" href="#submenu-5" aria-expanded="false" aria-controls="submenu-5">
				                <li class="<?php echo $diagnostico_page ?>"><i class="fas fa-caret-right"></i>Diagnóstico</li>
				            </a>
				        </div>

				        <div id="submenu-5" class="collapse" role="tabpanel" aria-labelledby="submenu-area-5">
				            <div class="first-dropdown">
				                <ul>
				                    <!-- <a class="regular-link soft-hover" href="/diagnostico-por-imagem-e-pet-ct"><li>Diagnóstico por imagem e PET-CT</li></a> -->
				                    <a class="regular-link soft-hover" href="/mamografia-digital"><li>Mamografia Digital</li></a>
				                    <a class="regular-link soft-hover" href="/ultrassonografia"><li>Ultrassonografia</li></a>
				                    <a class="regular-link soft-hover" href="/biopsias-guiadas-por-ultrassom"><li>Biópsias guiadas por ultrassom</li></a>
				                    <a class="regular-link soft-hover" href="/estereotaxia"><li>Estereotaxia</li></a>
				                </ul>
				            </div>
				        </div>

				        <div role="tab" id="submenu-area-6">
				            <a data-toggle="collapse" href="#submenu-6" aria-expanded="false" aria-controls="submenu-6">
				                <li class="<?php echo $ensino_pesquisa_page ?>"><i class="fas fa-caret-right"></i>Ensino & Pesquisa</li>
				            </a>
				        </div>
				        
				        <div id="submenu-6" class="collapse" role="tabpanel" aria-labelledby="submenu-area-6">
				            <div class="first-dropdown">
				                <ul>

				                	<div role="tab" id="submenu-area-7">
							            <a data-toggle="collapse" href="#submenu-7" aria-expanded="false" aria-controls="submenu-7">
							                <li><i class="fas fa-caret-right"></i>Residência médica em Cancerologia Clínica</li>
							            </a>
							        </div>

							        <div id="submenu-7" class="collapse" role="tabpanel" aria-labelledby="submenu-area-7">
							            <div class="second-dropdown">
							                <ul>
							                    <a class="regular-link soft-hover" href="/residencia-medica"><li>O Programa</li></a>
							                    <a class="regular-link soft-hover" href="/residencia-medica"><li>Informações</li></a>
							                    <a class="regular-link soft-hover" href="/residencia-medica"><li>Formas de Ingresso</li></a>
							                    <a class="regular-link soft-hover" href="/residencia-medica"><li>Médicos Residentes</li></a>
							                    <a class="regular-link soft-hover" href="/residencia-medica"><li>Corpo Acadêmico</li></a>
							                    <a class="regular-link soft-hover" href="/residencia-medica"><li>Editais</li></a>
							                </ul>
							            </div>
							        </div>

							        <div role="tab" id="submenu-area-8">
							            <a data-toggle="collapse" href="#submenu-8" aria-expanded="false" aria-controls="submenu-8">
							                <li><i class="fas fa-caret-right"></i>Ligas Acadêmicas</li>
							            </a>
							        </div>

							        <div id="submenu-8" class="collapse" role="tabpanel" aria-labelledby="submenu-area-8">
							            <div class="second-dropdown">
							                <ul>
							                    <a class="regular-link soft-hover" href="/lagom"><li>LAGOM</li></a>
							                    <a class="regular-link soft-hover" href="/lahonco"><li>LAHONCO</li></a>
							                </ul>
							            </div>
							        </div>

				                    <a class="regular-link soft-hover" href="/estagios-e-visitas"><li>Estágios e Visitas</li></a>

				                </ul>
				            </div>
				        </div>

						<a class="regular-link" href="/blog">
							<li class="<?php echo $blog_page ?>">Blog</li>
						</a>

						<a class="regular-link" href="/?area-de-contato=fale-conosco">
							<li>Contato</li>
						</a>

						<div id="infos-menu">
							
							<div class="contact">
								
								<i class="fas fa-phone fa-rotate-90"></i>
								
								<div class="text">

									<p>Contate-nos</p>

									<a href="tel:<?php echo $myAbout->getPhone(); ?>">
										<?php echo $myAbout->getPhone() ?>
									</a>

								</div>

								<br>

							</div>

							<div class="contact">

								<i class="far fa-clock"></i>
								
								<div class="text">

									<p>Contate-nos</p>

									<a>
										Seg. a Sex.: 08h-12h | 13h30-19h
									</a>

								</div>

								<br>

							</div>

						</div>

					</ul>

				</div>

				<br>

			</div>
		</nav>

	</header>

	<main>
		<?php
		    if(!isset($_GET['pg']) or empty($_GET['pg'])){
		        include("home.php");
		    }else{
		        if($_SERVER['REQUEST_METHOD'] == 'GET'){
		            $link = "{$_GET['pg']}.php";
		            if(file_exists($link)){
		                include($link);
		            }else{
		                include("erro.php");
		            }
		        }else{
		            include("erro.php");
		        }
		    }
		?>
	</main>

	<section id="cookies-warning">

		<div class="container">

			<div class="content">
				<div class="texts">
					<h6 class="section-title">Cookies</h6>
					<p class="text">Nosso site faz uso de cookies para tornar sua experiência melhor.</p>
				</div>
			</div>

			<div class="actions">
				<button class="bt-ok-cookie">Aceitar</button>
				<a class="see-more-details" href="/cookies">Detalhes</a>
			</div>

		</div>

	</section>

	<?php if(isset($_GET['pg'])) { 

		if ($_GET['pg'] == 'corpo-clinico' || $_GET['pg'] == 'perfil' || 
			$_GET['pg'] == 'cancer-center' || $_GET['pg'] == 'rede-einstein' || 
			$_GET['pg'] == 'corpo-multidisciplinar' || $_GET['pg'] == 'convenios' || 
			$_GET['pg'] == 'convenios-centro-de-mama'
		) { ?>

		<section id="servicos" 
			<?php if ($_GET['pg'] == 'cancer-center'
				   ) { echo 'style="background-color: #fff"'; } ?>
				   >

			<div class="container-fluid" id="slide">

				<div class="container">
					
					<div class="col-lg-4 col-md-12 col-sm-12 p-0">
						
						<div class="header">
							<img src="/img/header.png">
							<br>
						</div>

						<h1 class="slide-left">O que<br>fazemos?</h1>
						<p class="slide-left">Aqui o paciente oncológico é tratado de maneira única e o foco de toda equipe é o cuidado direcionado e a atenção dedicada.</p>

					</div>	

				</div>

				<div id="slide-cards" class='owl-carousel owl-theme'>

					<?php 

						if ($listOfServices->rowCount() > 0) {

			                while ($row = $listOfServices->fetch(PDO::FETCH_ASSOC)){
			                    extract($row);

			                    $image = $urlImages_Services.$image;

			                    $services = explode(';', $sub_categories);

			                    $services_list = "";
			                    $link = "";

			                    if (count($services) > 5) {

			                    	for ($i=0; $i < 3; $i++) { 
			                    		$services_list .= '<li>' . trim($services[$i]) . '</li>';
			                    	}

			                    	$link = "<a class='soft-hover' href='/tratamentos'>Ver mais
			                    				<svg xmlns='http://www.w3.org/2000/svg' width='16' height='10' viewBox='0 0 16 10' fill='none'>
													<path d='M15.4243 5.42426C15.6586 5.18995 15.6586 4.81005 15.4243 4.57574L11.6059 0.757359C11.3716 0.523045 10.9917 0.523045 10.7574 0.757359C10.523 0.991674 10.523 1.37157 10.7574 1.60589L14.1515 5L10.7574 8.39411C10.523 8.62843 10.523 9.00833 10.7574 9.24264C10.9917 9.47696 11.3716 9.47696 11.6059 9.24264L15.4243 5.42426ZM0 5.6H15V4.4H0V5.6Z' fill='#B6C72C'/>
												</svg>
			                    			 </a>";

			                    } else {

			                    	foreach ($services as $key => $service) {
				                    	$services_list .= '<li>' . trim($service) . '</li>';
				                    }

			                    }

			                    echo "

			                    <div class='item-banner'>
									<div class='card'>
										
										<div class='card-img' style='background-image: url({$image});'></div>
										
										<div class='card-body'>

											<h5 class='card-title btn'>{$name}</h5>
											
											<div class='card-text'>
												
												<ul>
													{$services_list}
												</ul>
												
												{$link}

									  		</div>

										</div>

									</div>
								</div>

			                    ";

			                }

			            }

					?>

				</div>

				<br>

			</div>
		</section>

	<?php 
	
		} 

	} ?>

	<?php 

		if (isset($_GET['pg']) && $_GET['pg'] == 'corpo-clinico' || 
				  $_GET['pg'] == 'perfil' || 
				  $_GET['pg'] == 'rede-einstein' || 
				  $_GET['pg'] == 'corpo-multidisciplinar' || 
				  $_GET['pg'] == 'convenios' ||
				  $_GET['pg'] == 'convenios-centro-de-mama' ||
				  $_GET['pg'] == 'oncologia' ||
				  $_GET['pg'] == 'oncohematologia' ||
				  $_GET['pg'] == 'hematologia' ||
				  $_GET['pg'] == 'mastologia' ||
				  $_GET['pg'] == 'radioterapia' ||
				  $_GET['pg'] == 'quimioterapia' || 
				  $_GET['pg'] == 'imunoterapia' ||
				  $_GET['pg'] == 'farmacia' ||
				  $_GET['pg'] == 'enfermagem' ||
				  $_GET['pg'] == 'mamografia-digital' # ||
				  // $_GET['pg'] == 'ultrassonografia' ||
				  // $_GET['pg'] == 'biopsias-guiadas-por-ultrassom' ||
				  // $_GET['pg'] == 'estereotaxia' 
			) {
			
			$changeStyle = 'style="background-color: #fff"';

		} else {

			$changeStyle = '';

		}

	?>

	<?php if ($_GET['pg'] != 'erro') { ?>

	<section id="contato" class="container-fluid" <?php echo $changeStyle ?>>
		<div class="container">

			<div id="area-de-contato"></div>
			
			<div id="side-left" class="col-lg-4 col-md-12">

				<h2>Contate-nos</h2>

	            <ul class="nav nav-tabs" role="tablist">

	                <li class="nav-item">
	                    <a class="nav-link active" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="true">
		                    <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 16 16" fill="none">
								<path d="M15 10.3333C15 10.7459 14.8361 11.1416 14.5444 11.4333C14.2527 11.725 13.857 11.8889 13.4444 11.8889H4.11111L1 15V2.55556C1 2.143 1.16389 1.74733 1.45561 1.45561C1.74733 1.16389 2.143 1 2.55556 1H13.4444C13.857 1 14.2527 1.16389 14.5444 1.45561C14.8361 1.74733 15 2.143 15 2.55556V10.3333Z" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
							</svg>Fale Conosco
		                </a>
	                </li>

	                <li class="nav-item">
	                    <a class="nav-link" id="schedule-tab" data-toggle="tab" href="#schedule" role="tab" aria-controls="schedule" aria-selected="true">
	                    	<svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 14 16" fill="none">
								<path d="M9.49117 2.27661H10.9194C11.2982 2.27661 11.6614 2.42708 11.9293 2.69493C12.1971 2.96277 12.3476 3.32604 12.3476 3.70483V13.7023C12.3476 14.0811 12.1971 14.4444 11.9293 14.7122C11.6614 14.9801 11.2982 15.1306 10.9194 15.1306H2.35009C1.9713 15.1306 1.60803 14.9801 1.34019 14.7122C1.07235 14.4444 0.921875 14.0811 0.921875 13.7023V3.70483C0.921875 3.32604 1.07235 2.96277 1.34019 2.69493C1.60803 2.42708 1.9713 2.27661 2.35009 2.27661H3.77831" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
								<path d="M8.7761 0.848389H4.49145C4.09706 0.848389 3.77734 1.16811 3.77734 1.5625V2.99071C3.77734 3.3851 4.09706 3.70482 4.49145 3.70482H8.7761C9.17049 3.70482 9.49021 3.3851 9.49021 2.99071V1.5625C9.49021 1.16811 9.17049 0.848389 8.7761 0.848389Z" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
							</svg>Agende sua consulta
	                    </a>
	                </li>

	                <li class="nav-item">
	                    <a class="nav-link" id="jobs-tab" data-toggle="tab" href="#jobs" role="tab" aria-controls="jobs" aria-selected="false">
	                    	<svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17" fill="none">
								<path d="M7.53321 2.93347H2.45182C2.06678 2.93347 1.6975 3.08643 1.42523 3.3587C1.15296 3.63097 1 4.00025 1 4.3853V14.5481C1 14.9331 1.15296 15.3024 1.42523 15.5747C1.6975 15.8469 2.06678 15.9999 2.45182 15.9999H12.6146C12.9996 15.9999 13.3689 15.8469 13.6412 15.5747C13.9135 15.3024 14.0664 14.9331 14.0664 14.5481V9.46668" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
								<path d="M12.9815 1.84458C13.2702 1.55579 13.6619 1.39355 14.0703 1.39355C14.4787 1.39355 14.8704 1.55579 15.1592 1.84458C15.448 2.13336 15.6102 2.52504 15.6102 2.93345C15.6102 3.34185 15.448 3.73353 15.1592 4.02232L8.26302 10.9185L5.35938 11.6444L6.08529 8.74074L12.9815 1.84458Z" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
							</svg>Trabalhe conosco
	                    </a>
	                </li>

	                <br>

	            </ul>
	        </div>

	        <div id="side-right" class="col-lg-8 col-md-12">
	            <div class="tab-content">

	                <div class="tab-pane fade show active" id="contact" role="tabpanel" aria-labelledby="contact-tab">

	                    <form id="send-contact">

							<div class="col-lg-6 col-md-6 col-sm-12 inputs">
								<label>Nome</label>
								<input class="required" type="text" name="name">
							</div>

							<div class="col-lg-6 col-md-6 col-sm-12 inputs">
								<label>Email</label>
								<input class="required" type="email" name="email">
							</div>

							<div class="col-lg-6 col-md-6 col-sm-12 inputs">
								<label>Telefone</label>
								<input class="required" type="text" name="phone">
							</div>

							<div class="col-lg-6 col-md-6 col-sm-12 inputs">
								<label>Assunto</label>
								<input class="required" type="text" name="subject">
							</div>

							<div class="col-lg-12 col-md-12 col-sm-12 inputs">
								<label>Mensagem</label>
								<input class="required" type="text" name="message">
							</div>

							<div class="col-lg-12 col-md-12 col-sm-12 inputs">
								<input class="soft-hover btn btn-contact" type="submit" name="send" value="Enviar">
							</div>

							<br>

						</form>

	                </div>

	                <div class="tab-pane fade" id="schedule" role="tabpanel" aria-labelledby="schedule-tab">
	                    
	                    <form id="schedule">

                            <p>Preencha o formulário com seus dados e selecione o melhor dia da semana e horário para ser atendido. Entraremos em contato com você para agendar uma data para o seu atendimento. Este formulário é apenas um pré-agendamento, aguarde nosso contato para confirmação.</p>
	                       

	                        <div class="col-lg-12 col-md-12 col-sm-12 inputs">
								<label>Nome</label>
								<input class="required" type="text" name="name">
							</div>

							<div class="col-lg-6 col-md-6 col-sm-12 inputs">
								<label>Email</label>
								<input class="required" type="email" name="email">
							</div>

							<div class="col-lg-6 col-md-6 col-sm-12 inputs">
								<label>Telefone</label>
								<input class="required" type="text" name="phone">
							</div>

							<div class="col-lg-6 col-md-6 col-sm-12 inputs">
								<label>Dia de preferência</label>
								<select name="day" class="required">
									<option value="Segunda-feira">Segunda-feira</option>
									<option value="Terça-feira">Terça-feira</option>
									<option value="Quarta-feira">Quarta-feira</option>
									<option value="Quinta-feira">Quinta-feira</option>
									<option value="Sexta-feira">Sexta-feira</option>
									<option value="Sábado">Sábado</option>
								</select>
							</div>

							<div class="col-lg-6 col-md-6 col-sm-12 inputs">
								<label>Horário de preferência</label>
								<select name="time" class="required">
									<option value="Manhã">Manhã</option>
									<option value="Tarde">Tarde</option>
								</select>
							</div>

							<div class="col-lg-12 col-md-12 col-sm-12 inputs">
								<label>Especialidade</label>
								<select name="specialty" class="required">
									<option value="Oncologia">Oncologia</option>
									<option value="Oncohematologia">Oncohematologia</option>
									<option value="Hematologia">Hematologia</option>
									<option value="Mastologia">Mastologia</option>
									<option value="Rádio-Oncologia">Rádio-Oncologia</option>
									<option value="Oncogenética">Oncogenética</option>
								</select>
							</div>

							<div class="col-lg-12 col-md-12 col-sm-12 inputs">
								<label>Mensagem</label>
								<textarea name="message" placeholder="Digite aqui sua mensagem"></textarea>
							</div>

							<div class="col-lg-12 col-md-12 col-sm-12 inputs">
								<input class="soft-hover btn btn-contact" type="submit" name="send" value="Enviar">
							</div>

							<br>

	                    </form>

	                </div>

	                <div class="tab-pane fade" id="jobs" role="tabpanel" aria-labelledby="jobs-tab">
	                    
	                    <form id="jobs">

                            <p>Em 12 anos de história a equipe Santé vem transformando e restaurando vidas. Contamos com uma equipe especializada em cuidado humanizado. Se você se identifica com o nosso propósito, venha construir o seu futuro conosco.</p>

	                        <div class="col-lg-12 col-md-12 col-sm-12 inputs">
								<label>Nome</label>
								<input class="required" type="text" name="name">
							</div>

							<div class="col-lg-6 col-md-6 col-sm-12 inputs">
								<label>Telefone</label>
								<input class="required" type="text" name="phone">
							</div>
							
							<div class="col-lg-6 col-md-6 col-sm-12 inputs">
								<label>Email</label>
								<input class="required" type="email" name="email">
							</div>

							<div class="file-drop-area">
								<div class="text-files">
									<h5>Curriculum Vitae</h5>
									<p class="file-msg">Você pode arrastar o seu curriculum para dentro do espaço delimitado<br>ou procurar nos seus arquivos</p>
								</div>
								
								<span class="fake-btn">Procurar...</span>
								<input class="file-input required" type="file" id="file" name="file">
							</div>
							
							<div class="col-lg-12 col-md-12 col-sm-12 inputs">
								<label>Mensagem</label>
								<textarea name="message" placeholder="Digite aqui sua mensagem"></textarea>
							</div>

							<div class="col-lg-12 col-md-12 col-sm-12 inputs">
								<input class="soft-hover btn btn-contact" type="submit" name="send" value="Enviar">
							</div>

							<br>

	                    </form>

	                </div>

	            </div>
	        </div>

	        <br>

		</div>
	</section>	
		
	<?php } ?>

	

	<?php if(!isset($_GET['pg']) or empty($_GET['pg'])) { ?>

	<section id="mapa" class="container-fluid">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3528.839226833438!2d-50.33553968439177!3d-27.814719238124333!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94e01f3663068e9f%3A0xafe175662240f6db!2sLe%20Sant%C3%A9%20-%20Centro%20Avan%C3%A7ado%20em%20Oncologia!5e0!3m2!1spt-BR!2sbr!4v1578009665300!5m2!1spt-BR!2sbr" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
	</section>

	<?php } ?>

	<footer id="bottom">

		<div class="container-fluid" id="info-contact">
			<div class="container">

				<div class="col-lg-4 col-md-12 col-sm-12 title">
					<h1>Santé - Cancer Center</h1>
				</div>

				<div class="col-lg-8 col-md-12 col-sm-12 infos">
					<ul>

						<li>
							<i class="fas fa-map-marker-alt"></i>
							<a href="/#mapa">Praça Joca Neves, 280. Bairro Centro. CEP 88501-045. | GPS: -27.8146280, -50.3335867</a>
						</li>

						<li>
							<i class="fas fa-phone fa-rotate-90"></i>
							<a href="tel:<?php echo $myAbout->getPhone(); ?>"><?php echo $myAbout->getPhone() ?></a>
						</li>

						<li class="responsive">
							<i class="far fa-clock"></i>
							<a>Seg. a Sex.: 08h-12h | 13h30-19h</a>
						</li>

					</ul>
				</div>

				<br>
			</div>
		</div>

		<div class="container-fluid" id="links">
			<div class="container">
				
				<div class="col-lg-8 col-md-12 col-sm-12 nav-menu">
					<div class="content-nav-menu">
						
						<h3>Site</h3>

						<nav>
							<ul class="site">
								<li><a class="soft-hover" href="/">Home</a></li>
								<li><a class="soft-hover" href="/cancer-center">O Cancer Center</a></li>
								<li><a class="soft-hover" href="/convenios">Convênios</a></li>
								<li><a class="soft-hover" href="/corpo-clinico">Corpo Clínico</a></li>
								<li><a class="soft-hover" href="/estagios-e-visitas">Estágios e Visitas</a></li>
								<li><a class="soft-hover" href="/blog">Blog</a></li>
							</ul>
						</nav>

					</div>
				</div>

				<div class="col-lg-4 col-md-12 col-sm-12 nav-menu">
					<div class="content-nav-menu">
						
						<h3>Ajuda</h3>

						<nav>
							<ul class="ajuda">
								<li><a class="soft-hover" href="/?area-de-contato=agende-sua-consulta">Agende sua consulta</a></li>
								<li><a class="soft-hover" href="/?area-de-contato=trabalhe-conosco">Trabalhe conosco</a></li>
							</ul>
						</nav>

					</div>
				</div>

				<br>	

			</div>
		</div>

		<div class="container-fluid" id="copyright">
			<div class="container">

				<div class="col-lg-4 col-md-6 col-sm-9 col-11 float-left pl-0">
					<p>Santé - Todos os direitos reservados.</p>
				</div>

				<div class="col-lg-6 col-md-2 col-sm-2 float-left pr-0 social-media">
					
					<ul>
						<a target="_blank" rel="noopener" href='<?php echo $myAbout->getFacebook() ?>'>
							<li><i class="soft-hover fab fa-facebook"></i></li>
						</a>
						<a target="_blank" rel="noopener" href='<?php echo $myAbout->getInstagram() ?>'>
							<li><i class="soft-hover fab fa-instagram"></i></li>
						</a>
						<a target="_blank" rel="noopener" href='<?php echo $myAbout->getLinkedin() ?>'>
							<li><i class="soft-hover fab fa-linkedin"></i></li>
						</a>
						<a target="_blank" rel="noopener" href='<?php echo $myAbout->getYoutube() ?>'>
							<li><i class="soft-hover fab fa-youtube"></i></li>
						</a>
					</ul>	

				</div>

				<div class="col-lg-2 col-md-6 col-sm-3 col-1 float-left pr-0">
					<a href="https://ueek.com.br/" target="blank">
						<img src="/img/logo-ueek.png" alt="Ueek">
					</a>
				</div>

				<br>
			</div>
		</div>

	</footer>

	<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>
	<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.5.4/dist/sweetalert2.all.min.js"></script>

	<script src="/lib/OwlCarousel2-2.3.4/docs/assets/owlcarousel/owl.carousel.min.js"></script>
	<script src="/lib/bootstrap-4.1.3/js/bootstrap.min.js"></script>
	<script src="/lib/menu-ueek/js/menu.js?vs=17"></script>

	<script type="text/javascript" src="/js/main.js?vs=17"></script>
	<script type="text/javascript" src="/js/mascaras.js?vs=17"></script>
	<script type="text/javascript" src="/js/contato.js?vs=17"></script>
	<script type="text/javascript" src="/js/site-validator.js?vs=17"></script>

</body>
</html>