<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Enfermagem</h1>
		</div>

		<div class="long-text">
			<p>
				O enfermeiro oncológico é de extrema importância para o tratamento oncológico uma vez que presta cuidados ao paciente, realizando avaliações diagnósticas e trabalhando no tratamento e reabilitação do paciente. <br>
				É o responsável por prestar cuidados ao paciente e familiares durante o momento em que se encontram na clínica, oferecendo supervisão e cuidado ao longo de todo período. 
			</p>
		</div>

	</div>
</section>

<section id="galery">
	<div class="container p-0">
		
		<div id="images">
			
<!-- 			<a href='/img/institucionais/enfermagem1.JPG' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/institucionais/enfermagem1.JPG);"></div>
			</a> -->

			<a href='/img/institucionais/enfermagem2.JPG' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/institucionais/enfermagem2.JPG);"></div>
			</a>

			<a href='/img/institucionais/enfermagem3.JPG' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/institucionais/enfermagem3.JPG);"></div>
			</a>

			<br>

		</div>

	</div>
</section>