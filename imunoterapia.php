<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Imunoterapia</h1>
		</div>

		<h6 class="content-title">O que é?</h6>

		<div class="long-text">
			<p>
				A imunoterapia é um tratamento que fortalece o sistema imunológico, induzindo o combate das células cancerígenas pelo próprio organismo. Ela é considerada uma revolução no tratamento oncológico por aumentar a qualidade de vida do paciente. 
				<br>
				<br>
				Nesta modalidade de tratamento o foco é direcionado para destruir ou diminuir o tumor mas re-ensinar o sistema imune a atuar não é simples. Por isso, o tratamento não consegue ser eficaz em todos os casos. Em compensação, quando se alcança sucesso, as respostas são duradouras.<br>
				A forma mais comum de administrar os medicamentos são: por via intravenosa (administrado diretamente na veia).
			</p>
		</div>

		<h6 class="content-title">Como funciona?</h6>

		<div class="long-text">
			<p>
				O corpo humano é capaz de reconhecer o tumor como um corpo estranho. Com o passar do tempo, as células cancerígenas conseguem se “camuflar” enganando o sistema imunológico e aproveitando para se desenvolver. 
				<br>
				<br>
				A função principal da imunoterapia é restaurar a resposta imunológica contra o agente agressor. Os medicamentos promovem uma melhora na resposta imune, estimulando os linfócitos para que eles passem a perceber o tumor como um corpo estranho. 
			</p>
		</div>

		<h6 class="content-title">Efeitos colaterais</h6>

		<div class="long-text">
			<p>
				Comparada à quimioterapia, a imunoterapia apresenta menores efeitos colaterais. Os pacientes que se submetem a este tratamento, normalmente não apresentam queda de cabelo, vômitos ou fraquezas, porém podem apresentar inflamações do intestino grosso ou pulmão. É um tratamento que apresenta menor impacto na qualidade de vida do paciente. 
			</p>
		</div>

		<h6 class="content-title">Duração do tratamento</h6>

		<div class="long-text">
			<p>
				A duração do tratamento da imunoterapia depende da eficácia das drogas no combate à doença e da reação de cada paciente ao tratamento. <br>
				É comum a utilização da imunoterapia por até dois anos. Não existe um período predeterminado.
			</p>
		</div>

	</div>
</section>

<section id="galery">
	<div class="container p-0">
		
		<div id="images">
			
			<a href='/img/infraestrutura.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/infraestrutura.jpg);"></div>
			</a>

			<a href='/img/sobre-nos.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/sobre-nos.jpg);"></div>
			</a>

			<a href='/img/infraestrutura.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/infraestrutura.jpg);"></div>
			</a>

			<br>

		</div>

	</div>
</section>