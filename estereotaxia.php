<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Estereotaxia</h1>
		</div>

		<div class="long-text">
			<p>
				A estereotaxia é utilizada para identificar nódulos mamários não palpáveis, neste exame é realizado a análise e remoção de material para realização da biópsia, auxiliando na identificação do diagnóstico. 
			</p>
		</div>

	</div>
</section>

<!-- <section id="galery">
	<div class="container p-0">
		
		<div id="images">
			
			<a href='/img/infraestrutura.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/infraestrutura.jpg);"></div>
			</a>

			<a href='/img/sobre-nos.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/sobre-nos.jpg);"></div>
			</a>

			<a href='/img/infraestrutura.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/infraestrutura.jpg);"></div>
			</a>

			<br>

		</div>

	</div>
</section> -->