<?php
	
    //Arquivos externos
    include_once '../models/about.php';
    include_once '../config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myAbout = new About($db);
    $stmtAbout = $myAbout->read("", "id", "LIMIT 1");
    if ($stmtAbout->rowCount() > 0) {

	    $row = $stmtAbout->fetch(PDO::FETCH_ASSOC);
		extract($row);
		$myAbout->setId($id);
        $myAbout->setMission($mission);
        $myAbout->setVision($vision);
        $myAbout->setCompanyValues($company_values);
        $myAbout->setInfrastructure($infrastructure);
        $myAbout->setAttendance($attendance);
        $myAbout->setTeam($team);
        $myAbout->setPhone($phone);
        $myAbout->setWhatsapp($whatsapp);
        $myAbout->setEmail($email);
        $myAbout->setFacebook($facebook);
        $myAbout->setInstagram($instagram);
        $myAbout->setTwitter($twitter);
        $myAbout->setLinkedin($linkedin);
        $myAbout->setYoutube($youtube);
        $myAbout->setImageStructure($image_structure);
        $myAbout->setImageAttendance($image_attendance);
        $myAbout->setImageTeam($image_team);
    }

    $urlImages = "uploads/about/"; 

?>
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Editar Dados Institucionais</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="edit-about">
        	<input autocomplete="off" type="text" style="display:none;">
			<input type="hidden" value="<?php  echo $myAbout->getId() ?>" name="about-id">

            <div id="fields">

                <!-- Missão -->
                <div class="form-group">
                    <label>Missão</label>
                    <textarea class="form-control" name="mission" placeholder="Missão"><?php  echo $myAbout->getMission() ?></textarea>
                </div>

                <!-- Visão -->
                <div class="form-group">
                    <label>Visão</label>
                    <textarea class="form-control" name="vision" placeholder="Visão"><?php  echo $myAbout->getVision() ?></textarea>
                </div>

                <!-- Valores -->
                <div class="form-group">
                    <label>Valores</label>
                    <textarea class="form-control" name="company_values" placeholder="Valores"><?php  echo $myAbout->getCompanyValues() ?></textarea>
                </div>   

                <!-- Infraestrutura -->
                <div class="form-group">
                    <label>Infraestrutura</label>
                    <textarea class="form-control" name="insfrastructure" placeholder="Infraestrutura"><?php  echo $myAbout->getInfrastructure() ?></textarea>
                </div> 

                <!-- Atendimento -->
                <div class="form-group">
                    <label>Atendimento</label>
                    <textarea class="form-control" name="attendance" placeholder="Atendimento"><?php  echo $myAbout->getAttendance() ?></textarea>
                </div> 

                <!-- Equipe -->
                <div class="form-group">
                    <label>Equipe</label>
                    <textarea class="form-control" name="team" placeholder="Equipe"><?php  echo $myAbout->getTeam() ?></textarea>
                </div> 

                <!-- Telefone -->
                <div class="form-group">
                    <label>Telefone*</label>
                    <input class="form-control" name="phone" placeholder="Telefone" type="text" maxlength="15" value="<?php  echo $myAbout->getPhone() ?>" onkeydown="Mascara(this,Telefone);">
                </div>

                <!-- Email -->
                <div class="form-group">
                    <label>Email*</label>
                    <input class="form-control" name="email" placeholder="Email" type="text" value="<?php  echo $myAbout->getEmail() ?>">
                </div>

                <!-- Facebook -->
                <div class="form-group">
                    <label>Facebook*</label>
                    <input class="form-control" name="facebook" placeholder="Facebook" type="text" value="<?php  echo $myAbout->getFacebook() ?>">
                </div>

                <!-- Instagram -->
                <div class="form-group">
                    <label>Instagram*</label>
                    <input class="form-control" name="instagram" placeholder="Instagram" type="text" value="<?php  echo $myAbout->getInstagram() ?>">
                </div>
                
                <!-- LinkedIn -->
                <div class="form-group">
                    <label>LinkedIn*</label>
                    <input class="form-control" name="linkedin" placeholder="LinkedIn" type="text" value="<?php  echo $myAbout->getLinkedin() ?>">
                </div>

                <!-- YouTube -->
                <div class="form-group">
                    <label>YouTube*</label>
                    <input class="form-control" name="youtube" placeholder="YouTube" type="text" value="<?php  echo $myAbout->getYoutube() ?>">
                </div>

                <div class="form-group">
                    <label>Imagem - Estrutura (Dimensões recomendadas: 1564x1117)*</label><br>
                    <input type="file" name="image_structure" id="image_structure">
                    <input type="hidden" value="<?php  echo $myAbout->getImageStructure() ?>" name="image_structure-old">

                    <?php
                        if ($myAbout->getImageStructure() != "") {
                            $image = $urlImages.$myAbout->getImageStructure();

                            echo 
                            "
                                <br><br>
                                <div class='thumb-inline' style='background: url({$image}) no-repeat center; background-size: cover;'>
                                </div> 

                            ";                            
                        }


                    ?>

                </div>

                <div class="form-group">
                    <label>Imagem - Atendimento (Dimensões recomendadas: 1564x1117)*</label><br>
                    <input type="file" name="image_attendance" id="image_attendance">
                    <input type="hidden" value="<?php  echo $myAbout->getImageAttendance() ?>" name="image_attendance-old">

                    <?php
                        if ($myAbout->getImageAttendance() != "") {
                            $image = $urlImages.$myAbout->getImageAttendance();

                            echo 
                            "
                                <br><br>
                                <div class='thumb-inline' style='background: url({$image}) no-repeat center; background-size: cover;'>
                                </div> 

                            ";                            
                        }


                    ?>

                </div>
                
                <div class="form-group">
                    <label>Imagem - Equipe (Dimensões recomendadas: 1564x1117)*</label><br>
                    <input type="file" name="image_team" id="image_team">
                    <input type="hidden" value="<?php  echo $myAbout->getImageTeam() ?>" name="image_team-old">

                    <?php
                        if ($myAbout->getImageTeam() != "") {
                            $image = $urlImages.$myAbout->getImageTeam();

                            echo 
                            "
                                <br><br>
                                <div class='thumb-inline' style='background: url({$image}) no-repeat center; background-size: cover;'>
                                </div> 

                            ";                            
                        }


                    ?>

                </div>
                <!-- Imagens -->
                <div class="form-group">
                    <label>Imagens</label>
                    <br>
                    <a target="_blank" href='index.php?pg=editar-fotos-institucionais'>
                        <button type='button' class='btn btn-warning'>
                            <i class='fa fa-image'></i> Ver/editar Fotos
                        </button>
                    </a>                    
                </div>                       

            </div>

            <img src="img/loading.gif" id="carregando">

            <br>
            <br>

            <a href='index.php?pg=lista-servicos'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>