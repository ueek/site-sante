<?php
  //Inicia a sessão
  session_start();

  //Segurança
  include("../funcoes/ueek-protect.php");

  if(!isset($_GET["pg"]) && count($_GET) > 0){
      header("HTTP/1.0 404 Not Found");
      exit;
  }else if(isset($_GET["pg"])){
      if(pncHacker($_GET["pg"]) != ""){
          echo pncHacker($_GET["pg"]);
      }
  }

  //Controle de exibição de erro
  error_reporting(ALL_E);

  //Controle de Usuário
  include "../funcoes/verifica-usuario.php";

  //Funções Úteis
  include "../funcoes/converte-data.php";
  include "../funcoes/limita-caracteres.php";
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Members App | ADM">
    <meta name="author" content="Ueek Ag">
    <meta name="robots" content="noindex">

    <title>Santé | ADM</title>

    <!-- Custom fonts for this template-->
    <link rel="stylesheet" href="vendor/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">

    <!-- Libs -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css">
    <link rel="stylesheet" href="css/editary.css">
    <link rel="stylesheet" href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css">

    <!-- Custom styles for this template-->
    <link rel="stylesheet" href="css/sb-admin-2.css?version=23">
    <link rel="stylesheet" href="css/admin.css?version=23">
    <link rel="stylesheet" href="vendor/datatables/dataTables.bootstrap4.min.css">

    <!-- include summernote css/js -->
    <link href="libs/summernote/summernote.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="libs/simditor-2.3.28/styles/simditor.css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />
    <link rel="shortcut icon" href="img/icon.png">

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBkCjWHrnX5WmNAvKvALJQ2O_EgJsz80co"></script>

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
                <div class="sidebar-brand-icon">
                    <img src='img/logo-min.png' class='toolbar-logo' />
                </div>
            </a>

            <hr class="sidebar-divider my-0">

            <li class="nav-item">
                <a class="nav-link" href="index.php">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <hr class="sidebar-divider">

            <div class="sidebar-heading">
                Acesso
            </div>

            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUsuarios" aria-expanded="true" aria-controls="collapseUsuarios">
                    <i class="fas fa-users-cog"></i>
                    <span>Usuários</span>
                </a>
                <div id="collapseUsuarios" class="collapse" aria-labelledby="headingOne" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="index.php?pg=lista-gestores">Gestores</a>
                    </div>
                </div>
            </li>

            <hr class="sidebar-divider">

            <div class="sidebar-heading">
                Home
            </div>

            <li class="nav-item">
                <a class="nav-link" href="index.php?pg=lista-banners">
                  <i class="fas fa-fw fa-images"></i>
                  <span>Banners</span>
                </a>
            </li>

            <hr class="sidebar-divider">

            <div class="sidebar-heading">
                O Santé Cancer Center
            </div>

            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseInstitucional" aria-expanded="true" aria-controls="collapseInstitucional">
                    <i class="fas fa-fw fa-building"></i>
                    <span>Institucional</span>
                </a>
                <div id="collapseInstitucional" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="index.php?pg=editar-institucional">Sobre Nós</a>
                    </div>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseEquipe" aria-expanded="true" aria-controls="collapseEquipe">
                    <i class="fas fa-users"></i>
                    <span>Equipe</span>
                </a>
                <div id="collapseEquipe" class="collapse" aria-labelledby="headingThree" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="index.php?pg=lista-doutores">Equipe Médica</a>
                        <a class="collapse-item" href="index.php?pg=lista-equipe">Equipe Multidisplinar</a>
                    </div>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="index.php?pg=lista-convenios">
                    <i class="far fa-handshake"></i>
                    <span>Convênios</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="index.php?pg=lista-convenios-centro-de-mama">
                    <i class="fas fa-fw fa-heart"></i>
                    <span>Convênios do Centro de Mama</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="index.php?pg=lista-servicos">
                    <i class="fas fa-list-ul"></i>
                    <span>Serviços</span>
                </a>
            </li>


            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseEnsino" aria-expanded="true" aria-controls="collapseEnsino">
                    <i class="fas fa-book-reader"></i>
                    <span>Ensino e Pesquisa</span>
                </a>
                <div id="collapseEnsino" class="collapse" aria-labelledby="headingFour" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="index.php?pg=lista-residencias">Residências</a>
                    </div>
                </div>
            </li>
          
            <hr class="sidebar-divider">
            
            <div class="sidebar-heading">
                Social
            </div>

            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBlog" aria-expanded="true" aria-controls="collapseUsuarios">
                    <i class="fas fa-fw fa-quote-right"></i>
                    <span>Blog</span>
                </a>
                <div id="collapseBlog" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="index.php?pg=lista-categorias-blog">Categorias</a>
                        <a class="collapse-item" href="index.php?pg=lista-blogs">Posts</a>
                    </div>
                </div>
            </li>
         
            <hr class="sidebar-divider d-none d-md-block">

            <div class="sidebar-heading">
                Fale Com a Ueek
            </div>

            <li class="nav-item">
                <a class="nav-link" href="index.php?pg=suporte">
                  <i class="fas fa-fw fa-headphones"></i>
                  <span>Suporte</span>
                </a>
            </li>

            <hr class="sidebar-divider d-none d-md-block">

            <div class="sidebar-heading">
                Outros
            </div>

            <li class="nav-item">
                <a class="nav-link" href="index.php?pg=lista-log">
                  <i class="fas fa-fw fa-user-cog"></i>
                  <span>Log</span>
                </a>
            </li>

<!-- 
            

            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div> -->
        </ul>

        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <ul class="navbar-nav ml-auto">
                        <?php
                            //include("index-notifications.php");
                        ?>

                        <div class="topbar-divider d-none d-sm-block"></div>

                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i> Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                </nav>

                <div class="container-fluid">
                    <?php
                      if(!isset($_GET['pg']) or empty($_GET['pg'])){
                          include("dashboard.php");
                      }else{
                          if($_SERVER['REQUEST_METHOD'] == 'GET'){
                              $link = "{$_GET['pg']}.php";
                              if(file_exists($link)){
                                  include($link);
                              }else{
                                  include("erro.php");
                              }
                          }else{
                              //header("HTTP/1.0 404 Not Found");
                            include("erro.php");
                          }
                      }
                  ?>
                </div>
            </div>
        </div>
    </div>

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tem certeza?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Clique em "Logout" abaixo se você deseja encerrar sua sessão atual.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <a class="btn btn-primary" href="logout.php">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="infoModal" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Informação</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label id="infoModalText"></label>
                </div>
                <div class="modal-footer" id="infoModalOkBtn">
                    <button class="btn btn-primary" type="button" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>

    <!-- CDN's -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
    
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="vendor/bootstrap/js/collapse.js"></script>
    <script src="vendor/bootstrap/js/transition.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>
    <script src="js/demo/datatables-demo.js"></script>

    <!-- Plugins -->
    <!--script src="vendor/chart.js/Chart.min.js"></script-->
    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <!--sscript src="js/demo/chart-area-demo.js"></script-->
    <!--sscript src="js/demo/chart-pie-demo.js"></script-->

    <!-- Libs -->
    <script src="js/datepicker.js"></script>
    <script src="js/mascaras.js"></script>
    <!-- <script src="js/editary.js"></script> -->

    <!-- Summernote -->
    <script src="libs/summernote/summernote.js"></script>  
     <!-- Simditor  -->
    <script type="text/javascript" src="libs/simditor-2.3.28/site/assets/scripts/module.js"></script>
    <script type="text/javascript" src="libs/simditor-2.3.28/site/assets/scripts/hotkeys.js"></script>
    <script type="text/javascript" src="libs/simditor-2.3.28/site/assets/scripts/uploader.js"></script>
    <script type="text/javascript" src="libs/simditor-2.3.28/site/assets/scripts/simditor.js"></script>

    <!-- Location Picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-locationpicker/0.1.12/locationpicker.jquery.min.js"></script>
    <!-- Admins Scripts -->
    <script src="js/index.js"></script>
    <script src="js/support.js"></script>
    
    <script src="js/resources/about.js?vs=6"></script>
    <script src="js/resources/about-photos.js?vs=6"></script>
    <script src="js/resources/admins.js?vs=6"></script>
    <script src="js/resources/banners.js?vs=6"></script>
    <script src="js/resources/blog-photos.js?vs=6"></script>
    <script src="js/resources/blog-categories.js?vs=6"></script>
    <script src="js/resources/blogs.js?vs=6"></script>
    <script src="js/resources/health-insurance.js?vs=6"></script>
    <script src="js/resources/health-insurance-breast-center.js?vs=6"></script>
    <script src="js/resources/team.js?vs=6"></script>
    <script src="js/resources/doctors.js?vs=6"></script>
    <script src="js/resources/services.js?vs=6"></script>
    <script src="js/resources/residencies.js?vs=6"></script>
    <script src="js/resources/residency-publishes.js?vs=6"></script>
    <script src="js/resources/residency-doctors.js?vs=6"></script>
    <script src="js/resources/residency-academic-team.js?vs=6"></script>
    <script src="js/resources/residency-partners.js?vs=6"></script>

</body>
</html>