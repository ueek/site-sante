<?php
    
    //Arquivos externos
    include_once '../models/blog.php';
    include_once '../models/blog-category.php';
    include_once '../config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myBlog = new Blog($db);

    // Buscar dados do blog
    $stmtBlog = $myBlog->readById($_GET['id']);


    if ($stmtBlog->rowCount() > 0) {
        $row = $stmtBlog->fetch(PDO::FETCH_ASSOC);
        extract($row);
        $myBlog->setId($id);
        $myBlog->setTitle($title);
        $myBlog->setText($text);
        $myBlog->setImage($image);
        $myBlog->setAuthor($author);
        $myBlog->setBlogCategoryId($blog_category_id);
        $myBlog->setPublishedAt($published_at);
    }


    $myBlogCategory = new BlogCategory($db);
    $listOfBlogCategorys = $myBlogCategory->read();

    $urlImages = "uploads/blogs/"; 


?>
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Editar Post</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-12">
        <form id="edit-blog">
            <input autocomplete="off" type="text" style="display:none;">
            <input type="hidden" value="<?php  echo $myBlog->getId() ?>" name="blog-id">

            <div id="fields">

                <!-- Título -->
                <div class="form-group">
                    <label>Título*</label>
                    <input class="form-control" type="text" placeholder="Título" name="title" value="<?php  echo $myBlog->getTitle() ?>">
                </div>       

                <!-- Autor -->
                <div class="form-group">
                    <label>Autor*</label>
                    <input class="form-control" type="text" placeholder="Autor" name="author" value="<?php  echo $myBlog->getAuthor() ?>">
                </div>                        

                <!-- Categoria -->
                <div class="form-group">
                    <label>Categoria*</label>
                    <select class="form-control" name="blog-category-id">
                        <?php


                        while ($row = $listOfBlogCategorys->fetch(PDO::FETCH_ASSOC)){
                            extract($row);

                            $selected = $id == $myBlog->getBlogCategoryId() ? "selected='selected'" : "";

                            echo"
                                <option value='$id' {$selected}>
                                    {$name}
                                </option>
                            ";
                        }

                        ?>
                    </select>
                </div>

                <div class="form-group">
                    <label>Data Publicação*</label>
                    <div class="input-group date" id="published_at" data-target-input="nearest">
                        <input onkeydown="return false" type="text" class="form-control datetimepicker-input" data-target="#published_at" data-format="yyyy-MM-dd hh:mm:ss" value="<?php echo date('d/m/Y H:', strtotime($myBlog->getPublishedAt()))  ?>" />
                        <div class="input-group-append" data-target="#published_at" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Imagem Principal (Dimensões recomendadas: 1564x1117)*</label><br>
                    <input type="file" name="image1" id="image1">
                    <input type="hidden" value="<?php  echo $myBlog->getImage() ?>" name="image1-old">

                    <?php
                        if ($myBlog->getImage() != "") {
                            $image = $urlImages.$myBlog->getImage();

                            echo 
                            "
                                <br><br>
                                <div class='thumb-inline' style='background: url({$image}) no-repeat center; background-size: cover;'>
                                </div> 

                            ";                            
                        }


                    ?>

                </div>

                <!-- Texto -->
                <div class="form-group">
                    <label>Texto</label>
                    <textarea class="form-control" id="text" name="text" placeholder="Texto"><?php  echo $myBlog->getText() ?></textarea>
                </div>

            </div>

            <img src="img/loading.gif" id="carregando">
            
            <br>
            <br>
            <a href='index.php?pg=lista-blogs'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <?php
                echo 
                "
                    <a target='_blank' href='index.php?pg=editar-fotos-post&id={$myBlog->getId()}'>
                        <button type='button' class='btn btn-warning'>
                            <i class='fa fa-image'></i> Ver/editar Fotos
                        </button>
                    </a>
                ";
            ?>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>