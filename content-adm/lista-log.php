<?php
    //External files
    include_once '../models/log.php';
    include_once '../models/admin.php';
    include_once '../config/database.php';

    // Initialize Database
    $database = new Database();
    $db = $database->getConnection();

    // Create object and read the data
    $myLogs = new Log($db);
    $logs = $myLogs->read();
   
?>

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Registro de Log</h1>
<hr>
<p class="mb-4">Todos os registros de alterações estão listados abaixo.</p>

<br>

<!-- Data -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Log</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Usuário</th>
                        <th>Ação</th>
                        <th>Em</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    if ($logs->rowCount() > 0) {
                        while ($row = $logs->fetch(PDO::FETCH_ASSOC)){

                            // Extracting the data
                            extract($row);

                            // Showing the rows

                            // Format date
                            $log_created_at = date("d/m/Y H:i:s", strtotime($created_at));

                            // Instanciar objeto
                            $myAdmin = new Admin($db);

                            // Buscar dados
                            $stmtAdmin = $myAdmin->readById($admin_id);

                            if ($stmtAdmin->rowCount() > 0) {

                                $row = $stmtAdmin->fetch(PDO::FETCH_ASSOC);
                                extract($row);
                                $myAdmin->setName($name);
                            }

                            echo "
                            <tr>
                                <td>{$myAdmin->getName()}</td>
                                <td>{$action}</td>
                                <td>{$log_created_at}</td>
                            </tr>
                            ";
                        }
                    }
                ?>
              </tbody>
            </table>
        </div>
    </div>
</div>