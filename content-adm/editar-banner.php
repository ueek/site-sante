<?php
    
    //Arquivos externos
    include_once '../models/banner.php';
    include_once '../config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myBanner = new Banner($db);

    // Buscar dados do banner
    $stmtBanner = $myBanner->readById($_GET['id']);


    $urlImages = "uploads/banners/"; 

    if ($stmtBanner->rowCount() > 0) {
        $row = $stmtBanner->fetch(PDO::FETCH_ASSOC);
        extract($row);
        $myBanner->setId($id);
        $myBanner->setTitle($title);
        $myBanner->setText($text);
        $myBanner->setButtonText($button_text);
        $myBanner->setButtonUrl($button_url);
        $myBanner->setVideoUrl($video_url);
        $myBanner->setSequence($sequence);
        $myBanner->setImageDesktop($image_desktop);
        $myBanner->setImageMobile($image_mobile);
        $image1 = $urlImages.$image_desktop;
        $image2 = $urlImages.$image_mobile;
                    
    }



?>
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Editar Banner</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="edit-banner">
        	<input autocomplete="off" type="text" style="display:none;">
			<input type="hidden" value="<?php echo $myBanner->getId() ?>" name="banner-id">

            <div id="fields">

                <!-- Posição -->
                <div class="form-group">
                    <label>Posição*</label>
                    <input class="form-control" type="text" placeholder="Posição do banner" name="sequence" onkeydown="Mascara(this,Integer);" value="<?php echo $myBanner->getSequence() ?>">
                </div>

                <!-- Título -->
                <div class="form-group">
                    <label>Título</label>
                    <input class="form-control" type="text" placeholder="Título" name="title" maxlength="50" value="<?php echo $myBanner->getTitle() ?>">
                </div>

                <!-- Texto do Banner -->
                <div class="form-group">
                    <label>Texto do Banner</label>
                    <input class="form-control" type="text" placeholder="Texto do Banner" name="text"  maxlength="200" value="<?php echo $myBanner->getText() ?>">
                </div>

                <!-- Texto para o Botão -->
                <div class="form-group">
                    <label>Texto para o Botão</label>
                    <input class="form-control" type="text" placeholder="Ex.: Ver mais, Ir para Página" name="button_text" maxlength="30" value="<?php echo $myBanner->getButtonText() ?>">
                </div>

                <!-- Link/URL - Botão -->
                <div class="form-group">
                    <label>Link/URL do Botão</label>
                    <input class="form-control" type="text" placeholder="Link/URL do Botão" name="button_url"  maxlength="200" value="<?php echo $myBanner->getButtonUrl() ?>">
                </div>

                <!-- Link/URL - Vídeo -->
                <div class="form-group">
                    <label>Link/URL do Vídeo</label>
                    <input class="form-control" type="text" placeholder="Link/URL do Vídeo" name="video_url"  maxlength="200" value="<?php echo $myBanner->getVideoUrl() ?>">
                </div>

                <!-- Imagem do banner para computadores (Dimensões recomendadas: 1920x1080)* -->
                <label>Imagem do banner para computadores (Dimensões recomendadas: 1920x1080)*</label><br>
                <input type="file" name="image1" id="image1">

                <input type="hidden" value="<?php echo $myBanner->getImageDesktop() ?>" name="image1-old">

                <?php 

                    if ($image1 != "") {
                       echo "
                            <br><br>
                            <div class='thumb' style='background: url($image1) no-repeat center; background-size: cover;'>
                            </div>
                        ";
                    }
                ?>

                <br>

                <!-- Imagem do banner para celulares (Dimensões recomendadas: 1080x1920)* -->
                <label>Imagem do banner para celulares (Dimensões recomendadas: 1080x1920)*</label><br>
                <input type="file" name="image2" id="image2">

                <input type="hidden" value="<?php echo $myBanner->getImageMobile() ?>" name="image2-old">

                <?php 

                    if ($image2 != "") {
                       echo "
                            <br><br>
                            <div class='thumb' style='background: url($image2) no-repeat center; background-size: cover;'>
                            </div>
                        ";
                    }
                ?>

            </div>

            <img src="img/loading.gif" id="carregando">
            
            <br>
            <br>
            <a href='index.php?pg=lista-banners'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>