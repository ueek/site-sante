<?php
	
	    //Arquivos externos
    include_once '../models/about.php';
    include_once '../models/about-photo.php';
    include_once '../config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myAboutPhoto = new AboutPhoto($db);

    $listOfAboutPhotos = $myAboutPhoto->read();

    $urlImages = "uploads/about/"; 
?>

<div class="row">
    <div class="col-lg-12">
		<h1 class='page-header'>Imagens Institucionais</h1>
		<hr>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="edit-about-photos" enctype="multipart/form-data">
			<div id="imagens-upload">
				<div class="form-group">
					<div class="form-group">
						<label>Imagem (Dimensões recomendadas: 1044x812)*</label><br>
						<input type="file" name="image1" id="image1">
            			<br>
            			<br>
            			<input type="submit" class="btn btn-success direita" value="Upload" id="botao">

						<hr>

						<label>Imagens Institucionais</label><br>

						<?php 


						    if ($listOfAboutPhotos->rowCount() > 0) {
		                        while ($about_photo = $listOfAboutPhotos->fetch(PDO::FETCH_ASSOC)){

		                            // Extracting the data
		                            extract($about_photo);

		                            $image = $urlImages.$image;

		                            echo 
		                            "
										<div class='thumb' style='background: url($image) no-repeat center; background-size: cover;'>

											<button type='button' class='btn btn-danger btn-circle btn-sm btn-remove-image remove-about-photo'>
							                    <span class='icon'>
							                      <i class='fas fa-times'></i>
							                    </span>
												<input type='hidden' value='$id' name='id_foto'>
											</button> 
				                		</div> 


		                            ";

								}
						    }else{
						    	echo "Nenhuma imagem adicionada.";
						    }
						?>
					<div>
					</div>

					</div>
				</div>

			</div>



			<img src="img/loading.gif" id="carregando">

            <br><br>
		</form>

	</div>
</div>