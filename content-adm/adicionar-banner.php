<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Novo Banner</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="add-banner">
            <div id="fields">

                <!-- Posição -->
                <div class="form-group">
                    <label>Posição*</label>
                    <input class="form-control" type="text" placeholder="Posição do banner" name="sequence" onkeydown="Mascara(this,Integer);">
                </div>

                <!-- Imagem do banner para computadores -->
                <div class="form-group">
                    <label>Imagem do banner para computadores (Dimensões recomendadas: 1920x1080, com 200px de espaço seguro no topo)*</label><br>
                    <input type="file" name="image1" id="image1">
                </div>

                <!-- Imagem do banner para celulares -->
                <div class="form-group">
                    <label>Imagem do banner para celulares (Dimensões recomendadas: 1080x1920, com 200px)*</label><br>
                    <input type="file" name="image2" id="image2">
                </div>

                <!-- Título -->
                <div class="form-group">
                    <label>Título</label>
                    <input class="form-control" type="text" placeholder="Título" name="title" maxlength="50">
                </div>

                <!-- Texto do Banner -->
                <div class="form-group">
                    <label>Texto do Banner</label>
                    <input class="form-control" type="text" placeholder="Texto do Banner" name="text" maxlength="200">
                </div>

                <!-- Texto para o Botão -->
                <div class="form-group">
                    <label>Texto para o Botão</label>
                    <input class="form-control" type="text" placeholder="Ex.: Ver mais, Ir para Página" name="button_text"  maxlength="30">
                </div>

                <!-- Link/URL -->
                <div class="form-group">
                    <label>Link/URL para o Botão</label>
                    <input class="form-control" type="text" placeholder="Link/URL do Botão" name="button_url"  maxlength="200">
                </div>

                <!-- Vídeo Link/URL -->
                <div class="form-group">
                    <label>Se desejar inserir um vídeo ao banner, cole aqui a URL/Link do vídeo</label>
                    <input class="form-control" type="text" placeholder="Link/URL do Vídeo" name="video_url"  maxlength="200">
                </div>

            </div>
            
            <img src="img/loading.gif" id="carregando">

            <br>
            <br>
            <a href='index.php?pg=lista-banners'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>