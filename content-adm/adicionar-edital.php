<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Novo Edital</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="add-publish">
            <input type="hidden" name="residency_id" value="<?php echo $_GET['id'] ?>">
            <div id="fields">

                <!-- Título -->
                <div class="form-group">
                    <label>Título*</label>
                    <input class="form-control" type="text" placeholder="Título" name="title" maxlength="80">
                </div>

                <!-- Arquivo -->
                <div class="form-group">
                    <label>Arquivo*</label><br>
                    <input type="file" name="arquivo1" id="arquivo1">
                </div>

            </div>
            
            <img src="img/loading.gif" id="carregando">

            <br>
            <br>
            
            <a href='index.php?pg=lista-editais&id=<?php echo $_GET['id'] ?>'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>