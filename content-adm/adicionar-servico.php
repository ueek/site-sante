<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Novo Serviço</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="add-service">
            <div id="fields">

                <!-- Título -->
                <div class="form-group">
                    <label>Título*</label>
                    <input class="form-control" type="text" placeholder="Nome" name="name">
                </div>

                <!-- Segmentos -->
                <div class="form-group">
                    <label>Segmentos*</label>
                    <input class="form-control" type="text" placeholder="Insira os segmentos deste serviço separados por ponto e vírgula. Ex.: Mamografia; Quiomioterapia" name="sub_categories">
                </div>

                <!-- Imagem -->
                <div class="form-group">
                    <label>Imagem (Dimensões recomendadas: 350x230)*</label><br>
                    <input type="file" name="image1" id="image1">
                </div>

            </div>

            <img src="img/loading.gif" id="carregando">

            <br>
            <br>

            <a href='index.php?pg=lista-servicos'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>