<?php
    
    //Arquivos externos
    include_once '../models/team.php';
    include_once '../config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myTeam = new Team($db);

    // Buscar dados da equipe
    $stmtTeam = $myTeam->readById($_GET['id']);

    $urlImages = "uploads/team/"; 

    if ($stmtTeam->rowCount() > 0) {
        $row = $stmtTeam->fetch(PDO::FETCH_ASSOC);
        extract($row);
        $myTeam->setId($id);
        $myTeam->setName($name);
        $myTeam->setCharge($charge);
        $myTeam->setLinkedin($linkedin);
        $myTeam->setSequence($sequence);
        $myTeam->setImage($image);
        $image1 = $urlImages.$image;
                    
    }

?>
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Editar Membro da Equipe</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="edit-member">
        	<input autocomplete="off" type="text" style="display:none;">
			<input type="hidden" value="<?php  echo $myTeam->getId() ?>" name="member-id">

            <div id="fields">

                        <!-- Ordem de aparição -->
                        <div class="form-group">
                            <label>Ordem*</label>
                            <input class="form-control" type="number" placeholder="Ordem de aparição" name="sequence" value="<?php echo $myTeam->getSequence() ?>">

                        <!-- Nome -->
                        <div class="form-group">
                            <label>Nome*</label>
                            <input class="form-control" type="text" placeholder="Nome" name="name" value="<?php  echo $myTeam->getName() ?>">
                        </div>

                        <!-- Cargo -->
                        <div class="form-group">
                            <label>Cargo*</label>
                            <input class="form-control" type="text" placeholder="Cargo" name="charge" value="<?php  echo $myTeam->getCharge() ?>">
                        </div>

                        <!-- LinkedIn -->
                        <div class="form-group">
                            <label>LinkedIn</label>
                            <input class="form-control" type="text" placeholder="Link/URL do LinkedIn" name="linkedin" value="<?php  echo $myTeam->getLinkedin() ?>">
                        </div>

                        <!-- Imagem -->
                        <label>Imagem (Dimensões recomendadas: 200x200)*</label><br>
                        <input type="file" name="image1" id="image1">

                        <input type="hidden" value="<?php  echo $myTeam->getImage() ?>" name="image1-old">

                        <?php 

                            if ($image1 != "") {
                                echo "
                                    <br><br>
                                    <div class='thumb' style='background: url($image1) no-repeat center; background-size: cover;'>
                                    </div>
                                ";
                            }
                        ?>

            </div>

            <img src="img/loading.gif" id="carregando">
            
            <br>
            <br>
            <a href='index.php?pg=lista-equipe'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>