<?php
    
    //Arquivos externos
    include_once '../models/doctors.php';
    include_once '../config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myDoctors = new Doctors($db);

    // Buscar dados da equipe
    $stmtDoctors = $myDoctors->readById($_GET['id']);

    $urlImages = "uploads/doctors/"; 

    if ($stmtDoctors->rowCount() > 0) {
        $row = $stmtDoctors->fetch(PDO::FETCH_ASSOC);
        extract($row);
        $myDoctors->setId($id);
        $myDoctors->setName($name);
        $myDoctors->setCharge($charge);
        $myDoctors->setCrm($crm);
        $myDoctors->setRqe($rqe);
        $myDoctors->setLinkedin($linkedin);
        $myDoctors->setGraduation($graduation);
        $myDoctors->setGraduationYear($graduation_year);
        $myDoctors->setGraduationInstitution($graduation_institution);
        $myDoctors->setEffective($effective);
        $myDoctors->setSequence($sequence);
        $myDoctors->setImage($image);
        $image1 = $urlImages.$image;
                    
    }

?>
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Editar Membro da Equipe Médica</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="edit-doctor">
            <input autocomplete="off" type="text" style="display:none;">
            <input type="hidden" value="<?php  echo $myDoctors->getId() ?>" name="doctor-id">

            <div id="fields">

                        <!-- Posição na ordenação -->
                        <div class="form-group">
                            <label>Posição na ordenação</label>
                            <input class="form-control" type="text" placeholder="Posição na ordenação de médicos no site" name="sequence" value="<?php echo $myDoctors->getSequence() ?>">
                        </div>

                        <!-- Nome -->
                        <div class="form-group">
                            <label>Nome*</label>
                            <input class="form-control" type="text" placeholder="Nome" name="name" value="<?php  echo $myDoctors->getName() ?>">
                        </div>

                        <!-- Cargo -->
                        <div class="form-group">
                            <label>Cargo*</label>
                            <input class="form-control" type="text" placeholder="Cargo" name="charge" value="<?php  echo $myDoctors->getCharge() ?>">
                        </div>

                        <!-- CRM -->
                        <div class="form-group">
                            <label>CRM</label>
                            <input class="form-control" type="text" placeholder="CRM" name="crm" value="<?php  echo $myDoctors->getCrm() ?>">
                        </div>

                        <!-- RQE -->
                        <div class="form-group">
                            <label>RQE</label>
                            <input class="form-control" type="text" placeholder="RQE" name="rqe" value="<?php  echo $myDoctors->getRqe() ?>">
                        </div>

                        <!-- LinkedIn -->
                        <div class="form-group">
                            <label>LinkedIn</label>
                            <input class="form-control" type="text" placeholder="Link/URL do LinkedIn" name="linkedin" value="<?php  echo $myDoctors->getLinkedin() ?>">
                        </div>

                        <!-- Formação -->
                        <div class="form-group">
                            <label>Formação</label>
                            <input class="form-control" type="text" placeholder="Formação Principal" name="graduation" maxlength="50" value="<?php  echo $myDoctors->getGraduation() ?>">
                        </div>

                        <!-- Ano de Conclusão -->
                        <div class="form-group">
                            <label>Ano de Conclusão</label>
                            <input class="form-control" type="text" placeholder="Ano de Conclusão da Formação Principal" name="graduation_year" maxlength="4" value="<?php  echo $myDoctors->getGraduationYear() ?>">
                        </div>

                        <!-- Instituição de Graduação -->
                        <div class="form-group">
                            <label>Instituição de Formação</label>
                            <input class="form-control" type="text" placeholder="Instituição de Formação" name="graduation_institution" maxlength="80" value="<?php  echo $myDoctors->getGraduationInstitution() ?>">
                        </div>

                        <!-- Efetivo/Residente -->
                        <div class="form-group">
                            <label>Situação atual*</label>
                            <select class="form-control" name="effective">
                                <option <?php echo $myDoctors->getEffective() == 1 ? 'selected' : '' ?> value="1">Efetivo/Contratado</option>
                                <option <?php echo $myDoctors->getEffective() == 0 ? 'selected' : '' ?> value="0">Residente</option>
                            </select>
                        </div>

                        <!-- Imagem -->
                        <label>Imagem (Dimensões recomendadas: 200x200)*</label><br>
                        <input type="file" name="image1" id="image1">

                        <input type="hidden" value="<?php  echo $myDoctors->getImage() ?>" name="image1-old">

                        <?php 

                            if ($image1 != "") {
                                echo "
                                    <br><br>
                                    <div class='thumb' style='background: url($image1) no-repeat center; background-size: cover;'>
                                    </div>
                                ";
                            }
                        ?>

            </div>

            <img src="img/loading.gif" id="carregando">
            
            <br>
            <br>
            <a href='index.php?pg=lista-doutores'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>