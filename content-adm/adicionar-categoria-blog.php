<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Nova Categoria</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="add-blog-category">
            <div id="fields">

                <div class="form-group">
                    <label>Nome*</label>
                    <input class="form-control" type="text" placeholder="Nome" name="name">
                </div>
            </div>

            <img src="img/loading.gif" id="carregando">

            <br>
            <br>
            <a href='index.php?pg=lista-categorias-blog'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>