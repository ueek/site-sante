<?php

	session_start();

	//Destrói
    session_destroy();
 
    //Limpa
    unset ($_SESSION['idGestor']);
    unset ($_SESSION['emailGestor']);
    unset ($_SESSION['senhaGestor']);
    unset ($_SESSION['nivelGestor']);

	header("Location: login.php");
	exit;

?>