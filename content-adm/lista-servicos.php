<?php
    //Arquivos externos
    include_once '../models/service.php';
    include_once '../config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myServices = new Service($db);
    $listOfServices = $myServices->read();
   
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Serviços</h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        Os serviços adicionados estão listados abaixo. Para editar as informações, clique no botão amarelo. Para remover, clique no botão vermelho.
        <br><br><br>

        <a href='index.php?pg=adicionar-servico'>
            <button type='button' class='btn btn-success'>
              <i class='fa fa-plus'></i>
              Cadastrar Serviço
            </button>
        </a>

        <br><br>

        <!-- Data -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Serviços</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Título</th>
                            <th>Segmentos</th>
                            <th>Adicionado em</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 

                            
                            if ($listOfServices->rowCount() > 0) {
                                while ($row = $listOfServices->fetch(PDO::FETCH_ASSOC)){
                                    
                                    extract($row);

                                    $created_at = date("d/m/Y H:i:s", strtotime($created_at));

                                    echo "
                                        <tr>
                                            <td>$name</td>
                                            <td>$sub_categories</td>
                                            <td>$created_at</td>
                                            <td>
                                                <a href='index.php?pg=editar-servico&id={$id}'>
                                                    <button type='button' class='btn btn-warning btn-circle'>
                                                        <i class='fa fa-pencil-alt'></i>
                                                    </button>
                                                </a>
                                                <button type='button' class='btn btn-danger btn-circle remove-servico'>
                                                    <i class='fa fa-trash-alt'></i>
                                                    <input type='hidden' value='{$id}' name='id'>
                                                </button>
                                            </td>
                                        </tr>
                                    ";
                                }
                            }
                        ?>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
            </div>
        </div>
    </div>
</div>