<?php
    
    //Arquivos externos
    include_once '../models/residency.php';
    include_once '../config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myResidencies = new Residency($db);

    // Buscar dados da equipe
    $stmtResidency = $myResidencies->readById($_GET['id']);

    if ($stmtResidency->rowCount() > 0) {
        $row = $stmtResidency->fetch(PDO::FETCH_ASSOC);
        extract($row);
        $myResidencies->setId($id);
        $myResidencies->setName($name);
        $myResidencies->setDescription($description);
        $myResidencies->setInformation($information);
        $myResidencies->setAdmition($admition);                    
    }

?>
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Editar Residência</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="edit-residency">
        	<input autocomplete="off" type="text" style="display:none;">
			<input type="hidden" value="<?php  echo $myResidencies->getId() ?>" name="residency-id">

            <div id="fields">

                <!-- Nome -->
                <div class="form-group">
                    <label>Nome*</label>
                    <input class="form-control" type="text" placeholder="Nome" name="name" value="<?php  echo $myResidencies->getName() ?>">
                </div>

                <!-- Descrição -->
                <div class="form-group">
                    <label>Descrição*</label>
                    <textarea class="form-control textarea" type="text" placeholder="Descrição" name="description"><?php echo $myResidencies->getDescription() ?></textarea> 
                </div>

                <!-- Informações -->
                <div class="form-group">
                    <label>Informações*</label>
                    <textarea class="form-control textarea" type="text" placeholder="Informações" name="information"><?php echo $myResidencies->getInformation() ?></textarea> 
                </div>

                <!-- Admissão -->
                <div class="form-group">
                    <label>Admissão*</label>
                    <textarea class="form-control textarea" type="text" placeholder="Admissão" name="admition"><?php echo $myResidencies->getAdmition() ?></textarea> 
                </div>

            </div>

            <img src="img/loading.gif" id="carregando">
            
            <br>
            <br>
            <a href='index.php?pg=lista-residencias'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>