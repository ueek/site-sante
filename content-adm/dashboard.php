<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Bem-vindo(a)!</h1>
<hr>
<p class="mb-4">Bem vindo(a) ao administrador do seu site. Caso possua alguma dificuldade, contate-nos via suporte@ueek.com.br ou utilize o botão abaixo para entrar em contato.</p>

<a href='index.php?pg=suporte'>
    <button type='button' class='btn btn-success'>
        <i class='fa fa-headphones'></i> Fale com a Ueek
    </button>
</a>