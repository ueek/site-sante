<?php
	
    //Arquivos externos
    include_once '../models/residency-publish.php';
    include_once '../config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myPublish = new ResidencyPublish($db);

    // Buscar dados do blog
    $stmtPublish = $myPublish->readById($_GET['id']);

    if ($stmtPublish->rowCount() > 0) {
        $row = $stmtPublish->fetch(PDO::FETCH_ASSOC);
        extract($row);
        $myPublish->setId($id);
        $myPublish->setResidencyId($residency_id);
        $myPublish->setTitle($title);
        $myPublish->setUrl($url);
    }

    $urlImages = "uploads/publishes/"; 

?>

<div class="row">
    <div class="col-lg-12">
		<h1 class='page-header'>Edital</h1>
		<hr>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="edit-publish" enctype="multipart/form-data">
			<input type="hidden" name="publish_id" value="<?php  echo $myPublish->getId() ?>">
			<input type="hidden" name="residency_id" value="<?php  echo $myPublish->getResidencyId() ?>">
			
			<div id="fields">

				<!-- Título -->
	            <div class="form-group">
	                <label>Título*</label>
	                <input class="form-control" type="text" placeholder="Título" name="title" value="<?php  echo $myPublish->getTitle() ?>">
	            </div>

	            <!-- Arquivo -->
	            <div class="form-group">
	                <label>Arquivo*</label><br>
	                <input type="file" name="arquivo1" id="arquivo1">
	            	<input type="hidden" value="<?php echo $myPublish->getUrl() ?>" name="arquivo1-old">
	            </div>

	        </div>

			<img src="img/loading.gif" id="carregando">

            <br>
            <br>

            <a href='index.php?pg=lista-editais&id=<?php echo $myPublish->getResidencyId() ?>'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

			<input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

			<hr>

		</form>
	</div>
</div>