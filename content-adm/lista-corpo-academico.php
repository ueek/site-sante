<?php
    //External files
    include_once '../models/residency-academic-team.php';
    include_once '../config/database.php';
    
    // Initialize Database
    $database = new Database();
    $db = $database->getConnection();

    // Create object and read the data
    $myResidencyTeam = new ResidencyAcademicTeam($db);
    $members = $myResidencyTeam->read("AND residencies_id = {$_GET['id']}");
   
?>

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Corpo Acadêmico</h1>
<hr>
<p class="mb-4">Os membros do corpo acadêmico adicionados estão listados abaixo. Para editar, clique no botão amarelo. Para remover, no botão vermelho.</p>

<a href='index.php?pg=lista-residencias'>
    <button type='button' class='btn btn-info'>
        <i class='fa fa-chevron-left'></i> Voltar
    </button>
</a>
<a href='index.php?pg=adicionar-membro-academico&id=<?php echo $_GET['id'] ?>'>
    <button type='button' class='btn btn-success'>
        <i class='fa fa-plus'></i> Adicionar Membro
    </button>
</a>

<br>
<br>

<!-- Data -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Membros</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Adicionado Em</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    if ($members->rowCount() > 0) {
                        while ($row = $members->fetch(PDO::FETCH_ASSOC)){

                            // Extracting the data
                            extract($row);

                            // Showing the rows

                            // Format date
                            $created_at = date("d/m/Y H:i:s", strtotime($created_at));

                            echo "
                            <tr>
                                <td>{$name}</td>
                                <td>{$created_at}</td>
                                <td>
                                    <a href='index.php?pg=editar-membro-academico&id={$id}'>
                                        <button type='button' class='btn btn-warning btn-circle'>
                                            <i class='fa fa-pencil-alt'></i>
                                        </button>
                                    </a>
                                    <button type='button' class='btn btn-danger btn-circle remove-membro-academico'>
                                        <i class='fa fa-trash-alt'></i>
                                        <input type='hidden' value='{$id}' name='id'>
                                    </button>
                                </td>
                            </tr>
                            ";
                        }
                    }
                ?>
              </tbody>
            </table>
        </div>
    </div>
</div>