<?php
    //Arquivos externos
    include_once '../models/blog-category.php';
    include_once '../config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myBlogCategory = new BlogCategory($db);

    $listOfBlogCategorys = $myBlogCategory->read();


?>

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Novo Post</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-12">
        <form id="add-blog">
            <div id="fields">

                <!-- Título -->
                <div class="form-group">
                    <label>Título*</label>
                    <input class="form-control" type="text" placeholder="Título" name="title">
                </div>

                <!-- Autor -->
                <div class="form-group">
                    <label>Autor*</label>
                    <input class="form-control" type="text" placeholder="Autor" name="author">
                </div>

                <!-- Categoria -->
                <div class="form-group">
                    <label>Categoria*</label>
                    <select class="form-control" name="blog-category-id">
                        <?php
                        
                        while ($row = $listOfBlogCategorys->fetch(PDO::FETCH_ASSOC)){
                            extract($row);
                            echo"
                                <option value='{$id}'>{$name}</option>
                            ";
                        }
                        ?>
                    </select>
                </div>

                <!-- Published at -->
                <div class="form-group">
                    <label>Data Publicação*</label>
                    <div class="input-group date" id="published_at" data-target-input="nearest">
                        <input onkeydown="return false" type="text" class="form-control datetimepicker-input" data-target="#published_at" data-format="yyyy-MM-dd hh:mm:ss"/>
                        <div class="input-group-append" data-target="#published_at" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Imagem Principal (Dimensões recomendadas: 1564x1117)*</label><br>
                    <input type="file" name="image1" id="image1">
                </div>

                <!-- Texto -->
                <div class="form-group">
                    <label>Texto</label>
                    <textarea class="form-control" id="text" name="text" placeholder="Texto"></textarea>
                </div>

            </div>

            <img src="img/loading.gif" id="carregando">

            <br>
            <br>
            <a href='index.php?pg=lista-blogs'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>