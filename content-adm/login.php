<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Members App | ADM">
  <meta name="author" content="Ueek Ag">
  <meta name="robots" content="noindex">

  <title>Santé | ADM</title>

  <link rel="stylesheet" href="vendor/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" >

  <link rel="stylesheet" href="css/sb-admin-2.css">
  <link rel="stylesheet" href="css/admin.css">
</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">
      <div class="col-xl-10 col-lg-12 col-md-9">
        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Seja Bem-vindo!</h1>
                  </div>
                  <form class="user" id="form-login">
                    <div class="form-group">
                      <input type="email" class="form-control form-control-user" name="email" aria-describedby="emailHelp" placeholder="Digite seu e-mail">
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" name="senha" placeholder="Sua senha">
                    </div>

                    <div class="form-group">
                      <div class="alert alert-danger alert-dismissable" id="erro">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        Seu e-mail e/ou senha estão incorretos, <strong>tente novamente.</strong>
                      </div>
                    </div>
                    
                    <input type="submit" class="btn btn-success" style="width:100%" value="Login"> 
                    <br>
                  </form>
                  <hr>
                  <div class="text-center">
                    <a class="small" href="forgot-password.html">Esqueceu sua senha?</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages -->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Ueek Scripts -->
  <script src="js/login.js"></script>
</body>
</html>