<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Nova Residência</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="add-residency">
            <div id="fields">

                <!-- Título -->
                <div class="form-group">
                    <label>Título*</label>
                    <input class="form-control" type="text" placeholder="Título" name="name">
                </div>

                <!-- Descrição -->
                <div class="form-group">
                    <label>Descrição*</label>
                    <textarea class="form-control textarea" type="text" placeholder="Descrição" name="description"></textarea> 
                </div> 

                <!-- Informações -->
                <div class="form-group">
                    <label>Informações*</label>
                    <textarea class="form-control textarea" type="text" placeholder="Informações" name="information"></textarea> 
                </div> 

                <!-- Admissão -->
                <div class="form-group">
                    <label>Admissão*</label>
                    <textarea class="form-control textarea" type="text" placeholder="Admissão" name="admition"></textarea> 
                </div>

            </div>

            <img src="img/loading.gif" id="carregando">

            <br>
            <br>
            <a href='index.php?pg=lista-residencias'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>