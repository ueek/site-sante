<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Fale com a Ueek</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="support">
            <div id="fields">

                <!-- Nome -->
                <div class="form-group">
                    <label>Nome*</label>
                    <input class="form-control" type="text" placeholder="Nome" name="name">
                </div>

                <div class="form-group">
                    <label>E-mail*</label>
                    <input class="form-control" type="text" placeholder="E-mail" name="email">
                </div>

                <!-- Descreva sua dúvida, problema ou solicitação -->
                <div class="form-group">
                    <label>Descreva sua dúvida, problema ou solicitação</label>
                    <textarea class="form-control" id="description" name="description" placeholder="Descreva sua dúvida, problema ou solicitação"></textarea>
                </div>
            </div>

            <img src="img/loading.gif" id="carregando">

            <br>
            <br>

            <input type="submit" class="btn btn-success direita" value="Enviar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>