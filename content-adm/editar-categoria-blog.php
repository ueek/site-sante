<?php
    
    //Arquivos externos
    include_once '../models/blog-category.php';
    include_once '../config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myBlogCategory = new BlogCategory($db);

    // Buscar dados
    $stmtBlogCategory = $myBlogCategory->readById($_GET['id']);


    if ($stmtBlogCategory->rowCount() > 0) {

        $row = $stmtBlogCategory->fetch(PDO::FETCH_ASSOC);
        extract($row);
        $myBlogCategory->setId($id);
        $myBlogCategory->setName($name);
    }



?>
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Editar Categoria</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="edit-blog-category">
            <input autocomplete="off" type="text" style="display:none;">
            <input type="hidden" value="<?php  echo $myBlogCategory->getId() ?>" name="blog-category-id">

            <div id="fields">
                <!-- Name -->
                <div class="form-group">
                    <label>Nome*</label>
                    <input class="form-control" type="text" placeholder="Nome" name="name" value="<?php  echo $myBlogCategory->getName() ?>">
                </div>

           </div>

            <img src="img/loading.gif" id="carregando">

            <br>
            <br>
            <a href='index.php?pg=lista-categorias-blog'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>