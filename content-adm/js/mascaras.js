$(document).ready(function() {
	//Datepicker
	var today = new Date();
	
	$('.calendario').datepicker({
	    format: "dd/mm/yyyy",
        todayHighlight: true
        //endDate: new Date(today.getFullYear(), today.getMonth(), today.getDate())
	});

	$('.calendario-atual-anterior').datepicker({
	    format: "dd/mm/yyyy",
        todayHighlight: true,
        endDate: new Date(today.getFullYear(), today.getMonth(), today.getDate())
	});

	$('.calendario-atual-posterior').datepicker({
	    format: "dd/mm/yyyy",
        todayHighlight: true,
        startDate: '+1d'
	});

	$("#accordion").accordion({
    	heightStyle: "content",
    	collapsible: true, 
    	active: false
    });

});
	

$(document).on("input", "#descricao-evento", function () {
    var limite = 240;
    var caracteresDigitados = $(this).val().length;
    var caracteresRestantes = limite - caracteresDigitados;

    $("#caracteres-restantes").text(caracteresRestantes);
});

$(document).on("input", "#texto-notificacao", function () {
    var limite = 160;
    var caracteresDigitados = $(this).val().length;
    var caracteresRestantes = limite - caracteresDigitados;

    $("#caracteres-restantes").text(caracteresRestantes);
});

//Máscaras
function Mascara(o,f){
	v_obj=o
	v_fun=f
	setTimeout("execmascara()",1)
}

function Tempo(v){
    v=v.replace(/\D/g,"");                    //Remove tudo o que não é dígito
    v=v.replace(/(\d{1})(\d{2})(\d{2})/,"$1:$2.$3");
    return v;
}

function Hora(v){
    v=v.replace(/\D/g,"");                    //Remove tudo o que não é dígito
    v=v.replace(/(\d{2})(\d)/,"$1:$2");
    return v;
}

function execmascara(){
	v_obj.value=v_fun(v_obj.value)
}

function leech(v){
	v=v.replace(/o/gi,"0")
	v=v.replace(/i/gi,"1")
	v=v.replace(/z/gi,"2")
	v=v.replace(/e/gi,"3")
	v=v.replace(/a/gi,"4")
	v=v.replace(/s/gi,"5")
	v=v.replace(/t/gi,"7")
	return v
}

function Integer(v){
	return v.replace(/\D/g,"")
}

function Login(v){
	return v.replace(/\W/g,"")
}

function Telefone(v){
	v=v.replace(/\D/g,"")
	v=v.replace(/^(\d\d)(\d)/g,"($1) $2")

	if (v.length == 13) {
		v=v.replace(/(\d{4})(\d)/,"$1-$2")
	} else if(v.length == 14) {
		v=v.replace(/(\d{5})(\d)/,"$1-$2")
	}

	return v
}

function Cpf(v){
	v=v.replace(/\D/g,"")
	v=v.replace(/(\d{3})(\d)/,"$1.$2")
	v=v.replace(/(\d{3})(\d)/,"$1.$2")
	v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2")
	return v
}

function Cnpj(v){
	v=v.replace(/\D/g,"")
	v=v.replace(/^(\d{2})(\d)/,"$1.$2")
	v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3")
	v=v.replace(/\.(\d{3})(\d)/,".$1/$2")
	v=v.replace(/(\d{4})(\d)/,"$1-$2")
	return v
}

function Cep(v){
	v=v.replace(/\D/g,"")
	v=v.replace(/^(\d{5})(\d)/,"$1-$2")
	return v
}

function Data(v){
	v=v.replace(/\D/g,"")
	v=v.replace(/(\d{2})(\d)/,"$1/$2")
	v=v.replace(/(\d{2})(\d)/,"$1/$2")
	return v
}

function checkMail(mail){
	var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
	if(typeof(mail) == "string"){
		if(er.test(mail)){ return true; }
	}else if(typeof(mail) == "object"){
		if(er.test(mail.value)){
			return true;
		}
	}else{
		return false;
	}
}

function Valor(v){
    v=v.replace(/\D/g,"");//Remove tudo o que não é dígito
    v=v.replace(/(\d)(\d{8})$/,"$1.$2");//coloca o ponto dos milhões
    v=v.replace(/(\d)(\d{5})$/,"$1.$2");//coloca o ponto dos milhares

    v=v.replace(/(\d)(\d{2})$/,"$1,$2");//coloca a virgula antes dos 2 últimos dígitos
    return v;
}
