$(document).ready(function(){
    //Cadastrar 
    $("form#add-admin").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        if($('input[name="name"]', form).val().trim() == ""){
            $('input[name="name"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="email"]', form).val().trim() == "" || !isEmail($('input[name="email"]', form).val())){
            $('input[name="email"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="password"]', form).val().trim() == ""){
            $('input[name="password"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }


        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('name', $('input[name=name]').val());
        valores.append('email', $('input[name=email]').val());
        valores.append('password', $('input[name=password]').val());

        $.ajax({
            url: "controllers/admins/create.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){

                    $('#infoModalText').html(json.msg);
                    $('#infoModalOkBtn').hide();
                    $("#infoModal").modal();

                    setTimeout(redirectToAdmins, 2000);
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    $('#infoModalText').html(json.msg);
                    $("#infoModal").modal();
                }
            }
        });
    });

    $("form#edit-admin").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        if($('input[name="name"]', form).val().trim() == ""){
            $('input[name="name"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="email"]', form).val().trim() == ""){
            $('input[name="email"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('admin-id', $('input[name=admin-id]').val());
        valores.append('name', $('input[name=name]').val());
        valores.append('email', $('input[name=email]').val());
        valores.append('password', $('input[name=password]').val());

        $.ajax({
            url: "controllers/admins/update.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){

                    $('#infoModalText').html(json.msg);
                    $('#infoModalOkBtn').hide();
                    $("#infoModal").modal();

                    setTimeout(redirectToAdmins, 2000);


                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    $('#infoModalText').html(json.msg);
                    $("#infoModal").modal();
                }
            },
            error    : function(retorno){
                alert("Error");
            }
        });
    });

    //Remover  
    $( ".remove-admin" ).click(function() {
        var icone    = $(this);
        var id       = $('input', icone).val();
        var confirma = confirm('Tem certeza que deseja excluir este registro?');
        if(confirma){
            $.ajax({
                type     : 'POST',
                url      : 'controllers/admins/delete.php',
                data     : {id : id},
                dataType : 'json',
                success  : function(data){
                    if(data.status == 1 || data.status == 2){
                        alert(data.msg);
                        location.reload();
                    }else{
                        alert(data.msg);
                    }
                },
                error   : function(data){
                    alert(data.responseText);
                }
            });
        }
    });

    function redirectToAdmins() {
      window.location.href="index.php?pg=admins"; 
    }
});