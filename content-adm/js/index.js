function showAlertDialog(msg, showOkButton){
    $('#infoModalText').html(msg);

    if (!showOkButton) {$('#infoModalOkBtn').hide();}

    $("#infoModal").modal();
}

function padNumber(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}

function returnFormattedDate(start_date){
	var year = start_date.substring(6,10);
	var month = start_date.substring(3,5);
	var day = start_date.substring(0,2);
	var hour = start_date.substring(11,13);
	var minute = start_date.substring(14,16);

	return year + "-" + month + "-" +  day + " " +
	        hour + ":" + minute;
}