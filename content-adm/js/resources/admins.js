$(document).ready(function(){

	//Cadastrar Gestor
	$('form#adicionar-gestor').submit(function(e){
		e.preventDefault();

		var form = $(this);

		//Validações
		$('input[type="text"]', form).css({'-webkit-box-shadow':'none', 'box-shadow':'none'});
		$('input[type="password"]', form).css({'-webkit-box-shadow':'none', 'box-shadow':'none'});

		if($('input[name="nome"]', form).val() == ""){
			$('input[name="nome"]', form).css({
				'-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
				'box-shadow':'inset 0 0 0 1px #BE0007'
			}).focus();
			return false;
		}

		if($('input[name="email"]', form).val() == "" || !checkMail($('input[name="email"]', form).val())){
			$('input[name="email"]', form).css({
				'-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
				'box-shadow':'inset 0 0 0 1px #BE0007'
			}).focus();
			return false;
		}

		if($('input[name="senha"]', form).val() == ""){
			$('input[name="senha"]', form).css({
				'-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
				'box-shadow':'inset 0 0 0 1px #BE0007'
			}).focus();
			return false;
		}

		if($('input[name="senha_confirmacao"]', form).val() == ""){
			$('input[name="senha_confirmacao"]', form).css({
				'-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
				'box-shadow':'inset 0 0 0 1px #BE0007'
			}).focus();
			return false;
		}

		if($('input[name="senha"]', form).val() != "" && $('input[name="senha_confirmacao"]', form).val() != "" && 
		   $('input[name="senha"]', form).val() != $('input[name="senha_confirmacao"]', form).val()){

			showAlertDialog("As senhas precisam ser iguais", false);

		   $('input[name="senha"]', form).css({
				'-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
				'box-shadow':'inset 0 0 0 1px #BE0007'
			}).focus();

			$('input[name="senha_confirmacao"]', form).css({
				'-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
				'box-shadow':'inset 0 0 0 1px #BE0007'
			}).focus();

		   	return false;
		}

		var valores = form.serialize();
		
		$('input', form).attr('disabled', 'disabled');

		$.ajax({
			type     : 'POST',
			url      : 'controllers/admins/create.php',
			data     : valores,
			dataType : 'json',
			success  : function(retorno){
				$('input', form).removeAttr('disabled');

                if(retorno.status == 1){

					showAlertDialog(retorno.msg, false);

                    setTimeout(redirectToList, 2000);
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

					showAlertDialog(retorno.msg, true);
                }
			}
		});
	});

	//Alterar Gestor
	$('form#editar-gestor').submit(function(e){
		e.preventDefault();

		var form = $(this);

		//Validações
		$('input[type="text"]', form).css({'-webkit-box-shadow':'none', 'box-shadow':'none'});
		$('input[type="password"]', form).css({'-webkit-box-shadow':'none', 'box-shadow':'none'});

		if($('input[name="nome"]', form).val() == ""){
			$('input[name="nome"]', form).css({
				'-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
				'box-shadow':'inset 0 0 0 1px #BE0007'
			}).focus();
			return false;
		}

		if($('input[name="email"]', form).val() == "" || !checkMail($('input[name="email"]', form).val())){
			$('input[name="email"]', form).css({
				'-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
				'box-shadow':'inset 0 0 0 1px #BE0007'
			}).focus();
			return false;
		}

		if($('input[name="senha"]', form).val() != ""){
			if($('input[name="senha_confirmacao"]', form).val() == ""){
				$('input[name="senha_confirmacao"]', form).css({
					'-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
					'box-shadow':'inset 0 0 0 1px #BE0007'
				}).focus();
				return false;
			}
		}

		if($('input[name="senha"]', form).val() != "" && $('input[name="senha_confirmacao"]', form).val() != "" && 
		   $('input[name="senha"]', form).val() != $('input[name="senha_confirmacao"]', form).val()){

		   	alert("As senhas precisam ser iguais");

		   $('input[name="senha"]', form).css({
				'-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
				'box-shadow':'inset 0 0 0 1px #BE0007'
			}).focus();

			$('input[name="senha_confirmacao"]', form).css({
				'-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
				'box-shadow':'inset 0 0 0 1px #BE0007'
			}).focus();

		   	return false;
		}

		var valores = form.serialize();
		
		$('input', form).attr('disabled', 'disabled');

		$.ajax({
			type     : 'POST',
			url      : 'controllers/admins/update.php',
			data     : valores,
			dataType : 'json',
			success  : function(retorno){
				$('input', form).removeAttr('disabled');
				
                if(retorno.status == 1){

                	showAlertDialog(retorno.msg);

                    setTimeout(redirectToList, 2000);
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    $('#infoModalText').html(retorno.msg);
                    $("#infoModal").modal();
                }
			}
		});
	});

	//Remover Gestor
    $('#dataTable tbody').on('click', '.remove-gestor', function () {
		var icone    = $(this);
		var id       = $('input', icone).val();
		var confirma = confirm('Deseja realmente remover este usuário gestor? Esta ação é irreversível.');
		if(confirma){
			$.ajax({
				type    : 'POST',
				url     : 'controllers/admins/delete.php',
				data    : {id : id},
				dataType : 'json',
				success : function(data){
                    if(data.status == 1 || data.status == 2){
                        showAlertDialog(data.msg, false);
                        setTimeout(redirectToList, 2000);
                    }else{
                        showAlertDialog(data.msg, true);
                    }
				}
			});
		}
	});

    function redirectToList() {
      window.location.href="index.php?pg=lista-gestores"; 
    }

});