$(document).ready(function(){

    $('.textarea').summernote(
        {
            height: 400,
            toolbar: [
              ['style', ['style']],
              ['font', ['bold', 'underline', 'clear']],
              ['fontname', ['fontname']],
              ['color', ['color']],
              ['para', ['ul', 'ol', 'paragraph']],
              ['insert', ['link', 'picture', 'video']],
              ['view', ['fullscreen', 'codeview', 'help']],
            ],
        }
    );

    //Cadastrar 
    $("form#add-residency").submit(function(e) {

        e.preventDefault();

        var form = $(this);

        if($('input[name="name"]', form).val() == ""){
            $('input[name="name"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('textarea[name="description"]', form).val() == ""){
            $('textarea[name="description"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('textarea[name="information"]', form).val() == ""){
            $('textarea[name="information"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('textarea[name="admition"]', form).val() == ""){
            $('textarea[name="admition"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('name', $('input[name=name]').val());
        valores.append('description', $('textarea[name=description]').val());
        valores.append('information', $('textarea[name=information]').val());
        valores.append('admition', $('textarea[name=admition]').val());

        $.ajax({
            url: "controllers/residencies/create.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){

                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){

                    showAlertDialog(json.msg, false);

                    setTimeout(redirectToList, 2000);
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    showAlertDialog(json.msg, true);
                }
            }
        });
    });

    $("form#edit-residency").submit(function(e) {

        e.preventDefault();

        var form = $(this);

        if($('input[name="name"]', form).val() == ""){
            $('input[name="name"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('textarea[name="description"]', form).val() == ""){
            $('textarea[name="description"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('textarea[name="information"]', form).val() == ""){
            $('textarea[name="information"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('textarea[name="admition"]', form).val() == ""){
            $('textarea[name="admition"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        // Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('residency-id', $('input[name=residency-id]').val());
        valores.append('name', $('input[name=name]').val());
        valores.append('description', $('textarea[name=description]').val());
        valores.append('information', $('textarea[name=information]').val());
        valores.append('admition', $('textarea[name=admition]').val());

        $.ajax({
            url: "controllers/residencies/update.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){

                    showAlertDialog(json.msg, false);

                    setTimeout(redirectToList, 2000);


                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    showAlertDialog(json.msg, true);
                }
            },
            error    : function(retorno){
                alert("Error");
            }
        });
    });

    //Remover  
    $('#dataTable tbody').on('click', '.remove-residencia', function () {        
        var icone    = $(this);
        var id       = $('input', icone).val();
        var confirma = confirm('Tem certeza que deseja excluir este registro?');
        if(confirma){
            $.ajax({
                type     : 'POST',
                url      : 'controllers/residencies/delete.php',
                data     : {id : id},
                dataType : 'json',
                success  : function(data){
                    if(data.status == 1 || data.status == 2){
                        showAlertDialog(data.msg, false);
                        setTimeout(redirectToList, 2000);
                    }else{
                        showAlertDialog(data.msg, true);
                    }
                },
                error   : function(data){
                    alert(data.responseText);
                }
            });
        }
    });

    function redirectToList() {
      window.location.href="index.php?pg=lista-residencias"; 
    }
});