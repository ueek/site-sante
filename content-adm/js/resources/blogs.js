$(document).ready(function(){

    // $('#text').summernote(
    //     {
    //         height: 400,
    //         toolbar: [
    //           ['style', ['style']],
    //           ['font', ['bold', 'underline', 'clear']],
    //           ['fontname', ['fontname']],
    //           ['color', ['color']],
    //           ['para', ['ul', 'ol', 'paragraph']],
    //           ['insert', ['link', 'picture', 'video']],
    //           ['view', ['fullscreen', 'codeview', 'help']],
    //         ],
    //     }
    // );

    Simditor.locale = 'en-US';
    toolbar = ['title', 'bold', 'italic', 'underline', 'strikethrough', 'fontScale', 'color', '|', 'ol', 'ul', 'blockquote', 'code', 'table', '|', 'link', 'image', 'hr', '|', 'indent', 'outdent', 'alignment'];

    if($("#text").length > 0){
        var text = new Simditor({
            textarea: $('#text'),
            //optional options
            toolbar: toolbar,
            pasteImage: true
        });

    }

    $('#published_at').datetimepicker({
        sideBySide  : true,
        locale      : 'pt-br.js' 
    });

    //Cadastrar 
    $("form#add-blog").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        if($('input[name="title"]', form).val().trim() == ""){
            $('input[name="title"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('textarea[name="text"]', form).val().trim() == ""){
            $('textarea[name="text"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        var published_at = $("#published_at").find("input").val();
        if(published_at == ""){
            $('input[name="published_at"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }else{
            published_at = returnFormattedDate(published_at);
        }

       if (document.getElementById("image1").files.length == 0) {
            $('input[name="image1"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('title', $('input[name=title]').val());
        valores.append('author', $('input[name=author]').val());
        valores.append('text', $('textarea[name=text]').val());
        valores.append('published_at', published_at);
        valores.append('blog-category-id', $('select[name=blog-category-id]').val());
        valores.append('image1', $("#image1")[0].files[0]);

        $.ajax({
            url: "controllers/blogs/create.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){

                    showAlertDialog(json.msg, false);

                    setTimeout(redirectToList, 2000);
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    showAlertDialog(json.msg, true);
                }
            }
        });
    });

    $("form#edit-blog").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        if($('input[name="title"]', form).val().trim() == ""){
            $('input[name="title"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        // if($('input[name="subtitle"]', form).val().trim() == ""){
        //     $('input[name="subtitle"]', form).css({
        //         '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
        //         'box-shadow':'inset 0 0 0 1px #BE0007'
        //     }).focus();
        //     return false;
        // }

        // if($('input[name="author"]', form).val().trim() == ""){
        //     $('input[name="author"]', form).css({
        //         '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
        //         'box-shadow':'inset 0 0 0 1px #BE0007'
        //     }).focus();
        //     return false;
        // }    

        var published_at = $("#published_at").find("input").val();
        if(published_at == ""){
            $('input[name="published_at"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }else{
            published_at = returnFormattedDate(published_at);
        }    

        if($('textarea[name="text"]', form).val().trim() == ""){
            $('textarea[name="text"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('blog-id', $('input[name=blog-id]').val());
        valores.append('title', $('input[name=title]').val());
        valores.append('subtitle', $('input[name=subtitle]').val());
        valores.append('author', $('input[name=author]').val());
        valores.append('text', $('textarea[name=text]').val());
        valores.append('published_at', published_at);
        valores.append('blog-category-id', $('select[name=blog-category-id]').val());

        if(document.getElementById("image1").value != "") {
            valores.append('image1', $("#image1")[0].files[0]);
        }else{
            valores.append('image1-old', $('input[name=image1-old]').val());
        }

        $.ajax({
            url: "controllers/blogs/update.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){

                    showAlertDialog(json.msg, false);

                    setTimeout(redirectToList, 2000);


                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    showAlertDialog(json.msg, true);
                }
            },
            error    : function(retorno){
                alert("Error");
            }
        });
    });

    //Copiar URL IMAGENS 
    $( ".copiar-url-foto-blog" ).click(function() {
        var icone    = $(this);
        var id       = $('input', icone).val();
        
        var copyText = document.getElementById('url-image-' + id);
        copyText.select();
        document.execCommand("copy");

        showAlertDialog("Copiado para área de transferência!", true);

    });


    //Remover  
    $('#dataTable tbody').on('click', '.remove-blog', function () {        
        var icone    = $(this);
        var id       = $('input', icone).val();
        var confirma = confirm('Tem certeza que deseja excluir este registro?');
        if(confirma){
            $.ajax({
                type     : 'POST',
                url      : 'controllers/blogs/delete.php',
                data     : {id : id},
                dataType : 'json',
                success  : function(data){
                    if(data.status == 1 || data.status == 2){
                        showAlertDialog(data.msg, false);
                        setTimeout(redirectToList, 2000);
                    }else{
                        showAlertDialog(data.msg, true);
                    }
                },
                error   : function(data){
                    alert(data.responseText);
                }
            });
        }
    });

    function redirectToList() {
      window.location.href="index.php?pg=lista-blogs"; 
    }

    function redirectToPhotos() {
      location.reload();
    }

});