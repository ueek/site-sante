$(document).ready(function(){
    $("form#edit-about").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        if($('textarea[name="mission"]', form).val().trim() == ""){
            $('textarea[name="mission"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('textarea[name="vision"]', form).val().trim() == ""){
            $('textarea[name="vision"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('textarea[name="company_values"]', form).val().trim() == ""){
            $('textarea[name="company_values"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('textarea[name="insfrastructure"]', form).val().trim() == ""){
            $('textarea[name="insfrastructure"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('textarea[name="attendance"]', form).val().trim() == ""){
            $('textarea[name="attendance"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('textarea[name="team"]', form).val().trim() == ""){
            $('textarea[name="team"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="phone"]', form).val().trim() == ""){
            $('input[name="phone"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="email"]', form).val().trim() == ""){
            $('input[name="email"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="facebook"]', form).val().trim() == ""){
            $('input[name="facebook"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="instagram"]', form).val().trim() == ""){
            $('input[name="instagram"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="linkedin"]', form).val().trim() == ""){
            $('input[name="linkedin"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="youtube"]', form).val().trim() == ""){
            $('input[name="youtube"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }


        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('about-id', $('input[name=about-id]').val());
        valores.append('mission', $('textarea[name=mission]').val());
        valores.append('vision', $('textarea[name=vision]').val());
        valores.append('company_values', $('textarea[name=company_values]').val());
        valores.append('insfrastructure', $('textarea[name=insfrastructure]').val());
        valores.append('attendance', $('textarea[name=attendance]').val());
        valores.append('team', $('textarea[name=team]').val());
        valores.append('phone', $('input[name=phone]').val());
        valores.append('email', $('input[name=email]').val());
        valores.append('facebook', $('input[name=facebook]').val());
        valores.append('instagram', $('input[name=instagram]').val());
        valores.append('linkedin', $('input[name=linkedin]').val());
        valores.append('youtube', $('input[name=youtube]').val());

        if(document.getElementById("image_structure").value != "") {
            valores.append('image_structure', $("#image_structure")[0].files[0]);
        }else{
            valores.append('image_structure-old', $('input[name=image_structure-old]').val());
        }

        if(document.getElementById("image_attendance").value != "") {
            valores.append('image_attendance', $("#image_attendance")[0].files[0]);
        }else{
            valores.append('image_attendance-old', $('input[name=image_attendance-old]').val());
        }

        if(document.getElementById("image_team").value != "") {
            valores.append('image_team', $("#image_team")[0].files[0]);
        }else{
            valores.append('image_team-old', $('input[name=image_team-old]').val());
        }

        $.ajax({
            url: "controllers/about/update.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){

                    showAlertDialog(json.msg, false);

                    setTimeout(redirectToList, 2000);


                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    showAlertDialog(json.msg, true);
                }
            },
            error    : function(retorno){
                alert("Error");
            }
        });
    });

    function redirectToList() {
      window.location.href="index.php?pg=editar-institucional"; 
    }
});