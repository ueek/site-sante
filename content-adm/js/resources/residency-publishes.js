$(document).ready(function(){

    /* Cadastrar */
    $("form#add-publish").submit(function(e) {

        e.preventDefault();

        var form = $(this);

        if($('input[name="title"]', form).val() == ""){
            $('input[name="title"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if (document.getElementById("arquivo1").files.length == 0) {
            $('input[name="arquivo1"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        /* Loading */
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('residency_id', $('input[name=residency_id]').val());
        valores.append('title', $('input[name=title]').val());
        valores.append('arquivo1', $("#arquivo1")[0].files[0]);

        $.ajax({
            url: "controllers/residency-publishes/create.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){

                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){

                    showAlertDialog(json.msg, false);

                    setTimeout(redirectToList, 2000);
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    showAlertDialog(json.msg, true);
                }
            }
        });
    });

    $("form#edit-publish").submit(function(e) {

        e.preventDefault();

        var form = $(this);

        if($('input[name="title"]', form).val() == ""){
            $('input[name="title"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        // Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('publish_id', $('input[name=publish_id]').val());
        valores.append('residency_id', $('input[name=residency_id]').val());
        valores.append('title', $('input[name=title]').val());

        if(document.getElementById("arquivo1").value != "") {
            valores.append('arquivo1', $("#arquivo1")[0].files[0]);
        }else{
            valores.append('arquivo1-old', $('input[name=arquivo1-old]').val());
        }

        $.ajax({
            url: "controllers/residency-publishes/update.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){

                    showAlertDialog(json.msg, false);

                    setTimeout(redirectToList, 2000);


                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    showAlertDialog(json.msg, true);
                }
            },
            error    : function(retorno){
                alert("Error");
            }
        });
    });

    //Remover  
    $('#dataTable tbody').on('click', '.remove-edital', function () {        
        var icone    = $(this);
        var id       = $('input', icone).val();
        var confirma = confirm('Tem certeza que deseja excluir este registro?');
        if(confirma){
            $.ajax({
                type     : 'POST',
                url      : 'controllers/residency-publishes/delete.php',
                data     : {id : id},
                dataType : 'json',
                success  : function(data){
                    if(data.status == 1 || data.status == 2){
                        showAlertDialog(data.msg, false);
                        setTimeout(redirectToList, 2000);
                    }else{
                        showAlertDialog(data.msg, true);
                    }
                },
                error   : function(data){
                    alert(data.responseText);
                }
            });
        }
    });

    function redirectToList() {
      window.location.href="index.php?pg=lista-residencias"; 
    }
});