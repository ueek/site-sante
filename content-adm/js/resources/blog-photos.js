$(document).ready(function(){
    //Cadastrar IMAGENS 
    $("form#upload-blog-photo").submit(function(e) {
        e.preventDefault();

        var data = new FormData(this);

        if (document.getElementById("image1").files.length == 0) {
            $('input[name="image1"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        //Loading
        $('#imagens-upload').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('blog-id', $('input[name=blog-id]').val());
        valores.append('image1', $("#image1")[0].files[0]);

        $.ajax({
            url: "controllers/blog-photos/create.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                var json = $.parseJSON(retorno);

                if(json.status == 1){

                    showAlertDialog(json.msg, false);

                    setTimeout(redirectToList, 2000);
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    showAlertDialog(json.msg, true);
                }
            }
        });
    });

    //Remover IMAGENS 
    $( ".remover-foto-blog-post" ).click(function() {
        var icone    = $(this);
        var id       = $('input', icone).val();
        var confirma = confirm('Deseja realmente remover esta imagem? Esta ação é irreversível.');
        if(confirma){
            $.ajax({
                type     : 'POST',
                url      : 'controllers/blog-photos/delete.php',
                data     : {id : id},
                dataType : 'json',
                success  : function(data){
                    if(data.status == 1){
                        showAlertDialog(data.msg, false);

                        setTimeout(redirectToList, 2000);
                    }else{
                        $('#carregando').hide();
                        $('#fields').show();

                        showAlertDialog(data.msg, true);
                    }
                },
                error   : function(data){
                    alert(data.responseText);
                }
            });
        }
    });

    function redirectToList() {
      location.reload();
    }
});