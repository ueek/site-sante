$(document).ready(function(){

    $("form#edit-residency-partners").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = form.serialize();
        $.ajax({
            type     : 'POST',
            url      : 'controllers/residency-partners/update.php',
            data     : valores,
            dataType : 'json',
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');
                
                if(retorno.status == 1){
                    showAlertDialog(retorno.msg, false);

                    setTimeout(redirectToList, 2000);
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    showAlertDialog(retorno.msg, true);
                }

            }
        });
    });

    function redirectToList() {
      window.location.href="index.php?pg=lista-residencias"; 
    }

});