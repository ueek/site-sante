$(document).ready(function(){
    //Cadastrar 
    $("form#add-banner").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        if($('input[name="sequence"]', form).val() == ""){
            $('input[name="sequence"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if (document.getElementById("image1").files.length == 0) {
            $('input[name="image1"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if (document.getElementById("image2").files.length == 0) {
            $('input[name="image2"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }


        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('title', $('input[name=title]').val());
        valores.append('text', $('input[name=text]').val());
        valores.append('button_text', $('input[name=button_text]').val());
        valores.append('button_url', $('input[name=button_url]').val());
        valores.append('video_url', $('input[name=video_url]').val());
        valores.append('sequence', $('input[name=sequence]').val());
        valores.append('image1', $("#image1")[0].files[0]);
        valores.append('image2', $("#image2")[0].files[0]);

        $.ajax({
            url: "controllers/banners/create.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){

                    showAlertDialog(json.msg, false);

                    setTimeout(redirectToList, 2000);
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    showAlertDialog(json.msg, true);
                }
            }
        });
    });

    $("form#edit-banner").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        if($('input[name="sequence"]', form).val() == ""){
            $('input[name="sequence"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }


        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('banner-id', $('input[name=banner-id]').val());
        valores.append('title', $('input[name=title]').val());
        valores.append('text', $('input[name=text]').val());
        valores.append('button_text', $('input[name=button_text]').val());
        valores.append('button_url', $('input[name=button_url]').val());
        valores.append('video_url', $('input[name=video_url]').val());
        valores.append('sequence', $('input[name=sequence]').val());

        if(document.getElementById("image1").value != "") {
            valores.append('image1', $("#image1")[0].files[0]);
        }else{
            valores.append('image1-old', $('input[name=image1-old]').val());
        }

        if(document.getElementById("image2").value != "") {
            valores.append('image2', $("#image2")[0].files[0]);
        }else{
            valores.append('image2-old', $('input[name=image2-old]').val());
        }

        $.ajax({
            url: "controllers/banners/update.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){
                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){

                    showAlertDialog(json.msg, false);

                    setTimeout(redirectToList, 2000);


                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    showAlertDialog(json.msg, true);
                }
            },
            error    : function(retorno){
                alert("Error");
            }
        });
    });

    //Remover  
    $('#dataTable tbody').on('click', '.remove-banner', function () {        
        var icone    = $(this);
        var id       = $('input', icone).val();
        var confirma = confirm('Tem certeza que deseja excluir este registro?');
        if(confirma){
            $.ajax({
                type     : 'POST',
                url      : 'controllers/banners/delete.php',
                data     : {id : id},
                dataType : 'json',
                success  : function(data){
                    if(data.status == 1 || data.status == 2){
                        showAlertDialog(data.msg, false);
                        setTimeout(redirectToList, 2000);
                    }else{
                        showAlertDialog(data.msg, true);
                    }
                },
                error   : function(data){
                    alert(data.responseText);
                }
            });
        }
    });

    function redirectToList() {
      window.location.href="index.php?pg=lista-banners"; 
    }
});