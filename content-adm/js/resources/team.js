$(document).ready(function(){

    //Cadastrar 
    $("form#add-member").submit(function(e) {

        e.preventDefault();

        var form = $(this);

        if($('input[name="name"]', form).val() == ""){
            $('input[name="name"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="charge"]', form).val() == ""){
            $('input[name="charge"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if (document.getElementById("image1").files.length == 0) {
            $('input[name="image1"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        //Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('sequence', $('input[name=sequence]').val());
        valores.append('name', $('input[name=name]').val());
        valores.append('charge', $('input[name=charge]').val());
        valores.append('linkedin', $('input[name=linkedin]').val());
        valores.append('image1', $("#image1")[0].files[0]);

        $.ajax({
            url: "controllers/team/create.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){

                $('input', form).removeAttr('disabled');
                $('textarea', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){

                    showAlertDialog(json.msg, false);

                    setTimeout(redirectToList, 2000);
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    showAlertDialog(json.msg, true);
                }
            }
        });
    });

    $("form#edit-member").submit(function(e) {
        e.preventDefault();

        var form = $(this);

        if($('input[name="name"]', form).val() == ""){
            $('input[name="name"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        if($('input[name="charge"]', form).val() == ""){
            $('input[name="charge"]', form).css({
                '-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
                'box-shadow':'inset 0 0 0 1px #BE0007'
            }).focus();
            return false;
        }

        // Loading
        $('#fields').hide();
        $('#carregando').show();

        var valores = new FormData();
        valores.append('member-id', $('input[name=member-id]').val());
        valores.append('sequence', $('input[name=sequence]').val());
        valores.append('name', $('input[name=name]').val());
        valores.append('charge', $('input[name=charge]').val());
        valores.append('linkedin', $('input[name=linkedin]').val());
    
        if(document.getElementById("image1").value != "") {
            valores.append('image1', $("#image1")[0].files[0]);
        }else{
            valores.append('image1-old', $('input[name=image1-old]').val());
        }

        $.ajax({
            url: "controllers/team/update.php",
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
            success  : function(retorno){

                $('input', form).removeAttr('disabled');

                var json = $.parseJSON(retorno);

                if(json.status == 1){

                    showAlertDialog(json.msg, false);

                    setTimeout(redirectToList, 2000);


                }else{
                    $('#carregando').hide();
                    $('#fields').show();

                    showAlertDialog(json.msg, true);
                }
            },
            error    : function(retorno){
                alert("Error");
            }
        });
    });

    //Remover  
    $('#dataTable tbody').on('click', '.remove-membro', function () {        
        var icone    = $(this);
        var id       = $('input', icone).val();
        var confirma = confirm('Tem certeza que deseja excluir este registro?');
        if(confirma){
            $.ajax({
                type     : 'POST',
                url      : 'controllers/team/delete.php',
                data     : {id : id},
                dataType : 'json',
                success  : function(data){
                    if(data.status == 1 || data.status == 2){
                        showAlertDialog(data.msg, false);
                        setTimeout(redirectToList, 2000);
                    }else{
                        showAlertDialog(data.msg, true);
                    }
                },
                error   : function(data){
                    alert(data.responseText);
                }
            });
        }
    });

    function redirectToList() {
      window.location.href="index.php?pg=lista-equipe"; 
    }
});