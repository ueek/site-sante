$(document).ready(function(){

	//Cadastrar Gestor
	$('form#support').submit(function(e){
		e.preventDefault();

		var form = $(this);

		//Validações
		if($('input[name="name"]', form).val() == ""){
			$('input[name="name"]', form).css({
				'-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
				'box-shadow':'inset 0 0 0 1px #BE0007'
			}).focus();
			return false;
		}

		if($('input[name="email"]', form).val() == "" || !checkMail($('input[name="email"]', form).val())){
			$('input[name="email"]', form).css({
				'-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
				'box-shadow':'inset 0 0 0 1px #BE0007'
			}).focus();
			return false;
		}

		if($('textarea[name="description"]', form).val() == ""){
			$('textarea[name="description"]', form).css({
				'-webkit-box-shadow':'inset 0 0 0 1px #BE0007',
				'box-shadow':'inset 0 0 0 1px #BE0007'
			}).focus();
			return false;
		}

		var valores = form.serialize();
		
		$('input', form).attr('disabled', 'disabled');

		$.ajax({
			type     : 'POST',
			url      : 'controllers/support/send-email.php',
			data     : valores,
			dataType : 'json',
			success  : function(retorno){
				$('input', form).removeAttr('disabled');

                if(retorno.status == 1){

					showAlertDialog(retorno.msg, false);

                    setTimeout(redirectToList, 2000);
                }else{
                    $('#carregando').hide();
                    $('#fields').show();

					showAlertDialog(retorno.msg, true);
                }
			}
		});
	});

    function redirectToList() {
      window.location.href="index.php?pg=dashboard"; 
    }

});