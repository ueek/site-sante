<?php
    //External files
    include_once '../models/blog.php';
    include_once '../config/database.php';
    

    // Initialize Database
    $database = new Database();
    $db = $database->getConnection();

    // Create object and read the data
    $myBlog = new Blog($db);
    $listOfBlogs = $myBlog->readWithCategory();
   
?>

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Posts</h1>
<hr>
<p class="mb-4">Todos os dados cadastrados pelo gestor são exibidos aqui.</b> Para editar as informações, clique no botão amarelo. Para adicionar imagens à galeria do Post, clique no botão azul. Para remover, clique no botão vermelho.</p>

<a href='index.php?pg=adicionar-blog'>
    <button type='button' class='btn btn-success'>
        <i class='fa fa-plus'></i> Adicionar Post
    </button>
</a>

<br>
<br>

<!-- Data -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Posts</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" data-order='[[ 0, "asc" ]]' width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Título</th>
                        <th>Categoria</th>
                        <th>Autor</th>
                        <th>Data Publicação</th>
                        <th>Adicionado Em</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    if ($listOfBlogs->rowCount() > 0) {
                        $index=1;
                        while ($row = $listOfBlogs->fetch(PDO::FETCH_ASSOC)){

                            // Extracting the data
                            extract($row);

                            // Showing the rows

                            // Format date
                            $published_at = date("d/m/Y H:i:s", strtotime($published_at));
                            $created_at = date("d/m/Y H:i:s", strtotime($created_at));

                            echo "
                            <tr>
                                <td>{$index}</td>
                                <td>{$title}</td>
                                <td>{$category}</td>
                                <td>{$author}</td>
                                <td>{$published_at}</td>
                                <td>{$created_at}</td>
                                <td>
                                    <a href='index.php?pg=editar-blog&id={$id}'>
                                        <button type='button' class='btn btn-warning btn-circle'>
                                            <i class='fa fa-pencil-alt'></i>
                                        </button>
                                    </a>
                                    <a href='index.php?pg=editar-fotos-post&id={$id}' target='_blank'>
                                        <button type='button' class='btn btn-info btn-circle'>
                                            <i class='fa fa-image'></i>
                                        </button>
                                    </a>  
                                  <a href='https://santecancercenter.com.br/preview-post/{$url}'>
                                        <button type='button' class='btn btn-info btn-circle'>
                                            <i class='fa fa-eye'></i>
                                        </button>
                                    </a>  
                                    <button type='button' class='btn btn-danger btn-circle remove-blog'>
                                        <i class='fa fa-trash-alt'></i>
                                        <input type='hidden' value='{$id}' name='id'>
                                    </button>
                                </td>
                            </tr>
                            ";

                            $index++;
                        }
                    }
                ?>
              </tbody>
            </table>
        </div>
    </div>
</div>