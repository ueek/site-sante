<?php
    //External files
    include_once '../models/residency-publish.php';
    include_once '../config/database.php';
    

    // Initialize Database
    $database = new Database();
    $db = $database->getConnection();

    // Create object and read the data
    $myPublishes = new ResidencyPublish($db);
    $listOfPublishes = $myPublishes->read("AND residency_id = {$_GET['id']}");

    $id = $_GET['id'];
   
?>

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Editais</h1>
<hr>
<p class="mb-4">Para editar as informações, clique no botão amarelo. Para remover, clique no botão vermelho.</p>

<a href='index.php?pg=lista-residencias'>
    <button type='button' class='btn btn-info'>
        <i class='fa fa-chevron-left'></i> Voltar
    </button>
</a>

<a href='index.php?pg=adicionar-edital&id=<?php echo $id ?>'>
    <button type='button' class='btn btn-success'>
        <i class='fa fa-plus'></i> Adicionar Edital
    </button>
</a>

<br>
<br>

<!-- Data -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Editais</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Título</th>
                        <th>Adicionado Em</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    if ($listOfPublishes->rowCount() > 0) {
                        while ($row = $listOfPublishes->fetch(PDO::FETCH_ASSOC)){

                            // Extracting the data
                            extract($row);

                            // Showing the rows

                            // Format date
                            $created_at = date("d/m/Y H:i:s", strtotime($created_at));

                            echo "
                            <tr>
                                <td>{$title}</td>
                                <td>{$created_at}</td>
                                <td>
                                    <a href='index.php?pg=editar-edital&id={$id}'>
                                        <button type='button' class='btn btn-warning btn-circle'>
                                            <i class='fa fa-pencil-alt'></i>
                                        </button>
                                    </a>                                  
                                    <button type='button' class='btn btn-danger btn-circle remove-edital'>
                                        <i class='fa fa-trash-alt'></i>
                                        <input type='hidden' value='{$id}' name='id'>
                                    </button>
                                </td>
                            </tr>
                            ";
                        }
                    }
                ?>
              </tbody>
            </table>
        </div>
    </div>
</div>