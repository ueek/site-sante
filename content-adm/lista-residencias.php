<?php
    //External files
    include_once '../models/residency.php';
    include_once '../config/database.php';
    

    // Initialize Database
    $database = new Database();
    $db = $database->getConnection();

    // Create object and read the data
    $myResidencies = new Residency($db);
    $residencies = $myResidencies->read();
       
?>

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Residências</h1>
<hr>
<p class="mb-4">As residências adicionadas estão listadas abaixo. Para editar os médicos residentes, clique no botão verde. Para editar as informações do corpo acadêmico, no botão azul. Para editar os editais, no botão roxo. Para editar as informações da residência, clique no botão amarelo. Para remover, no botão vermelho.</p>

<a href='index.php?pg=adicionar-residencia'>
    <button type='button' class='btn btn-success'>
        <i class='fa fa-plus'></i> Adicionar Residência
    </button>
</a>

<br>
<br>

<!-- Data -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Residências</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Adicionado Em</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    if ($residencies->rowCount() > 0) {
                        while ($row = $residencies->fetch(PDO::FETCH_ASSOC)){

                            // Extracting the data
                            extract($row);

                            // Showing the rows

                            // Format date
                            $created_at = date("d/m/Y H:i:s", strtotime($created_at));

                            echo "
                            <tr>
                                <td>{$name}</td>
                                <td>{$created_at}</td>
                                <td>
                                    <a href='index.php?pg=editar-medicos-residentes&id={$id}'>
                                        <button type='button' class='btn btn-info btn-circle'>
                                            <i class='fas fa-stethoscope'></i>
                                        </button>
                                    </a>
                                    <a href='index.php?pg=lista-corpo-academico&id={$id}'>
                                        <button type='button' class='btn btn-primary btn-circle'>
                                            <i class='fas fa-user-friends'></i>
                                        </button>
                                    </a>
                                    <a href='index.php?pg=lista-editais&id={$id}'>
                                        <button type='button' class='btn btn-secondary btn-circle'>
                                            <i class='fas fa-file-alt'></i>
                                        </button>
                                    </a>
                                    <a href='index.php?pg=editar-instituicoes-parceiras&id={$id}'>
                                        <button type='button' class='btn btn-tertiary btn-circle'>
                                            <i class='fas fa-h-square'></i>
                                        </button>
                                    </a>
                                    <a href='index.php?pg=editar-residencia&id={$id}'>
                                        <button type='button' class='btn btn-warning btn-circle'>
                                            <i class='fa fa-pencil-alt'></i>
                                        </button>
                                    </a>
                                    <button type='button' class='btn btn-danger btn-circle remove-residencia'>
                                        <i class='fa fa-trash-alt'></i>
                                        <input type='hidden' value='{$id}' name='id'>
                                    </button>
                                </td>
                            </tr>
                            ";
                        }
                    }
                ?>
              </tbody>
            </table>
        </div>
    </div>
</div>