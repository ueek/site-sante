<?php
    //External files
    include_once '../models/doctors.php';
    include_once '../config/database.php';
    

    // Initialize Database
    $database = new Database();
    $db = $database->getConnection();

    // Create object and read the data
    $myDoctors = new Doctors($db);
    $members = $myDoctors->read();
   
?>

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Equipe Médica</h1>
<hr>
<p class="mb-4">Os membros da equipe médica adicionados estão listados abaixo. Para editar, clique no botão amarelo. Para remover, no botão vermelho.</p>

<a href='index.php?pg=adicionar-doutor'>
    <button type='button' class='btn btn-success'>
        <i class='fa fa-plus'></i> Adicionar membro
    </button>
</a>

<br>
<br>

<!-- Data -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Médicos</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Posição</th>
                        <th>Nome</th>
                        <th>Especialidade/Cargo</th>
                        <th>CRM</th>
                        <th>RQE</th>
                        <th>Adicionado Em</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    if ($members->rowCount() > 0) {
                        while ($row = $members->fetch(PDO::FETCH_ASSOC)){

                            // Extracting the data
                            extract($row);

                            // Showing the rows

                            // Format date
                            $created_at = date("d/m/Y H:i:s", strtotime($created_at));

                            echo "
                            <tr>
                                <td>{$sequence}</td>
                                <td>{$name}</td>
                                <td>{$charge}</td>
                                <td>{$crm}</td>
                                <td>{$rqe}</td>
                                <td>{$created_at}</td>
                                <td>
                                    <a href='index.php?pg=editar-doutor&id={$id}'>
                                        <button type='button' class='btn btn-warning btn-circle'>
                                            <i class='fa fa-pencil-alt'></i>
                                        </button>
                                    </a>
                                    <button type='button' class='btn btn-danger btn-circle remove-doutor'>
                                        <i class='fa fa-trash-alt'></i>
                                        <input type='hidden' value='{$id}' name='id'>
                                    </button>
                                </td>
                            </tr>
                            ";
                        }
                    }
                ?>
              </tbody>
            </table>
        </div>
    </div>
</div>