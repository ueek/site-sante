<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Novo Convênio do Centro de Mama</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="add-health-insurance-breast-center">
            <div id="fields">

                <!-- Título -->
                <div class="form-group">
                    <label>Nome*</label>
                    <input class="form-control" type="text" placeholder="Nome" name="name" maxlength="50">
                </div>

                <!-- Imagem -->
                <div class="form-group">
                    <label>Imagem (Dimensões recomendadas: 816x816)*</label><br>
                    <input type="file" name="image1" id="image1">
                </div>

            </div>
            
            <img src="img/loading.gif" id="carregando">

            <br>
            <br>
            
            <a href='index.php?pg=lista-convenios-centro-de-mama'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>