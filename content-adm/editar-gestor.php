<?php
	
    //Arquivos externos
    include_once '../models/admin.php';
    include_once '../config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myAdmin = new Admin($db);

    // Buscar dados
    $stmtAdmin = $myAdmin->readById($_GET['id']);


    if ($stmtAdmin->rowCount() > 0) {

	    $row = $stmtAdmin->fetch(PDO::FETCH_ASSOC);
		extract($row);
		$myAdmin->setId($id);
		$myAdmin->setName($name);
		$myAdmin->setEmail($email);
		$myAdmin->setPassword($password);
    }



?>
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Editar Gestor</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="editar-gestor">
        	<input autocomplete="off" type="text" style="display:none;">
			<input type="hidden" value="<?php  echo $myAdmin->getId() ?>" name="admin-id">

            <div id="fields">
                <!-- Name -->
                <div class="form-group">
                    <label>Nome*</label>
                    <input class="form-control" type="text" placeholder="Nome" name="nome" value="<?php  echo $myAdmin->getName() ?>">
                </div>

                <!-- Email -->
                <div class="form-group">
                    <label>E-mail*</label>
                    <input class="form-control" type="text" placeholder="E-mail" name="email" value="<?php  echo $myAdmin->getEmail() ?>">
                </div>

                <hr>
                <strong>Preencha somente se quiser alterar a senha.</strong>
                <br><br>

                <!-- Password -->
                <div class="form-group">
                    <label>Senha*</label>
                    <input class="form-control" type="password" placeholder="Senha" name="senha">
                </div>

                <!-- Password -->
                <div class="form-group">
                    <label>Confirme a Senha (digite novamente)*</label>
                    <input class="form-control" type="password" placeholder="Senha" name="senha_confirmacao">
                </div>
            </div>

            <img src="img/loading.gif" id="carregando">

            <br>
            <br>
            <a href='index.php?pg=lista-gestores'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>