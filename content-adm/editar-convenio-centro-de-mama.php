<?php
    
    //Arquivos externos
    include_once '../models/health-insurance-breast-center.php';
    include_once '../config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myInsurance = new HealthInsuranceBreastCenter($db);

    // Buscar dados do convênio
    $stmtInsurance = $myInsurance->readById($_GET['id']);

    $urlImages = "uploads/health-insurance-breast-center/"; 

    if ($stmtInsurance->rowCount() > 0) {
        $row = $stmtInsurance->fetch(PDO::FETCH_ASSOC);
        extract($row);
        $myInsurance->setId($id);
        $myInsurance->setName($name);
        $myInsurance->setImage($image);
        $image1 = $urlImages.$image;
                    
    }



?>
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Editar Convênio do Centro de Mama</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="edit-insurance-breast-center">
        	<input autocomplete="off" type="text" style="display:none;">
			<input type="hidden" value="<?php  echo $myInsurance->getId() ?>" name="insurance-id">

            <div id="fields">


                        <!-- Nome -->
                        <div class="form-group">
                            <label>Nome*</label>
                            <input class="form-control" type="text" placeholder="Nome" name="name" value="<?php  echo $myInsurance->getName() ?>">
                        </div>

                        <!-- Imagem -->
                        <label>Imagem (Dimensões recomendadas: 816x816)*</label><br>
                        <input type="file" name="image1" id="image1">

                        <input type="hidden" value="<?php  echo $myInsurance->getImage() ?>" name="image1-old">

                        <?php 

                            if ($image1 != "") {
                                echo "
                                    <br><br>
                                    <div class='thumb' style='background: url($image1) no-repeat center; background-size: cover;'>
                                    </div>
                                ";
                            }
                        ?>

            </div>

            <img src="img/loading.gif" id="carregando">
            
            <br>
            <br>
            <a href='index.php?pg=lista-convenios-centro-de-mama'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>