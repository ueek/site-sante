<?php
	
	    //Arquivos externos
    include_once '../models/blog.php';
    include_once '../models/blog-photo.php';
    include_once '../config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myBlog = new Blog($db);

    // Buscar dados do post
    $stmtBlog = $myBlog->readById($_GET['id']);

    if ($stmtBlog->rowCount() > 0) {
        $row = $stmtBlog->fetch(PDO::FETCH_ASSOC);
        extract($row);
        $myBlog->setId($id);
        $myBlog->setTitle($title);
    }

    $myBlogPhoto = new BlogPhoto($db);

    $listOfBlogPhotos = $myBlogPhoto->read("AND blog_id = {$_GET['id']}");

    $urlImages = "http://lesante-site.cliente.ueek.com.br/content-adm/uploads/blogs/"; 
?>

<div class="row">
    <div class="col-lg-12">

    	<?php
    		echo 
    		"
        		<h1 class='page-header'>Imagens Blog - {$myBlog->getTitle()}</h1>
    			<hr>
    			<br>
    		";

    	?>
    </div>
</div>
<div class="row pd-plus">
    <div class="col-lg-6">
		<form id="upload-blog-photo" enctype="multipart/form-data">
			<input type="hidden" value="<?php  echo $myBlog->getId() ?>" name="blog-id">
			<div id="imagens-upload">
				<div class="form-group">
					<div class="form-group">
						<label>Imagem*</label><br>
						<input type="file" name="image1" id="image1">
            			<br>
            			<br>
            			<input type="submit" class="btn btn-success direita" value="Upload" id="botao">

						<hr>

						<label>Imagens do Blog</label><br>

						<?php 


						    if ($listOfBlogPhotos->rowCount() > 0) {
		                        while ($blog_photo = $listOfBlogPhotos->fetch(PDO::FETCH_ASSOC)){

		                            // Extracting the data
		                            extract($blog_photo);

		                            $image = $urlImages.$image;

		                            echo 
		                            "
										<div class='thumb' style='background: url($image) no-repeat center; background-size: cover;'>

											<button type='button' class='btn btn-danger btn-circle btn-sm btn-remove-image remover-foto-blog-post'>
							                    <span class='icon'>
							                      <i class='fas fa-times'></i>
							                    </span>
												<input type='hidden' value='$id' name='id_foto'>
											</button> 

											<button type='button' class='btn btn-primary btn-icon-split btn-sm btn-copy-image-url copiar-url-foto-blog'>
							                    <span class='icon text-white-50'>
							                      <i class='fas fa-copy'></i>
							                    </span>
												<span class='text'>Copiar URL</span>
												<input type='hidden' value='{$id}''>
											</button> 

											<input class='input-url-to-copy' type='input' value='{$image}' id='url-image-{$id}'>

				                		</div> 


		                            ";

								}
						    }else{
						    	echo "Nenhuma imagem adicionada.";
						    }
						?>
					<div>
					</div>

					</div>
				</div>

			</div>



			<img src="img/loading.gif" id="carregando">

			<br><br>
			<a href='index.php?pg=lista-blogs'>
				<button type='button' class='btn btn-info'>
					<i class='fa fa-chevron-left'></i>
					Voltar 
				</button>
			</a>
            <br><br>
		</form>

	</div>
</div>