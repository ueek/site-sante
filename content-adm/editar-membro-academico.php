<?php
    
    //Arquivos externos
    include_once '../models/residency-academic-team.php';
    include_once '../config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myResidencyTeam = new ResidencyAcademicTeam($db);

    // Buscar dados da equipe
    $stmtResidencyTeam = $myResidencyTeam->readById($_GET['id']);

    $urlImages = "uploads/academic-team/"; 

    if ($stmtResidencyTeam->rowCount() > 0) {
        $row = $stmtResidencyTeam->fetch(PDO::FETCH_ASSOC);
        extract($row);
        $myResidencyTeam->setId($id);
        $myResidencyTeam->setName($name);
        $myResidencyTeam->setCharge($charge);
        $myResidencyTeam->setCrm($crm);
        $myResidencyTeam->setRqe($rqe);
        $myResidencyTeam->setLinkedin($linkedin);
        $myResidencyTeam->setSector($sector);
        $myResidencyTeam->setImage($image);
        $image1 = $urlImages.$image;
                    
    }

?>
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Editar Membro do Corpo Acadêmico</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="edit-academic-member">
            <input autocomplete="off" type="text" style="display:none;">
            
            <input type="hidden" name="residencies_id" value="<?php echo $_GET['id'] ?>">
            <input type="hidden" name="academic-member-id" value="<?php  echo $myResidencyTeam->getId() ?>">

            <div id="fields">


                        <!-- Nome -->
                        <div class="form-group">
                            <label>Nome*</label>
                            <input class="form-control" type="text" placeholder="Nome" name="name" value="<?php  echo $myResidencyTeam->getName() ?>">
                        </div>

                        <!-- Cargo -->
                        <div class="form-group">
                            <label>Cargo</label>
                            <input class="form-control" type="text" placeholder="Cargo" name="charge" value="<?php  echo $myResidencyTeam->getCharge() ?>">
                        </div>

                        <!-- CRM -->
                        <div class="form-group">
                            <label>CRM</label>
                            <input class="form-control" type="text" placeholder="CRM" name="crm" value="<?php  echo $myResidencyTeam->getCrm() ?>">
                        </div>

                        <!-- RQE -->
                        <div class="form-group">
                            <label>RQE</label>
                            <input class="form-control" type="text" placeholder="RQE" name="rqe" value="<?php  echo $myResidencyTeam->getRqe() ?>">
                        </div>

                        <!-- LinkedIn -->
                        <div class="form-group">
                            <label>LinkedIn*</label>
                            <input class="form-control" type="text" placeholder="Link/URL do LinkedIn" name="linkedin" value="<?php  echo $myResidencyTeam->getLinkedin() ?>">
                        </div>

    
                        <!-- Setor -->
                        <div class="form-group">
                            <label>Setor*</label>
                            <select class="form-control" name="sector">
                                <option <?php echo $myResidencyTeam->getSector() == 1 ? 'selected' : '' ?> value="1">Coordenação do Programa</option>
                                <option <?php echo $myResidencyTeam->getSector() == 2 ? 'selected' : '' ?> value="2">Preceptores</option>
                                <option <?php echo $myResidencyTeam->getSector() == 3 ? 'selected' : '' ?> value="3">Coordenação Pedagógica</option>
                            </select>
                        </div>

                        <!-- Imagem -->
                        <label>Imagem (Dimensões recomendadas: 200x200)*</label><br>
                        <input type="file" name="image1" id="image1">

                        <input type="hidden" value="<?php  echo $myResidencyTeam->getImage() ?>" name="image1-old">

                        <?php 

                            if ($image1 != "") {
                                echo "
                                    <br><br>
                                    <div class='thumb' style='background: url($image1) no-repeat center; background-size: cover;'>
                                    </div>
                                ";
                            }
                        ?>

            </div>

            <img src="img/loading.gif" id="carregando">
            
            <br>
            <br>
            <a href='index.php?pg=lista-corpo-academico&id=<?php echo $_GET['id'] ?>'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>