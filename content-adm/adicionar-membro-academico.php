<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Novo Membro do Corpo Acadêmico</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="add-academic-member">
            <div id="fields">
                <input type="hidden" name="residencies_id" value="<?php echo $_GET['id'] ?>">

                <!-- Nome -->
                <div class="form-group">
                    <label>Nome*</label>
                    <input class="form-control" type="text" placeholder="Nome" name="name" maxlength="50">
                </div>

                <!-- Especialidade Médica -->
                <div class="form-group">
                    <label>Especialidade Médica</label>
                    <input class="form-control" type="text" placeholder="Especialidade Médica" name="charge" maxlength="80">
                </div>

                <!-- CRM -->
                <div class="form-group">
                    <label>CRM</label>
                    <input class="form-control" type="text" placeholder="CRM" name="crm" maxlength="50">
                </div>

                <!-- RQE -->
                <div class="form-group">
                    <label>RQE</label>
                    <input class="form-control" type="text" placeholder="RQE" name="rqe" maxlength="50">
                </div>

                <!-- Perfil no LinkedIn -->
                <div class="form-group">
                    <label>LinkedIn*</label>
                    <input class="form-control" type="text" placeholder="URL/Link do Perfil no LinkedIn" name="linkedin" maxlength="100">
                </div>

                <!-- Setor -->
                <div class="form-group">
                    <label>Setor*</label>
                    <select class="form-control" name="sector">
                        <option value="1">Coordenação do Programa</option>
                        <option value="2">Preceptores</option>
                        <option value="3">Coordenação Pedagógica</option>
                    </select>
                </div>

                <!-- Imagem -->
                <div class="form-group">
                    <label>Imagem (Dimensões recomendadas: 200x200)*</label><br>
                    <input type="file" name="image1" id="image1">
                </div>

            </div>
            
            <img src="img/loading.gif" id="carregando">

            <br>
            <br>
            
            <a href='index.php?pg=lista-corpo-academico&id=<?php echo $_GET['id']?>'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>