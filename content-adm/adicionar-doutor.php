<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Novo Membro da Equipe Médica</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="add-doctor">
            <div id="fields">

                <!-- Posição na ordenação -->
                <div class="form-group">
                    <label>Posição na ordenação</label>
                    <input class="form-control" type="text" placeholder="Posição na ordenação de médicos no site" name="sequence">
                </div>

                <!-- Nome -->
                <div class="form-group">
                    <label>Nome*</label>
                    <input class="form-control" type="text" placeholder="Nome" name="name" maxlength="50">
                </div>

                <!-- Especialidade Médica -->
                <div class="form-group">
                    <label>Especialidade Médica*</label>
                    <input class="form-control" type="text" placeholder="Especialidade Médica" name="charge" maxlength="80">
                </div>

                <!-- CRM -->
                <div class="form-group">
                    <label>CRM</label>
                    <input class="form-control" type="text" placeholder="CRM" name="crm" maxlength="50">
                </div>

                <!-- RQE -->
                <div class="form-group">
                    <label>RQE</label>
                    <input class="form-control" type="text" placeholder="RQE" name="rqe" maxlength="50">
                </div>

                <!-- Perfil no LinkedIn -->
                <div class="form-group">
                    <label>LinkedIn</label>
                    <input class="form-control" type="text" placeholder="URL/Link do Perfil no LinkedIn" name="linkedin" maxlength="100">
                </div>

                <!-- Formação -->
                <div class="form-group">
                    <label>Formação</label>
                    <input class="form-control" type="text" placeholder="Formação Principal" name="graduation" maxlength="50">
                </div>

                <!-- Ano de Conclusão -->
                <div class="form-group">
                    <label>Ano de Conclusão</label>
                    <input class="form-control" type="text" placeholder="Ano de Conclusão da Formação Principal" name="graduation_year" maxlength="50">
                </div>

                <!-- Instituição de Graduação -->
                <div class="form-group">
                    <label>Instituição de Formação</label>
                    <input class="form-control" type="text" placeholder="Instituição de Formação" name="graduation_institution" maxlength="80">
                </div>

                <!-- Efetivo/Residente -->
                <div class="form-group">
                    <label>Situação atual*</label>
                    <select class="form-control" name="effective">
                        <option value="1">Efetivo/Contratado</option>
                        <option value="0">Residente</option>
                    </select>
                </div>

                <!-- Imagem -->
                <div class="form-group">
                    <label>Imagem (Dimensões recomendadas: 200x200)*</label><br>
                    <input type="file" name="image1" id="image1">
                </div>

            </div>
            
            <img src="img/loading.gif" id="carregando">

            <br>
            <br>
            
            <a href='index.php?pg=lista-doutores'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>