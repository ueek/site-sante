<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Novo Gestor</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="adicionar-gestor">
            <div id="fields">

                <div class="form-group">
                    <label>Nome*</label>
                    <input class="form-control" type="text" placeholder="Nome" name="nome">
                </div>

                <div class="form-group">
                    <label>E-mail*</label>
                    <input class="form-control" type="text" placeholder="E-mail" name="email">
                </div>

                <div class="form-group">
                    <label>Senha*</label>
                    <input class="form-control" name="senha" id="senha" placeholder="Senha" type="password"  readonly onfocus="this.removeAttribute('readonly');" />
                </div>

                <div class="form-group">
                    <label>Confirme a Senha (digite novamente)*</label>
                    <input class="form-control" name="senha_confirmacao" id="senha" placeholder="Senha" type="password"  readonly onfocus="this.removeAttribute('readonly');" />
                </div>
            </div>

            <img src="img/loading.gif" id="carregando">

            <br>
            <br>
            <a href='index.php?pg=lista-gestores'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>