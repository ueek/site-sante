<?php
    
    //Arquivos externos
    include_once '../models/service.php';
    include_once '../config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myServices = new Service($db);

    // Buscar dados da equipe
    $stmtServices = $myServices->readById($_GET['id']);

    $urlImages = "uploads/services/"; 

    if ($stmtServices->rowCount() > 0) {
        $row = $stmtServices->fetch(PDO::FETCH_ASSOC);
        extract($row);
        $myServices->setId($id);
        $myServices->setName($name);
        $myServices->setSubCategories($sub_categories);
        $myServices->setImage($image);
        $image1 = $urlImages.$image;
                    
    }

?>
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Editar Serviço</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="edit-service">
        	<input autocomplete="off" type="text" style="display:none;">
			<input type="hidden" value="<?php  echo $myServices->getId() ?>" name="service-id">

            <div id="fields">

                <!-- Título -->
                <div class="form-group">
                    <label>Título*</label>
                    <input class="form-control" type="text" placeholder="Título" name="name" value="<?php  echo $myServices->getName() ?>">
                </div>

                <!-- Segmentos -->
                <div class="form-group">
                    <label>Segmentos*</label>
                    <input class="form-control" type="text" placeholder="Segmentos" name="sub_categories" value="<?php  echo $myServices->getSubCategories() ?>">
                </div>

                <!-- Imagem -->
                <label>Imagem (Dimensões recomendadas: 350x230)*</label><br>
                <input type="file" name="image1" id="image1">

                <input type="hidden" value="<?php  echo $myServices->getImage() ?>" name="image1-old">

                <?php 

                    if ($image1 != "") {
                        echo "
                            <br><br>
                            <div class='thumb' style='background: url($image1) no-repeat center; background-size: cover;'>
                            </div>
                        ";
                    }
                ?>

            </div>

            <img src="img/loading.gif" id="carregando">
            
            <br>
            <br>
            <a href='index.php?pg=lista-servicos'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>