<?php
	
    // Arquivos externos
    include_once '../models/residency.php';
    include_once '../models/residency-partner.php';
    include_once '../config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myResidency = new Residency($db);
    $stmtResidency = $myResidency->readById($_GET['id']);
    if ($stmtResidency->rowCount() > 0) {

	    $row = $stmtResidency->fetch(PDO::FETCH_ASSOC);
		extract($row);
		$myResidency->setId($id);
        $myResidency->setName($name);
    }

    $myPartners = new ResidencyPartner($db);
    $stmtPublish = $myPartners->readById($_GET['id']);

    if ($stmtPublish->rowCount() > 0) {
        $row = $stmtPublish->fetch(PDO::FETCH_ASSOC);
        extract($row);
        $myPartners->setId($id);
        $myPartners->setResidencyId($residency_id);
        $myPartners->setInstitutions($institutions);
    }

?>
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Instituições Parceiras - <?php echo $myResidency->getName() ?></h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="edit-residency-partners">
        	<input autocomplete="off" type="text" style="display:none;">

            <input type="hidden" value="<?php echo $myResidency->getId() ?>" name="residency-id">
			<input type="hidden" value="<?php echo $myPartners->getId() ?>" name="partner-id">

            <div id="fields">

                <!-- Instituições Parceiras -->
                <div class="form-group">
                    <label>Instituições Parceiras*</label>
                    <input class="form-control" type="text" placeholder="Insira as instituições separadas por ponto e vírgula. Ex: Hospital Albert Einstein; Hospital Tereza Ramos" name="institutions" value="<?php  echo $myPartners->getInstitutions() ?>">
                </div>

            </div>

            <img src="img/loading.gif" id="carregando">

            <br>
            <br>

            <a href='index.php?pg=lista-residencias'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>