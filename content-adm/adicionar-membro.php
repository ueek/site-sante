<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Novo Membro da Equipe</h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="add-member">
            <div id="fields">

                <!-- Ordem de aparição -->
                <div class="form-group">
                    <label>Ordem*</label>
                    <input class="form-control" type="number" placeholder="Ordem de aparição" name="sequence">
                </div>

                <!-- Nome -->
                <div class="form-group">
                    <label>Nome*</label>
                    <input class="form-control" type="text" placeholder="Nome" name="name" maxlength="50">
                </div>

                <!-- Cargo -->
                <div class="form-group">
                    <label>Cargo*</label>
                    <input class="form-control" type="text" placeholder="Cargo" name="charge" maxlength="50">
                </div>

                <!-- Perfil no LinkedIn -->
                <div class="form-group">
                    <label>LinkedIn</label>
                    <input class="form-control" type="text" placeholder="URL/Link do Perfil no LinkedIn" name="linkedin" maxlength="50">
                </div>

                <!-- Imagem -->
                <div class="form-group">
                    <label>Imagem (Dimensões recomendadas: 200x200)*</label><br>
                    <input type="file" name="image1" id="image1">
                </div>

            </div>
            
            <img src="img/loading.gif" id="carregando">

            <br>
            <br>
            
            <a href='index.php?pg=lista-equipe'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>

            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>