<?php
	
    // Arquivos externos
    include_once '../models/doctors.php';
    include_once '../models/residency.php';
    include_once '../models/residency-doctor.php';
    include_once '../config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myResidency = new Residency($db);
    $stmtResidency = $myResidency->readById($_GET['id']);
    if ($stmtResidency->rowCount() > 0) {

	    $row = $stmtResidency->fetch(PDO::FETCH_ASSOC);
		extract($row);
		$myResidency->setId($id);
        $myResidency->setName($name);
    }

    // Ler médicos residentes
    $myDoctors = new Doctors($db);
    $listOfDoctors = $myDoctors->read("AND effective = 0", "name");


?>
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Médicos Residentes - <?php echo $myResidency->getName() ?></h1>
<hr>

<div class="row pd-plus">
    <div class="col-lg-6">
        <form id="edit-residency-doctors">
        	<input autocomplete="off" type="text" style="display:none;">
			<input type="hidden" value="<?php echo $myResidency->getId() ?>" name="residency-id">

            <div id="fields">

                <?php

                    while ($row = $listOfDoctors->fetch(PDO::FETCH_ASSOC)){
                        extract($row);

                            // Ler vestibulares que o estudante passou
                            $myResidencyDoctors = new ResidencyDoctor($db);

                            // checar se o vestibular foi selecionado
                            $selectedDoctors = $myResidencyDoctors->read("AND residency_id = {$_GET['id']} AND clinic_team_id = {$id}");

                            if ($selectedDoctors->rowCount() > 0) {
                                $checked = "checked='checked'";
                            }else{
                                $checked = "";
                            }

                        echo"
                            <div class='form-group'>
                                <label class='checkbox-inline'>
                                <input type='checkbox' name='doctor-{$id}' id='doctor-{$id}' {$checked}>
                                {$name} / {$charge}
                            </div> 
                        ";
                        
                    }

                ?>


            </div>

            <img src="img/loading.gif" id="carregando">

            <br>
            <br>
            <a href='index.php?pg=lista-residencias'>
                <button type='button' class='btn btn-info'>
                    <i class='fa fa-chevron-left'></i> Voltar
                </button>
            </a>
            <input type="submit" class="btn btn-success direita" value="Salvar" id="botao">

            <br>
            <br>
        </form>
    </div>
</div>