<?php
    //Arquivos externos

    include_once '../../../models/log.php';
    include_once '../../../models/residency-publish.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/upload-file.php';
    include_once '../../../funcoes/salvar-log.php';

	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myPublish = new ResidencyPublish($db);

	// Upload imagem 1
	// ----------------------------------------------------------------------------------------------------------------------

	if (!isset($_POST['arquivo1-old'])) {

		$arquivo1 = uploadArquivo("arquivo1", "../../uploads/publishes/");
		if($arquivo1 == 1) {
			$retorno = array("status" => 0, "msg" => "O arquivo é muito grande.");
			echo json_encode($retorno);
			exit;
		}else if($arquivo1 == 2){
			$retorno = array("status" => 0, "msg" => "O arquivo possui uma extensão não compatível. Apenas .doc, .docx, .xls, .xlsx e .pdf são permitidos.");
			echo json_encode($retorno);
			exit;
		}else if($arquivo1 == 3 || $arquivo1 == 4){
			$arquivo1 = "";
		}

	}else{
		$arquivo1 = $_POST['arquivo1-old'];
	}

	$myPublish->setId($_POST['publish_id']);
	$myPublish->setResidencyId($_POST['residency_id']);
	$myPublish->setTitle($_POST['title']);
	$myPublish->setUrl($arquivo1);
	
	if (!$myPublish->update()){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao alterar, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
    	saveLog("Alterou o edital da residência médica de ID {$myPublish->getId()}");
		$retorno = array("status" => 1, "msg" => "Item alterado com sucesso!");
		echo json_encode($retorno);
		exit;
	}

?>
