<?php
    //Arquivos externos

    include_once '../../../models/log.php';
    include_once '../../../models/residency-publish.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/upload-file.php';
    include_once '../../../funcoes/salvar-log.php';
	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myPublish = new ResidencyPublish($db);

    // Upload arquivo
	// ----------------------------------------------------------------------------------------------------------------------
	$arquivo1 = uploadArquivo("arquivo1", "../../uploads/publishes/");
	if($arquivo1 == 1) {
		$retorno = array("status" => 0, "msg" => "O arquivo é muito grande.");
		echo json_encode($retorno);
		exit;
	}else if($arquivo1 == 2){
		$retorno = array("status" => 0, "msg" => "O arquivo possui uma extensão não compatível. Apenas .doc, .docx, .xls, .xlsx e .pdf são permitidos.");
		echo json_encode($retorno);
		exit;
	}else if($arquivo1 == 3 || $arquivo1 == 4){
		$arquivo1 = "";
	}

	$myPublish->setResidencyId($_POST['residency_id']);
	$myPublish->setTitle($_POST['title']);
	$myPublish->setUrl($arquivo1);

	if (!$myPublish->create()){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao cadastrar, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
    	saveLog("Adicionou um novo edital da residência médica");
		$retorno = array("status" => 1, "msg" => "Item cadastrado com sucesso!");
		echo json_encode($retorno);
		exit;
	}

?>
