<?php
    //Arquivos externos
    include_once '../../../models/log.php';
    include_once '../../../models/blog-category.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/salvar-log.php';

	//==============================================================//
	
    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    //Instancia objeto
    $myBlogCategory = new BlogCategory($db);

    //Trata variáveis
    $name  = trim($_POST['name']);

    //Verificações caso o javascript não esteja funcionando corretamente
	if ($name == "") {
		$retorno = array("status" => 0, "msg" => "Você deve digitar um Nome.");
		echo json_encode($retorno);
		exit;
	}else{
		//Seta campo
		$myBlogCategory->setName($name);
	}

	//Inserção
	if (!$myBlogCategory->create()){
		$retorno = array("status" => 0, "msg" => "Ocorreu um erro ao adicionar. Tente novamente mais tarde.");
		echo json_encode($retorno);
		exit;
	}else{
    	saveLog("Adicionou uma nova categoria de blog");
		$retorno = array("status" => 1, "msg" => "Adicionado com sucesso!");
		echo json_encode($retorno);
		exit;
	}
?>
