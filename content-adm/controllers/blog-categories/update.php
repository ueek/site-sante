<?php
    //Arquivos externos
    include_once '../../../models/log.php';
    include_once '../../../models/blog-category.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/salvar-log.php';
	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myBlogCategory = new BlogCategory($db);

    //Trata variáveis
    $name  = trim($_POST['name']);

    //Seta campo
	$myBlogCategory->setId($_POST['blog-category-id']);

	//Verificações caso o javascript não esteja funcionando corretamente
	if ($name == "") {
		$retorno = array("status" => 0, "msg" => "Você deve digitar um nome.");
		echo json_encode($retorno);
		exit;
	}else{
		//Seta campo
		$myBlogCategory->setName($name);
	}

	//Atualização
	if (!$myBlogCategory->update()){
		$retorno = array("status" => 0, "msg" => "Ocorreu um erro ao atualizar, tente novamente mais tarde.");
		echo json_encode($retorno);
		exit;
	}else{
    	saveLog("Alterou a categoria de blog de ID {$myBlogCategory->getId()}");
		$retorno = array("status" => 1, "msg" => "Atualizado com sucesso!");
		echo json_encode($retorno);
		exit;
	}

?>
