<?php
    //Arquivos externos
    include_once '../../../models/log.php';
    include_once '../../../models/about.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/upload-imagens.php';
    include_once '../../../funcoes/salvar-log.php';
	//==============================================================//

    // Upload Images

    // ESTRUTURA
    if (!isset($_POST['image_structure-old'])) {
        $image_structure = uploadImagem ("image_structure", "../../uploads/about/");
        if($image_structure == 1) {
            $retorno = array("status" => 0, "msg" => "Imagem Estrutura muito grande");
            echo json_encode($retorno);
            exit;
        }else if($image_structure == 2){
            $retorno = array("status" => 0, "msg" => "Imagem Estrutura possui uma extensão diferente de png, jpg, jpeg e gif");
            echo json_encode($retorno);
            exit;
        }else if($image_structure == 3 || $image_structure == 4){
            $image_structure = "";
        }

    }else{
        $image_structure = $_POST['image_structure-old'];
    }

    //  ATENDIMENTO
    if (!isset($_POST['image_attendance-old'])) {
        $image_attendance = uploadImagem ("image_attendance", "../../uploads/about/");
        if($image_attendance == 1) {
            $retorno = array("status" => 0, "msg" => "Imagem Atendimento muito grande");
            echo json_encode($retorno);
            exit;
        }else if($image_attendance == 2){
            $retorno = array("status" => 0, "msg" => "Imagem Atendimento possui uma extensão diferente de png, jpg, jpeg e gif");
            echo json_encode($retorno);
            exit;
        }else if($image_attendance == 3 || $image_attendance == 4){
            $image_attendance = "";
        }

    }else{
        $image_attendance = $_POST['image_attendance-old'];
    }

            
    // EQUIPE
    if (!isset($_POST['image_team-old'])) {
        $image_team = uploadImagem ("image_team", "../../uploads/about/");
        if($image_team == 1) {
            $retorno = array("status" => 0, "msg" => "Imagem Equipe muito grande");
            echo json_encode($retorno);
            exit;
        }else if($image_team == 2){
            $retorno = array("status" => 0, "msg" => "Imagem Equipe possui uma extensão diferente de png, jpg, jpeg e gif");
            echo json_encode($retorno);
            exit;
        }else if($image_team == 3 || $image_team == 4){
            $image_team = "";
        }

    }else{
        $image_team = $_POST['image_team-old'];
    }


    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myAbout = new About($db);

	// $retorno = array("status" => 0, "msg" => $image2);
	// echo json_encode($retorno);
	// exit;
		
    //Seta campo
	$myAbout->setId($_POST['about-id']);
    $myAbout->setMission($_POST['mission']);
    $myAbout->setVision($_POST['vision']);
    $myAbout->setCompanyValues($_POST['company_values']);
    $myAbout->setInfrastructure($_POST['insfrastructure']);
    $myAbout->setAttendance($_POST['attendance']);
    $myAbout->setTeam($_POST['team']);
    $myAbout->setPhone($_POST['phone']);
    $myAbout->setEmail($_POST['email']);
    $myAbout->setFacebook($_POST['facebook']);
    $myAbout->setInstagram($_POST['instagram']);
    $myAbout->setLinkedin($_POST['linkedin']);
    $myAbout->setYoutube($_POST['youtube']);
    $myAbout->setImageStructure($image_structure);
    $myAbout->setImageAttendance($image_attendance);
    $myAbout->setImageTeam($image_team);

	//Atualização
	if (!$myAbout->update()){
		$retorno = array("status" => 0, "msg" => "Ocorreu um erro ao atualizar, tente novamente mais tarde.");
		echo json_encode($retorno);
		exit;
	}else{
        saveLog("Alterou os dados institucionais");
		$retorno = array("status" => 1, "msg" => "Atualizado com sucesso!");
		echo json_encode($retorno);
		exit;
	}

?>
