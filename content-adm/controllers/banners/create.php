<?php
    //Arquivos externos
    include_once '../../../models/log.php';
    include_once '../../../models/banner.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/upload-imagens.php';
    include_once '../../../funcoes/salvar-log.php';
	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myBanner = new Banner($db);

	// Upload imagem 1
	// ----------------------------------------------------------------------------------------------------------------------
	$image1 = uploadImagem ("image1", "../../uploads/banners/");
	if($image1 == 1) {
		$retorno = array("status" => 0, "msg" => "Imagem do banner para computadores muito grande");
		echo json_encode($retorno);
		exit;
	}else if($image1 == 2){
		$retorno = array("status" => 0, "msg" => "Imagem do banner para computadores possui uma extensão diferente de png, jpg, jpeg e gif");
		echo json_encode($retorno);
		exit;
	}else if($image1 == 3 || $image1 == 4){
		$image1 = "";
	}

	$image2 = uploadImagem ("image2", "../../uploads/banners/");
	if($image2 == 1) {
		$retorno = array("status" => 0, "msg" => "Imagem do banner para celulares muito grande");
		echo json_encode($retorno);
		exit;
	}else if($image2 == 2){
		$retorno = array("status" => 0, "msg" => "Imagem do banner para celulares possui uma extensão diferente de png, jpg, jpeg e gif");
		echo json_encode($retorno);
		exit;
	}else if($image2 == 3 || $image2 == 4){
		$image2 = "";
	}

	$myBanner->setTitle($_POST['title']);
	$myBanner->setText($_POST['text']);
	$myBanner->setButtonText($_POST['button_text']);
	$myBanner->setButtonUrl($_POST['button_url']);
	$myBanner->setVideoUrl($_POST['video_url']);
	$myBanner->setSequence($_POST['sequence']);
	$myBanner->setImageDesktop($image1);
	$myBanner->setImageMobile($image2);


	if (!$myBanner->create()){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao cadastrar, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
    	saveLog("Criou um novo banner");
		$retorno = array("status" => 1, "msg" => "Item cadastrado com sucesso!");
		echo json_encode($retorno);
		exit;
	}




?>
