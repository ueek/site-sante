<?php
    //Arquivos externos
    include_once '../../../models/log.php';
    include_once '../../../models/team.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/upload-imagens.php';
    include_once '../../../funcoes/salvar-log.php';
	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myTeam = new Team($db);

	// Upload imagem 1
	// ----------------------------------------------------------------------------------------------------------------------
	$image1 = uploadImagem ("image1", "../../uploads/team/");
	if($image1 == 1) {
		$retorno = array("status" => 0, "msg" => "Imagem 1 muito grande");
		echo json_encode($retorno);
		exit;
	}else if($image1 == 2){
		$retorno = array("status" => 0, "msg" => "Imagem 1 possui uma extensão diferente de png, jpg, jpeg e gif");
		echo json_encode($retorno);
		exit;
	}else if($image1 == 3 || $image1 == 4){
		$image1 = "";
	}

	$myTeam->setSequence($_POST['sequence']);
	$myTeam->setName($_POST['name']);
	$myTeam->setCharge($_POST['charge']);
	$myTeam->setLinkedin($_POST['linkedin']);
	$myTeam->setImage($image1);

	if (!$myTeam->create()){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao cadastrar, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
    	saveLog("Adicionou um novo membro da equipe multidisciplinar");
		$retorno = array("status" => 1, "msg" => "Item cadastrado com sucesso!");
		echo json_encode($retorno);
		exit;
	}

?>
