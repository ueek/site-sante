<?php
    //Arquivos externos
    include_once '../../../models/log.php';
    include_once '../../../models/blog.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/limpa-url.php';
    include_once '../../../funcoes/upload-imagens.php';
    include_once '../../../funcoes/salvar-log.php';
	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myBlog = new Blog($db);

	if (!isset($_POST['image1-old'])) {
		$image1 = uploadImagem ("image1", "../../uploads/blogs/");
		if($image1 == 1) {
			$retorno = array("status" => 0, "msg" => "Imagem 1 muito grande");
			echo json_encode($retorno);
			exit;
		}else if($image1 == 2){
			$retorno = array("status" => 0, "msg" => "Imagem 1 possui uma extensão diferente de png, jpg, jpeg e gif");
			echo json_encode($retorno);
			exit;
		}else if($image1 == 3 || $image1 == 4){
			$image1 = "";
		}

	}else{
		$image1 = $_POST['image1-old'];
	}

	$myBlog->setId($_POST['blog-id']);
	$myBlog->setTitle($_POST['title']);
	$myBlog->setText($_POST['text']);
	$myBlog->setAuthor($_POST['author']);
	$myBlog->setPublishedAt($_POST['published_at']);   
	$myBlog->setImage($image1);
	$myBlog->setBlogCategoryId($_POST['blog-category-id']);

	$myBlog->setUrl(urlAmigavel($myBlog->getTitle()));

	if (!$myBlog->update()){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao alterar, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
    	saveLog("Alterou o post de ID {$myBlog->getId()}");
		$retorno = array("status" => 1, "msg" => "Item alterado com sucesso!");
		echo json_encode($retorno);
		exit;
	}




?>
