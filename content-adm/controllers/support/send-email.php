<?php
	//Configurações
	$client    = "COC Floripa";
	$destinatario    = "suporte@ueek.com.br";

	//Dados do formulario
	$nomeRemetente      	= $_POST['name'];
	$emailRemetente     	= $_POST['email'];


	$titulo = utf8_encode("Mensagem de Suporte Gestor - {$client}");   

    //Montando o cabeçalho da mensagem
    $headers = "MIME-Version: 1.1\r\n";
    $headers .= "Content-type: text/html; charset=UTF-8\r\n";
    $headers .= "From: $emailRemetente\r\n"; // remetente
    //$headers .= "Cc: $copia\r\n"; // Cópia
    $headers .= "Return-Path: $emailRemetente\r\n"; // return-path


	//Texto do e-mail
	$mensagem = 
	"
		<!DOCTYPE html>
		<html>
		<head>
			<title>Mensagem de Suporte Gestor</title>

			<meta charset='utf-8'>
			<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700&display=swap' rel='stylesheet'>
			
		</head>
		<body>
				<h1>Mensagem de Suporte Gestor - {$client}</h1>

				<p>
					<b>Enviado por: </b>{$_POST['name']}
				</p>

				<p>
					{$_POST['description']}
				</p>

				<p>
					<i>Mensagem automática enviada pelo Sistema Gestor.</i>
				</p>
		</body>
		</html>
	"; 


	// Enviando a mensagem
	if(!mail($destinatario, $titulo, $mensagem, $headers)){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao enviar sua mensagem, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 1, "msg" => "Sua solicitação foi enviada com sucesso. Entraremos em contato em breve!");
		echo json_encode($retorno);
		exit;
	}
?>