<?php
    //Arquivos externos
    include_once '../../../models/log.php';
    include_once '../../../models/doctors.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/upload-imagens.php';
    include_once '../../../funcoes/salvar-log.php';
	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myDoctors = new Doctors($db);

	// Upload imagem 1
	// ----------------------------------------------------------------------------------------------------------------------
	$image1 = uploadImagem ("image1", "../../uploads/doctors/");
	if($image1 == 1) {
		$retorno = array("status" => 0, "msg" => "Imagem 1 muito grande");
		echo json_encode($retorno);
		exit;
	}else if($image1 == 2){
		$retorno = array("status" => 0, "msg" => "Imagem 1 possui uma extensão diferente de png, jpg, jpeg e gif");
		echo json_encode($retorno);
		exit;
	}else if($image1 == 3 || $image1 == 4){
		$image1 = "";
	}

	$myDoctors->setName($_POST['name']);
	$myDoctors->setCharge($_POST['charge']);
	$myDoctors->setCrm($_POST['crm']);
	$myDoctors->setRqe($_POST['rqe']);
	$myDoctors->setLinkedin($_POST['linkedin']);
	$myDoctors->setGraduation($_POST['graduation']);
	$myDoctors->setGraduationYear($_POST['graduation_year']);
	$myDoctors->setGraduationInstitution($_POST['graduation_institution']);
	$myDoctors->setEffective($_POST['effective']);
	$myDoctors->setImage($image1);
	$myDoctors->setSequence($_POST['sequence']);

	if (!$myDoctors->create()){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao cadastrar, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
    	saveLog("Adicionou um novo médico à equipe médica");
		$retorno = array("status" => 1, "msg" => "Item cadastrado com sucesso!");
		echo json_encode($retorno);
		exit;
	}

?>
