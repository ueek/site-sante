<?php
    //Arquivos externos

    include_once '../../../models/log.php';
    include_once '../../../models/residency-academic-team.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/upload-imagens.php';
    include_once '../../../funcoes/salvar-log.php';

	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myResidencyTeam = new ResidencyAcademicTeam($db);

	// Upload imagem 1
	// ----------------------------------------------------------------------------------------------------------------------

	if (!isset($_POST['image1-old'])) {

		$image1 = uploadImagem ("image1", "../../uploads/academic-team/");
		if($image1 == 1) {
			$retorno = array("status" => 0, "msg" => "Imagem 1 muito grande");
			echo json_encode($retorno);
			exit;
		}else if($image1 == 2){
			$retorno = array("status" => 0, "msg" => "Imagem 1 possui uma extensão diferente de png, jpg, jpeg e gif");
			echo json_encode($retorno);
			exit;
		}else if($image1 == 3 || $image1 == 4){
			$image1 = "";
		}

	}else{
		$image1 = $_POST['image1-old'];
	}

	$myResidencyTeam->setId($_POST['academic-member-id']);
	$myResidencyTeam->setResidenciesId($_POST['residencies_id']);
	$myResidencyTeam->setName($_POST['name']);
	$myResidencyTeam->setCharge($_POST['charge']);
	$myResidencyTeam->setCrm($_POST['crm']);
	$myResidencyTeam->setRqe($_POST['rqe']);
	$myResidencyTeam->setLinkedin($_POST['linkedin']);
	$myResidencyTeam->setSector($_POST['sector']);
	$myResidencyTeam->setImage($image1);

	if (!$myResidencyTeam->update()){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao alterar, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
    	saveLog("Alterou informações do membro do corpo acadêmico de ID {$myResidencyTeam->setId()}");
		$retorno = array("status" => 1, "msg" => "Item alterado com sucesso!");
		echo json_encode($retorno);
		exit;
	}

?>
