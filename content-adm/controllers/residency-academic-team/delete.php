<?php
    //Arquivos externos
    include_once '../../../models/log.php';
    include_once '../../../models/residency-academic-team.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/salvar-log.php';

	//==============================================================//
	
    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myResidencyTeam = new ResidencyAcademicTeam($db);

	// Excluir 
	if ($myResidencyTeam->delete($_POST['id'])) {
    	saveLog("Removeu o membro do corpo acadêmico de ID {$_POST['id']}");
		$retorno = array("status" => 1, "msg" => "Removido com sucesso!");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 0, "msg" => "Ocorreu um erro ao excluir, tente novamente mais tarde.");
		echo json_encode($retorno);
		exit;
	}
?>
