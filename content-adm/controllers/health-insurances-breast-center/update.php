<?php
    //Arquivos externos
    include_once '../../../models/log.php';
    include_once '../../../models/health-insurance-breast-center.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/upload-imagens.php';
    include_once '../../../funcoes/salvar-log.php';

	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myInsurance = new HealthInsuranceBreastCenter($db);

	// Upload imagem 1
	// ----------------------------------------------------------------------------------------------------------------------

	if (!isset($_POST['image1-old'])) {



		$image1 = uploadImagem ("image1", "../../uploads/health-insurance-breast-center/");
		if($image1 == 1) {
			$retorno = array("status" => 0, "msg" => "Imagem 1 muito grande");
			echo json_encode($retorno);
			exit;
		}else if($image1 == 2){
			$retorno = array("status" => 0, "msg" => "Imagem 1 possui uma extensão diferente de png, jpg, jpeg e gif");
			echo json_encode($retorno);
			exit;
		}else if($image1 == 3 || $image1 == 4){
			$image1 = "";
		}

	}else{
		$image1 = $_POST['image1-old'];
	}

	$myInsurance->setId($_POST['insurance-id']);
	$myInsurance->setName($_POST['name']);
	$myInsurance->setImage($image1);


	if (!$myInsurance->update()){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao alterar, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
    	saveLog("Alterou o convênio de saúde do Centro de Mama de ID {$myInsurance->getId()}");
		$retorno = array("status" => 1, "msg" => "Item alterado com sucesso!");
		echo json_encode($retorno);
		exit;
	}




?>
