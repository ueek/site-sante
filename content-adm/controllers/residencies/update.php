<?php
    //Arquivos externos
    include_once '../../../models/residency.php';
    include_once '../../../config/database.php';

	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myResidency = new Residency($db);

	$myResidency->setId($_POST['residency-id']);
	$myResidency->setName($_POST['name']);
	$myResidency->setDescription($_POST['description']);
	$myResidency->setInformation($_POST['information']);
	$myResidency->setAdmition($_POST['admition']);
	
	if (!$myResidency->update()){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao alterar, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
    	saveLog("Alterou a residência de ID {$myResidency->getId()}");
		$retorno = array("status" => 1, "msg" => "Item alterado com sucesso!");
		echo json_encode($retorno);
		exit;
	}

?>
