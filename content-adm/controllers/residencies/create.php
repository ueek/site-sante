<?php
    //Arquivos externos

    include_once '../../../models/log.php';
    include_once '../../../models/residency.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/salvar-log.php';
	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myResidency = new Residency($db);
	
	$myResidency->setName($_POST['name']);
	$myResidency->setDescription($_POST['description']);
	$myResidency->setInformation($_POST['information']);
	$myResidency->setAdmition($_POST['admition']);

	if (!$myResidency->create()){
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao cadastrar, tente novamente.");
		echo json_encode($retorno);
		exit;
	}else{
    	saveLog("Adicionou uma residência");
		$retorno = array("status" => 1, "msg" => "Item cadastrado com sucesso!");
		echo json_encode($retorno);
		exit;
	}

?>
