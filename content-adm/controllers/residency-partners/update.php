<?php
    //Arquivos externos

    include_once '../../../models/log.php';
    include_once '../../../models/residency-partner.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/salvar-log.php';
	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // excluir os antigos
    $myResidencyPartners = new ResidencyPartner($db);
    $myResidencyPartners->deleteAllByResidencyId($_POST['residency-id']);

    $myResidencyPartners->setResidencyId($_POST['residency-id']);
    $myResidencyPartners->setInstitutions($_POST['institutions']);
    $myResidencyPartners->create();

    saveLog("Alterou as institutições parceiras da residência médica");
    $retorno = array("status" => 1, "msg" => "Atualizado com sucesso!");
    echo json_encode($retorno);
    exit;

?>
