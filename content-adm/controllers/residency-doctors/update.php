<?php
    //Arquivos externos
    include_once '../../../models/log.php';
    include_once '../../../models/doctors.php';
    include_once '../../../models/residency-doctor.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/salvar-log.php';
	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myDoctors = new Doctors($db);
    $listOfDoctors= $myDoctors->read();

    // excluir os antigos
    $myResidencyDoctor = new ResidencyDoctor($db);
    $myResidencyDoctor->deleteAllByResidencyId($_POST['residency-id']);

    while ($rowDoctor = $listOfDoctors->fetch(PDO::FETCH_ASSOC)){
        extract($rowDoctor);

        if (isset($_POST['doctor-'.$id])) {

            $myResidencyDoctor->setResidencyId($_POST['residency-id']);
            $myResidencyDoctor->setClinicTeamId($id);
            $myResidencyDoctor->create();
        }
    }

    saveLog("Alterou médicos da residência");
    $retorno = array("status" => 1, "msg" => "Atualizado com sucesso!");
    echo json_encode($retorno);
    exit;

?>
