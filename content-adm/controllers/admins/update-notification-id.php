<?php
    //Arquivos externos
    include_once '../../../models/admin.php';
    include_once '../../../config/database.php';
	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myAdmin = new Admin($db);


	$myAdmin->setId($_POST['admin-id']);
	$myAdmin->setLastReadNotificationId($_POST['last-notification-id']);

	if (!$myAdmin->updateLastReadNotificationId()){
		$retorno = array("status" => 0, "msg" => "An error occured updating this record. Please, try again later.");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 1, "msg" => "Successfully updated!");
		echo json_encode($retorno);
		exit;
	}




?>
