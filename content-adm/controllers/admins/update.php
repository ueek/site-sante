<?php
    //Arquivos externos
    include_once '../../../models/log.php';
    include_once '../../../models/admin.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/salvar-log.php';

	//==============================================================//

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    $myAdmin = new Admin($db);

    //Trata variáveis
    $nome  = trim($_POST['nome']);
    $email = trim($_POST['email']);

    //Seta campo
	$myAdmin->setId($_POST['admin-id']);

	//Verificações caso o javascript não esteja funcionando corretamente
	if ($nome == "") {
		$retorno = array("status" => 0, "msg" => "Você deve digitar um nome.");
		echo json_encode($retorno);
		exit;
	}else{
		//Seta campo
		$myAdmin->setName($nome);
	}

	if ($email == "") {
		$retorno = array("status" => 0, "msg" => "Você deve digitar um e-mail.");
		echo json_encode($retorno);
		exit;
	}else{
		//Seta campo
		$myAdmin->setEmail($email);
	}

	if ($_POST['senha'] != $_POST['senha_confirmacao']) {
		$retorno = array("status" => 0, "msg" => "A senha e a confirmação devem ser iguais.");
		echo json_encode($retorno);
		exit;
	}elseif ($_POST['senha'] != "") {
		//Seta campo
		$myAdmin->setPassword(md5($_POST['senha']));
	}

	//Atualização
	if ($_POST['senha'] == "") {
		if (!$myAdmin->updateKeepPassword()){
			$retorno = array("status" => 0, "msg" => "Ocorreu um erro ao atualizar, tente novamente mais tarde.");
			echo json_encode($retorno);
			exit;
		}else{
        	saveLog("Alterou o cadastro do gestor de ID {$myAdmin->getId()}");
			$retorno = array("status" => 1, "msg" => "Atualizado com sucesso!");
			echo json_encode($retorno);
			exit;
		}			
	}else{
		if (!$myAdmin->update()){
			$retorno = array("status" => 0, "msg" => "Ocorreu um erro ao atualizar, tente novamente mais tarde.");
			echo json_encode($retorno);
			exit;
		}else{
        	saveLog("Alterou o cadastro do gestor de ID {$myAdmin->getId()}");
			$retorno = array("status" => 1, "msg" => "Atualizado com sucesso!");
			echo json_encode($retorno);
			exit;
		}		
	}

?>
