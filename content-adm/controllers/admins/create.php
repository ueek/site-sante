<?php
    //Arquivos externos
    include_once '../../../models/log.php';
    include_once '../../../models/admin.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/salvar-log.php';

	//==============================================================//
	
    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    //Instancia objeto
    $myAdmin = new Admin($db);

    //Trata variáveis
    $nome  = trim($_POST['nome']);
    $email = trim($_POST['email']);

    //Verificações caso o javascript não esteja funcionando corretamente
	if ($nome == "") {
		$retorno = array("status" => 0, "msg" => "Você deve digitar um nome.");
		echo json_encode($retorno);
		exit;
	}else{
		//Seta campo
		$myAdmin->setName($nome);
	}

	if ($email == "") {
		$retorno = array("status" => 0, "msg" => "Você deve digitar um e-mail.");
		echo json_encode($retorno);
		exit;
	}else{
		//Seta campo
		$myAdmin->setEmail($email);
	}

	if ($_POST['senha'] != $_POST['senha_confirmacao']) {
		$retorno = array("status" => 0, "msg" => "A senha e a confirmação devem ser iguais.");
		echo json_encode($retorno);
		exit;
	}elseif ($_POST['senha'] != "") {
		//Seta campo
		$myAdmin->setPassword(md5($_POST['senha']));
	}

	//Inserção
	if ($myAdmin->create() == 0){
		$retorno = array("status" => 0, "msg" => "Ocorreu um erro ao adicionar. Tente novamente mais tarde.");
		echo json_encode($retorno);
		exit;
	}else{
        saveLog("Adicionou um gestor");
		$retorno = array("status" => 1, "msg" => "Adicionado com sucesso!");
		echo json_encode($retorno);
		exit;
	}
?>
