<?php
    //Arquivos externos
    include_once '../../../models/log.php';
    include_once '../../../models/about-photo.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/salvar-log.php';

	//==============================================================//
	// $retorno = array("status" => 0, "msg" => $_POST['id']);
	// echo json_encode($retorno);
	// exit;
	
    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myAboutPhoto = new AboutPhoto($db);

	// Excluir imagem
	if ($myAboutPhoto->delete($_POST['id'])) {
        saveLog("Excluiu a foto institucional de ID {$_POST['id']}");
		$retorno = array("status" => 1, "msg" => "Imagem excluída com sucesso!");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao excluir esta imagem, tente novamente.");
		echo json_encode($retorno);
		exit;
	}
?>
