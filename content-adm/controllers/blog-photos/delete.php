<?php
    //Arquivos externos
    include_once '../../../models/blog-photo.php';
    include_once '../../../config/database.php';

	//==============================================================//
	// $retorno = array("status" => 0, "msg" => $_POST['id']);
	// echo json_encode($retorno);
	// exit;
	
    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myBlogPhoto = new BlogPhoto($db);

	// Excluir imagem
	if ($myBlogPhoto->delete($_POST['id'])) {
    	saveLog("Excluiu uma foto de post de ID {$_POST['id']}");
		$retorno = array("status" => 1, "msg" => "Imagem excluída com sucesso!");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 0, "msg" => "Ocorreu algum erro ao excluir esta imagem, tente novamente.");
		echo json_encode($retorno);
		exit;
	}
?>
