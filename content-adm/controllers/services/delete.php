<?php
    //Arquivos externos
    include_once '../../../models/log.php';
    include_once '../../../models/service.php';
    include_once '../../../config/database.php';
    include_once '../../../funcoes/salvar-log.php';

	//==============================================================//
	
    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // Instanciar objeto
    $myServices = new Service($db);

	// Excluir 
	if ($myServices->delete($_POST['id'])) {
    	saveLog("Excluiu o serviço de ID {$_POST['id']}");
		$retorno = array("status" => 1, "msg" => "Removido com sucesso!");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 0, "msg" => "Ocorreu um erro ao excluir, tente novamente mais tarde.");
		echo json_encode($retorno);
		exit;
	}
?>
