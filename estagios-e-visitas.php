<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Ensino & Pesquisa</h1>
		</div>

		<div class="long-text">

			<h5 class="content-title">Estágios e Visitas</h5>

			<div class="accordion" id="area-1">
                <div id="heading-1">

                    <div class="col-12 toggle-accordion">
                        <button data-toggle="collapse" data-target="#step-1">
                            <i class="fas fa-plus"></i>Alunos de graduação em Medicina e médicos com interesse em visita à Residência Médica
                        </button>
                    </div>

                    <div id="step-1" class="collapse" aria-labelledby="heading-1" data-parent="#area-1">
                    	
                    	<p class="text-accordion">
                    		O solicitante deverá atender uma das condições abaixo:<br>
							- Estar no internato (quinto e sexto ano de graduação) ou;<br>
							- Estar formado cursando área (pré-requisito) de Residência Médica ou;<br>
							- Estar formado sem ter cursado Residência.<br>
                    	</p>

                    	<br>

                    	<form id="form1" class="col-lg-9 col-md-9 col-sm-12 inputs estagios-e-visitas">

            		  		<input type="hidden" name="residency-visitation">

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Nome</label>
								<input class="required" type="text" name="name" placeholder="Nome completo">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>CPF</label>
								<input class="required" type="text" name="cpf" placeholder="CPF">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Telefone</label>
								<input class="required" type="text" name="phone" maxlength="15" onkeydown="Mascara(this,Telefone);" placeholder="(xx) xxxx-xxxx">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Email</label>
								<input class="required" type="email" name="email" placeholder="Email">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Universidade de Origem</label>
								<input class="required" type="text" name="university" placeholder="Universidade de Origem">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Cidade, UF</label>
								<input class="required" type="text" name="city-uf" placeholder="Cidade, UF">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Data de Início</label>
								<input class="required" type="text" name="initial-date" placeholder="00/00/0000">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Data de Término</label>
								<input class="required" type="text" name="final-date" placeholder="00/00/0000">
							</div>

							<div class="col-lg-12 col-md-12 col-sm-12 inputs">
								<input class="soft-hover btn btn-contact" type="submit" name="send" value="Enviar Solicitação">
							</div>

							<br>

						</form>

                    	<br>

                    </div>

                </div>
            </div>

            <div class="accordion" id="area-2">
                <div id="heading-2">

                    <div class="col-12 toggle-accordion">
                        <button data-toggle="collapse" data-target="#step-2">
                            <i class="fas fa-plus"></i>Alunos de Residência com interesse em estágio optativo
                        </button>
                    </div>

                    <div id="step-2" class="collapse" aria-labelledby="heading-2" data-parent="#area-2">
                    	
                    	<p class="text-accordion">
                    		A solicitação deve ser realizada com no mínimo três meses de antecedência do início do estágio optativo.
                    	</p>

                    	<br>

                    	<form id="form2" class="col-lg-9 col-md-9 col-sm-12 inputs estagios-e-visitas">

            		  		<input type="hidden" name="residency-students">

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Nome</label>
								<input class="required" type="text" name="name" placeholder="Nome completo">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>CPF</label>
								<input class="required" type="text" name="cpf" placeholder="CPF">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Telefone</label>
								<input class="required" type="text" name="phone" maxlength="15" onkeydown="Mascara(this,Telefone);" placeholder="(xx) xxxx-xxxx">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Email</label>
								<input class="required" type="email" name="email" placeholder="Email">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Universidade de Origem</label>
								<input class="required" type="text" name="university" placeholder="Universidade de Origem">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Cidade, UF</label>
								<input class="required" type="text" name="city-uf" placeholder="Cidade, UF">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Número do Conselho de Classe</label>
								<input class="required" type="text" name="class-number" placeholder="Número do Conselho de Classe">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Descrever em tópicos as atividades que deseja participar</label>
								<input class="required" type="text" name="topics" placeholder="Descreva aqui">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Data de Início</label>
								<input class="required" type="text" name="initial-date" placeholder="00/00/0000">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Data de Término</label>
								<input class="required" type="text" name="final-date" placeholder="00/00/0000">
							</div>

							<div class="col-lg-12 col-md-12 col-sm-12 inputs">
								<input class="soft-hover btn btn-contact" type="submit" name="send" value="Enviar Solicitação">
							</div>

							<br>

						</form>

                    	<br>

                    </div>

                </div>
            </div>

            <div class="accordion" id="area-3">
                <div id="heading-3">

                    <div class="col-12 toggle-accordion">
                        <button data-toggle="collapse" data-target="#step-3">
                            <i class="fas fa-plus"></i>Alunos de graduação em Medicina interessados em estágio curricular e extra-curricular
                        </button>
                    </div>

                    <div id="step-3" class="collapse" aria-labelledby="heading-3" data-parent="#area-3">

                    	<form id="form3" class="col-lg-9 col-md-9 col-sm-12 inputs estagios-e-visitas">

                    		<input type="hidden" name="med-students">

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Nome</label>
								<input class="required" type="text" name="name" placeholder="Nome completo">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>CPF</label>
								<input class="required" type="text" name="cpf" placeholder="CPF">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Telefone</label>
								<input class="required" type="text" name="phone" maxlength="15" onkeydown="Mascara(this,Telefone);" placeholder="(xx) xxxx-xxxx">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Email</label>
								<input class="required" type="email" name="email" placeholder="Email">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Universidade de Origem</label>
								<input class="required" type="text" name="university" placeholder="Universidade de Origem">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Cidade, UF</label>
								<input class="required" type="text" name="city-uf" placeholder="Cidade, UF">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Número do Conselho de Classe</label>
								<input class="required" type="text" name="class-number" placeholder="Número do Conselho de Classe">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Descrever em tópicos as atividades que deseja participar</label>
								<input class="required" type="text" name="topics" placeholder="Descreva aqui">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Data de Início</label>
								<input class="required" type="text" name="initial-date" placeholder="00/00/0000">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Data de Término</label>
								<input class="required" type="text" name="final-date" placeholder="00/00/0000">
							</div>

							<div class="col-lg-12 col-md-12 col-sm-12 inputs">
								<input class="soft-hover btn btn-contact" type="submit" name="send" value="Enviar Solicitação">
							</div>

							<br>

						</form>

                    	<br>

                    </div>

                </div>
            </div>

            <div class="accordion" id="area-4">
                <div id="heading-4">

                    <div class="col-12 toggle-accordion">
                        <button data-toggle="collapse" data-target="#step-4">
                            <i class="fas fa-plus"></i>Visitas Técnicas para alunos ou profissionais
                        </button>
                    </div>

                    <div id="step-4" class="collapse" aria-labelledby="heading-4" data-parent="#area-4">
                    	
                    	<form id="form4" class="col-lg-9 col-md-9 col-sm-12 inputs estagios-e-visitas">

                    		<input type="hidden" name="technical-visitation-students-teachers">

                    		<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Área</label>
								<input class="required" type="text" name="area" placeholder="Em que área deseja realizar a visita?">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Data pretendida</label>
								<input class="required" type="text" name="intended-date" placeholder="Data pretendida da visita ou do inicio do período de visita 00/00/0000">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Nome</label>
								<input class="required" type="text" name="name" placeholder="Nome completo">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>CPF</label>
								<input class="required" type="text" name="cpf" placeholder="CPF">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Telefone</label>
								<input class="required" type="text" name="phone" maxlength="15" onkeydown="Mascara(this,Telefone);" placeholder="(xx) xxxx-xxxx">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Email</label>
								<input class="required" type="email" name="email" placeholder="Email">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Local de Trabalho</label>
								<input class="required" type="text" name="work" placeholder="Local de Trabalho">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Cidade, UF</label>
								<input class="required" type="text" name="city-uf" placeholder="Cidade, UF">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Formação</label>
								<input class="required" type="text" name="graduation" placeholder="Formação">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Profissão</label>
								<input class="required" type="text" name="profession" placeholder="Profissão">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Objetivos</label>
								<input class="required" type="text" name="objectives" placeholder="Quais objetivos a serem alcançados?">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Temas</label>
								<input class="required" type="text" name="themes" placeholder="Quais temas são imprescindíveis de serem abordados?">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Resultados</label>
								<input class="required" type="text" name="results" placeholder="Quais resultados (tangíveis) pretende alcançar com esta visita?">
							</div>

							<div class="col-lg-6 col-md-12 col-sm-12 inputs">
								<label>Observações</label>
								<input type="text" name="observations" placeholder="Observações">
							</div>

							<div class="col-lg-12 col-md-12 col-sm-12 inputs">
								<input class="soft-hover btn btn-contact" type="submit" name="send" value="Enviar Solicitação">
							</div>

							<br>

						</form>

                    	<br>

                    </div>

                </div>
            </div>

		</div>
		
	</div>
</section>