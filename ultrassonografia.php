<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Ultrassonografia</h1>
		</div>

		<div class="long-text">
			<p>
				O ultrassom de mamas é utilizado para examinar algumas alterações que foram identificados na mamografia, normalmente é utilizado para que o médico posicione a agulha ao realizar uma biópsia. <br> 
				A ultrassonografia não utiliza nenhum tipo de radiação ionizante para realização do exame, são utilizadas ondas sonoras de alta frequência, que realizam a imagem. 
			</p>
		</div>

	</div>
</section>

<!-- <section id="galery">
	<div class="container p-0">
		
		<div id="images">
			
			<a href='/img/infraestrutura.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/infraestrutura.jpg);"></div>
			</a>

			<a href='/img/sobre-nos.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/sobre-nos.jpg);"></div>
			</a>

			<a href='/img/infraestrutura.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/infraestrutura.jpg);"></div>
			</a>

			<br>

		</div>

	</div>
</section> -->