<?php
	
    //Arquivos externos
    include_once 'config/database.php';

    include_once 'models/doctors.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // ==========================================================
    // Load Doctor data
    // ==========================================================
    $urlImages_Doctor = "/content-adm/uploads/doctors/"; 
    
    $myDoctor = new Doctors($db);
    $stmtDoctor = $myDoctor->read("AND effective = 1", "sequence, name");
    // ==========================================================

?>

<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/sobre-nos.jpg);"></section> -->

<section id="content">
	<div class="container p-0">
		
		<div class="header-title">
			<h1>Corpo Clínico</h1>
		</div>

		<div class="team-cards">

			<h1>Equipe Médica</h1>
			
		    <?php 

			    while ($row = $stmtDoctor->fetch(PDO::FETCH_ASSOC)){
			        extract($row);

			        $linkedin_area = "";

			        if ($linkedin != '') {
			        	$linkedin_area = "
				        	<a class='linkedin soft-hover' href='{$linkedin}'>
								<i class='fab fa-linkedin'></i>LinkedIn
							</a>";
			        }

			        if ($rqe != "") {
			        	$rqe = "RQE: " . $rqe; 
			        }
			        echo 
			        "
						<div class='card-doc'>
							
							<div class='img-profile' style='background-image: url({$urlImages_Doctor}{$image});'>
								<div class='watermark' style='background-image: url(/img/profile.png)'></div>
							</div>
							

							<div class='main-infos'>

								<h2>{$name}</h2>
								<h5>{$charge}</h5>
								<span>{$crm} | {$rqe}</span>

								{$linkedin_area}

							</div>

							<br>

							<!--div class='info-graduation'>
								<h5>Formação</h5>

								<p class='graduation'>
								<svg xmlns='http://www.w3.org/2000/svg' width='6' height='6' viewBox='0 0 6 6' fill='none'>
									<circle cx='3' cy='3' r='3' fill='#C4C4C4'/>
								</svg>{$graduation} ({$graduation_year})</p>
								<p class='institution'>{$graduation_institution}</p>
							</div-->

						</div>						
			        ";

			    }

		    ?>
		
			<br>

		</div>

	</div>
</section>