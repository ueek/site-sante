<?php	

	include_once '../models/about.php';
	include_once '../config/database.php';

	include_once '../funcoes/upload-file.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // ==========================================================
    // Load About data
    // ==========================================================
    $myAbout = new About($db);
    $stmtAbout = $myAbout->read("", "id", "LIMIT 1");
    if ($stmtAbout->rowCount() > 0) {

	    $row = $stmtAbout->fetch(PDO::FETCH_ASSOC);
		extract($row);
        $myAbout->setEmail($email);
    }

	//Configurações
	$destinatario = "lesante@lesante.com.br";

	//Dados do formulario
	$name      	= $_POST['name'];
	$email     	= $_POST['email'];
	$phone      = $_POST['phone'];
	$message    = $_POST['message'];

	$file = uploadArquivo ("file", "../curriculos/");


	$dataHora           = date("d/m/Y")." as ".date("H:i:s");
    
    $msgHora = "Horário da mensagem: {$dataHora}";

	$user_data = utf8_encode("Dados do Interessado:");

	$txt = "Olá, você recebeu uma nova submissão de currículo enviada através do site Santé Cancer Center";



	$title = "Submissão de Currículo - Santé";

    //Montando o cabeçalho da mensagem
    $headers = "MIME-Version: 1.1\r\n";
    $headers .= "Content-type: text/html; charset=UTF-8\r\n";
    $headers .= "From: $emailRemetente\r\n"; // remetente
    //$headers .= "Cc: $copia\r\n"; // Cópia
    $headers .= "Return-Path: $emailRemetente\r\n"; // return-path


    //Validações
	if(empty($name) or empty($email) or empty($phone) or empty($message)){
		$retorno = array("status" => 0, "msg" => "Todos os campos em destaque devem ser preenchidos!");
		echo json_encode($retorno);
		exit;
	}

	if($file == 2){
		$retorno = array("status" => 0, "msg" => "Por favor, anexe um arquivo no formato PDF!");
		echo json_encode($retorno);
		exit;
	}


	//Texto do e-mail
	$mensagem = "<!DOCTYPE html>
	<html lang='pt-br'>
	<head>
		<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
		<meta charset='utf-8'>

		<title>$title</title>		
	</head>

	<body>
		$txt <br><br>
		
		<strong>{$user_data}</strong><br>
		Nome: 					{$name}<br>
		Telefone:				{$phone}<br>
		E-mail: 				{$email}<br>
		Currículo: 				

		<a href='https://www.santecancercenter.com.br/curriculos/{$file}' target='_blank' download>Clique aqui</a><br>

		<br>

		Mensagem: {$message}

		<br><br>
		{$msgHora}
	</body>

	</html>"; // Corpo da mensagem em formato HTML


	// Enviando a mensagem
	if(mail($destinatario, $title, $mensagem, $headers)){
		$retorno = array("status" => 1, "msg" => "Mensagem de contato enviada com sucesso! Em breve entraremos em contato.");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 0, "msg" => "Falha ao enviar mensagem de contato, tente novamente.");
		echo json_encode($retorno);
		exit;
	}
?>