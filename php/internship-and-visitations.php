<?php	

	include_once '../models/about.php';
	include_once '../config/database.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // ==========================================================
    // Load About data
    // ==========================================================
    $myAbout = new About($db);
    $stmtAbout = $myAbout->read("", "id", "LIMIT 1");
    if ($stmtAbout->rowCount() > 0) {

	    $row = $stmtAbout->fetch(PDO::FETCH_ASSOC);
		extract($row);
        $myAbout->setEmail($email);
    }

	//Configurações
	$destinatario = "lesante@lesante.com.br";

	//Dados do formulario
	$name      	= $_POST['name'];
	$email     	= $_POST['email'];
	$phone      = $_POST['phone'];
	$cpf    	= $_POST['cpf'];
	$city_uf   	= $_POST['city-uf'];

	$fields =
	"
		Nome: 		{$name}<br>
		E-mail: 	{$email}<br>
		Telefone:	{$phone}<br>
		CPF:		{$cpf}<br>
		Cidade:		{$city_uf}<br>
	";

	if (isset($_POST['residency-visitation'])) {
		$university    		= $_POST['university'];
		$initial_date    	= $_POST['initial-date'];
		$final_date	    	= $_POST['final-date'];

		$title = "Mensagem de contato - Alunos de graduação em Medicina e médicos com interesse em visita à Residência Médica"; 

		$fields .=
		"
			Universidade: 		{$university}<br>
			Data Inicial: 		{$initial_date}<br>
			Data Final: 		{$final_date}<br>
		";

	}else if (isset($_POST['residency-students'])) {
		$university    		= $_POST['university'];
		$class_number    	= $_POST['class-number'];
		$topics    			= $_POST['topics'];
		$initial_date    	= $_POST['initial-date'];
		$final_date	    	= $_POST['final-date'];


		$title = "Mensagem de contato - Alunos de Residência com interesse em estágio optativo"; 

		$fields .=
		"
			Universidade: 				{$university}<br>
			Conselho de classe número: 	{$class_number}<br>
			Tópicos: 					{$topics}<br>
			Data Inicial: 				{$initial_date}<br>
			Data Final: 				{$final_date}<br>
		";

	}else if (isset($_POST['med-students'])) {
		$university    		= $_POST['university'];
		$class_number    	= $_POST['class-number'];
		$topics    			= $_POST['topics'];
		$initial_date    	= $_POST['initial-date'];
		$final_date	    	= $_POST['final-date'];

		$title = "Mensagem de contato - Alunos de graduação em Medicina interessados em estágio curricular e extra-curricular"; 

		$fields .=
		"
			Universidade: 				{$university}<br>
			Conselho de classe número: 	{$class_number}<br>
			Tópicos: 					{$topics}<br>
			Data Inicial: 				{$initial_date}<br>
			Data Final: 				{$final_date}<br>
		";

	}else if (isset($_POST['technical-visitation-students-teachers'])) {
		$area    			= $_POST['area'];
		$intended_date    	= $_POST['intended-date'];
		$work    			= $_POST['work'];
		$graduation    		= $_POST['graduation'];
		$profession    		= $_POST['profession'];
		$objectives    		= $_POST['objectives'];
		$themes    			= $_POST['themes'];
		$results    		= $_POST['results'];
		$observations    	= $_POST['observations'];


		$title = "Mensagem de contato - Visitas Técnicas para alunos ou profissionais"; 

		$fields .=
		"
			Área: 					{$area}<br>
			Data pretendida: 		{$intended_date}<br>
			Local de Trabalho: 		{$work}<br>
			Formação: 				{$graduation}<br>
			Área de atuação: 		{$profession}<br>
			Objetivos: 				{$objectives}<br>
			Temas: 					{$themes}<br>
			Resultados esperados: 	{$results}<br>
			Observações: 			{$observations}<br>
		";



	}


	$dataHora           = date("d/m/Y")." as ".date("H:i:s");
    
    $msgHora = "Horário da mensagem: {$dataHora}";

	$user_data = utf8_encode("Dados do Solicitante:");

	$txt = "Olá, você recebeu uma nova solicitação de Visita ou estágio enviada através do site Santé Cancer Center";


    //Montando o cabeçalho da mensagem
    $headers = "MIME-Version: 1.1\r\n";
    $headers .= "Content-type: text/html; charset=UTF-8\r\n";
    $headers .= "From: $emailRemetente\r\n"; // remetente
    //$headers .= "Cc: $copia\r\n"; // Cópia
    $headers .= "Return-Path: $emailRemetente\r\n"; // return-path

	//Texto do e-mail
	$mensagem = 
	"
		<!DOCTYPE html>
			<html lang='pt-br'>
			<head>
				<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
				<meta charset='utf-8'>

				<title>$title</title>		
			</head>

			<body>
				$txt <br><br>
				Assunto: <strong>{$title}</strong><br><br>

				<strong>{$user_data}</strong><br>

				{$fields}

				<br>

				Mensagem: {$message}

				<br><br>
				{$msgHora}
			</body>

			</html>
		"; // Corpo da mensagem em formato HTML


	// Enviando a mensagem
	if(mail($destinatario, $title, $mensagem, $headers)){
		$retorno = array("status" => 1, "msg" => "Mensagem de contato enviada com sucesso! Em breve entraremos em contato.");
		echo json_encode($retorno);
		exit;
	}else{
		$retorno = array("status" => 0, "msg" => "Falha ao enviar mensagem de contato, tente novamente.");
		echo json_encode($retorno);
		exit;
	}
?>