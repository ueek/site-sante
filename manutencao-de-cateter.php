<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Manutenção de cateter</h1>
		</div>

		<h6 class="content-title">O que é?</h6>

		<div class="long-text">
			<p>
				O cateter é um dispositivo instalado no paciente para que a medicação possa ser administrada diretamente em uma veia profunda. Com isso, não há a necessidade de puncionar uma nova veia a cada administração de quimioterapia. A indicação para colocar um cateter desse tipo é feita pelo médico e, em alguns casos, pela enfermeira após avaliação sobre as condições de suas veias. 
				<br>
				O cateter é chamado de porth-o-cath ou “totalmente implantável” e fica instalado embaixo da pele. Este cateter tem uma câmara feita de material especial que permite a inserção de uma agulha diversas vezes, para várias aplicações, sem a necessidade de trocar o cateter  ou pegar outras veias; esse cateter é sempre colocado por um cirurgião em um centro cirúrgico.
				<br>
				Como o cateter fica sob a pele, você não terá muitas preocupações, apenas evite traumas na área do implante (esportes com bola, deitar sobre ele etc.). Nos pacientes masculinos, é aconselhável que seja feita a retirada dos pelos da área do cateter para que o curativo, a ser colocado no dia do tratamento fique adequadamente fixado. Você pode tomar banho normalmente, higienizando a pele sobre o cateter. 
				<br>
				*Quando for realizar o tratamento, procure ir com uma camisa de botões, para que o enfermeiro possa realizar a antissepsia da pele corretamente e visualizar amplamente a área para o procedimento de punção.
				<br>
				Se você tem esse dispositivo instalado, o mais importante é saber que será preciso comparecer ao Santé Cancer Center para fazer a manutenção do cateter. Caso você possua um porth-o-cath ou “totalmente implantável”, essa manutenção deve ser realizada a cada 30 dias.
				<br>
				Observe sempre o local onde está instalado o seu cateter para identificar se está havendo alguma alteração, como vermelhidão, inchaço, dor, ardência ou alteração na pele na região do cateter. Se algum desses sintomas estiver presente, procure o seu médico ou a equipe de enfermagem do Santé Cancer Center.
			</p>
		</div>

		<h6 class="content-title">Como funciona?</h6>

		<div class="long-text">
			<p>
				Ambos os exames consistem em retirar uma pequena quantidade de material para amostra, seja um fragmento do osso ou da medula óssea. Normalmente as áreas selecionadas para coleta são o osso ilíaco (bacia) ou osso esterno (peito).  Todo o exame é realizado sob anestesia local, uma vez que é necessário uma agulha especial que consiga perfurar o osso. Após a coleta, o material é analisado em laboratório.
			</p>
		</div>

		<h6 class="content-title">Efeitos colaterais</h6>

		<div class="long-text">
			<p>
				Na semana após o exame é comum que o paciente sinta dores no local. As complicações são raras mas pode existir caso de sangramento em excesso, alergia ou infecção local. 
				Caso o paciente sinta dores, febre ou sangramentos, é necessário buscar auxílio da sua equipe médica imediatamente.
			</p>
		</div>

		<h6 class="content-title">Duração do tratamento</h6>

		<div class="long-text">
			<p>
				A duração do exame costuma ser rápida, porém demanda um tempo de preparação para anestesia e algumas vezes é necessário sedação. 
			</p>
		</div>

	</div>
</section>