<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Drogas de alvo molecular/terapia-alvo</h1>
		</div>

		<h6 class="content-title">O que são?</h6>

		<div class="long-text">
			<p>
				As drogas de alvo molecular/terapia alvo agem diretamente no tumor e preservam as células saudáveis.  São atingidas as células com alterações genéticas (que podem se tornar tumores), o que torna este um tratamento menos agressivo e, em alguns casos, mais assertivo e eficiente.
				<br>
				<br>
				Este tipo de terapia de alvo molecular pode ser utilizado de forma isolada ou em conjunto com outros tratamentos. Caso a terapia seja utilizada em conjunto com a cirurgia, as drogas podem ser utilizadas em duas situações: 
				<br>
				<br>
				<b>Neoadjuvante:</b><br> 
				Antes da cirurgia, auxiliando na redução do tumor, facilitando a cirurgia e preservando os tecidos saudáveis. 
				<br>
				<br>
				<b>Adjuvante:</b><br>
				Após a cirurgia com a intenção de diminuir as chances de que a doença tenha uma recidiva. 
			</p>
		</div>

		<h6 class="content-title">Como funciona?</h6>

		<div class="long-text">
			<p>
				As terapias-alvo foram desenvolvidas com base nas proteínas encontradas na superfície ou no interior das células tumorais, com o objetivo  de combater as moléculas específicas, impedindo a sequência de eventos que leva à proliferação de células malignas no corpo e direcionando a ação dos medicamentos, exclusivamente ou quase exclusivamente, às células tumorais, reduzindo assim, suas atividades sobre as células saudáveis e os efeitos colaterais.
			</p>
		</div>

		<h6 class="content-title">Efeitos colaterais</h6>

		<div class="long-text">
			<p>
				As drogas de alvo molecular são menos tóxicas do que a quimioterapia. Causa menores efeitos colaterais como náuseas, vômito e queda de cabelo. Há algumas reações adversas que podem acontecer e é importante que o paciente esteja atento em caso de hipertensão arterial, inflamação na parte interna da boca, alterações na pele e/ou diarreia. 
			</p>
		</div>

		<h6 class="content-title">Duração do tratamento</h6>

		<div class="long-text">
			<p>
				Irá variar de acordo com o tamanho do tumor e suas características moleculares. 
			</p>
		</div>

	</div>
</section>