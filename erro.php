<section id="error" class="container-fluid">
	<div class="container">
		
		<div class="content">
			<img src="/img/logo.png" alt="Santé Cancer Center">
			<h1>Página não encontrada</h1>
			<h3>Infelizmente esse endereço não está disponível</h3>
			<a href="/">Voltar para o início</a>
		</div>

	</div>
</section>