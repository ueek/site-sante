<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Oncologia</h1>
		</div>

		<div class="long-text">
			<p>
				A oncologia é a área da medicina que é responsável pelo diagnóstico e tratamento especializado para cada tipo de câncer, levando em conta o organismo único de cada paciente. <br>
				O câncer é uma doença que pode variar de acordo com vários aspectos e é ideal que seja tratado de acordo com estes aspectos, levando em conta as particulares de cada organismo, estágios e desenvolvimento. A partir disto, o oncologista poderá definir a melhor abordagem clínica para tratar cada paciente. 
			</p>
		</div>

	</div>
</section>

<section id="galery">
	<div class="container p-0">
		
		<div id="images">
			
			<a href='/img/infraestrutura.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/infraestrutura.jpg);"></div>
			</a>

			<a href='/img/sobre-nos.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/sobre-nos.jpg);"></div>
			</a>

			<a href='/img/infraestrutura.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/infraestrutura.jpg);"></div>
			</a>

			<br>

		</div>

	</div>
</section>