<?php
	
    //Arquivos externos
    include_once 'config/database.php';

    include_once 'models/about.php';

    // Inicializar banco de dados
    $database = new Database();
    $db = $database->getConnection();

    // ==========================================================
    // Load About data
    // ==========================================================
    $urlImages_About = "/content-adm/uploads/about/"; 
    
    $myAbout = new About($db);
    $stmtAbout = $myAbout->read("", "id", "LIMIT 1");
    if ($stmtAbout->rowCount() > 0) {

	    $row = $stmtAbout->fetch(PDO::FETCH_ASSOC);
		extract($row);
		$myAbout->setId($id);
        $myAbout->setMission($mission);
        $myAbout->setVision($vision);
        $myAbout->setCompanyValues($company_values);
        $myAbout->setInfrastructure($infrastructure);
        $myAbout->setAttendance($attendance);
        $myAbout->setTeam($team);
        $myAbout->setPhone($phone);
        $myAbout->setWhatsapp($whatsapp);
        $myAbout->setEmail($email);
        $myAbout->setFacebook($facebook);
        $myAbout->setInstagram($instagram);
        $myAbout->setTwitter($twitter);
        $myAbout->setLinkedin($linkedin);
        $myAbout->setYoutube($youtube);
        $myAbout->setImageStructure($urlImages_About.$image_structure);
        $myAbout->setImageAttendance($urlImages_About.$image_attendance);
        $myAbout->setImageTeam($urlImages_About.$image_team);
    }

    
    // ==========================================================

?>

<section id="inicio" class="container-fluid leftShow">

	<div class="fullbanner banner-desktop owl-carousel owl-theme">

		<?php 
            if ($listOfBannersDesktop->rowCount() > 0) {

                while ($row = $listOfBannersDesktop->fetch(PDO::FETCH_ASSOC)){
                    extract($row);

                    $image_desktop = $urlImages_Banners.$image_desktop;

                    if ($video_url == '') {

                    	$button = '';

                    	if ($button_text != '') {
	                    	$button .= 
		                    	"<a class='btn soft-hover' href='{$button_url}'>
					                {$button_text}
					            </a>";
	                    }
                    	
                    	echo "
	                        <div class='item-banner' style='background-image: url({$image_desktop});'>
								<div class='container content-banner'>

						            <h1>{$title}</h1>

						            <p>{$text}</p>

						            {$button}
									 
								</div>
							</div>
                   		";

                    } else {

                    	echo "
                    	<div class='item-banner video' style='background-image: url({$image_desktop});'>
							<div class='container content-banner'>

					            <h1>{$title}</h1>

					            <p>{$text}</p>

				            	<a class='btn soft-hover' data-fancybox='galery-banner' data-width='640' data-height='360' href='{$video_url}'>
					                <i class='fas fa-caret-right pr-2'></i>Play
					            </a>
								 
							</div>
						</div>
                    	";

                    }

                }

            }
        ?>

	</div>

	<div class="fullbanner banner-mobile owl-carousel owl-theme">

		<?php 
            if ($listOfBannersMobile->rowCount() > 0) {

                while ($row = $listOfBannersMobile->fetch(PDO::FETCH_ASSOC)){
                    extract($row);

                    $image_mobile = $image_mobile != '' ? $urlImages_Banners.$image_mobile : $urlImages_Banners.$image_desktop;

                    if ($video_url == '') {

                    	$button = '';

                    	if ($button_text != '') {
	                    	$button .= 
		                    	"<a class='btn soft-hover' href='{$button_url}'>
					                {$button_text}
					            </a>";
	                    }

                   		echo "
	                        <div class='item-banner banner-mobile' style='background-image: linear-gradient(207.16deg, rgba(255, 255, 255, 0.156) 13.84%, rgba(0, 0, 0, 0.3) 58.26%), url({$image_mobile});'>
								<div class='container content-banner'>

						            <h1>{$title}</h1>

						            <p>{$text}</p>

						            {$button}
									 
								</div>
							</div>
                   		";

                    } else {

                    	echo "
                    	<div class='item-banner video banner-mobile' style='background-image: linear-gradient(0deg, rgba(37, 37, 37, 0.9), rgba(37, 37, 37, 0.9)), url({$image_mobile});'>
							<div class='container content-banner'>

					            <h1>{$title}</h1>

					            <p>{$text}</p>

				            	<a class='btn soft-hover' data-fancybox='galery-banner' data-width='640' data-height='360' href='{$video_url}'>
					                <i class='fas fa-caret-right pr-2'></i>Play
					            </a>
								 
							</div>
						</div>
                    	";

                    }

                }

            }
        ?>

	</div>

</section>

<section id="destaques" class="container-fluid">
	<div  class="container">

		<div class='col-lg-3 col-md-6 col-sm-12 item sequenced-left'>
			<div class='img' style='background-image: url(/img/atencao-dedicada.svg)'></div>
			<h2>Atenção Dedicada</h2>
			<p>Exclusividade e atenção personalizada é uma das melhores formas para acolher e tratar o paciente oncológico, potencializando os resultados.</p>
		</div>

		<div class='col-lg-3 col-md-6 col-sm-12 item sequenced-left'>
			<div class='img' style='background-image: url(/img/tratamento-personalizado.svg)'></div>
			<h2>Tratamentos personalizados</h2>
			<p>Cada paciente recebe um diagnóstico e tratamento personalizado, auxiliando numa melhor recuperação.</p>
		</div>

		<div class='col-lg-3 col-md-6 col-sm-12 item sequenced-left'>
			<div class='img' style='background-image: url(/img/qualidade-e-seguranca.svg)'></div>
			<h2>Qualidade e segurança</h2>
			<p>Qualidade de vida e bem estar é essencial, por isso, o paciente tem diversos especialistas à sua disposição.</p>
		</div>

		<div class='col-lg-3 col-md-6 col-sm-12 item sequenced-left'>
			<div class='img' style='background-image: url(/img/centro-de-mama.svg)'></div>
			<h2>Centro de Mama</h2>
			<p>Investimos em um espaço exclusivo para cuidado feminino onde é realizado consulta, mamografia e ultrassom em um só lugar.</p>
		</div>

		<br>

	</div>
</section>

<section id="sobre-nos" class="container-fluid leftShow">
	<div class="container">

		<div class="header">
			<img src="/img/header.png">
		</div>

		<div class="col-lg-8 col-md-12 col-sm-12 float-left pl-0 img-about slide-left">
			<div id="img-about-us" style='background-image: url(<?php echo $myAbout->getImageStructure() ?>), url(/img/img-background.jpg)'></div><br>
		</div>

		<div class="col-lg-4 col-md-12 col-sm-12 float-left pr-0 text-about slide-right">
			<div id="content-about-us">

				<h1>Conheça o Cancer Center</h1>

				<p><?php echo $myAbout->getInfrastructure() ?></p>

				<a class='btn soft-hover' href='/cancer-center'>
	                O Cancer Center
	            </a>

			</div>
		</div>

		<br>
		
	</div>
</section>

<section id="servicos" class="container-fluid rightShow">

	<div class="container-fluid" id="slide">

		<div class="container">
			
			<div class="col-lg-4 col-md-12 col-sm-12 p-0">
				
				<div class="header">
					<img src="/img/header.png">
					<br>
				</div>

				<h1>O que<br>fazemos?</h1>
				<p>Aqui o paciente oncológico é tratado de maneira única e o foco de toda equipe é o cuidado direcionado e a atenção dedicada.</p>

			</div>	

		</div>

		<div id="slide-cards" class='owl-carousel owl-theme'>

			<?php 

				if ($listOfServices->rowCount() > 0) {

	                while ($row = $listOfServices->fetch(PDO::FETCH_ASSOC)){
	                    extract($row);

	                    $image = $urlImages_Services.$image;

	                    $services = explode(';', $sub_categories);

	                    $services_list = "";
	                    $link = "";

	                    if (count($services) > 5) {

	                    	for ($i=0; $i < 3; $i++) { 
	                    		$services_list .= '<li>' . trim($services[$i]) . '</li>';
	                    	}

	                    	$link = "<a class='soft-hover' href='/tratamentos'>Ver mais
	                    				<svg xmlns='http://www.w3.org/2000/svg' width='16' height='10' viewBox='0 0 16 10' fill='none'>
											<path d='M15.4243 5.42426C15.6586 5.18995 15.6586 4.81005 15.4243 4.57574L11.6059 0.757359C11.3716 0.523045 10.9917 0.523045 10.7574 0.757359C10.523 0.991674 10.523 1.37157 10.7574 1.60589L14.1515 5L10.7574 8.39411C10.523 8.62843 10.523 9.00833 10.7574 9.24264C10.9917 9.47696 11.3716 9.47696 11.6059 9.24264L15.4243 5.42426ZM0 5.6H15V4.4H0V5.6Z' fill='#B6C72C'/>
										</svg>
	                    			 </a>";

	                    } else {

	                    	foreach ($services as $key => $service) {
		                    	$services_list .= '<li>' . trim($service) . '</li>';
		                    }

	                    }

	                    echo "

	                    <div class='item-banner'>
							<div class='card'>
								
								<div class='card-img' style='background-image: url({$image});'></div>
								
								<div class='card-body'>

									<h5 class='card-title btn'>{$name}</h5>
									
									<div class='card-text'>
										
										<ul>
											{$services_list}
										</ul>
										
										{$link}

							  		</div>

								</div>

							</div>
						</div>

	                    ";

	                }

	            }

			?>

		</div>

		<br>

	</div>
</section>

<section id="ensino" class="container-fluid leftShow">
    <div id="parallax">

        <div id="content" class="container-fluid">
            <div class="container">

            	<div class="desktop">
            		
            		<div class="col-lg-6 col-md-12 col-sm-12 float-left slide-left" id="img-parallax" style="background-image: url(/img/ensino-pesquisa.png);"></div>

	            	<div class="col-lg-6 col-md-12 col-sm-12 float-left slide-right">
	            		
	            		<div id="content-parallax">

							<h2>Ensino & Pesquisa</h2>

							<p>
								Movidos pela missão de promover a saúde e a qualidade de vida, com foco na excelência dos serviços prestados na área de oncologia e hematologia, O Santé Cancer Center busca constantemente promover o ensino e a pesquisa no tratamento do câncer.
							</p>

							<a class='btn soft-hover' href='/residencia-medica'>
				                Ensino & Pesquisa
				            </a>

						</div>

	            	</div>

	            	<br>

            	</div>

            	<div class="col-12 responsive">
            		
            		<div id="content-parallax">

						<h2>Ensino & Pesquisa</h2>

        				<div class="col-12" id="img-parallax" style="background-image: url(/img/ensino-pesquisa.png);"></div>

						<p>
							Movidos pela missão de promover a saúde e a qualidade de vida, com foco na excelência dos serviços prestados na área de oncologia e hematologia, O Santé Cancer Center busca constantemente promover o ensino e a pesquisa no tratamento do câncer.
						</p>

						<a class='btn soft-hover' href='/residencia-medica'>
			                Ensino & Pesquisa
			            </a>

					</div>

            	</div>

            </div>	
        </div>

    </div>
</section>

<section id="blog" class="container-fluid mt-0">
	<div class="container p-0">
		
		<div class="header">
			<img src="/img/sante-blog.png">
		</div>

		<div class="posts">

			<?php 

				if ($listOfPosts->rowCount() > 0) {

	                while ($row = $listOfPosts->fetch(PDO::FETCH_ASSOC)){
	                    extract($row);

	                    $image = $urlImages_Post.$image;

	                    // $published_at = date("F j, Y", strtotime($published_at));
	                    $published_at = strftime('%B %d, %Y', strtotime($published_at));

	                    echo "

	                    	<a class='soft-hover' href='/post/{$url}'>
				
								<div class='card'>

									<div class='card-img soft-hover' style='background-image: url({$image});'></div>
									
									<div class='card-body'>

										<h5 class='card-title'>
											{$title}
										</h5>
										
									</div>

									<div class='card-footer'>

										<span class='author'>{$author}</span>
										<span class='date'>{$published_at}</span>
										
									</div>

								</div>

							</a>

	                    ";


	                }

	            }

			?>

			<br>

		</div>

		<div class="bottom">
			<a class="btn" href="/blog">Ver Mais</a>
		</div>

	</div>
</section>

<a id="open-warning-modal" data-fancybox data-src="#warning-modal" href="javascript:;"></a>

<div id="warning-modal">
	
	<h4>Comunicado aos pacientes oncológicos</h4>

	<hr>

	<p>
		Para passarmos por esse momento delicado da melhor forma, continuamos muito atentos à saúde e ao bem-estar de cada pessoa. Por isso, vamos priorizar consulta de pacientes oncológicos: 
	</p>

	<ul>
		<li>Com diagnóstico ou suspeita de câncer;</li>
		<li>Em tratamento oncológico;</li>
		<li>Em seguimento com suspeita de recidiva.</li>
	</ul>

	<p>
		Pedimos que caso você se enquadre em qualquer um dos casos acima ou entenda que o seu caso é de urgência, envie seus exames para o e-mail: <b>lesante@lesante.com.br / recepcao@lesante.com.br </b> que nossa equipe retornará para você. Estamos sempre buscando a melhor forma para cuidar de você.
	</p>
	
	<div class="img" id="covid-img-1"></div>
	<div class="img" id="covid-img-2"></div>

</div>