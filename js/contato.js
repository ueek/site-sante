$(document).ready(function(){

	$('body').on('click', '.btn-close', function(){
		
		window.location.reload();

	});

	var sendContact = function(form) {

		$.ajax({
			url      : 'php/send-contact.php',
			type     : 'POST',
			dataType : 'JSON',
			data     : form.serialize(),
			success  : function(retorno) {

				$('input[name="send"]').removeAttr('disabled');

                if(retorno.status == 1){

					Swal.fire({
						text: retorno.msg,
						showConfirmButton: true,
						confirmButtonColor: '#B6C72C',
						onClose: () => {
							window.location.reload(); 
						}
					});

				}else{

                    Swal.fire({
						text: retorno.msg,
						showConfirmButton: true,
						confirmButtonColor: '#B6C72C',
					});
                    return false;

                }

			},
			error  : function(retorno) {
				alert("Error");
			}

		});	
	
	}

	$('form#send-contact').submit(function(e) {

		let form = $(this);

		e.preventDefault();

		if(validateInputs(form)){

			$('input[name="send"]').attr('disabled', 'disabled');
			
			sendContact(form);

		}

	});

	var schedule = function(form) {

		$.ajax({
			url      : 'php/schedule.php',
			type     : 'POST',
			dataType : 'JSON',
			data     : form.serialize(),
			success  : function(retorno) {

				$('input[name="send"]').removeAttr('disabled');

                if(retorno.status == 1){

					Swal.fire({
						text: retorno.msg,
						showConfirmButton: true,
						confirmButtonColor: '#B6C72C',
						onClose: () => {
							window.location.reload(); 
						}
					});

				}else{

                    Swal.fire({
						text: retorno.msg,
						showConfirmButton: true,
						confirmButtonColor: '#B6C72C',
					});
                    return false;

                }

			},
			error  : function(retorno) {
				alert("Error");
			}

		});	
	
	}

	$('form#schedule').submit(function(e) {

		let form = $(this);

		e.preventDefault();

		if(validateInputs(form)){

			$('input[name="send"]').attr('disabled', 'disabled');
			
			schedule(form);

		}

	});

	var jobs = function(form) {
        var valores = new FormData();
        valores.append('name', $('input[name=name]', form).val());
        valores.append('email', $('input[name=email]', form).val());
        valores.append('phone', $('input[name=phone]', form).val());
        valores.append('message', $('textarea[name=message]', form).val());
        valores.append('file', $("#file")[0].files[0]);

        $.ajax({
            url      : 'php/jobs.php',
            type: "POST",
            data     : valores,
            processData: false,
            contentType: false,
			success  : function(retorno) {

				$('input[name="send"]').removeAttr('disabled');

             	var json = $.parseJSON(retorno);

                if(json.status == 1){

					Swal.fire({
						text: json.msg,
						showConfirmButton: true,
						confirmButtonColor: '#B6C72C',
						onClose: () => {
							window.location.reload(); 
						}
					});

				}else{

                    Swal.fire({
						text: json.msg,
						showConfirmButton: true,
						confirmButtonColor: '#B6C72C',
					});
                    return false;

                }

			},
			error  : function(retorno) {
				alert("Error");
			}

		});	
	
	}

	$('form#jobs').submit(function(e) {

		let form = $(this);

		e.preventDefault();

		if(validateInputs(form)){

			$('input[name="send"]').attr('disabled', 'disabled');
			
			jobs(form);

		}

	});



	var internshipAndVisitations = function(form) {

		$.ajax({
			url      : 'php/internship-and-visitations.php',
			type     : 'POST',
			dataType : 'JSON',
			data     : form.serialize(),
			success  : function(retorno) {

				$('input[name="send"]').removeAttr('disabled');

                if(retorno.status == 1){

					Swal.fire({
						text: retorno.msg,
						showConfirmButton: true,
						confirmButtonColor: '#B6C72C',
						onClose: () => {
							window.location.reload(); 
						}
					});

				}else{

                    Swal.fire({
						text: retorno.msg,
						showConfirmButton: true,
						confirmButtonColor: '#B6C72C',
					});
                    return false;

                }

			},
			error  : function(retorno) {
				alert("Error");
			}

		});	
	
	}

	$('form.estagios-e-visitas').submit(function(e) {

		let form = $(this);

		e.preventDefault();

		if(validateInputs(form)){

			$('input[name="send"]').attr('disabled', 'disabled');
			
			internshipAndVisitations(form);

		}

	});
});