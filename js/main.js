function getCookie(cname) {

	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
	
	for(var i = 0; i <ca.length; i++) {
		
		var c = ca[i];

		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}

		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}

function setCookie(cname, cvalue, exdays) {

	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";

}

$(document).ready(function(){

	$('a[href*="#"]').not('[href="#"]').not('[href="#0"]').not('[href="#contact"]').not('[href="#schedule"]').not('[href="#jobs"]').click(function(event) {
		if(location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
			
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			
			if(target.length){

				event.preventDefault();
				
				if($(window).width() > 900) {

					$('html, body').animate({
						scrollTop: target.offset().top
					}, 600, function() {

						var $target = $(target);
						$target.focus();

						if ($target.is(":focus")) {
							return false;
						} else {
							$target.attr('tabindex','-1');
							$target.focus();
						};
					});

				} else {

					$('html, body').animate({
						scrollTop: target.offset().top
					}, 500, function() {
					
						var $target = $(target);
						$target.focus();

						if ($target.is(":focus")) {
							return false;
						} else {

							$target.attr('tabindex','-1');
							$target.focus();
						};

					});

				}
			}
		}
	});

	// $('#header-page').addClass('leftShow');

	$('.posts .card').addClass('bottomShow');

	$('p.long-text').addClass('bottomShow');

	/* Remove efeito em celulares */
	if($(window).width() < 680) {

		$('.rightShow').removeClass('rightShow');
		$('.leftShow').removeClass('leftShow');
		$('.slide-left').removeClass('slide-left');
		$('.slide-right').removeClass('slide-right');
		$('.slide-up').removeClass('slide-up');
		$('.sequenced-left').removeClass('sequenced-left');

	}

	/* Efeitos */
	window.sr = ScrollReveal({ reset: true, distance: '80px' });

	sr.reveal('.efeito');

	sr.reveal('.fadeIn', { duration: 3000 });

	sr.reveal('.rightShow', { 
	  origin: 'right', 
	  duration: 3000 
	});

	sr.reveal('.leftShow', { 
	  origin: 'left', 
	  duration: 3000 
	});

	sr.reveal('.topShow', { 
	  origin: 'top', 
	  duration: 3000 
	});

	sr.reveal('.bottomShow', { 
	  origin: 'bottom', 
	  duration: 3000 
	});

	var slideBottom = {
	    distance: '100%',
	    origin: 'bottom',
	    opacity: null,
	    reset: false,
	    duration: 3200
	};
	ScrollReveal().reveal('.slide-up', slideBottom);


	var slideLeft = {
	    distance: '100%',
	    origin: 'left',
	    opacity: null,
	    reset: false,
	    duration: 3200
	};
	ScrollReveal().reveal('.slide-left', slideLeft);


	var slideRight = {
	    distance: '100%',
	    origin: 'right',
	    opacity: null,
	    reset: false,
	    duration: 3200
	};
	ScrollReveal().reveal('.slide-right', slideRight);

	var slideTop = {
	    distance: '100%',
	    origin: 'top',
	    opacity: null,
	    reset: false,
	    duration: 3200
	};
	ScrollReveal().reveal('.slide-top', slideTop);

	var slideSequencedLeft = {
	    origin: 'left',
	    reset: false,
	    duration: 2000,
	    interval: 400
	};
	ScrollReveal().reveal('.sequenced-left', slideSequencedLeft);

	/* Banners */
	$('.fullbanner.owl-carousel').owlCarousel({
	    nav: true,
	    navText: ["<i class='fas fa-chevron-left'></i>","<i class='fas fa-chevron-right'></i>"],
		loop: true,
		lazyLoad: true,
		dots: true,
		margin: 0,
		autoplay: true,
		autoplayTimeout: 10000,
		items: 1
	});

	$('#slide-cards.owl-carousel').owlCarousel({
        items:3,
		dots: false,
        loop:false,
        margin:10,
        nav:true,
		navText: ["<i class='fas fa-arrow-left'></i>","<i class='fas fa-arrow-right'></i>"],
		responsive: {
			0: {
				items: 1,
				stagePadding: 20,
				margin: 15,
			},
			330: {
				items: 1,
				stagePadding: 20,
				margin: 20,
			},
			380: {
				items: 1,
				stagePadding: 40,
				margin: 20,
			},
			430: {
				items: 1,
				stagePadding: 120,
				margin: 20,
			},
			690: {
				items: 2,
				stagePadding: 40,
				margin: 20,
			},
			770: {
				items: 2,
				stagePadding: 80,
				margin: 20,
			},
			1000: {
				items: 2,
				stagePadding: 10,
				margin: 30,
			},
			1100: {
				items: 2,
				stagePadding: 60,
				margin: 30,
			},
			1290: {
				items: 2,
				stagePadding: 100,
				margin: 30,
			},
			1370: {
				items: 2,
				stagePadding: 130,
				margin: 30,
			},
			1450: {
				items: 3,
				margin: 10,
			},
		}
	});

	$(window).bind('scroll', function () {

		if($(window).width() > 1024){

			if ($(window).scrollTop() > 5){
				
				$('#bar-header').fadeOut(200);
				$('#header-desktop').css('top', '0px');
				$('#menu-desktop').css('top', '140px');

			}

			if($(window).scrollTop() < 5){

				$('#bar-header').fadeIn(200);
				$('#header-desktop').css('top', '50px');
				$('#menu-desktop').css('top', '190px');

			} 

		} else {

			if ($(window).scrollTop() > 5){
				
				$('#bar-header').fadeOut(100);
				$('#menu-responsive').css('top', '0px');

			}

			if($(window).scrollTop() < 5){

				$('#bar-header').fadeIn(100);
				$('#menu-responsive').css('top', '50px');

			} 

		}

	});

	/* Alterações no campo de envio de arquivos */
	var fileInput = $('.file-input');
	var droparea  = $('.file-drop-area');
	var btn       = $('.fake-btn');

	// highlight drag area
	fileInput.on('dragenter focus click', function() {
		droparea.addClass('is-active');
	});

	// back to normal state
	fileInput.on('dragleave blur drop', function() {
		droparea.removeClass('is-active');
	});

	// change inner text
	fileInput.on('change', function() {
		var filesCount    = $(this)[0].files.length;
		var textContainer = $('.file-msg');
		
		btn.text("Alterar");

		if (filesCount === 1) {
			// if single file is selected, show file name
			var fileName = $(this).val().split('\\').pop();
			textContainer.text(fileName);
		} else {
			// otherwise show number of files
			textContainer.text(filesCount + ' arquivos selecionados');
		}
	});

	/* Menu Lateral */
	$("#open-menu").click(function() {
		$("nav#menu").show("slide", { direction: "right" }, 600);
	});
	$("#close-menu").click(function() {
		$("nav#menu").hide("slide", { direction: "right" }, 600);
	});
	
	/* Controle dos ícones dos collapses de linha de produto */
	$(".product-line .card-header").click(function() {

		var icon = $(this).find('img');

		if ($(this).next('.collapse').hasClass('show')) {
			icon.hide(200);
			icon.attr("src", "img/plus.svg");
			icon.show(200);
		} else {
			icon.hide(200);
			icon.attr("src", "img/minus.svg");
			icon.show(200);
		}

	});

	$('#content-article .collapse')
	    .on('shown.bs.collapse', function() {
	        $(this)
	            .parent()
	            .find(".fa-plus")
	            .removeClass("fa-plus")
	            .addClass("fa-minus");
	    })
	    .on('hidden.bs.collapse', function() {
	        $(this)
	            .parent()
	            .find(".fa-minus")
	            .removeClass("fa-minus")
	            .addClass("fa-plus");
	    });

	var getUrlParameter = function(sParam) {

	    var sPageURL = window.location.search.substring(1),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;

	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');

	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
	        }
	    }

	};

	var url = getUrlParameter('area-de-contato');

	if (url != undefined) {

		switch(url) {

			case 'agende-sua-consulta':

			    $('html, body').animate({
					scrollTop: $('#area-de-contato').offset().top
				}, 500);

				$('#schedule-tab').click();

			    break;

			case 'trabalhe-conosco':

			    $('html, body').animate({
					scrollTop: $('#area-de-contato').offset().top
				}, 500);

				$('#jobs-tab').click();
				
			    break;

			case 'fale-conosco':

			    $('html, body').animate({
					scrollTop: $('#area-de-contato').offset().top
				}, 500);

				$('#contact-tab').click();
				
			    break;
			    
		}	

	}

	if (!document.cookie.includes('open-modal=false')) {
		$('#open-warning-modal').click();		
		document.cookie = "open-modal=false";
	}

	var showCookieWarning = getCookie('santecookiesacceptance');

	if(showCookieWarning != '1'){

		$("#cookies-warning").fadeIn();

	}

	$('.bt-ok-cookie').click(function(){ 
		setCookie('santecookiesacceptance', '1', 7); 
		$('#cookies-warning').remove(); 
	});

});

/* Máscaras */
function Mascara(o,f){
	v_obj=o
	v_fun=f
	setTimeout("execmascara()",1)
}

function execmascara(){
	v_obj.value=v_fun(v_obj.value)
}

function leech(v){
	v=v.replace(/o/gi,"0")
	v=v.replace(/i/gi,"1")
	v=v.replace(/z/gi,"2")
	v=v.replace(/e/gi,"3")
	v=v.replace(/a/gi,"4")
	v=v.replace(/s/gi,"5")
	v=v.replace(/t/gi,"7")
	return v
}

function Integer(v){
	return v.replace(/\D/g,"")
}

function Telefone(v){
	v=v.replace(/\D/g,"")
	v=v.replace(/^(\d\d)(\d)/g,"($1) $2")
	v=v.replace(/(\d{4})(\d)/,"$1-$2")
	return v
}

function Data(v){
	v=v.replace(/\D/g,"")
	v=v.replace(/(\d{2})(\d)/,"$1/$2")
	v=v.replace(/(\d{2})(\d)/,"$1/$2")
	return v
}


