<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Hematologia</h1>
		</div>

		<div class="long-text">
			<p>
				A Hematologia é a especialidade da medicina que é responsável por cuidar principalmente da fisiologia e patologia do sangue e seus componentes (glóbulos brancos, glóbulos vermelhos e plaquetas) também estuda os órgão onde são produzidos, incluindo a medula óssea e gânglios linfáticos. 
			</p>
		</div>

	</div>
</section>

<section id="galery">
	<div class="container p-0">
		
		<div id="images">
			
			<a href='/img/infraestrutura.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/infraestrutura.jpg);"></div>
			</a>

			<a href='/img/sobre-nos.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/sobre-nos.jpg);"></div>
			</a>

			<a href='/img/infraestrutura.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/infraestrutura.jpg);"></div>
			</a>

			<br>

		</div>

	</div>
</section>