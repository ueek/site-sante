<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Diagnóstico por imagem e PET-CT</h1>
		</div>

		<div class="long-text">
			<p>
				O tratamento personalizado é outro diferencial da Le Santé, já que todo o conceito de atendimento e estrutura foi planejado a partir da noção de que cada indivíduo possui necessidades e características específicas. Com todos esses aspectos em mente, a Le Santé definiu sua política de qualidade, que é "promover a saúde e a qualidade de vida, com foco na excelência dos serviços prestados, oferecendo atendimento personalizado e dedicação completa a cada paciente.
			</p>
		</div>

	</div>
</section>