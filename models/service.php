<?php

include_once 'base-model.php';

class Service extends BaseModel
{
    // object properties
    private $name;
    private $image;
    private $sub_categories;
    private $url;

	function __construct($db)
	{
        $this->setConn($db);
		$this->setTableName("services");
        $this->setBaseQueryFields
        (
            "
                name=:name, 
                image=:image,
                sub_categories=:sub_categories,
                url=:url

            "
        );
	}

    private function sanitize(){
        $this->name=htmlspecialchars(strip_tags($this->name));     
        $this->image=htmlspecialchars(strip_tags($this->image));    
        $this->sub_categories=htmlspecialchars(strip_tags($this->sub_categories));     
        $this->url=htmlspecialchars(strip_tags($this->url));     
    }

    private function bindParams(){
        $this->getStmt()->bindParam(":name", $this->name);
        $this->getStmt()->bindParam(":image", $this->image);
        $this->getStmt()->bindParam(":sub_categories", $this->sub_categories);
        $this->getStmt()->bindParam(":url", $this->url);
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    created_at=NOW(),
                    updated_at=NOW(),
                    status=1";

        // prepare query
        //$this->getConn()->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
        $this->setStmt($this->getConn()->prepare($query));
     

        // sanitize
        // sanitize
        $this->sanitize();
     
        // bind values
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return $this->getConn()->lastInsertId($this->getTableName());
        }

        // echo "\nPDOStatement::errorInfo():\n";
        // $arr = $this->getConn()->errorInfo();
        // error_log(print_r($arr));

        return 0;            
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    updated_at=NOW() 
                WHERE id=:id";

        // prepare query
        $this->setStmt($this->getConn()->prepare($query));
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->sanitize();
     
        // bind values
        $id = $this->getId();
        $this->getStmt()->bindParam(":id", $id);
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return true;
        }

        return false;
    }

    

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     *
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubCategories()
    {
        return $this->sub_categories;
    }

    /**
     * @param mixed $sub_categories
     *
     * @return self
     */
    public function setSubCategories($sub_categories)
    {
        $this->sub_categories = $sub_categories;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }
}