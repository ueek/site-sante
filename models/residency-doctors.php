<?php


include_once 'base-model.php';


class ResidencyDoctors extends BaseModel
{
    // object properties
    private $reside;
    private $image;   
    private $charge;   
    private $crm;   
    private $rqe;   
    private $linkedin;   

	function __construct($db)
	{
        $this->setConn($db);
		$this->setTableName("clinic_team");
        $this->setBaseQueryFields
        (
            "
                name=:name,
                image=:image,
                charge=:charge,
                crm=:crm,
                rqe=:rqe,
                linkedin=:linkedin
            "
        );
	}

    private function sanitize(){       
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->image=htmlspecialchars(strip_tags($this->image));              
        $this->charge=htmlspecialchars(strip_tags($this->charge));              
        $this->crm=htmlspecialchars(strip_tags($this->crm));              
        $this->rqe=htmlspecialchars(strip_tags($this->rqe));              
        $this->linkedin=htmlspecialchars(strip_tags($this->linkedin));              
    }

    private function bindParams(){ 
        $this->getStmt()->bindParam(":name", $this->name);   
        $this->getStmt()->bindParam(":image", $this->image);            
        $this->getStmt()->bindParam(":charge", $this->charge);            
        $this->getStmt()->bindParam(":crm", $this->crm);            
        $this->getStmt()->bindParam(":rqe", $this->rqe);            
        $this->getStmt()->bindParam(":linkedin", $this->linkedin);            
    }

    function create(){

        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    created_at=NOW(),
                    updated_at=NOW(),
                    status=1";

        // prepare query
        //$this->getConn()->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
        $this->setStmt($this->getConn()->prepare($query));     

        // sanitize
        // sanitize
        $this->sanitize();
     
        // bind values
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return $this->getConn()->lastInsertId($this->getTableName());
        }

        // echo "\nPDOStatement::errorInfo():\n";
        // $arr = $this->getConn()->errorInfo();
        // error_log(print_r($arr));

        return 0;            
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    updated_at=NOW() 
                WHERE id=:id";

        // prepare query
        $this->setStmt($this->getConn()->prepare($query));
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->sanitize();
     
        // bind values
        $id = $this->getId();
        $this->getStmt()->bindParam(":id", $id);
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return true;
        }

        return false;
    }   

}