<?php

include_once 'base-model.php';

class ResidencyPublish extends BaseModel
{
    // object properties
    private $residency_id;
    private $title;   
    private $url;   

	function __construct($db)
	{
        $this->setConn($db);
		$this->setTableName("residency_publishes");
        $this->setBaseQueryFields
        (
            "
                residency_id=:residency_id,
                title=:title,
                url=:url
            "
        );
	}

    private function sanitize(){       
        $this->residency_id=htmlspecialchars(strip_tags($this->residency_id));
        $this->title=htmlspecialchars(strip_tags($this->title));              
        $this->url=htmlspecialchars(strip_tags($this->url));           
    }

    private function bindParams(){ 
        $this->getStmt()->bindParam(":residency_id", $this->residency_id);   
        $this->getStmt()->bindParam(":title", $this->title);            
        $this->getStmt()->bindParam(":url", $this->url);                      
    }

    function create(){

        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    created_at=NOW(),
                    updated_at=NOW(),
                    status=1";

        // prepare query
        //$this->getConn()->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
        $this->setStmt($this->getConn()->prepare($query));     

        // sanitize
        // sanitize
        $this->sanitize();
     
        // bind values
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return $this->getConn()->lastInsertId($this->getTableName());
        }

        // echo "\nPDOStatement::errorInfo():\n";
        // $arr = $this->getConn()->errorInfo();
        // error_log(print_r($arr));

        return 0;            
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    updated_at=NOW() 
                WHERE id=:id";

        // prepare query
        $this->setStmt($this->getConn()->prepare($query));
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->sanitize();
     
        // bind values
        $id = $this->getId();
        $this->getStmt()->bindParam(":id", $id);
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return true;
        }

        return false;
    }   

    function readByResidencyId($id){

        $query = "SELECT rp.* FROM {$this->getTableName()} rp
             WHERE residency_id = {$id} AND rp.status = 1";

        // Executar a query e retornar os resultados
        $stmt = $this->getConn()->prepare( $query );
        $stmt->execute();

        $arr = $stmt->errorInfo();

        return $stmt;
    }

    /**
     * @return mixed
     */
    public function getResidencyId()
    {
        return $this->residency_id;
    }

    /**
     * @param mixed $residency_id
     *
     * @return self
     */
    public function setResidencyId($residency_id)
    {
        $this->residency_id = $residency_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

}