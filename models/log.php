<?php
include_once 'base-model.php';
class Log extends BaseModel
{
    // object properties
    private $admin_id;
    private $action;

	function __construct($db)
	{
        $this->setConn($db);
		$this->setTableName("logs");
        $this->setBaseQueryFields
        (
            "
                admin_id=:admin_id, 
                action=:action 
            "
        );
	}

    private function sanitize(){
        $this->admin_id=htmlspecialchars(strip_tags($this->admin_id));
        $this->action=htmlspecialchars(strip_tags($this->action));
    }

    private function bindParams(){
        $this->getStmt()->bindParam(":admin_id", $this->admin_id);
        $this->getStmt()->bindParam(":action", $this->action);
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    created_at=NOW(),
                    updated_at=NOW(),
                    status=1";

        // prepare query
        //$this->getConn()->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
        $this->setStmt($this->getConn()->prepare($query));
     

        // sanitize
        // sanitize
        $this->sanitize();
     
        // bind values
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return $this->getConn()->lastInsertId($this->getTableName());
        }

        // echo "\nPDOStatement::errorInfo():\n";
        // $arr = $this->getConn()->errorInfo();
        // error_log(print_r($arr));

        return 0;            
    }

    /**
     * @return mixed
     */
    public function getAdminId()
    {
        return $this->admin_id;
    }

    /**
     * @param mixed $admin_id
     *
     * @return self
     */
    public function setAdminId($admin_id)
    {
        $this->admin_id = $admin_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param mixed $action
     *
     * @return self
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }
}