<?php
include_once 'base-model.php';
class BlogPhoto extends BaseModel
{
    // object properties
    private $blog_id;
    private $image;
    
    function __construct($db)
    {
        $this->setConn($db);
        $this->setTableName("blog_photos");
        $this->setBaseQueryFields
        (
            "
                blog_id=:blog_id,
                image=:image
            "
        );
    }

    private function sanitize(){
        $this->blog_id=htmlspecialchars(strip_tags($this->blog_id));        
        $this->image=htmlspecialchars(strip_tags($this->image));        
    }

    private function bindParams(){
        $this->getStmt()->bindParam(":blog_id", $this->blog_id);     
        $this->getStmt()->bindParam(":image", $this->image);     
    }

    function readAllByBlogId($blog_id){
        $query= "SELECT * 
                FROM ".$this->getTableName()."
                 WHERE blog_id= {$blog_id} AND status=1";

        // Executar a query e retornar os resultados
        $stmt = $this->getConn()->prepare( $query );
        $stmt->execute();

        // $arr = $stmt->errorInfo();

        // error_log($query, 0);

        return $stmt;
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    created_at=NOW(),
                    updated_at=NOW(),
                    status=1";

        // prepare query
        //$this->getConn()->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
        $this->setStmt($this->getConn()->prepare($query));
     

        // sanitize
        // sanitize
        $this->sanitize();
     
        // bind values
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return $this->getConn()->lastInsertId($this->getTableName());
        }

        // echo "\nPDOStatement::errorInfo():\n";
        // $arr = $this->getConn()->errorInfo();
        // error_log(print_r($arr));

        return 0;            
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    updated_at=NOW() 
                WHERE id=:id";

        // prepare query
        $this->setStmt($this->getConn()->prepare($query));
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->sanitize();
     
        // bind values
        $id = $this->getId();
        $this->getStmt()->bindParam(":id", $id);
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return true;
        }

        return false;
    }


    /**
     * @return mixed
     */
    public function getBlogId()
    {
        return $this->blog_id;
    }

    /**
     * @param mixed $blog_id
     *
     * @return self
     */
    public function setBlogId($blog_id)
    {
        $this->blog_id = $blog_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     *
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }
}