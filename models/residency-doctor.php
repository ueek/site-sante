<?php


include_once 'base-model.php';


class ResidencyDoctor extends BaseModel
{
    // object properties
    private $residency_id;
    private $clinic_team_id;   

	function __construct($db)
	{
        $this->setConn($db);
		$this->setTableName("residency_doctors");
        $this->setBaseQueryFields
        (
            "
                residency_id=:residency_id,
                clinic_team_id=:clinic_team_id
            "
        );
	}

    function readDoctorsByResidencyId($residency_id){
        $query = "SELECT clinic_team.*
                    FROM {$this->getTableName()} residency_doctor
                    JOIN clinic_team clinic_team ON clinic_team.id = residency_doctor.clinic_team_id
                    WHERE residency_doctor.status = 1 AND residency_doctor.residency_id = {$residency_id}
                    ORDER BY clinic_team.name
                 ";

        //error_log($query);

        // Executar a query e retornar os resultados
        $stmt = $this->getConn()->prepare( $query );
        $stmt->execute();

        $arr = $stmt->errorInfo();

        return $stmt;
    }

    private function sanitize(){       
        $this->residency_id=htmlspecialchars(strip_tags($this->residency_id));
        $this->clinic_team_id=htmlspecialchars(strip_tags($this->clinic_team_id));                   
    }

    private function bindParams(){ 
        $this->getStmt()->bindParam(":residency_id", $this->residency_id);   
        $this->getStmt()->bindParam(":clinic_team_id", $this->clinic_team_id);                   
    }

    function create(){

        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    created_at=NOW(),
                    updated_at=NOW(),
                    status=1";

        // prepare query
        //$this->getConn()->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
        $this->setStmt($this->getConn()->prepare($query));     

        // sanitize
        // sanitize
        $this->sanitize();
     
        // bind values
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return $this->getConn()->lastInsertId($this->getTableName());
        }

        // echo "\nPDOStatement::errorInfo():\n";
        // $arr = $this->getConn()->errorInfo();
        // error_log(print_r($arr));

        return 0;            
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    updated_at=NOW() 
                WHERE id=:id";

        // prepare query
        $this->setStmt($this->getConn()->prepare($query));
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->sanitize();
     
        // bind values
        $id = $this->getId();
        $this->getStmt()->bindParam(":id", $id);
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return true;
        }

        return false;
    }   

    function deleteAllByResidencyId($residency_id){
        $query = "DELETE
                    FROM {$this->getTableName()} 
                    WHERE residency_id = {$residency_id}
                 ";

        // Executar a query e retornar os resultados
        $stmt = $this->getConn()->prepare( $query );
        $stmt->execute();

        $arr = $stmt->errorInfo();

        return $stmt;
    }

    /**
     * @return mixed
     */
    public function getResidencyId()
    {
        return $this->residency_id;
    }

    /**
     * @param mixed $residency_id
     *
     * @return self
     */
    public function setResidencyId($residency_id)
    {
        $this->residency_id = $residency_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getClinicTeamId()
    {
        return $this->clinic_team_id;
    }

    /**
     * @param mixed $clinic_team_id
     *
     * @return self
     */
    public function setClinicTeamId($clinic_team_id)
    {
        $this->clinic_team_id = $clinic_team_id;

        return $this;
    }
    
}