<?php


include_once 'base-model.php';


class Doctors extends BaseModel
{
    // object properties
    private $name;
    private $image;   
    private $charge;   
    private $crm;   
    private $rqe;   
    private $linkedin;   
    private $graduation;   
    private $graduation_year;   
    private $graduation_institution;   
    private $effective;   
    private $sequence;   

  	function __construct($db)
  	{
          $this->setConn($db);
  		    $this->setTableName("clinic_team");
          $this->setBaseQueryFields
          (
              "
                  name=:name,
                  image=:image,
                  charge=:charge,
                  crm=:crm,
                  rqe=:rqe,
                  linkedin=:linkedin,
                  graduation=:graduation,
                  graduation_year=:graduation_year,
                  graduation_institution=:graduation_institution,
                  effective=:effective,
                  sequence=:sequence
              "
          );
  	}

    private function sanitize(){       
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->image=htmlspecialchars(strip_tags($this->image));              
        $this->charge=htmlspecialchars(strip_tags($this->charge));              
        $this->crm=htmlspecialchars(strip_tags($this->crm));              
        $this->rqe=htmlspecialchars(strip_tags($this->rqe));              
        $this->linkedin=htmlspecialchars(strip_tags($this->linkedin));              
        $this->graduation=htmlspecialchars(strip_tags($this->graduation));              
        $this->graduation_year=htmlspecialchars(strip_tags($this->graduation_year));              
        $this->graduation_institution=htmlspecialchars(strip_tags($this->graduation_institution));              
        $this->effective=htmlspecialchars(strip_tags($this->effective));              
        $this->sequence=htmlspecialchars(strip_tags($this->sequence));              
    }

    private function bindParams(){ 
        $this->getStmt()->bindParam(":name", $this->name);   
        $this->getStmt()->bindParam(":image", $this->image);            
        $this->getStmt()->bindParam(":charge", $this->charge);            
        $this->getStmt()->bindParam(":crm", $this->crm);            
        $this->getStmt()->bindParam(":rqe", $this->rqe);            
        $this->getStmt()->bindParam(":linkedin", $this->linkedin);            
        $this->getStmt()->bindParam(":graduation", $this->graduation);            
        $this->getStmt()->bindParam(":graduation_year", $this->graduation_year);            
        $this->getStmt()->bindParam(":graduation_institution", $this->graduation_institution);            
        $this->getStmt()->bindParam(":effective", $this->effective);            
        $this->getStmt()->bindParam(":sequence", $this->sequence);            
    }

    function create(){

        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    created_at=NOW(),
                    updated_at=NOW(),
                    status=1";

        // prepare query
        //$this->getConn()->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
        $this->setStmt($this->getConn()->prepare($query));     

        // sanitize
        // sanitize
        $this->sanitize();
     
        // bind values
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return $this->getConn()->lastInsertId($this->getTableName());
        }

        // echo "\nPDOStatement::errorInfo():\n";
        // $arr = $this->getConn()->errorInfo();
        // error_log(print_r($arr));

        return 0;            
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    updated_at=NOW() 
                WHERE id=:id";

        // prepare query
        $this->setStmt($this->getConn()->prepare($query));
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->sanitize();
     
        // bind values
        $id = $this->getId();
        $this->getStmt()->bindParam(":id", $id);
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return true;
        }

        return false;
    }   

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     *
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCharge()
    {
        return $this->charge;
    }

    /**
     * @param mixed $charge
     *
     * @return self
     */
    public function setCharge($charge)
    {
        $this->charge = $charge;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCrm()
    {
        return $this->crm;
    }

    /**
     * @param mixed $crm
     *
     * @return self
     */
    public function setCrm($crm)
    {
        $this->crm = $crm;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRqe()
    {
        return $this->rqe;
    }

    /**
     * @param mixed $rqe
     *
     * @return self
     */
    public function setRqe($rqe)
    {
        $this->rqe = $rqe;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLinkedin()
    {
        return $this->linkedin;
    }

    /**
     * @param mixed $linkedin
     *
     * @return self
     */
    public function setLinkedin($linkedin)
    {
        $this->linkedin = $linkedin;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGraduation()
    {
        return $this->graduation;
    }

    /**
     * @param mixed $graduation
     *
     * @return self
     */
    public function setGraduation($graduation)
    {
        $this->graduation = $graduation;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGraduationYear()
    {
        return $this->graduation_year;
    }

    /**
     * @param mixed $graduation_year
     *
     * @return self
     */
    public function setGraduationYear($graduation_year)
    {
        $this->graduation_year = $graduation_year;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGraduationInstitution()
    {
        return $this->graduation_institution;
    }

    /**
     * @param mixed $graduation_institution
     *
     * @return self
     */
    public function setGraduationInstitution($graduation_institution)
    {
        $this->graduation_institution = $graduation_institution;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEffective()
    {
        return $this->effective;
    }

    /**
     * @param mixed $effective
     *
     * @return self
     */
    public function setEffective($effective)
    {
        $this->effective = $effective;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * @param mixed $sequence
     *
     * @return self
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;

        return $this;
    }
}