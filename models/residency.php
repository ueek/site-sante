<?php
include_once 'base-model.php';
class Residency extends BaseModel
{
    // object properties
    private $name;
    private $description;   
    private $information;   
    private $admition;   

	function __construct($db)
	{
        $this->setConn($db);
		$this->setTableName("residencies");
        $this->setBaseQueryFields
        (
            "
                name=:name,
                description=:description,
                information=:information,
                admition=:admition
            "
        );
	}

    private function sanitize(){       
        $this->name=htmlspecialchars(strip_tags($this->name));
        // $this->description=htmlspecialchars($this->description);              
        // $this->information=htmlspecialchars($this->information);              
        // $this->admition=htmlspecialchars($this->admition);              
    }

    private function bindParams(){ 
        $this->getStmt()->bindParam(":name", $this->name);   
        $this->getStmt()->bindParam(":description", $this->description);            
        $this->getStmt()->bindParam(":information", $this->information);            
        $this->getStmt()->bindParam(":admition", $this->admition);            
    }

    function create(){

        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    created_at=NOW(),
                    updated_at=NOW(),
                    status=1";

        // prepare query
        //$this->getConn()->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
        $this->setStmt($this->getConn()->prepare($query));     

        // sanitize
        // sanitize
        $this->sanitize();
     
        // bind values
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return $this->getConn()->lastInsertId($this->getTableName());
        }

        // echo "\nPDOStatement::errorInfo():\n";
        // $arr = $this->getConn()->errorInfo();
        // error_log(print_r($arr));

        return 0;            
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    updated_at=NOW() 
                WHERE id=:id";

        // prepare query
        $this->setStmt($this->getConn()->prepare($query));
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->sanitize();
     
        // bind values
        $id = $this->getId();
        $this->getStmt()->bindParam(":id", $id);
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInformation()
    {
        return $this->information;
    }

    /**
     * @param mixed $information
     *
     * @return self
     */
    public function setInformation($information)
    {
        $this->information = $information;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdmition()
    {
        return $this->admition;
    }

    /**
     * @param mixed $admition
     *
     * @return self
     */
    public function setAdmition($admition)
    {
        $this->admition = $admition;

        return $this;
    }
    
}