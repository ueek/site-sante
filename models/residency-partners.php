<?php

include_once 'base-model.php';

class ResidencyPartner extends BaseModel
{
    // object properties
    private $residency_id;
    private $institutions;   

	function __construct($db)
	{
        $this->setConn($db);
		$this->setTableName("residency_partners");
        $this->setBaseQueryFields
        (
            "
                residency_id=:residency_id,
                institutions=:institutions
            "
        );
	}

    private function sanitize(){       
        $this->residency_id=htmlspecialchars(strip_tags($this->residency_id));
        $this->institutions=htmlspecialchars(strip_tags($this->institutions));       
    }

    private function bindParams(){ 
        $this->getStmt()->bindParam(":residency_id", $this->residency_id);   
        $this->getStmt()->bindParam(":institutions", $this->institutions);                     
    }

    function create(){

        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    created_at=NOW(),
                    updated_at=NOW(),
                    status=1";

        // prepare query
        //$this->getConn()->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
        $this->setStmt($this->getConn()->prepare($query));     

        // sanitize
        // sanitize
        $this->sanitize();
     
        // bind values
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return $this->getConn()->lastInsertId($this->getTableName());
        }

        // echo "\nPDOStatement::errorInfo():\n";
        // $arr = $this->getConn()->errorInfo();
        // error_log(print_r($arr));

        return 0;            
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    updated_at=NOW() 
                WHERE id=:id";

        // prepare query
        $this->setStmt($this->getConn()->prepare($query));
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->sanitize();
     
        // bind values
        $id = $this->getId();
        $this->getStmt()->bindParam(":id", $id);
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return true;
        }

        return false;
    }   


    /**
     * @return mixed
     */
    public function getResidencyId()
    {
        return $this->residency_id;
    }

    /**
     * @param mixed $residency_id
     *
     * @return self
     */
    public function setResidencyId($residency_id)
    {
        $this->residency_id = $residency_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInstitutions()
    {
        return $this->institutions;
    }

    /**
     * @param mixed $institutions
     *
     * @return self
     */
    public function setInstitutions($institutions)
    {
        $this->institutions = $institutions;

        return $this;
    }

}