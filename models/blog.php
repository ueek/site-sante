<?php
include_once 'base-model.php';
class Blog extends BaseModel
{
    // object properties
    private $blog_category_id;
    private $title;
    private $text;
    private $url;
    private $author;
    private $image;
    private $published_at;

	function __construct($db)
	{
        $this->setConn($db);
		$this->setTableName("blogs");
        $this->setBaseQueryFields
        (
            "
                blog_category_id=:blog_category_id,
                title=:title,
                url=:url,
                author=:author,
                image=:image,
                text=:text,
                published_at=:published_at
            "
        );
	}

    private function sanitize(){
        $this->blog_category_id=htmlspecialchars(strip_tags($this->blog_category_id));        
        $this->title=htmlspecialchars(strip_tags($this->title));        
        $this->url=htmlspecialchars(strip_tags($this->url));        
        $this->author=htmlspecialchars(strip_tags($this->author));        
        $this->image=htmlspecialchars(strip_tags($this->image));        
        $this->published_at=htmlspecialchars(strip_tags($this->published_at));        
        // $this->text=htmlspecialchars(strip_tags($this->text));        
    }

    private function bindParams(){
        $this->getStmt()->bindParam(":blog_category_id", $this->blog_category_id);     
        $this->getStmt()->bindParam(":title", $this->title);     
        $this->getStmt()->bindParam(":url", $this->url);     
        $this->getStmt()->bindParam(":author", $this->author);     
        $this->getStmt()->bindParam(":image", $this->image);     
        $this->getStmt()->bindParam(":text", $this->text);     
        $this->getStmt()->bindParam(":published_at", $this->published_at, PDO::PARAM_STR);     
    }

    function readWithCategory(){
        $query = "SELECT b.*, c.name AS category
                    FROM {$this->getTableName()} b
                    JOIN blog_categories c ON c.id = b.blog_category_id
                    WHERE b.status = 1
                    ORDER BY published_at DESC
                 ";

        // Executar a query e retornar os resultados
        $stmt = $this->getConn()->prepare( $query );
        $stmt->execute();

        $arr = $stmt->errorInfo();

        return $stmt;
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    created_at=NOW(),
                    updated_at=NOW(),
                    status=1";

        // prepare query
        //$this->getConn()->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
        $this->setStmt($this->getConn()->prepare($query));
     

        // sanitize
        // sanitize
        $this->sanitize();
     
        // bind values
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return $this->getConn()->lastInsertId($this->getTableName());
        }

        // echo "\nPDOStatement::errorInfo():\n";
        // $arr = $this->getConn()->errorInfo();
        // error_log(print_r($arr));

        return 0;            
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    updated_at=NOW() 
                WHERE id=:id";

        // prepare query
        $this->setStmt($this->getConn()->prepare($query));
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->sanitize();
     
        // bind values
        $id = $this->getId();
        $this->getStmt()->bindParam(":id", $id);
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return true;
        }

        return false;
    }


    /**
     * @return mixed
     */
    public function getBlogCategoryId()
    {
        return $this->blog_category_id;
    }

    /**
     * @param mixed $blog_category_id
     *
     * @return self
     */
    public function setBlogCategoryId($blog_category_id)
    {
        $this->blog_category_id = $blog_category_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     *
     * @return self
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     *
     * @return self
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     *
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     *
     * @return self
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPublishedAt()
    {
        return $this->published_at;
    }

    /**
     * @param mixed $published_at
     *
     * @return self
     */
    public function setPublishedAt($published_at)
    {
        $this->published_at = $published_at;

        return $this;
    }
    
}