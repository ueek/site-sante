<?php


include_once 'base-model.php';


class ResidencyAcademicTeam extends BaseModel
{
    // object properties
    private $residencies_id;
    private $name;
    private $image;   
    private $charge;   
    private $crm;   
    private $rqe;   
    private $linkedin;   
    private $sector;   

  	function __construct($db)
  	{
          $this->setConn($db);
  		    $this->setTableName("residency_team");
          $this->setBaseQueryFields
          (
              "
                  residencies_id=:residencies_id,
                  name=:name,
                  image=:image,
                  charge=:charge,
                  crm=:crm,
                  rqe=:rqe,
                  linkedin=:linkedin,
                  sector=:sector
              "
          );
  	}

    private function sanitize(){       
        $this->residencies_id=htmlspecialchars(strip_tags($this->residencies_id));
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->image=htmlspecialchars(strip_tags($this->image));              
        $this->charge=htmlspecialchars(strip_tags($this->charge));              
        $this->crm=htmlspecialchars(strip_tags($this->crm));              
        $this->rqe=htmlspecialchars(strip_tags($this->rqe));              
        $this->linkedin=htmlspecialchars(strip_tags($this->linkedin));              
        $this->sector=htmlspecialchars(strip_tags($this->sector));              
    }

    private function bindParams(){ 
        $this->getStmt()->bindParam(":residencies_id", $this->residencies_id);   
        $this->getStmt()->bindParam(":name", $this->name);   
        $this->getStmt()->bindParam(":image", $this->image);            
        $this->getStmt()->bindParam(":charge", $this->charge);            
        $this->getStmt()->bindParam(":crm", $this->crm);            
        $this->getStmt()->bindParam(":rqe", $this->rqe);            
        $this->getStmt()->bindParam(":linkedin", $this->linkedin);            
        $this->getStmt()->bindParam(":sector", $this->sector);           
    }

    function create(){

        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    created_at=NOW(),
                    updated_at=NOW(),
                    status=1";

        // prepare query
        //$this->getConn()->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
        $this->setStmt($this->getConn()->prepare($query));     

        // sanitize
        // sanitize
        $this->sanitize();
     
        // bind values
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return $this->getConn()->lastInsertId($this->getTableName());
        }

        // echo "\nPDOStatement::errorInfo():\n";
        // $arr = $this->getConn()->errorInfo();
        // error_log(print_r($arr));

        return 0;            
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    updated_at=NOW() 
                WHERE id=:id";

        // prepare query
        $this->setStmt($this->getConn()->prepare($query));
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->sanitize();
     
        // bind values
        $id = $this->getId();
        $this->getStmt()->bindParam(":id", $id);
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return true;
        }

        return false;
    }  

    /**
     * @return mixed
     */
    public function getResidenciesId()
    {
        return $this->residencies_id;
    }

    /**
     * @param mixed $residencies_id
     *
     * @return self
     */
    public function setResidenciesId($residencies_id)
    {
        $this->residencies_id = $residencies_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     *
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCharge()
    {
        return $this->charge;
    }

    /**
     * @param mixed $charge
     *
     * @return self
     */
    public function setCharge($charge)
    {
        $this->charge = $charge;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCrm()
    {
        return $this->crm;
    }

    /**
     * @param mixed $crm
     *
     * @return self
     */
    public function setCrm($crm)
    {
        $this->crm = $crm;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRqe()
    {
        return $this->rqe;
    }

    /**
     * @param mixed $rqe
     *
     * @return self
     */
    public function setRqe($rqe)
    {
        $this->rqe = $rqe;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLinkedin()
    {
        return $this->linkedin;
    }

    /**
     * @param mixed $linkedin
     *
     * @return self
     */
    public function setLinkedin($linkedin)
    {
        $this->linkedin = $linkedin;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSector()
    {
        return $this->sector;
    }

    /**
     * @param mixed $sector
     *
     * @return self
     */
    public function setSector($sector)
    {
        $this->sector = $sector;

        return $this;
    }
}