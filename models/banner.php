<?php
include_once 'base-model.php';
class Banner extends BaseModel
{
    // object properties
    private $image_desktop;  
    private $image_mobile;  
    private $title;
    private $text;
    private $button_text;
    private $button_url;  
    private $video_url;  
    private $sequence;  

	function __construct($db)
	{
        $this->setConn($db);
		$this->setTableName("banners");
        $this->setBaseQueryFields
        (
            "
                image_desktop=:image_desktop,
                image_mobile=:image_mobile,
                title=:title,
                text=:text,
                button_text=:button_text,
                button_url=:button_url,
                video_url=:video_url,
                sequence=:sequence
            "
        );
	}

    private function sanitize(){
        $this->image_desktop=htmlspecialchars(strip_tags($this->image_desktop));        
        $this->image_mobile=htmlspecialchars(strip_tags($this->image_mobile));        
        $this->title=htmlspecialchars(strip_tags($this->title));             
        $this->text=htmlspecialchars(strip_tags($this->text));     
        $this->button_text=htmlspecialchars(strip_tags($this->button_text));   
        $this->button_url=htmlspecialchars(strip_tags($this->button_url));           
        $this->video_url=htmlspecialchars(strip_tags($this->video_url));           
        $this->sequence=htmlspecialchars(strip_tags($this->sequence));           
    }

    private function bindParams(){
        $this->getStmt()->bindParam(":image_desktop", $this->image_desktop);    
        $this->getStmt()->bindParam(":image_mobile", $this->image_mobile);    
        $this->getStmt()->bindParam(":title", $this->title);       
        $this->getStmt()->bindParam(":text", $this->text);    
        $this->getStmt()->bindParam(":button_text", $this->button_text);   
        $this->getStmt()->bindParam(":button_url", $this->button_url);       
        $this->getStmt()->bindParam(":video_url", $this->video_url);       
        $this->getStmt()->bindParam(":sequence", $this->sequence);       
    }


    function create(){
        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    created_at=NOW(),
                    updated_at=NOW(),
                    status=1";

        // prepare query
        //$this->getConn()->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
        $this->setStmt($this->getConn()->prepare($query));
     

        // sanitize
        // sanitize
        $this->sanitize();
     
        // bind values
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return $this->getConn()->lastInsertId($this->getTableName());
        }

        // echo "\nPDOStatement::errorInfo():\n";
        // $arr = $this->getConn()->errorInfo();
        // error_log(print_r($arr));

        return 0;            
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    updated_at=NOW() 
                WHERE id=:id";

        // prepare query
        $this->setStmt($this->getConn()->prepare($query));
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->sanitize();
     
        // bind values
        $id = $this->getId();
        $this->getStmt()->bindParam(":id", $id);
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return true;
        }

        return false;
    }    
    
    

    /**
     * @return mixed
     */
    public function getImageDesktop()
    {
        return $this->image_desktop;
    }

    /**
     * @param mixed $image_desktop
     *
     * @return self
     */
    public function setImageDesktop($image_desktop)
    {
        $this->image_desktop = $image_desktop;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImageMobile()
    {
        return $this->image_mobile;
    }

    /**
     * @param mixed $image_mobile
     *
     * @return self
     */
    public function setImageMobile($image_mobile)
    {
        $this->image_mobile = $image_mobile;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     *
     * @return self
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getButtonText()
    {
        return $this->button_text;
    }

    /**
     * @param mixed $button_text
     *
     * @return self
     */
    public function setButtonText($button_text)
    {
        $this->button_text = $button_text;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getButtonUrl()
    {
        return $this->button_url;
    }

    /**
     * @param mixed $button_url
     *
     * @return self
     */
    public function setButtonUrl($button_url)
    {
        $this->button_url = $button_url;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getVideoUrl()
    {
        return $this->video_url;
    }

    /**
     * @param mixed $video_url
     *
     * @return self
     */
    public function setVideoUrl($video_url)
    {
        $this->video_url = $video_url;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * @param mixed $sequence
     *
     * @return self
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;

        return $this;
    }
}