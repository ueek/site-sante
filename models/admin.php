<?php
include_once 'base-model.php';
class Admin extends BaseModel
{
    // object properties
    private $name;
    private $email;
    private $password;

	function __construct($db)
	{
        $this->setConn($db);
		$this->setTableName("admins");
        $this->setBaseQueryFields
        (
            "
                name=:name, 
                email=:email, 
                password=:password
            "
        );
	}

    private function sanitize(){
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->email=htmlspecialchars(strip_tags($this->email));
        $this->password=htmlspecialchars(strip_tags($this->password));        
    }

    private function bindParams(){
        $this->getStmt()->bindParam(":name", $this->name);
        $this->getStmt()->bindParam(":email", $this->email);
        $this->getStmt()->bindParam(":password", $this->password);     
    }

    function readByUserAndPassword($user, $password){
        $query= "SELECT * FROM admins WHERE email = '{$user}' AND password = '{$password}' AND status = 1";
        
        // Executar a query e retornar os resultados
        $this->setStmt($this->getConn()->prepare($query));
        $this->getStmt()->execute();

        return $this->getStmt();
    }

    function create(){
        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    created_at=NOW(),
                    updated_at=NOW(),
                    status=1";

        // prepare query
        //$this->getConn()->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
        $this->setStmt($this->getConn()->prepare($query));
     

        // sanitize
        // sanitize
        $this->sanitize();
     
        // bind values
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return $this->getConn()->lastInsertId($this->getTableName());
        }

        // echo "\nPDOStatement::errorInfo():\n";
        // $arr = $this->getConn()->errorInfo();
        // error_log(print_r($arr));

        return 0;            
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    updated_at=NOW() 
                WHERE id=:id";

        // prepare query
        $this->setStmt($this->getConn()->prepare($query));
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->sanitize();
     
        // bind values
        $id = $this->getId();
        $this->getStmt()->bindParam(":id", $id);
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return true;
        }

        return false;
    }

    function updateKeepPassword(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    name=:name, 
                    email=:email, 
                    updated_at=NOW() 
                WHERE id=:id";

        // prepare query
        $this->setStmt($this->getConn()->prepare($query));
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->email=htmlspecialchars(strip_tags($this->email));
     
        // bind values
        $id = $this->getId();
        $this->getStmt()->bindParam(":id", $id);
        $this->getStmt()->bindParam(":name", $this->name);
        $this->getStmt()->bindParam(":email", $this->email);
     
        // execute query
        if($this->getStmt()->execute()){
            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getLastReadNotificationId()
    {
        return $this->last_read_notification_id;
    }

    /**
     * @param mixed $last_read_notification_id
     *
     * @return self
     */
    public function setLastReadNotificationId($last_read_notification_id)
    {
        $this->last_read_notification_id = $last_read_notification_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     *
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }
}