<?php
include_once 'base-model.php';
class About extends BaseModel
{
    // object properties
    private $mission;
    private $vision;
    private $company_values;
    private $infrastructure;
    private $attendance;
    private $team;
    private $phone;
    private $whatsapp;
    private $email;
    private $facebook;
    private $instagram;
    private $twitter;
    private $linkedin;
    private $youtube;
    private $image_structure;
    private $image_attendance;
    private $image_team;


	function __construct($db)
	{
        $this->setConn($db);
		$this->setTableName("about");
        $this->setBaseQueryFields
        (
            "
                mission=:mission,
                vision=:vision,
                company_values=:company_values,
                infrastructure=:infrastructure,
                attendance=:attendance,
                team=:team,
                phone=:phone,
                whatsapp=:whatsapp,
                email=:email,
                facebook=:facebook,
                instagram=:instagram,
                twitter=:twitter,
                linkedin=:linkedin,
                youtube=:youtube,
                image_structure=:image_structure,
                image_attendance=:image_attendance,
                image_team=:image_team
                
            "
        );
	}

    private function sanitize(){
        $this->mission=htmlspecialchars(strip_tags($this->mission));        
        $this->vision=htmlspecialchars(strip_tags($this->vision));        
        $this->company_values=htmlspecialchars(strip_tags($this->company_values));        
        $this->infrastructure=htmlspecialchars(strip_tags($this->infrastructure));        
        $this->attendance=htmlspecialchars(strip_tags($this->attendance));        
        $this->team=htmlspecialchars(strip_tags($this->team));        
        $this->phone=htmlspecialchars(strip_tags($this->phone));        
        $this->whatsapp=htmlspecialchars(strip_tags($this->whatsapp));        
        $this->email=htmlspecialchars(strip_tags($this->email));        
        $this->facebook=htmlspecialchars(strip_tags($this->facebook));        
        $this->instagram=htmlspecialchars(strip_tags($this->instagram));        
        $this->twitter=htmlspecialchars(strip_tags($this->twitter));        
        $this->linkedin=htmlspecialchars(strip_tags($this->linkedin));        
        $this->youtube=htmlspecialchars(strip_tags($this->youtube));            
        $this->image_structure=htmlspecialchars(strip_tags($this->image_structure));            
        $this->image_attendance=htmlspecialchars(strip_tags($this->image_attendance));            
        $this->image_team=htmlspecialchars(strip_tags($this->image_team));            
    }

    private function bindParams(){
        $this->getStmt()->bindParam(":mission", $this->mission);     
        $this->getStmt()->bindParam(":vision", $this->vision);     
        $this->getStmt()->bindParam(":company_values", $this->company_values);     
        $this->getStmt()->bindParam(":infrastructure", $this->infrastructure);     
        $this->getStmt()->bindParam(":attendance", $this->attendance);     
        $this->getStmt()->bindParam(":team", $this->team);     
        $this->getStmt()->bindParam(":phone", $this->phone);     
        $this->getStmt()->bindParam(":whatsapp", $this->whatsapp);     
        $this->getStmt()->bindParam(":email", $this->email);     
        $this->getStmt()->bindParam(":facebook", $this->facebook);     
        $this->getStmt()->bindParam(":instagram", $this->instagram);     
        $this->getStmt()->bindParam(":twitter", $this->twitter);     
        $this->getStmt()->bindParam(":linkedin", $this->linkedin);     
        $this->getStmt()->bindParam(":youtube", $this->youtube);     
        $this->getStmt()->bindParam(":image_structure", $this->image_structure);     
        $this->getStmt()->bindParam(":image_attendance", $this->image_attendance);     
        $this->getStmt()->bindParam(":image_team", $this->image_team);     
    }


    function create(){
        // query to insert record
        $query = "INSERT INTO
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    created_at=NOW(),
                    updated_at=NOW(),
                    status=1";

        // prepare query
        //$this->getConn()->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
        $this->setStmt($this->getConn()->prepare($query));
     

        // sanitize
        // sanitize
        $this->sanitize();
     
        // bind company_values
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return $this->getConn()->lastInsertId($this->getTableName());
        }

        // echo "\nPDOStatement::errorInfo():\n";
        // $arr = $this->getConn()->errorInfo();
        // error_log(print_r($arr));

        return 0;            
    }

    function update(){
        // query to insert record
        $query = "UPDATE
                    ".$this->getTableName()."
                  SET
                    {$this->getBaseQueryFields()}, 
                    updated_at=NOW() 
                WHERE id=:id";

        // prepare query
        $this->getConn()->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
        $this->setStmt($this->getConn()->prepare($query));
     
        // sanitize
        $this->setId(htmlspecialchars(strip_tags($this->getId())));
        $this->sanitize();
     
        // bind company_values
        $id = $this->getId();
        $this->getStmt()->bindParam(":id", $id);
        $this->bindParams();
     
        // execute query
        if($this->getStmt()->execute()){
            return true;
        }

                echo "\nPDOStatement::errorInfo():\n";
        $arr = $this->getConn()->errorInfo();
        error_log(print_r($arr));

        return false;
    }

    

    /**
     * @return mixed
     */
    public function getMission()
    {
        return $this->mission;
    }

    /**
     * @param mixed $mission
     *
     * @return self
     */
    public function setMission($mission)
    {
        $this->mission = $mission;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getVision()
    {
        return $this->vision;
    }

    /**
     * @param mixed $vision
     *
     * @return self
     */
    public function setVision($vision)
    {
        $this->vision = $vision;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompanyValues()
    {
        return $this->company_values;
    }

    /**
     * @param mixed $company_values
     *
     * @return self
     */
    public function setCompanyValues($company_values)
    {
        $this->company_values = $company_values;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInfrastructure()
    {
        return $this->infrastructure;
    }

    /**
     * @param mixed $infrastructure
     *
     * @return self
     */
    public function setInfrastructure($infrastructure)
    {
        $this->infrastructure = $infrastructure;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttendance()
    {
        return $this->attendance;
    }

    /**
     * @param mixed $attendance
     *
     * @return self
     */
    public function setAttendance($attendance)
    {
        $this->attendance = $attendance;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @param mixed $team
     *
     * @return self
     */
    public function setTeam($team)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     *
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWhatsapp()
    {
        return $this->whatsapp;
    }

    /**
     * @param mixed $whatsapp
     *
     * @return self
     */
    public function setWhatsapp($whatsapp)
    {
        $this->whatsapp = $whatsapp;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * @param mixed $facebook
     *
     * @return self
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInstagram()
    {
        return $this->instagram;
    }

    /**
     * @param mixed $instagram
     *
     * @return self
     */
    public function setInstagram($instagram)
    {
        $this->instagram = $instagram;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * @param mixed $twitter
     *
     * @return self
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLinkedin()
    {
        return $this->linkedin;
    }

    /**
     * @param mixed $linkedin
     *
     * @return self
     */
    public function setLinkedin($linkedin)
    {
        $this->linkedin = $linkedin;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getYoutube()
    {
        return $this->youtube;
    }

    /**
     * @param mixed $youtube
     *
     * @return self
     */
    public function setYoutube($youtube)
    {
        $this->youtube = $youtube;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImageStructure()
    {
        return $this->image_structure;
    }

    /**
     * @param mixed $image_structure
     *
     * @return self
     */
    public function setImageStructure($image_structure)
    {
        $this->image_structure = $image_structure;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImageAttendance()
    {
        return $this->image_attendance;
    }

    /**
     * @param mixed $image_attendance
     *
     * @return self
     */
    public function setImageAttendance($image_attendance)
    {
        $this->image_attendance = $image_attendance;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImageTeam()
    {
        return $this->image_team;
    }

    /**
     * @param mixed $image_team
     *
     * @return self
     */
    public function setImageTeam($image_team)
    {
        $this->image_team = $image_team;

        return $this;
    }
}