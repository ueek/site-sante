<?php
class BaseModel
{
    // Conexão de BD
    private $conn;
    private $table_name = "";

    private $stmt;
    private $base_query_fields;

    // object properties
    private $id;

    private $created_at;
    private $updated_at;
    private $status;
    

     /**
     * @return mixed
     */
    public function getConn()
    {
        return $this->conn;
    }

    /**
     * @param mixed $conn
     *
     * @return self
     */
    public function setConn($conn)
    {
        $this->conn = $conn;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTableName()
    {
        return $this->table_name;
    }

    /**
     * @param mixed $table_name
     *
     * @return self
     */
    public function setTableName($table_name)
    {
        $this->table_name = $table_name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     *
     * @return self
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     *
     * @return self
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }
    
    function read($filter = "", $orderBy = "Id", $limit = ""){
        try{
            $query= "SELECT * 
                    FROM ".$this->table_name."
                     WHERE status= 1 ".$filter.
                    " ORDER BY ".$orderBy." ".$limit;

           //var_dump($query);
            
            // Executar a query e retornar os resultados
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();

            //$arr = $stmt->errorInfo();

            return $stmt;
        }catch(Exception $ex){
        }
    }

    function getCount($filter = ""){
        $query= "SELECT COUNT(id) AS count
                FROM ".$this->table_name."
                 WHERE status= 1 ".$filter;

       
        // Executar a query e retornar os resultados
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();

      // Iterate results to build the json array to return
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

            extract($row);

            return $count;
        }

        return 0;
    }

    function readById($id){
        try{
            $query= "SELECT * 
                    FROM ".$this->table_name."
                     WHERE id= {$id}";

            // Executar a query e retornar os resultados
            $stmt = $this->conn->prepare( $query );
            $stmt->execute();

            $arr = $stmt->errorInfo();

            return $stmt;
        }catch(Exception $ex){
        }
    }

    function delete($id){
       $query= "UPDATE ".$this->table_name."
                 SET status=0 
                 WHERE id= {$id}";

         //// error_log($query, 0);
         
        // Executar a query e retornar os resultados
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();

        $arr = $stmt->errorInfo();

        return $stmt;
    }

    /**
     * @return mixed
     */
    public function getStmt()
    {
        return $this->stmt;
    }

    /**
     * @param mixed $stmt
     *
     * @return self
     */
    public function setStmt($stmt)
    {
        $this->stmt = $stmt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBaseQueryFields()
    {
        return $this->base_query_fields;
    }

    /**
     * @param mixed $base_query_fields
     *
     * @return self
     */
    public function setBaseQueryFields($base_query_fields)
    {
        $this->base_query_fields = $base_query_fields;

        return $this;
    }
}