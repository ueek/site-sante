<!-- <section id="header-page" class="container-fluid" style="background-image: url(/img/infraestrutura.jpg);"></section> -->

<section id="content-article">
	<div class="container">
		
		<div class="header-title">
			<h1>Acelerador Linear<br>
			Varian TrueBeam</h1>

			<h2>Alta tecnologia. Inovação. Inteligência. </h2>
		</div>

		<div class="long-text">
			<p>
				Para oferecer um tratamento de excelência, o Santé Cancer Center conta com o mais moderno equipamento para radioterapia, o Varian TrueBeam. 
			</p>

			<p>			
				O TrueBeam oferece alta assertividade e precisão no combate de tumores, proporcionando maior conforto para o paciente e evitando reações adversas graves como queimaduras na pele e dor. 
				Importado dos Estados Unidos, o acelerador linear TrueBeam conta com tecnologia avançada de radioterapia e radiocirurgia, ambas guiadas por imagem. 
			</p>

			<p>
				O Santé Cancer Center se preocupa e prioriza o bem estar do paciente, pensando nisso, investimos em tecnologia em favor da saúde para que o tratamento seja realizado da melhor forma, unindo medicina de alto nível com tecnologia internacional.
			</p>
		</div>


		<!-- Diferenciais -->
		<div class="header-title">
			<h1>Diferenciais </h1>
		</div>

		<div class="long-text">
			<p>
				Conheça o pontos que fazem do TrueBeam o equipamento mais avançado no tratamento de Radioterapia
			</p>

			<div class="truebeam-img"></div>
		</div>


		<!-- Diferenciais -->
		<div class="header-title">
			<h1>
				Mais conforto<br>
				Menos efeitos colaterais<br>
				Maior assertividade 
			 </h1>
		</div>

		<div class="long-text">
			<p>
				O TrueBeam é capaz de reduzir de forma bastante significativa o tempo de tratamento e também as sessões que o paciente se submete aos raios radioativos. Essa tecnologia também oferece ao paciente maior eficácia e assertividade no tratamento já que administra dosagens altas e precisas de radiação, poupando os tecidos, evitando efeitos colaterais e atingindo o tumor com maior precisão. 
			</p>
		</div>

	</div>
</section>

<!-- <section id="galery">
	<div class="container p-0">
		
		<div id="images">
			
			<a href='/img/infraestrutura.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/infraestrutura.jpg);"></div>
			</a>

			<a href='/img/sobre-nos.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/sobre-nos.jpg);"></div>
			</a>

			<a href='/img/infraestrutura.jpg' data-fancybox='galery-desktop'>
				<div class="img" style="background-image: url(/img/infraestrutura.jpg);"></div>
			</a>

			<br>

		</div>

	</div>
</section> -->